package com.dcode.inventory.data.model;

public class GR_LINES_TYP {
    public String ITCODE;

    public String ITUNIT;

    public String BATCHNO;

    public String MANFDATE;

    public String EXPDATE;

    public float QTY;

    public int FACTOR;

    public String REC_TYPE;

    public long SLNO;

    public long SEQ_NO;
}
