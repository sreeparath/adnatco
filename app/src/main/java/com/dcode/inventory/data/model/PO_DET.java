package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;


@Entity(tableName = "PO_DET")
public class PO_DET implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "DET_ID")
    public long DET_ID;

    @ColumnInfo(name = "ORDNO")
    public String ORDNO;

    @ColumnInfo(name = "ORTYP")
    public String ORTYP;

    @ColumnInfo(name = "ITCODE")
    public String ITCODE;

    @ColumnInfo(name = "ITDESC")
    public String ITDESC;

    @ColumnInfo(name = "PACKING")
    public String PACKING;

    @ColumnInfo(name = "ITUNIT")
    public String ITUNIT;

    @ColumnInfo(name = "FACTOR")
    public float FACTOR;

    @ColumnInfo(name = "CARTON")
    public long CARTON;

    @ColumnInfo(name = "ITQTY")
    public float ITQTY;

    @ColumnInfo(name = "RCQTY")
    public float RCQTY;

    @ColumnInfo(name = "CNQTY")
    public float CNQTY;

    @ColumnInfo(name = "TOTQTY")
    public float TOTQTY;

    @ColumnInfo(name = "SLNO")
    public long SLNO;

    @ColumnInfo(name = "SUP_BCODE")
    public String SUP_BCODE;

    @ColumnInfo(name = "BATCHFLG")
    public int BATCHFLG;

    @ColumnInfo(name = "ITRTE")
    public float ITRTE;

    @ColumnInfo(name = "TOL_QTY")
    public float TOL_QTY;

    @ColumnInfo(name = "IS_QTY_VALIDATE")
    public int IS_QTY_VALIDATE;
}
