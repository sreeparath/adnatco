package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "MATERIAL_LIST")
public class MATERIAL_LIST implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    @NotNull
    public int ID;

    @ColumnInfo(name = "MATERIAL")
    @NotNull
    public String MATERIAL;

    @ColumnInfo(name = "MATL_DESC")
    public String MATL_DESC;

    @ColumnInfo(name = "MFRPN")
    public String MFRPN;

    @ColumnInfo(name = "OLDMATCODE")
    public String OLDMATCODE;

    @ColumnInfo(name = "LEGACYMATCODE")
    public String LEGACYMATCODE;

    @ColumnInfo(name = "DEFAULTLOC")
    public String DEFAULTLOC;

    @ColumnInfo(name = "MATBINID")
    public int MATBINID;

    @ColumnInfo(name = "SLOC_NAME")
    public String SLOC_NAME;
}
