package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "PUR_RET_ITEMS_RECEIPT")
public class PUR_RET_ITEMS_RECEIPT implements Serializable {


    @PrimaryKey
    @NotNull
    @ColumnInfo(name = "SLNO")
    public long SLNO;

    @ColumnInfo(name = "REF_NO")
    public String REF_NO;

    @ColumnInfo(name = "RECNO")
    public String RECNO;


    @ColumnInfo(name = "ITCODE")
    public String ITCODE;

    @ColumnInfo(name = "IUNITS")
    public String IUNITS;

    @ColumnInfo(name = "BATCHNO")
    public String BATCHNO;


    @ColumnInfo(name = "MANFDATE")
    public String MANFDATE;


    @ColumnInfo(name = "EXPDATE")
    public String EXPDATE;

    @ColumnInfo(name = "CNTQTY")
    public float CNTQTY;

    @ColumnInfo(name="SCANTIME")
    public String SCANTIME;

    @ColumnInfo(name="REASON")
    public String REASON;

    @ColumnInfo(name="ERPBATCH")
    public String ERPBATCH;
}
