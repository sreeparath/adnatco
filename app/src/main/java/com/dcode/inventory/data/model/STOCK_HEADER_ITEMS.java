package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "STOCK_HEADER_ITEMS")
public class STOCK_HEADER_ITEMS implements Serializable {

    @PrimaryKey
    @NotNull
    @ColumnInfo(name = "ITCODE")
    public String ITCODE;

    @ColumnInfo(name = "YRCD")
    public String YRCD;

    @ColumnInfo(name = "ORTYP")
    public String ORTYP;

    @ColumnInfo(name = "ORDNO")
    public String ORDNO;

    @ColumnInfo(name = "SL_NO")
    public String SL_NO;
}
