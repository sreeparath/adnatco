package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "PUR_RET_HDR")
public class PUR_RET_HDR implements Serializable {


    @PrimaryKey()
    @NotNull
    @ColumnInfo(name = "ORDNO")
    public String ORDNO;

    @ColumnInfo(name = "YRCD")
    public String YRCD;

    @ColumnInfo(name = "ORTYP")
    public String ORTYP;



    @ColumnInfo(name = "ORDTE")
    public String ORDTE;

    @ColumnInfo(name = "REFNO")
    public String REFNO;

    @ColumnInfo(name = "LOCODE")
    public String LOCODE;

    @ColumnInfo(name = "USERID")
    public String USERID;

    @ColumnInfo(name = "ITBRND")
    public String ITBRND;

    @ColumnInfo(name = "REMARKS")
    public String REMARKS;

    @ColumnInfo(name = "ACTIVE")
    public String ACTIVE;

    @ColumnInfo(name = "PLANNED_START_DATE")
    public String PLANNED_START_DATE;

    @ColumnInfo(name = "PLANNED_END_DATE")
    public String PLANNED_END_DATE;

    @ColumnInfo(name = "IS_CLOSED")
    public String IS_CLOSED;

    @ColumnInfo(name = "IS_VAR_POSTED")
    public String IS_VAR_POSTED;
}
