package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "AD_PO_ITEMS_SPR")
public class AD_PO_ITEMS_SPR implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ITEM_ID")
    public long ITEM_ID;

    @ColumnInfo(name = "PO_ITEM")
    public String PO_ITEM;

    @ColumnInfo(name = "PO_NUMBER")
    public String PO_NUMBER;

    @ColumnInfo(name = "MATERIAL")
    public String MATERIAL;

    @ColumnInfo(name = "SHORT_TEXT")
    public String SHORT_TEXT;

    @ColumnInfo(name = "MATL_DESC")
    public String MATL_DESC;

    @ColumnInfo(name = "PLANT")
    public String PLANT;

    @ColumnInfo(name = "PO_UNIT")
    public String PO_UNIT;

    @ColumnInfo(name = "QUANTITY")
    public float QUANTITY;

    @ColumnInfo(name = "PO_UNIT_ISO")
    public String PO_UNIT_ISO;

    @ColumnInfo(name = "QUAL_INSP")
    public String QUAL_INSP;

    @ColumnInfo(name = "VAL_TYPE")
    public String VAL_TYPE;

    @ColumnInfo(name = "NO_MORE_GR")
    public String NO_MORE_GR;

    @ColumnInfo(name = "DELIV_QTY")
    public float DELIV_QTY;

    @ColumnInfo(name = "PO_PR_QNT")
    public float PO_PR_QNT;

    @ColumnInfo(name = "PDTRECPT")
    public float PDTRECPT;

    @ColumnInfo(name = "WBS_ELEMENT")
    public String WBS_ELEMENT;

    @ColumnInfo(name = "OLDMATCODE")
    public String OLDMATCODE;

    @ColumnInfo(name = "BAL_TO_RCV")
    public float BAL_TO_RCV;

    @ColumnInfo(name = "ENTRY_QNT")
    public float ENTRY_QNT;

    @ColumnInfo(name = "STCK_TYPE")
    public String STCK_TYPE;

    @ColumnInfo(name = "MOVE_REAS")
    public String MOVE_REAS;

    @ColumnInfo(name = "BIN_ID")
    public int BIN_ID;

    @ColumnInfo(name = "IS_DELETED")
    public int IS_DELETED;

    @ColumnInfo(name = "DEFAULTLOC")
    public String DEFAULTLOC;

    @ColumnInfo(name = "STGE_LOC")
    public String STGE_LOC;

    @ColumnInfo(name = "DELIV_NUMB")
    public String DELIV_NUMB;

    @ColumnInfo(name = "RCQTY")
    public float RCQTY;
}
