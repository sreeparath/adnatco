package com.dcode.inventory.data;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.dcode.inventory.data.dao.GenericDao;
import com.dcode.inventory.data.model.AD_PO_HDR;
import com.dcode.inventory.data.model.AD_PO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.AD_PO_ITEMS_SPR;
import com.dcode.inventory.data.model.APP_SETTINGS;
import com.dcode.inventory.data.model.BARCODE_PRINTERS;
import com.dcode.inventory.data.model.CONFIRM_DOC_DET;
import com.dcode.inventory.data.model.CONFIRM_DOC_HDR;
import com.dcode.inventory.data.model.DRIVER_LIST;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.GR_DET;
import com.dcode.inventory.data.model.GR_HDR;
import com.dcode.inventory.data.model.HELPER_LIST;
import com.dcode.inventory.data.model.LABEL_FORMATS;
import com.dcode.inventory.data.model.LOAD_SLIP_ITEMS_NEW;
import com.dcode.inventory.data.model.LOAD_SLIP_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.LOCATIONS;
import com.dcode.inventory.data.model.LS_HDR;
import com.dcode.inventory.data.model.LS_ITEMS;
import com.dcode.inventory.data.model.MATERIAL_LIST;
import com.dcode.inventory.data.model.OBJECTS;
import com.dcode.inventory.data.model.ORTYP;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS_STOCK;
import com.dcode.inventory.data.model.PLANT_LIST;
import com.dcode.inventory.data.model.PO_DET;
import com.dcode.inventory.data.model.PO_HDR;
import com.dcode.inventory.data.model.PUR_RET_HDR;
import com.dcode.inventory.data.model.PUR_RET_ITEMS;
import com.dcode.inventory.data.model.PUR_RET_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.REASON_TYPE;
import com.dcode.inventory.data.model.REC_TYPE;
import com.dcode.inventory.data.model.RET_REASON_LIST;
import com.dcode.inventory.data.model.RE_WO_HDR_SPR;
import com.dcode.inventory.data.model.RE_WO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.RE_WO_ITEM_SPR;
import com.dcode.inventory.data.model.SALE_RET_DET;
import com.dcode.inventory.data.model.SALE_RET_DOC_DET;
import com.dcode.inventory.data.model.SALE_RET_DOC_HDR;
import com.dcode.inventory.data.model.SALE_RET_HDR;
import com.dcode.inventory.data.model.SLIP_DET;
import com.dcode.inventory.data.model.SLIP_HDR;
import com.dcode.inventory.data.model.SLOC_LIST_SPR;
import com.dcode.inventory.data.model.STK_COUNT_DET;
import com.dcode.inventory.data.model.STK_COUNT_HDR;
import com.dcode.inventory.data.model.STK_COUNT_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.STK_TRN_PRJ_PRJ_RECIEPT;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT221;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT221Q;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT222;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT222Q;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT411Q;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT415Q;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR__REV_RECIEPT;
import com.dcode.inventory.data.model.STOCK_HEADER;
import com.dcode.inventory.data.model.STOCK_HEADER_ITEMS;
import com.dcode.inventory.data.model.STOCK_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.STORE_TRANSFER_ITEMS;
import com.dcode.inventory.data.model.STORE_TRANSFER_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.USERS;
import com.dcode.inventory.data.model.VEHICLE_LIST;
import com.dcode.inventory.data.model.WO_HDR_SPR;
import com.dcode.inventory.data.model.WO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.WO_ITEM_SPR;

@Database(entities = {
        APP_SETTINGS.class,
        USERS.class,
        OBJECTS.class,
        LOCATIONS.class,
        ORTYP.class,
        REC_TYPE.class,
        PO_HDR.class,
        PO_DET.class,
        GR_HDR.class,
        GR_DET.class,
        BARCODE_PRINTERS.class,
        LABEL_FORMATS.class,
        LS_HDR.class,
        LS_ITEMS.class,
        SLIP_HDR.class,
        SLIP_DET.class,
        PICK_LIST_ITEMS.class,
        PICK_LIST_ITEMS_RECEIPT.class,
        PICK_LIST_ITEMS_STOCK.class,
        LOAD_SLIP_ITEMS_NEW.class,
        LOAD_SLIP_ITEMS_RECEIPT.class,
        STOCK_HEADER.class,
        STOCK_HEADER_ITEMS.class,
        STOCK_ITEMS_RECEIPT.class,
        STORE_TRANSFER_ITEMS.class,
        STORE_TRANSFER_ITEMS_RECEIPT.class,
        DRIVER_LIST.class,
        HELPER_LIST.class,
        VEHICLE_LIST.class,
        DocType.class,
        PUR_RET_ITEMS_RECEIPT.class,
        PUR_RET_ITEMS.class,
        PUR_RET_HDR.class,
        REASON_TYPE.class,
        SALE_RET_DOC_DET.class,
        SALE_RET_DOC_HDR.class,
        SALE_RET_HDR.class,
        SALE_RET_DET.class,
        CONFIRM_DOC_HDR.class,
        CONFIRM_DOC_DET.class,



        AD_PO_HDR.class,
        SLOC_LIST_SPR.class,
        AD_PO_ITEMS_SPR.class,
        AD_PO_ITEMS_RECIEPT.class,
        WO_HDR_SPR.class,
        WO_ITEM_SPR.class,
        WO_ITEMS_RECIEPT.class,
        RE_WO_HDR_SPR.class,
        RE_WO_ITEM_SPR.class,
        RE_WO_ITEMS_RECIEPT.class,
        PLANT_LIST.class,
        RET_REASON_LIST.class,
        MATERIAL_LIST.class,
        STK_TRN_PRJ_WHR_RECIEPT221.class,
        STK_TRN_PRJ_WHR_RECIEPT221Q.class,
        STK_TRN_PRJ_WHR__REV_RECIEPT.class,
        STK_TRN_PRJ_PRJ_RECIEPT.class,
        STK_TRN_PRJ_WHR_RECIEPT222.class,
        STK_TRN_PRJ_WHR_RECIEPT222Q.class,
        STK_TRN_PRJ_WHR_RECIEPT411Q.class,
        STK_TRN_PRJ_WHR_RECIEPT415Q.class,
        STK_COUNT_HDR.class,
        STK_COUNT_DET.class,
        STK_COUNT_ITEMS_RECIEPT.class,
}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract GenericDao genericDao();
}
