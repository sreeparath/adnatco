package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "RE_WO_HDR_SPR")
public class RE_WO_HDR_SPR implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "HDR_ID")
    public long HDR_ID;

    @NotNull
    @ColumnInfo(name = "WO_NUMBER")
    public String WO_NUMBER;

    @ColumnInfo(name = "TRAN_TYPE")
    public String TRAN_TYPE;

    @ColumnInfo(name = "DOC_DATE")
    public String DOC_DATE;

    @ColumnInfo(name = "PLANT")
    public String PLANT;
}
