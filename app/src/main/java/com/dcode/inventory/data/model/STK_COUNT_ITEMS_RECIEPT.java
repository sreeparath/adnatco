package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "STK_COUNT_ITEMS_RECIEPT")
public class STK_COUNT_ITEMS_RECIEPT implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NotNull
    @ColumnInfo(name = "SLNO")
    public long SLNO;

    @ColumnInfo(name = "DOC_NO")
    public String DOC_NO;

    @ColumnInfo(name = "GUI_ID")
    public String GUI_ID;

    @ColumnInfo(name = "HEAD_ID")
    public long HEAD_ID;

    @ColumnInfo(name = "ITEM")
    public long ITEM;

    @ColumnInfo(name = "MATERIAL")
    public String MATERIAL;

    @ColumnInfo(name = "ENTRY_UOM")
    public String ENTRY_UOM;

    @ColumnInfo(name = "ENTRY_QNT")
    public float ENTRY_QNT;

    @ColumnInfo(name = "BATCH")
    public String BATCH;

    @ColumnInfo(name = "WBS")
    public String WBS;

    @ColumnInfo(name = "MAT_DESC")
    public String MAT_DESC;

}
