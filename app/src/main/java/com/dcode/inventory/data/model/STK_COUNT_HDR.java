package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "STK_COUNT_HDR")
public class STK_COUNT_HDR implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "SL_NO")
    public long SL_NO;

    @NotNull
    @ColumnInfo(name = "ID")
    public long ID;

    @NotNull
    @ColumnInfo(name = "DOC_NO")
    public String DOC_NO;

    @ColumnInfo(name = "DOC_DATE")
    public String DOC_DATE;

    @ColumnInfo(name = "REMARKS")
    public String REMARKS;

    @ColumnInfo(name = "STGE_LOC")
    public String STGE_LOC;

    @ColumnInfo(name = "IS_ACTIVE")
    public int IS_ACTIVE;

    @ColumnInfo(name = "IS_DIFF_POSTED")
    public int IS_DIFF_POSTED;

}
