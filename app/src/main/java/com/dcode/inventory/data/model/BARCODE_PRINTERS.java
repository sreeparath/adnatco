package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "BARCODE_PRINTERS")
public class BARCODE_PRINTERS {

    @PrimaryKey
    @ColumnInfo(name = "PrinterID")
    public long PrinterID;

    @ColumnInfo(name = "PrinterType")
    public String PrinterType;

    @ColumnInfo(name = "FriendlyName")
    public String FriendlyName;

    @ColumnInfo(name = "PrinterNameOrIP")
    public String PrinterNameOrIP;

    @ColumnInfo(name = "PrinterPort")
    public int PrinterPort;

    @ColumnInfo(name = "IsDefault")
    public int IsDefault;

    @ColumnInfo(name = "LocCode")
    public String LocCode;

    @Override
    public String toString() {
        return FriendlyName;
    }
}
