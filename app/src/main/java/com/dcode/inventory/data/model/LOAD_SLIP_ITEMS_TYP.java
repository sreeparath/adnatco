package com.dcode.inventory.data.model;

public class LOAD_SLIP_ITEMS_TYP {
    public String ITCODE;
    public String ITUNIT;
    public String BATCHNO;
    public String MANFDATE;
    public String EXPDATE;
    public float QTY;
    public int FACTOR;
    public long SLNO;
    public long SEQ_NO;
    public String LOCODE;
    public String REFNO;
}
