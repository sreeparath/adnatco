package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "GR_HDR")
public class GR_HDR {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "HDR_ID")
    public long HDR_ID;

    @ColumnInfo(name = "RECEIPT_NO")
    public long RECEIPT_NO;

    @ColumnInfo(name = "GUID")
    public String GUID;

    @ColumnInfo(name = "ORDNO")
    public String ORDNO;

    @ColumnInfo(name = "ORTYP")
    public String ORTYP;

    @ColumnInfo(name = "CLSUP")
    public String CLSUP;

    @ColumnInfo(name = "CLNAME")
    public String CLNAME;

    @ColumnInfo(name = "YRCD")
    public String YRCD;

    @ColumnInfo(name = "POTYPE")
    public String POTYPE;

    @ColumnInfo(name = "POTYPENAME")
    public String POTYPENAME;

    @ColumnInfo(name = "DEVICE_DATE")
    public String DEVICE_DATE;

    @ColumnInfo(name = "IS_UPLOADED")
    public long IS_UPLOADED;

    @ColumnInfo(name = "S_INVOICE_NO")
    public String S_INVOICE_NO;

    @ColumnInfo(name = "S_INVOICE_DATE")
    public String S_INVOICE_DATE;

    @ColumnInfo(name = "BILL_NO")
    public String BILL_NO;

    @ColumnInfo(name = "CONTAINER_NO")
    public String CONTAINER_NO;

    @ColumnInfo(name = "REMARKS")
    public String REMARKS;

    public String ORNAME;
}
