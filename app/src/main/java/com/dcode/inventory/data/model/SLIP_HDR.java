package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "SLIP_HDR")
public class SLIP_HDR {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "HDR_ID")
    public long HDR_ID;

    @ColumnInfo(name = "RECEIPT_NO")
    public long RECEIPT_NO;

    @ColumnInfo(name = "GUID")
    public String GUID;

    @ColumnInfo(name = "ORDNO")
    public String ORDNO;

    @ColumnInfo(name = "ORTYP")
    public String ORTYP;

    @ColumnInfo(name = "YRCD")
    public String YRCD;

    @ColumnInfo(name = "LOCODE")
    public String LOCODE;

    @ColumnInfo(name = "PRICING")
    public String PRICING;

    @ColumnInfo(name = "DEVICE_DATE")
    public String DEVICE_DATE;

    @ColumnInfo(name = "IS_UPLOADED")
    public long IS_UPLOADED;

    @ColumnInfo(name = "REFNO")
    public String REFNO;

    @ColumnInfo(name = "REFDT")
    public String REFDT;

    @ColumnInfo(name = "NEW_VEH_NO")
    public String NEW_VEH_NO;

    @ColumnInfo(name = "NEW_DRIVER")
    public String NEW_DRIVER;

    @ColumnInfo(name = "NEW_HELPER")
    public String NEW_HELPER;

    @ColumnInfo(name = "REMARKS")
    public String REMARKS;

    @ColumnInfo(name = "GENERTE_INVOICE")
    public int GENERTE_INVOICE;

}
