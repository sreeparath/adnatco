package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "STORE_TRANSFER_ITEMS_RECEIPT")
public class STORE_TRANSFER_ITEMS_RECEIPT implements Serializable {


    @PrimaryKey
    @NotNull
    @ColumnInfo(name = "SEQ_NO")
    public long SEQ_NO;

    @ColumnInfo(name = "SL_NO")
    public long SL_NO;


    @ColumnInfo(name = "ORD_NO")
    public String ORD_NO;

    @ColumnInfo(name = "ITCODE")
    public String ITCODE;

    @ColumnInfo(name = "IUNITS")
    public String IUNITS;

    @ColumnInfo(name = "BATCHNO")
    public String BATCHNO;


    @ColumnInfo(name = "MANFDATE")
    public String MANFDATE;


    @ColumnInfo(name = "EXPDATE")
    public String EXPDATE;

    @ColumnInfo(name = "QTY")
    public float QTY;

    @ColumnInfo(name = "FACTOR")
    public float FACTOR;

    @ColumnInfo(name = "REC_NO")
    public String REC_NO;

    @ColumnInfo(name = "REMARKS")
    public String REMARKS;

    @ColumnInfo(name="IS_COMPLETE")
    public String IS_COMPLETE;
}
