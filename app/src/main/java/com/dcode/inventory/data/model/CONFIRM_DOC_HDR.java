package com.dcode.inventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "CONFIRM_DOC_HDR")
public class CONFIRM_DOC_HDR implements Serializable  {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ORDNO")
    public String ORDNO;

    @ColumnInfo(name = "ORTYP")
    public String ORTYP;

    @ColumnInfo(name = "CLSUP")
    public String CLSUP;

    @ColumnInfo(name = "CLNAME")
    public String CLNAME;

    @ColumnInfo(name = "YRCD")
    public String YRCD;

    @ColumnInfo(name = "POTYPE")
    public String POTYPE;

    @ColumnInfo(name = "POTYPENAME")
    public String POTYPENAME;

    public String ORNAME;
}
