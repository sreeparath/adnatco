package com.dcode.inventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "REASON_TYPE")
public class REASON_TYPE implements Serializable {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "CODE")
    public String CODE;

    @ColumnInfo(name = "NAME")
    public String NAME;

    @Override
    public String toString() {
        return NAME;
    }
}
