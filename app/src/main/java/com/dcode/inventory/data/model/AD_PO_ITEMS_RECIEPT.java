package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "AD_PO_ITEMS_RECIEPT")
public class AD_PO_ITEMS_RECIEPT implements Serializable {

    @PrimaryKey
    @NotNull
    @ColumnInfo(name = "SLNO")
    public long SLNO;

    @ColumnInfo(name = "ITEM_ID")
    public long ITEM_ID;

    @ColumnInfo(name = "PO_ITEM")
    public String PO_ITEM;

    @ColumnInfo(name = "PO_NUMBER")
    public String PO_NUMBER;

    @ColumnInfo(name = "MATERIAL")
    public String MATERIAL;

    @ColumnInfo(name = "SHORT_TEXT")
    public String SHORT_TEXT;

    @ColumnInfo(name = "MATL_DESC")
    public String MATL_DESC;

    @ColumnInfo(name = "PLANT")
    public String PLANT;

    @ColumnInfo(name = "LOC")
    public String LOC;

    @ColumnInfo(name = "RCQTY")
    public float RCQTY;

    @ColumnInfo(name = "NOOFLABELS")
    public float NOOFLABELS;

    @ColumnInfo(name = "PRICE_UNIT")
    public float PRICE_UNIT;

    @ColumnInfo(name = "NET_PRICE")
    public float NET_PRICE;

    @ColumnInfo(name = "VAL_TYPE")
    public String VAL_TYPE;

    @ColumnInfo(name = "WBS_ELEMENT")
    public String WBS_ELEMENT;

    @ColumnInfo(name = "FULLLOCATIONCODE")
    public String FULLLOCATIONCODE;

    @ColumnInfo(name = "NET_WEIGHT")
    public float NET_WEIGHT;

    @ColumnInfo(name = "IS_MARKED")
    public int IS_MARKED;

    @ColumnInfo(name = "MFRPN")
    public String MFRPN;

    @ColumnInfo(name = "MATLDESC")
    public String MATLDESC;

    @ColumnInfo(name = "OLD_MAT_CODE")
    public String OLD_MAT_CODE;

    @ColumnInfo(name = "WBSELEMENT")
    public String WBSELEMENT;

    @ColumnInfo(name = "BATCH")
    public String BATCH;

    @ColumnInfo(name = "PMATERIAL")
    public String PMATERIAL;

    @ColumnInfo(name = "PODATE")
    public String PODATE;

    @ColumnInfo(name = "BIN_ID")
    public int BIN_ID;

    @ColumnInfo(name = "STGE_LOC")
    public String STGE_LOC;

    @ColumnInfo(name = "QUALITY")
    public String QUALITY;

    @ColumnInfo(name = "MOVE_TYPE")
    public String MOVE_TYPE;

    @ColumnInfo(name = "MVT_IND")
    public String MVT_IND;

    @ColumnInfo(name = "QUANTITY")
    public float QUANTITY;

    @ColumnInfo(name = "MOVE_REAS")
    public String MOVE_REAS;


}
