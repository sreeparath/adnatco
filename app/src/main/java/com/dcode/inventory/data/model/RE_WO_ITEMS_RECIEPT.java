package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "RE_WO_ITEMS_RECIEPT")
public class RE_WO_ITEMS_RECIEPT implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NotNull
    @ColumnInfo(name = "SLNO")
    public long SLNO;

    @ColumnInfo(name = "WO_NO")
    public String WO_NO;

    @ColumnInfo(name = "GUI_ID")
    public String GUI_ID;

    @ColumnInfo(name = "MATERIAL")
    public String MATERIAL;

    @ColumnInfo(name = "ENTRY_QNT")
    public float ENTRY_QNT;

    @ColumnInfo(name = "ENTRY_UOM_ISO")
    public String ENTRY_UOM_ISO;

    @ColumnInfo(name = "PLANT")
    public String PLANT;

    @ColumnInfo(name = "STGE_LOC")
    public String STGE_LOC;

    @ColumnInfo(name = "VAL_TYPE")
    public String VAL_TYPE;

    @ColumnInfo(name = "MOVE_STLOC")
    public String MOVE_STLOC;

    @ColumnInfo(name = "BIN_ID")
    public int BIN_ID;

    @ColumnInfo(name = "MATL_DESC")
    public String MATL_DESC;

    @ColumnInfo(name = "RES_ITEM")
    public String RES_ITEM;

}
