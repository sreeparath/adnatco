package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "STOCK_ITEMS_RECEIPT")
public class STOCK_ITEMS_RECEIPT implements Serializable {


    @PrimaryKey
    @NotNull
    @ColumnInfo(name = "SL_NO")
    public long SL_NO;

    @ColumnInfo(name = "ORD_NO")
    public String ORD_NO;


    @ColumnInfo(name = "ITCODE")
    public String ITCODE;

    @ColumnInfo(name = "ITUNITS")
    public String ITUNITS;

    @ColumnInfo(name = "BATCHNO")
    public String BATCHNO;


    @ColumnInfo(name = "MANFDATE")
    public String MANFDATE;


    @ColumnInfo(name = "EXPDATE")
    public String EXPDATE;

    @ColumnInfo(name = "CNTQTY")
    public float CNTQTY;

    @ColumnInfo(name="SCANTIME")
    public String SCANTIME;

    @ColumnInfo(name="ERPBATCH")
    public String ERPBATCH;

}
