package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "RE_WO_ITEM_SPR")
public class RE_WO_ITEM_SPR implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ITEM_ID")
    public long ITEM_ID;

    @ColumnInfo(name = "ORDERID")
    public String ORDERID;

    @NotNull
    @ColumnInfo(name = "MATERIAL")
    public String MATERIAL;

    @ColumnInfo(name = "ITEM_NUMBER")
    public String ITEM_NUMBER;

    @ColumnInfo(name = "REQUIREMENT_QUANTITY")
    public float REQUIREMENT_QUANTITY;

    @ColumnInfo(name = "REQUIREMENT_QUANTITY_UNIT")
    public String REQUIREMENT_QUANTITY_UNIT;

    @ColumnInfo(name = "REQUIREMENT_QUANTITY_UNIT_ISO")
    public String REQUIREMENT_QUANTITY_UNIT_ISO;

    @ColumnInfo(name = "MOVE_TYPE")
    public String MOVE_TYPE;

    @ColumnInfo(name = "COMMITED_QUAN")
    public float COMMITED_QUAN;

    @ColumnInfo(name = "RCQTY")
    public float RCQTY;

    @ColumnInfo(name = "MATL_DESC")
    public String MATL_DESC;

    @ColumnInfo(name = "SLOC_NAME")
    public String SLOC_NAME;

    @ColumnInfo(name = "DEFAULTLOC")
    public String DEFAULTLOC;

    @ColumnInfo(name = "BIN_ID")
    public int BIN_ID;
}
