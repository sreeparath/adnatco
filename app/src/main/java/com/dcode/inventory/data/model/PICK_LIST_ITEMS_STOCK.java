package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "PICK_LIST_ITEMS_STOCK")
public class PICK_LIST_ITEMS_STOCK implements Serializable {


    @PrimaryKey
    @NotNull
    @ColumnInfo(name = "ITCODE")
    public String ITCODE;


    @ColumnInfo(name = "ITDESC")
    public String ITDESC;

    @ColumnInfo(name = "ITUNIT")
    public String ITUNIT;

    @ColumnInfo(name = "BATCHNO")
    public String BATCHNO;


    @ColumnInfo(name = "STOCK")
    public float STOCK;


    @ColumnInfo(name = "MANFDATE")
    public String MANFDATE;

    @ColumnInfo(name = "EXPDATE")
    public String EXPDATE;

}
