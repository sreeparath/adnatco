package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "LS_ITEMS", primaryKeys = {"ORDNO", "ORTYP", "ITCODE"})
public class LS_ITEMS implements Serializable {
    @NotNull
    @ColumnInfo(name = "ORDNO")
    public String ORDNO;

    @NotNull
    @ColumnInfo(name = "ORTYP")
    public String ORTYP;

    @NotNull
    @ColumnInfo(name = "ITCODE")
    public String ITCODE;

    @ColumnInfo(name = "ITDESC")
    public String ITDESC;

    @ColumnInfo(name = "PACKING")
    public String PACKING;

    @ColumnInfo(name = "ITUNIT")
    public String ITUNIT;

    @ColumnInfo(name = "LOCODE")
    public String LOCODE;

    @ColumnInfo(name = "FACTOR")
    public int FACTOR;

    @ColumnInfo(name = "CARTON")
    public String CARTON;

    @ColumnInfo(name = "ITQTY")
    public float ITQTY;

    @ColumnInfo(name = "RCQTY")
    public float RCQTY;

    @ColumnInfo(name = "CNQTY")
    public float CNQTY;

    @ColumnInfo(name = "TOTQTY")
    public float TOTQTY;

    @ColumnInfo(name = "STKQTY")
    public float STKQTY;

    @ColumnInfo(name = "SLNO")
    public long SLNO;

    @ColumnInfo(name = "SUP_BCODE")
    public String SUP_BCODE;

    @ColumnInfo(name = "BATCHFLG")
    public int BATCHFLG;
}
