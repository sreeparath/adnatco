package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "SLOC_LIST_SPR")
public class SLOC_LIST_SPR implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    @NotNull
    public int ID;

    @ColumnInfo(name = "SLOC_ID")
    @NotNull
    public String SLOC_ID;

    @ColumnInfo(name = "WERKS")
    public String WERKS;

    @ColumnInfo(name = "LGORT")
    public String LGORT;

    @ColumnInfo(name = "LGOBE")
    public String LGOBE;

    @ColumnInfo(name = "RACK_ID")
    public String RACK_ID;

    @ColumnInfo(name = "RACK_CODE")
    public String RACK_CODE;

    @ColumnInfo(name = "RACK_NAME")
    public String RACK_NAME;

    @ColumnInfo(name = "BIN_ID")
    public String BIN_ID;

    @ColumnInfo(name = "BIN_CODE")
    public String BIN_CODE;

    @ColumnInfo(name = "BIN_NAME")
    public String BIN_NAME;
}
