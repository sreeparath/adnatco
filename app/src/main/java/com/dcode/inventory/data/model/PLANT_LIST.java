package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "PLANT_LIST")
public class PLANT_LIST implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    @NotNull
    public int ID;

    @ColumnInfo(name = "WERKS")
    @NotNull
    public String WERKS;

    @ColumnInfo(name = "NAME1")
    public String NAME1;

    @ColumnInfo(name = "SAP_USER")
    public String SAP_USER;

    @ColumnInfo(name = "DLOAD_TIME")
    public String DLOAD_TIME;

    @ColumnInfo(name = "USER_ID")
    public String USER_ID;
}
