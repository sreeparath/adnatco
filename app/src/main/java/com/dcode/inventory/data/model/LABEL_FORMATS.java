package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "LABEL_FORMATS")
public class LABEL_FORMATS {

    @PrimaryKey
    @ColumnInfo(name = "ID")
    public long ID;

    @ColumnInfo(name = "OBJECT_CODE")
    public String OBJECT_CODE;

    @ColumnInfo(name = "OBJECT_NAME")
    public String OBJECT_NAME;

    @ColumnInfo(name = "REMARKS")
    public String REMARKS;

    @Override
    public String toString() {
        return OBJECT_NAME;
    }
}
