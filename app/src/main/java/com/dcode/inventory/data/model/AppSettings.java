package com.dcode.inventory.data.model;

public class AppSettings {

    public String pdtId;
    public String plant;

    public AppSettings() {
    }

    public AppSettings(String pdtId,String plant) {
        this.pdtId = pdtId;
        this.plant = plant;
    }

    public String getPdtId() {
        return pdtId;
    }

    public void setPdtId(String pdtId) {
        this.pdtId = pdtId;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }
}
