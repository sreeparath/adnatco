package com.dcode.inventory.data.model;

public class LS_ITEMS_TYP {
    public String ITCODE;
    public String ITUNIT;
    public String FACTOR;
    public String LOCODE;
    public String SLNO;
    public long SEQ_NO;
    public float QTY;
    public String BATCHNO;
    public String MANFDATE;
    public String EXPDATE;
}
