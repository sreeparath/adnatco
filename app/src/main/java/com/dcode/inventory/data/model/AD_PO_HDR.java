package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "AD_PO_HDR")
public class AD_PO_HDR implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "HDR_ID")
    public long HDR_ID;

    @ColumnInfo(name = "PO_NUMBER")
    public String PO_NUMBER;

    @ColumnInfo(name = "CREAT_DATE")
    public String CREAT_DATE;

    @ColumnInfo(name = "VENDOR")
    public String VENDOR;

    @ColumnInfo(name = "LineCnt")
    public int LineCnt;

    @ColumnInfo(name = "STATUS")
    public String STATUS;

    @ColumnInfo(name = "REMARK")
    public String REMARK;

    @ColumnInfo(name = "TYPE")
    public String TYPE;
}
