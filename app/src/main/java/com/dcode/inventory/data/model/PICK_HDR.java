package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "PICK_HDR")
public class PICK_HDR implements Serializable {

    @NotNull
    @PrimaryKey
    @ColumnInfo(name = "ORD_NO")
    public String ORD_NO;

    @ColumnInfo(name = "PICKBY")
    public String PICKBY;

    @ColumnInfo(name = "REMARKS")
    public String REMARKS;


}
