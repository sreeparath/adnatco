package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "STORE_TRANSFER_ITEMS")
public class STORE_TRANSFER_ITEMS implements Serializable {

    @PrimaryKey
    @NotNull
    @ColumnInfo(name = "SL_NO")
    public long SL_NO;


    @ColumnInfo(name = "ORTYP")
    public String ORTYP;

    @ColumnInfo(name = "SEQ_NO")
    public long SEQ_NO;

    @ColumnInfo(name = "ORD_NO")
    public String ORD_NO;


    @ColumnInfo(name = "ITCODE")
    public String ITCODE;

    @ColumnInfo(name = "ITDESC")
    public String ITDESC;


    @ColumnInfo(name = "ITUNIT")
    public String ITUNIT;


    @ColumnInfo(name = "FACTOR")
    public String FACTOR;

    @ColumnInfo(name = "QTY")
    public int QTY;

    @ColumnInfo(name="REC_QTY")
    public int REC_QTY;

    @Ignore
    @ColumnInfo(name = "QTY_EDITABLE")
    public String QTY_EDITABLE;

    @ColumnInfo(name = "BASE_UNIT")
    public String BASE_UNIT;

    @ColumnInfo(name="REMARKS")
    public String REMARKS;


}
