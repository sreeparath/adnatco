package com.dcode.inventory.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.sqlite.db.SupportSQLiteQuery;

import com.dcode.inventory.data.model.AD_PO_HDR;
import com.dcode.inventory.data.model.AD_PO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.AD_PO_ITEMS_SPR;
import com.dcode.inventory.data.model.APP_SETTINGS;
import com.dcode.inventory.data.model.BARCODE_PRINTERS;
import com.dcode.inventory.data.model.CLS_PK_ID;
import com.dcode.inventory.data.model.CLS_PO_NUMBER;
import com.dcode.inventory.data.model.CONFIRM_DOC_DET;
import com.dcode.inventory.data.model.CONFIRM_DOC_HDR;
import com.dcode.inventory.data.model.DRIVER_LIST;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.GENERIC_COUNT;
import com.dcode.inventory.data.model.GENERIC_OBJECT;
import com.dcode.inventory.data.model.GR_DET;
import com.dcode.inventory.data.model.GR_HDR;
import com.dcode.inventory.data.model.GR_LINES_TYP;
import com.dcode.inventory.data.model.HELPER_LIST;
import com.dcode.inventory.data.model.LABEL_FORMATS;
import com.dcode.inventory.data.model.LOAD_SLIP_ITEMS_NEW;
import com.dcode.inventory.data.model.LOAD_SLIP_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.LOAD_SLIP_ITEMS_TYP;
import com.dcode.inventory.data.model.LOCATIONS;
import com.dcode.inventory.data.model.LS_HDR;
import com.dcode.inventory.data.model.LS_ITEMS;
import com.dcode.inventory.data.model.MATERIAL_LIST;
import com.dcode.inventory.data.model.MAX_VALUE;
import com.dcode.inventory.data.model.ORTYP;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.PLANT_LIST;
import com.dcode.inventory.data.model.PO_DET;
import com.dcode.inventory.data.model.PO_HDR;
import com.dcode.inventory.data.model.PUR_RET_ITEMS;
import com.dcode.inventory.data.model.PUR_RET_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.REASON_TYPE;
import com.dcode.inventory.data.model.REC_TYPE;
import com.dcode.inventory.data.model.RET_REASON_LIST;
import com.dcode.inventory.data.model.RE_WO_HDR_SPR;
import com.dcode.inventory.data.model.RE_WO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.RE_WO_ITEM_SPR;
import com.dcode.inventory.data.model.SALE_RET_DET;
import com.dcode.inventory.data.model.SALE_RET_DOC_DET;
import com.dcode.inventory.data.model.SALE_RET_DOC_HDR;
import com.dcode.inventory.data.model.SALE_RET_HDR;
import com.dcode.inventory.data.model.SEARCH_TYPE;
import com.dcode.inventory.data.model.SLIP_DET;
import com.dcode.inventory.data.model.SLIP_HDR;
import com.dcode.inventory.data.model.SLOC_LIST_SPR;
import com.dcode.inventory.data.model.SR_LINES_TYP;
import com.dcode.inventory.data.model.STK_COUNT_DET;
import com.dcode.inventory.data.model.STK_COUNT_HDR;
import com.dcode.inventory.data.model.STK_COUNT_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.STK_TRN_PRJ_PRJ_RECIEPT;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT221;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT221Q;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT222;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT222Q;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT411Q;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT415Q;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR__REV_RECIEPT;
import com.dcode.inventory.data.model.STOCK_HEADER;
import com.dcode.inventory.data.model.STOCK_HEADER_ITEMS;
import com.dcode.inventory.data.model.STOCK_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.STORE_TRANSFER_ITEMS;
import com.dcode.inventory.data.model.STORE_TRANSFER_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.USERS;
import com.dcode.inventory.data.model.VEHICLE_LIST;
import com.dcode.inventory.data.model.WO_HDR_SPR;
import com.dcode.inventory.data.model.WO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.WO_ITEM_SPR;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface GenericDao {

    @RawQuery
    MAX_VALUE getMaxValue(SupportSQLiteQuery query);

    @RawQuery
    GENERIC_COUNT getCount(SupportSQLiteQuery query);

    @Query("DELETE FROM OBJECTS WHERE MASTER_ID=:master_id")
    void deleteAllMasters(long master_id);

    @Query("DELETE FROM APP_SETTINGS")
    void deleteAllAppSettings();

    @Insert(onConflict = REPLACE)
    void insertAllAppSettings(APP_SETTINGS... app_settings);

    @Query("SELECT * FROM APP_SETTINGS ORDER BY ID DESC LIMIT 1")
    APP_SETTINGS getAppSettings();

    @Query("DELETE FROM BARCODE_PRINTERS")
    void deleteAllPrinters();

    @Insert(onConflict = REPLACE)
    void insertPrinters(BARCODE_PRINTERS... barcode_printers);

    @Query("SELECT * FROM BARCODE_PRINTERS")
    List<BARCODE_PRINTERS> getAllBarcodePrinters();

    @Query("DELETE FROM LABEL_FORMATS")
    void deleteAllLabels();

    @Insert(onConflict = REPLACE)
    void insertLabels(LABEL_FORMATS... label_formats);

    @Query("SELECT ID, OBJECT_CODE Code, OBJECT_NAME Name, REMARKS Remarks FROM LABEL_FORMATS")
    List<GENERIC_OBJECT> getAllLabelFormats();

    // region " USERS "

    @Query("SELECT * FROM USERS LIMIT 1")
    USERS getAllUsers();

    @Query("SELECT * FROM USERS WHERE USER_NAME=:userName AND USER_PWD=:userPassword LIMIT 1")
    USERS validateUser(String userName, String userPassword);

    @Insert(onConflict = REPLACE)
    void insertUser(USERS... user);

    @Query("DELETE FROM USERS WHERE ID=:userID")
    void deleteUser(int userID);

    @Query("DELETE FROM USERS")
    void deleteAllUser();

    @Query("SELECT * FROM USERS WHERE USER_NAME=:loginName AND USER_PWD=:passWd")
    USERS getUser(String loginName, String passWd);

    // endregion: " USERS "

    // region " LOCATIONS "

    @Query("SELECT * FROM LOCATIONS WHERE COCODE = :coCode")
    List<LOCATIONS> getAllLocations(String coCode);

    @Insert(onConflict = REPLACE)
    void insertLocations(LOCATIONS... locations);

    @Query("DELETE FROM LOCATIONS WHERE COCODE = :coCode")
    void deleteAllLocationsByCompany(String coCode);

    // endregion: " LOCATIONS "

    // region " ORTYP "

    @Query("DELETE FROM ORTYP")
    void deleteAllOrTyps();

    @Insert(onConflict = REPLACE)
    void insertOrTyps(ORTYP... ortyps);



    // endregion: " ORTYP "

    // region: " REC_TYPE "

    @Query("DELETE FROM REC_TYPE")
    void deleteAllRecTypes();

    @Insert(onConflict = REPLACE)
    void insertRecTypes(REC_TYPE... rec_types);


    @Query("SELECT DISTINCT FACTOR AS CODE, ITUNIT AS NAME,NULL AS BIN_ID FROM PICK_LIST_ITEMS WHERE ITCODE=:code ")
    List<DocType> getAllUnits(String code);

    @Query("SELECT FACTOR AS CODE, ITUNIT AS NAME,NULL AS BIN_ID FROM LOAD_SLIP_ITEMS_NEW WHERE UPPER(ITCODE)=UPPER(:code) ")
    List<DocType> getAllUnitsLoadSlip(String code);

    // endregion: " REC_TYPE "

    // region " PO "



    @Query("DELETE FROM PO_DET")
    void deleteAllPO_DET();

    @Query("DELETE FROM PO_DET WHERE ORDNO=:ordNo AND ORTYP=:orTyp")
    void deletePO_DET(String ordNo, String orTyp);

    @Query("DELETE FROM AD_PO_ITEMS_SPR WHERE PO_NUMBER =:ordNo  ")
    void deleteAD_PO_ITEMS_SPR(String ordNo);

    @Query("DELETE FROM AD_PO_HDR WHERE PO_NUMBER=:ordNo AND TYPE=:orTyp")
    void deleteAD_PO_HDR(String ordNo, String orTyp);

//    @Query("SELECT HDR.*, ORTYP.ORNAME FROM PO_HDR HDR " +
//            "INNER JOIN ORTYP WHERE ORTYP.ORCODE = HDR.ORTYP " +
//            "AND ORDNO=:ordNo AND ORTYP=:orTyp")
    @Query("SELECT HDR.* FROM PO_HDR HDR " +
        " WHERE ORDNO=:ordNo AND ORTYP=:orTyp")
    PO_HDR getPO_HDR(String ordNo, String orTyp);

    @Query("SELECT HDR.* FROM AD_PO_HDR HDR " +
            " WHERE PO_NUMBER=:ordNo ")
    AD_PO_HDR getAD_PO_HDR(String ordNo);

    @Insert()
    void insertAD_PO_HDR(AD_PO_HDR... po_hdr);

    @Insert()
    void insertAD_PO_ITEMS_SPR(AD_PO_ITEMS_SPR... ad_po_items_sprs);

    @Insert()
    void insertPO_DET(PO_DET... po_det);

    @Query("SELECT P.DET_ID, P.ORDNO, P.ORTYP, P.ITCODE, P.ITDESC, P.PACKING, P.ITUNIT, " +
            "P.FACTOR, P.CARTON, P.ITQTY, IFNULL(GR.RCQTY, 0) AS RCQTY, P.CNQTY, P.TOTQTY, " +
            "P.SLNO, P.SUP_BCODE, P.BATCHFLG, P.ITRTE,P.TOL_QTY,P.IS_QTY_VALIDATE " +
            "FROM PO_DET P " +
            "LEFT OUTER JOIN (SELECT D.ITCODE, D.SLNO, SUM(D.RCQTY) AS RCQTY " +
            "FROM GR_HDR H INNER JOIN GR_DET D ON D.HDR_ID = H.HDR_ID " +
            "WHERE H.RECEIPT_NO = 0 AND H.ORDNO = :ordNo AND H.ORTYP = :orTyp " +
            "GROUP BY D.ITCODE, D.SLNO) GR ON P.ITCODE = GR.ITCODE AND P.SLNO = GR.SLNO " +
            "WHERE P.ORDNO=:ordNo AND P.ORTYP=:orTyp")
    List<PO_DET> getPO_DET(String ordNo, String orTyp);

    @Query("SELECT P.DET_ID, P.ORDNO, P.ORTYP, P.ITCODE, P.ITDESC, P.PACKING, P.ITUNIT, " +
            "P.FACTOR, P.CARTON, P.ITQTY, IFNULL(GR.RCQTY, 0) AS RCQTY, P.CNQTY, P.TOTQTY, " +
            "P.SLNO, P.SUP_BCODE, P.BATCHFLG, P.ITRTE,P.TOL_QTY,P.IS_QTY_VALIDATE " +
            "FROM PO_DET P " +
            "LEFT OUTER JOIN (SELECT D.ITCODE, D.SLNO, SUM(D.RCQTY) AS RCQTY " +
            "FROM GR_HDR H INNER JOIN GR_DET D ON D.HDR_ID = H.HDR_ID " +
            "WHERE H.RECEIPT_NO = 0 AND H.ORDNO = :ordNo AND H.ORTYP = :orTyp " +
            "GROUP BY D.ITCODE, D.SLNO) GR ON P.ITCODE = GR.ITCODE AND P.SLNO = GR.SLNO " +
            "WHERE P.ORDNO=:ordNo AND P.ORTYP=:orTyp AND P.SLNO = :slNo")
    PO_DET getPO_DET(String ordNo, String orTyp, long slNo);




    // endregion: " PO "

    // region " GR "

    @Insert(onConflict = REPLACE)
    long insertGR_HDR(GR_HDR gr_hdr);

    @Insert(onConflict = REPLACE)
    void insertGR_DET(GR_DET gr_det);

    @Query("DELETE FROM GR_HDR WHERE HDR_ID=:hdr_id")
    void deleteGR_HDR(long hdr_id);

    @Query("DELETE FROM GR_DET WHERE HDR_ID=:hdr_id")
    void deleteGR_DET(long hdr_id);

    @Query("SELECT HDR_ID PK_ID FROM GR_HDR WHERE IS_UPLOADED = 0")
    List<CLS_PK_ID> getAllGRForUpload();

    @Query("SELECT * FROM GR_HDR WHERE HDR_ID=:pk_id")
    GR_HDR getGR_HDR(long pk_id);

    @Query("SELECT d.*, r.NAME REC_DESC FROM GR_DET d " +
            "INNER JOIN REC_TYPE r ON r.CODE = d.REC_TYPE " +
            "WHERE d.HDR_ID = :pk_id AND d.SLNO = :sl_no")
    List<GR_DET> getGR_DET(long pk_id, long sl_no);

    @Query("SELECT d.*, r.NAME REC_DESC FROM GR_DET d " +
            "INNER JOIN REC_TYPE r ON r.CODE = d.REC_TYPE " +
            "WHERE d.HDR_ID = :pk_id")
    List<GR_DET> getGR_DETByHDR_ID(long pk_id);

    @Query("SELECT ITCODE, ITUNIT, BATCHNO, MANFDATE, EXPDATE, " +
            "RCQTY QTY, FACTOR, REC_TYPE, SLNO, SEQ_NO " +
            "FROM GR_DET WHERE HDR_ID=:pk_id")
    List<GR_LINES_TYP> getGRLinesForUpload(long pk_id);

    @Query("UPDATE GR_HDR SET IS_UPLOADED = 1, RECEIPT_NO = :receipt_no WHERE HDR_ID = :pk_id")
    void updateGR_HDR(long pk_id, long receipt_no);

//    @Query("SELECT * FROM GR_HDR WHERE ORDNO = :ordNo and ORTYP = :orTyp AND RECEIPT_NO <= 0")
//    GR_HDR getPendingGRByDocNo(String ordNo, String orTyp);

    @Query("SELECT * FROM GR_HDR WHERE ORDNO = :ordNo AND RECEIPT_NO <= 0")
    GR_HDR getPendingGRByDocNo(String ordNo);

    @Query("SELECT * FROM GR_DET WHERE DET_ID = :det_id")
    GR_DET getGR_DETByDet_ID(long det_id);

    @Query("DELETE FROM GR_DET WHERE DET_ID = :det_id")
    void deleteGR_DETByDet_ID(long det_id);

    @Query("DELETE FROM PICK_LIST_ITEMS_RECEIPT WHERE SL_NO = :sl_no")
    void deletePICK_LIST_ITEMS_RECEIPTBysl_no(long sl_no);

    @Query("DELETE FROM LOAD_SLIP_ITEMS_RECEIPT WHERE SEQ_NO = :sl_no")
    void deleteLOAD_SLIP_ITEMS_RECEIPTBysl_no(long sl_no);

    @Query("DELETE FROM PICK_LIST_ITEMS_RECEIPT")
    void deletePICK_LIST_ITEMS_RECEIPT();

    @Query("DELETE FROM LOAD_SLIP_ITEMS_RECEIPT WHERE ORD_NO=  :ord_no")
    void deleteLOAD_SLIP_ITEMS_RECEIPT(String ord_no);

    @Query("DELETE FROM PICK_LIST_ITEMS WHERE ORD_NO=  :ord_no")
    void deletePICK_LIST_ITEMS(String ord_no);

    @Query("DELETE FROM LOAD_SLIP_ITEMS_NEW WHERE ORD_NO=  :ord_no")
    void deleteLOAD_SLIP_ITEMS_NEW(String ord_no);

    // endregion: " GR "

    //region " LS "

    @Insert(onConflict = REPLACE)
    void insertLS_HDR(LS_HDR... ls_hdr);

    @Insert(onConflict = REPLACE)
    void insertLS_ITEMS(LS_ITEMS... ls_items);

    @Insert(onConflict = REPLACE)
    void insertPL_ITEMS(PICK_LIST_ITEMS... pl_items);

    @Insert(onConflict = REPLACE)
    void insertLOAD_SLIP_ITEMS_NEW(LOAD_SLIP_ITEMS_NEW... pl_items);

    @Query("UPDATE PICK_LIST_ITEMS_RECEIPT SET REMARKS = :remarks WHERE ORD_NO = :ord_no AND ITCODE = :itcode")
    void updatePL_ITEMS_RECEIPT_REMARK(String ord_no, String  itcode,String remarks);

    @Query("UPDATE LOAD_SLIP_ITEMS_RECEIPT SET REMARKS = :remarks,IS_COMPLETE = :isComplete WHERE ORD_NO = :ord_no AND ITCODE = :itcode AND SL_NO =:sl_no")
    void updateLOAD_SLIP_ITEMS_RECEIPT_REMARK(String ord_no, String  itcode,String remarks,long sl_no,String isComplete);

    @Insert()
    long insertPL_ITEMS_RECEIPT(PICK_LIST_ITEMS_RECEIPT pl_item_rec);

    @Insert()
    long insertLOAD_SLIP_ITEMS_RECEIPT(LOAD_SLIP_ITEMS_RECEIPT pl_item_rec);

    @Query("SELECT SL_NO,ITCODE,IUNITS,BATCHNO,MANFDATE,EXPDATE,QTY,FACTOR,REC_NO,REMARKS FROM PICK_LIST_ITEMS_RECEIPT WHERE ORD_NO =:ordNo")
    List<PICK_LIST_ITEMS_RECEIPT> GetPL_ITEMS_RECEIPT(String ordNo);

    @Query("SELECT SEQ_NO,SL_NO,ITCODE,IUNITS,BATCHNO,MANFDATE,EXPDATE,QTY,FACTOR,RECNO,REMARKS,IS_COMPLETE,ERPBATCH FROM LOAD_SLIP_ITEMS_RECEIPT WHERE ORD_NO =:ordNo")
    List<LOAD_SLIP_ITEMS_RECEIPT> GetLOAD_SLIP_ITEMS_RECEIPT(String ordNo);

    @Query("SELECT * FROM SLIP_HDR WHERE ORDNO = :order_no AND ORTYP = :or_type AND IS_UPLOADED = 0")
    SLIP_HDR GetSLIP_HDR(String order_no, String or_type);

    @Query("SELECT * FROM SLIP_HDR WHERE HDR_ID = :pk_id")
    SLIP_HDR GetSLIP_HDR(long pk_id);

    @Query("SELECT * FROM PICK_LIST_ITEMS WHERE ORD_NO = :ord_no AND ITCODE=:itcode AND QTY > 0 ORDER BY CAST(FACTOR as float) DESC")
    List<PICK_LIST_ITEMS> GetPICKLIST_ITEMS_DETAILS(String ord_no,String itcode);

    @Query("SELECT * FROM LOAD_SLIP_ITEMS_NEW WHERE ORD_NO = :ord_no AND ITCODE=:itcode AND QTY > 0 ORDER BY CAST(FACTOR as float) DESC")
    List<LOAD_SLIP_ITEMS_NEW> GetLOAD_SLIP_ITEMS_DETAILS(String ord_no,String itcode);

    @Query("SELECT 0 as SL_NO,PICKITM.ORD_NO,PICKITM.ITCODE,PICKITM.ITDESC,PICKITM.BASE_UNIT,\n" +
            "SUM(PICKITM.FACTOR*PICKITM.QTY) AS QTY,(IFNULL(B.REC_QTY, 0)) AS REC_QTY\n" +
            "FROM PICK_LIST_ITEMS PICKITM \n" +
            "LEFT OUTER JOIN (SELECT R.ITCODE, SUM(R.FACTOR*R.QTY) REC_QTY FROM PICK_LIST_ITEMS_RECEIPT R GROUP BY R.ITCODE) B\n" +
            "\tON B.ITCODE = PICKITM.ITCODE\n" +
            "WHERE PICKITM.ORD_NO = :ord_no \n" +
            "GROUP BY PICKITM.ORD_NO,PICKITM.ITCODE,PICKITM.ITDESC,PICKITM.BASE_UNIT")
    List<PICK_LIST_ITEMS> GetPICKLIST_ITEMS(String ord_no);

    @Query("SELECT 0 as SL_NO,SEQ_NO,PICKITM.ORD_NO,PICKITM.ITCODE,PICKITM.ITDESC,PICKITM.BASE_UNIT,\n" +
            "SUM(PICKITM.FACTOR*PICKITM.QTY) AS QTY,(IFNULL(B.REC_QTY, 0)) AS REC_QTY,PICKITM.TOL_QTY,PICKITM.IS_QTY_VALIDATE\n" +
            "FROM LOAD_SLIP_ITEMS_NEW PICKITM \n" +
            "LEFT OUTER JOIN (SELECT R.ITCODE, SUM(R.FACTOR*R.QTY) REC_QTY FROM LOAD_SLIP_ITEMS_RECEIPT R GROUP BY R.ITCODE) B\n" +
            "\tON B.ITCODE = PICKITM.ITCODE\n" +
            "WHERE PICKITM.ORD_NO = :ord_no \n" +
            "GROUP BY PICKITM.ORD_NO,PICKITM.ITCODE,PICKITM.ITDESC,PICKITM.BASE_UNIT,PICKITM.TOL_QTY,PICKITM.IS_QTY_VALIDATE")
    List<LOAD_SLIP_ITEMS_NEW> GetLOAD_SLIP_ITEMS_NEW(String ord_no);

    @Query("SELECT PICKITM.SL_NO,0 as SEQ_NO,PICKITM.ORD_NO,PICKITM.ITCODE,PICKITM.ITDESC,PICKITM.ITUNIT,\n" +
            "SUM(PICKITM.QTY) AS QTY,B.REC_QTY AS REC_QTY,B.REMARKS,PICKITM.TOL_QTY,PICKITM.IS_QTY_VALIDATE\n" +
            "FROM LOAD_SLIP_ITEMS_NEW PICKITM \n" +
            "LEFT OUTER JOIN (SELECT R.ITCODE, SUM(R.QTY) REC_QTY,R.SL_NO,R.REMARKS FROM LOAD_SLIP_ITEMS_RECEIPT R GROUP BY R.ITCODE,R.SL_NO,R.REMARKS) B\n" +
            "\tON B.ITCODE = PICKITM.ITCODE AND  B.SL_NO = PICKITM.SL_NO \n" +
            "WHERE PICKITM.ORD_NO = :ord_no \n" +
            "GROUP BY PICKITM.SL_NO,PICKITM.ORD_NO,PICKITM.ITCODE,PICKITM.ITDESC,PICKITM.BASE_UNIT,B.REMARKS,PICKITM.TOL_QTY,PICKITM.IS_QTY_VALIDATE")
    List<LOAD_SLIP_ITEMS_NEW> GetLOAD_SLIP_ITEMS_INDIVIDUAL(String ord_no);

    @Query("SELECT SUM(FACTOR*QTY) FROM PICK_LIST_ITEMS_RECEIPT   WHERE ITCODE =:itcode AND ORD_NO = :ord_no")
    int getTotalPickListItemReceipt(String ord_no,String itcode);

    @Query("SELECT SUM(QTY) FROM LOAD_SLIP_ITEMS_RECEIPT   WHERE ITCODE =:itcode AND ORD_NO = :ord_no and  SL_NO =:sl_no")
    int getTotalLoadSlipItemReceiptIndividual(String ord_no,String itcode,int sl_no);

    @Query("SELECT SUM(FACTOR*QTY) FROM LOAD_SLIP_ITEMS_RECEIPT   WHERE ITCODE =:itcode AND ORD_NO = :ord_no")
    int getTotalLoadSlipItemReceipt(String ord_no,String itcode);

    @Query("SELECT* FROM PICK_LIST_ITEMS_RECEIPT   WHERE ITCODE =:itcode AND ORD_NO = :ord_no")
    PICK_LIST_ITEMS_RECEIPT getPickListItemReceipt(String ord_no,String itcode);

    @Query("SELECT* FROM LOAD_SLIP_ITEMS_RECEIPT   WHERE ITCODE =:itcode AND ORD_NO = :ord_no")
    LOAD_SLIP_ITEMS_RECEIPT getLoadItemReceipt(String ord_no, String itcode);

    @Query("SELECT* FROM LOAD_SLIP_ITEMS_RECEIPT   WHERE ITCODE =:itcode AND ORD_NO = :ord_no and  SL_NO =:sl_no")
    LOAD_SLIP_ITEMS_RECEIPT getLoadItemReceiptIndividual(String ord_no, String itcode,long sl_no);

    @Query("SELECT SUM(FACTOR*QTY) FROM PICK_LIST_ITEMS   WHERE ITCODE =:itcode AND ORD_NO = :ord_no")
    int getTotalPickListItem(String ord_no,String itcode);

    @Query("SELECT SUM(FACTOR*QTY) FROM LOAD_SLIP_ITEMS_NEW   WHERE ITCODE =:itcode AND ORD_NO = :ord_no")
    int getTotalLoadSlipItem(String ord_no,String itcode);

    @Query("SELECT SUM(QTY) FROM LOAD_SLIP_ITEMS_NEW   WHERE ITCODE =:itcode AND ORD_NO = :ord_no and  SL_NO =:sl_no")
    int getTotalLoadSlipItemIndividual(String ord_no,String itcode,int sl_no);

    @Query("SELECT SUM(TOL_QTY) FROM LOAD_SLIP_ITEMS_NEW   WHERE ITCODE =:itcode AND ORD_NO = :ord_no and  SL_NO =:sl_no")
    float getLoadSlipItemTolQty(String ord_no,String itcode,int sl_no);

    @Query("SELECT IS_QTY_VALIDATE FROM LOAD_SLIP_ITEMS_NEW   WHERE ITCODE =:itcode AND ORD_NO = :ord_no and  SL_NO =:sl_no")
    int getIsQtyValidate(String ord_no,String itcode,int sl_no);

    @Query("SELECT *  FROM(SELECT 0 as SL_NO,PICKITM.ORD_NO,PICKITM.ITCODE,PICKITM.ITDESC,PICKITM.BASE_UNIT,\n" +
            "SUM(PICKITM.FACTOR*PICKITM.QTY) AS QTY,(IFNULL(B.REC_QTY, 0)) AS REC_QTY\n" +
            "FROM PICK_LIST_ITEMS PICKITM \n" +
            "LEFT OUTER JOIN (SELECT R.ITCODE, SUM(R.FACTOR*R.QTY) REC_QTY FROM PICK_LIST_ITEMS_RECEIPT R GROUP BY R.ITCODE) B\n" +
            "\tON B.ITCODE = PICKITM.ITCODE\n" +
            "WHERE PICKITM.ORD_NO = :ord_no \n" +
            "GROUP BY PICKITM.ORD_NO,PICKITM.ITCODE,PICKITM.ITDESC,PICKITM.BASE_UNIT) WHERE QTY - REC_QTY > 0 ")
    List<PICK_LIST_ITEMS> GetPICKLIST_ITEMS_PENDING(String ord_no);

//    @Query("SELECT *  FROM(SELECT SL_NO,SEQ_NO,PICKITM.ORD_NO,PICKITM.ITCODE,PICKITM.ITDESC,PICKITM.BASE_UNIT,\n" +
//            "(PICKITM.QTY) AS QTY,(IFNULL(B.REC_QTY, 0)) AS REC_QTY,B.REMARKS\n" +
//            "FROM LOAD_SLIP_ITEMS_NEW PICKITM \n" +
//            "LEFT OUTER JOIN (SELECT R.ITCODE, SUM(R.QTY) REC_QTY,R.REMARKS,R.SL_NO FROM LOAD_SLIP_ITEMS_RECEIPT R GROUP BY R.ITCODE,R.REMARKS,R.SL_NO) B\n" +
//            "\tON B.ITCODE = PICKITM.ITCODE AND  B.SL_NO = PICKITM.SL_NO \n" +
//            "WHERE PICKITM.ORD_NO = :ord_no \n" +
//            ") WHERE QTY - REC_QTY > 0 ")
//    List<LOAD_SLIP_ITEMS_NEW> GetLOADSLIP_ITEMS_PENDING(String ord_no);



    @Query("SELECT a.ORTYP, a.ORDNO, a.ITCODE, a.ITDESC, a.PACKING, a.ITUNIT, a.LOCODE, " +
            "a.FACTOR, a.CARTON, a.ITQTY, IFNULL(b.RCQTY, 0) AS RCQTY, a.CNQTY, a.TOTQTY, " +
            "a.STKQTY, a.Slno, a.SUP_BCODE, a.BATCHFLG " +
            "FROM LS_ITEMS a " +
            "LEFT OUTER JOIN (SELECT d.ITCODE, d.SLNO, SUM(d.QTY) RCQTY FROM SLIP_HDR H " +
            "INNER JOIN SLIP_DET d ON d.HDR_ID = h.HDR_ID " +
            "WHERE h.ORDNO = :order_no AND h.ORTYP = :ortyp AND h.IS_UPLOADED = 0 " +
            "GROUP BY d.ITCODE) b ON b.ITCODE = a.ITCODE AND b.SLNO = a.SLNO " +
            "WHERE a.ORDNO = :order_no AND a.ORTYP = :ortyp")
    List<LS_ITEMS> getLS_Items(String order_no, String ortyp);

/*    @Query("SELECT a.ORTYP, a.ORDNO, a.ITCODE, a.ITDESC, a.PACKING, a.ITUNIT, a.LOCODE, " +
            "a.FACTOR, a.CARTON, a.ITQTY, IFNULL(b.RCQTY, 0) AS RCQTY, a.CNQTY, a.TOTQTY, " +
            "a.STKQTY, a.Slno, a.SUP_BCODE, a.BATCHFLG " +
            "FROM LS_ITEMS a " +
            "LEFT OUTER JOIN (SELECT d.ITCODE, d.SLNO, SUM(d.QTY) RCQTY FROM SLIP_HDR H " +
            "INNER JOIN SLIP_DET d ON d.HDR_ID = h.HDR_ID " +
            "WHERE h.ORDNO = :order_no AND h.ORTYP = :ortyp AND h.IS_UPLOADED = 0 " +
            "GROUP BY d.ITCODE) b ON b.ITCODE = a.ITCODE AND b.SLNO = a.SLNO " +
            "WHERE a.ORDNO = :order_no AND a.ORTYP = :ortyp AND a.SLNO = :sl_no")
    LS_ITEMS getLS_Items(String order_no, String ortyp, long sl_no);*/

    @Query("SELECT a.ORTYP, a.ORDNO, a.ITCODE, a.ITDESC, a.PACKING, a.ITUNIT, a.LOCODE, " +
            "a.FACTOR, a.CARTON, a.ITQTY, IFNULL(b.RCQTY, 0) AS RCQTY, a.CNQTY, a.TOTQTY, " +
            "a.STKQTY, a.Slno, a.SUP_BCODE, a.BATCHFLG " +
            "FROM LS_ITEMS a " +
            "LEFT OUTER JOIN (SELECT d.ITCODE, d.SLNO, SUM(d.QTY) RCQTY FROM SLIP_HDR H " +
            "INNER JOIN SLIP_DET d ON d.HDR_ID = h.HDR_ID " +
            "WHERE h.ORDNO = :order_no AND h.ORTYP = :ortyp AND d.ITCODE = :itcode AND h.IS_UPLOADED = 0 " +
            "GROUP BY d.ITCODE) b ON b.ITCODE = a.ITCODE AND b.SLNO = a.SLNO " +
            "WHERE a.ORDNO = :order_no AND a.ORTYP = :ortyp AND TRIM(a.ITCODE) = :itcode")
    LS_ITEMS getLS_Items(String order_no, String ortyp, String itcode);

    @Insert(onConflict = REPLACE)
    long insertSlip_HDR(SLIP_HDR slip_hdr);

    @Insert(onConflict = REPLACE)
    long insertSlip_DET(SLIP_DET slip_det);

    @Query("SELECT * FROM SLIP_DET WHERE DET_ID = :det_id")
    SLIP_DET getSLIP_DET_ByID(long det_id);

    @Query("SELECT * FROM SLIP_DET " +
            "WHERE HDR_ID = :hdr_id AND SLNO = :sl_no")
    List<SLIP_DET> getSLIP_DET(long hdr_id, long sl_no);

    @Query("SELECT * FROM SLIP_DET " +
            "WHERE HDR_ID = :hdr_id")
    List<SLIP_DET> getSLIP_DETByHDR_ID(long hdr_id);

    @Query("SELECT ITCODE, ITUNIT, BATCHNO, MANFDATE, EXPDATE, QTY, " +
            "FACTOR, SLNO, SEQ_NO, LOCODE, REFNO FROM SLIP_DET " +
            "WHERE HDR_ID = :hdr_id")
    List<LOAD_SLIP_ITEMS_TYP> getSLIP_DETByHDR_IDForUpload(long hdr_id);

    @Query("DELETE FROM SLIP_DET WHERE DET_ID = :pk_id")
    void deleteSLIP_DETByDet_ID(long pk_id);

    @Query("UPDATE SLIP_HDR SET IS_UPLOADED = 1, RECEIPT_NO = :receipt_no WHERE HDR_ID = :pk_id")
    void updateSLIP_HDR(long pk_id, long receipt_no);

    //endregion " LS "

    //Stock take module
    @Query("SELECT * FROM STOCK_HEADER")
    List<STOCK_HEADER> getStockHeadItems();

    @Insert(onConflict = REPLACE)
    void insertSTOCK_HEADER(STOCK_HEADER... stock_header);

    @Insert(onConflict = REPLACE)
    void insertSTOCK_HEADER_ITEMS(STOCK_HEADER_ITEMS... stock_header_items);

    @Query("SELECT * FROM STOCK_HEADER_ITEMS WHERE ITCODE=:code ")
    List<STOCK_HEADER_ITEMS> getStockItemDetails(String code);

    @Insert()
    long insertSTOCK_ITEMS_RECEIPT(STOCK_ITEMS_RECEIPT st_item_rec);

    @Query("SELECT SL_NO,ITCODE,ITUNITS,BATCHNO,MANFDATE,EXPDATE,CNTQTY FROM STOCK_ITEMS_RECEIPT WHERE ORD_NO =:ordNo ORDER BY SL_NO DESC")
    List<STOCK_ITEMS_RECEIPT> GetSTOCK_ITEMS_RECEIPT(String ordNo);

    @Query("SELECT SLNO,ITCODE,IUNITS,BATCHNO,MANFDATE,EXPDATE,CNTQTY FROM PUR_RET_ITEMS_RECEIPT WHERE REF_NO =:refNo")
    List<PUR_RET_ITEMS_RECEIPT> GetPUR_RET_ITEMS_RECEIPT(String refNo);

    @Query("DELETE FROM STOCK_ITEMS_RECEIPT WHERE SL_NO = :sl_no")
    void deleteSTOCK_ITEMS_RECEIPTBysl_no(long sl_no);

    @Query("DELETE FROM PUR_RET_ITEMS_RECEIPT WHERE SLNO = :sl_no")
    void deletePUR_RET_ITEMS_RECEIPTBysl_no(long sl_no);

    @Query("SELECT SL_NO,ITCODE,ITUNITS,BATCHNO,MANFDATE,EXPDATE,CNTQTY,SCANTIME,ERPBATCH FROM STOCK_ITEMS_RECEIPT WHERE ORD_NO =:ordNo")
    List<STOCK_ITEMS_RECEIPT> GetSTOCK_ITEMS_RECEIPT_upload(String ordNo);

    @Query("DELETE FROM STOCK_HEADER WHERE ORDNO = :ordNo")
    void deleteSTOCK_HEADER(String ordNo);

    @Query("DELETE FROM STOCK_HEADER_ITEMS WHERE ORDNO =:ordNo")
    void deleteSTOCK_HEADER_ITEMS(String ordNo);

    @Query("DELETE FROM STOCK_ITEMS_RECEIPT WHERE ORD_NO =:ordNo")
    void deleteSTOCK_ITEMS_RECEIPT(String ordNo);

    //Store transfer module
    @Insert(onConflict = REPLACE)
    void insertSTORE_TRANSFER_ITEMS(STORE_TRANSFER_ITEMS... pl_items);

    @Query("SELECT PICKITM.SL_NO,0 as SEQ_NO,PICKITM.ORD_NO,PICKITM.ITCODE,PICKITM.ITDESC,PICKITM.ITUNIT,\n" +
            "SUM(PICKITM.QTY) AS QTY,(IFNULL(B.REC_QTY, 0)) AS REC_QTY,B.REMARKS\n" +
            "FROM STORE_TRANSFER_ITEMS PICKITM \n" +
            "LEFT OUTER JOIN (SELECT R.ITCODE, SUM(R.QTY) REC_QTY,R.SL_NO,R.REMARKS FROM STORE_TRANSFER_ITEMS_RECEIPT R GROUP BY R.ITCODE,R.SL_NO,R.REMARKS) B\n" +
            "\tON B.ITCODE = PICKITM.ITCODE AND  B.SL_NO = PICKITM.SL_NO \n" +
            "WHERE PICKITM.ORD_NO = :ord_no \n" +
            "GROUP BY PICKITM.SL_NO,PICKITM.ORD_NO,PICKITM.ITCODE,PICKITM.ITDESC,PICKITM.BASE_UNIT,B.REMARKS")
    List<STORE_TRANSFER_ITEMS> GetSTORE_TRANSFER_ITEMS(String ord_no);

    @Query("SELECT SEQ_NO,SL_NO,ITCODE,IUNITS,BATCHNO,MANFDATE,EXPDATE,QTY,FACTOR,REC_NO,REMARKS FROM STORE_TRANSFER_ITEMS_RECEIPT WHERE ORD_NO =:ordNo")
    List<STORE_TRANSFER_ITEMS_RECEIPT> GetSTORE_TRANSFER_ITEMS_RECEIPT(String ordNo);

    @Query("SELECT SUM(QTY) FROM STORE_TRANSFER_ITEMS_RECEIPT   WHERE ITCODE =:itcode AND ORD_NO = :ord_no and  SL_NO =:sl_no")
    int getTotalStoreTransferItemReceiptIndividual(String ord_no,String itcode,int sl_no);

    @Query("SELECT SUM(QTY) FROM STORE_TRANSFER_ITEMS   WHERE ITCODE =:itcode AND ORD_NO = :ord_no and  SL_NO =:sl_no")
    int getTotalStoreTransferItemIndividual(String ord_no,String itcode,int sl_no);

    @Insert()
    long insertSTORE_TRANSFER_ITEMS_RECEIPT(STORE_TRANSFER_ITEMS_RECEIPT pl_item_rec);

    @Query("SELECT SL_NO AS CODE, ITUNIT AS NAME,NULL AS BIN_ID FROM STORE_TRANSFER_ITEMS WHERE ITCODE=:code ")
    List<DocType> getAllUnitsStoreTransfer(String code);

    @Query("DELETE FROM STORE_TRANSFER_ITEMS_RECEIPT WHERE SEQ_NO = :sl_no")
    void deleteSTORE_TRANSFER_ITEMS_RECEIPTBysl_no(long sl_no);

    @Query("SELECT* FROM STORE_TRANSFER_ITEMS_RECEIPT   WHERE ITCODE =:itcode AND ORD_NO = :ord_no and  SL_NO =:sl_no")
    STORE_TRANSFER_ITEMS_RECEIPT getStoreTransferItemReceiptIndividual(String ord_no, String itcode,long sl_no);

    @Query("UPDATE STORE_TRANSFER_ITEMS_RECEIPT SET REMARKS = :remarks,IS_COMPLETE = :isComplete WHERE ORD_NO = :ord_no AND ITCODE = :itcode AND SL_NO =:sl_no")
    void updateSTORE_TRANSFER_ITEMS_RECEIPT_REMARK(String ord_no, String  itcode,String remarks,long sl_no,String isComplete);

    @Query("DELETE FROM STORE_TRANSFER_ITEMS WHERE ORD_NO=  :ord_no")
    void deleteSTORE_TRANSFER_ITEMS(String ord_no);

    @Query("DELETE FROM STORE_TRANSFER_ITEMS_RECEIPT  WHERE ORD_NO=  :ord_no")
    void deleteSTORE_TRANSFER_ITEMS_RECEIPT(String ord_no);

    @Query("SELECT 0 AS PK_ID,CODE, NAME,1 AS PARENT_ID FROM REC_TYPE")
    List<SEARCH_TYPE> getREC_TYPE();

    @Query("DELETE FROM DRIVER_LIST")
    void deleteAllDriverList();

    @Insert(onConflict = REPLACE)
    void insertDriverList(DRIVER_LIST... driver_lists);

    @Query("SELECT NULL AS PK_ID,CODE,NAME,NULL AS PARENT_ID FROM DRIVER_LIST")
    List<SEARCH_TYPE> getDriverList();

    @Query("DELETE FROM HELPER_LIST")
    void deleteAllHelperList();

    @Insert(onConflict = REPLACE)
    void insertHelperList(HELPER_LIST... helper_lists);

    @Query("SELECT NULL AS PK_ID,CODE,NAME,NULL AS PARENT_ID FROM HELPER_LIST")
    List<SEARCH_TYPE> getHelperList();

    @Query("DELETE FROM VEHICLE_LIST")
    void deleteAllVehicleList();

    @Insert(onConflict = REPLACE)
    void insertVehicleList(VEHICLE_LIST... vehicle_listsc);

    @Query("SELECT NULL AS PK_ID,CODE,NAME,NULL AS PARENT_ID FROM VEHICLE_LIST")
    List<SEARCH_TYPE> getVehicleList();

    @Query("DELETE FROM DOCTYPE")
    void deleteAllUnitList();

    @Insert(onConflict = REPLACE)
    void insertUnitList(DocType... doctypes);

    @Query("SELECT CODE, CODE AS NAME,NULL AS BIN_ID FROM DOCTYPE  ")
    List<DocType> getAllUnits();

    @Insert(onConflict = REPLACE)
    void insertPUR_RET_ITEMS(PUR_RET_ITEMS... pur_ret_items);

    @Query("DELETE FROM REASON_TYPE")
    void deleteAllReasonTypes();

    @Insert(onConflict = REPLACE)
    void insertReasonTypes(REASON_TYPE... reason_types);

    @Query("SELECT CODE, NAME FROM REASON_TYPE")
    List<REASON_TYPE> getAllReasonTypes();

    @Insert()
    long insertPUR_RET_ITEMS_RECEIPT(PUR_RET_ITEMS_RECEIPT pur_ret_items_receipt);

    @Query("SELECT * FROM PUR_RET_ITEMS_RECEIPT WHERE REF_NO =:refNo ")
    PUR_RET_ITEMS_RECEIPT GetFirstAddedItemBatch(String refNo);

    @Query("SELECT SLNO,ITCODE,IUNITS,BATCHNO,MANFDATE,EXPDATE,CNTQTY,SCANTIME,REF_NO,REASON,RECNO,ERPBATCH FROM PUR_RET_ITEMS_RECEIPT WHERE REF_NO =:refNo")
    List<PUR_RET_ITEMS_RECEIPT> GetPUR_RET_ITEMS_RECEIPT_upload(String refNo);

    @Query("DELETE FROM PUR_RET_ITEMS_RECEIPT  WHERE REF_NO=  :refNo")
    void deletePUR_RET_ITEMS_RECEIPT(String refNo);

    @Query("SELECT * FROM SALE_RET_HDR WHERE ORDNO = :ordNo AND RECEIPT_NO <= 0")
    SALE_RET_HDR getPendingSALE_RET_HDRByDocNo(String ordNo);

    @Query("DELETE FROM SALE_RET_DOC_HDR WHERE ORDNO=:ordNo AND ORTYP=:orTyp")
    void deleteSALE_RET_DOC_HDR(String ordNo, String orTyp);

    @Insert()
    void insertSALE_RET_DOC_HDR(SALE_RET_DOC_HDR... sale_ret_doc_hdrs);

    @Query("DELETE FROM SALE_RET_DOC_DET WHERE ORDNO=:ordNo AND ORTYP=:orTyp")
    void deleteSALE_RET_DOC_DET(String ordNo, String orTyp);

    @Insert()
    void insertSALE_RET_DOC_DET(SALE_RET_DOC_DET... sale_ret_doc_dets);

    @Query("SELECT HDR.* FROM SALE_RET_DOC_HDR HDR " +
            " WHERE ORDNO=:ordNo AND ORTYP=:orTyp")
    SALE_RET_DOC_HDR getSALE_RET_DOC_HDR(String ordNo, String orTyp);

    @Query("SELECT P.DET_ID, P.ORDNO, P.ORTYP, P.ITCODE, P.ITDESC, P.PACKING, P.ITUNIT, " +
            "P.FACTOR, P.CARTON, P.ITQTY, IFNULL(GR.RCQTY, 0) AS RCQTY, P.CNQTY, P.TOTQTY, " +
            "P.SLNO, P.SUP_BCODE, P.BATCHFLG, P.ITRTE,P.TOL_QTY,P.IS_QTY_VALIDATE,P.BATCHNO,P.MANFDATE,P.EXPDATE,P.ERPBATCH " +
            "FROM SALE_RET_DOC_DET P " +
            "LEFT OUTER JOIN (SELECT D.ITCODE, D.SLNO, SUM(D.RCQTY) AS RCQTY " +
            "FROM SALE_RET_HDR H INNER JOIN SALE_RET_DET D ON D.HDR_ID = H.HDR_ID " +
            "WHERE H.RECEIPT_NO = 0 AND H.ORDNO = :ordNo AND H.ORTYP = :orTyp " +
            "GROUP BY D.ITCODE, D.SLNO) GR ON P.ITCODE = GR.ITCODE AND P.SLNO = GR.SLNO " +
            "WHERE P.ORDNO=:ordNo AND P.ORTYP=:orTyp")
    List<SALE_RET_DOC_DET> getSALE_RET_DOC_DET(String ordNo, String orTyp);

    @Query("SELECT * FROM SALE_RET_HDR WHERE HDR_ID=:pk_id")
    SALE_RET_HDR getSALE_RET_HDR(long pk_id);

    @Insert(onConflict = REPLACE)
    long insertSALE_RET_HDR(SALE_RET_HDR sale_ret_hdr);

    @Query("SELECT P.DET_ID, P.ORDNO, P.ORTYP, P.ITCODE, P.ITDESC, P.PACKING, P.ITUNIT, " +
            "P.FACTOR, P.CARTON, P.ITQTY, IFNULL(GR.RCQTY, 0) AS RCQTY, P.CNQTY, P.TOTQTY, " +
            "P.SLNO, P.SUP_BCODE, P.BATCHFLG, P.ITRTE,P.TOL_QTY,P.IS_QTY_VALIDATE,P.BATCHNO,P.MANFDATE,P.EXPDATE,P.ERPBATCH " +
            "FROM SALE_RET_DOC_DET P " +
            "LEFT OUTER JOIN (SELECT D.ITCODE, D.SLNO, SUM(D.RCQTY) AS RCQTY " +
            "FROM SALE_RET_HDR H INNER JOIN SALE_RET_DET D ON D.HDR_ID = H.HDR_ID " +
            "WHERE H.RECEIPT_NO = 0 AND H.ORDNO = :ordNo AND H.ORTYP = :orTyp " +
            "GROUP BY D.ITCODE, D.SLNO) GR ON P.ITCODE = GR.ITCODE AND P.SLNO = GR.SLNO " +
            "WHERE P.ORDNO=:ordNo AND P.ORTYP=:orTyp AND P.SLNO = :slNo")
    SALE_RET_DOC_DET getSALE_RET_DOC_DET(String ordNo, String orTyp, long slNo);

    @Insert(onConflict = REPLACE)
    void insertSALE_RET_DET(SALE_RET_DET sale_ret_det);

    @Query("SELECT * FROM SALE_RET_DET WHERE DET_ID = :det_id")
    SALE_RET_DET getSALE_RET_DETByDet_ID(long det_id);

    @Query("DELETE FROM SALE_RET_DET WHERE DET_ID = :det_id")
    void deleteSALE_RET_DETByDet_ID(long det_id);

    @Query("SELECT * FROM SALE_RET_DET WHERE HDR_ID = :pk_id AND SLNO = :sl_no")
    List<SALE_RET_DET> getSALE_RET_DET(long pk_id, long sl_no);

    @Query("SELECT * FROM SALE_RET_DET WHERE HDR_ID = :pk_id")
    List<SALE_RET_DET> getSALE_RET_DETByHDR_ID(long pk_id);

    @Query("DELETE FROM SALE_RET_HDR WHERE HDR_ID=:hdr_id")
    void deleteSALE_RET_HDR(long hdr_id);

    @Query("SELECT ITCODE, ITUNIT, BATCHNO, MANFDATE, EXPDATE, " +
            "RCQTY QTY, FACTOR, REC_TYPE, SLNO, SEQ_NO,REASON,ERPBATCH " +
            "FROM SALE_RET_DET WHERE HDR_ID=:pk_id")
    List<SR_LINES_TYP> getSRLinesForUpload(long pk_id);

    @Query("UPDATE SALE_RET_HDR SET IS_UPLOADED = 1, RECEIPT_NO = :receipt_no WHERE HDR_ID = :pk_id")
    void updateSALE_RET_HDR(long pk_id, long receipt_no);

    @Query("DELETE FROM SALE_RET_HDR  WHERE ORDNO=  :ordNo")
    void deleteSALE_RET_HDR(String ordNo);

    @Query("DELETE FROM SALE_RET_DET  WHERE HDR_ID =:hdrid")
    void deleteSALE_RET_DET(long hdrid);

    @Query("DELETE FROM SALE_RET_DOC_HDR  WHERE ORDNO=  :ordNo")
    void deleteSALE_RET_DOC_HDR(String ordNo);

    @Query("DELETE FROM SALE_RET_DOC_DET WHERE ORDNO=:ordNo ")
    void deleteSALE_RET_DOC_DET(String ordNo);

    @Query("SELECT * FROM SALE_RET_DOC_DET WHERE ITCODE = :itcode AND ITUNIT =:unit")
    SALE_RET_DOC_DET getSALE_RET_DOC_DETByITCODEandUNIT(String itcode,String unit);

    @Query("DELETE FROM CONFIRM_DOC_HDR WHERE ORDNO=:ordNo AND ORTYP=:orTyp")
    void deleteCONFIRM_DOC_HDR(String ordNo, String orTyp);

    @Insert()
    void insertCONFIRM_DOC_HDR(CONFIRM_DOC_HDR... confirm_doc_hdrs);

    @Query("DELETE FROM CONFIRM_DOC_DET WHERE ORDNO=:ordNo AND ORTYP=:orTyp")
    void deleteCONFIRM_DOC_DET(String ordNo, String orTyp);

    @Insert()
    void insertCONFIRM_DOC_DET(CONFIRM_DOC_DET... confirm_doc_dets);

    @Query("SELECT HDR.* FROM CONFIRM_DOC_HDR HDR " +
            " WHERE ORDNO=:ordNo AND ORTYP=:orTyp")
    CONFIRM_DOC_HDR getCONFIRM_DOC_HDR(String ordNo, String orTyp);

    @Query("SELECT P.DET_ID, P.ORDNO, P.ORTYP, P.ITCODE, P.ITDESC, P.PACKING, P.ITUNIT, " +
            "P.FACTOR, P.CARTON, P.ITQTY, 0 AS RCQTY, P.CNQTY, P.TOTQTY, " +
            "P.SLNO, P.SUP_BCODE, P.BATCHFLG, P.ITRTE,P.TOL_QTY,P.IS_QTY_VALIDATE,P.BATCHNO,P.MANFDATE,P.EXPDATE,P.ERPBATCH " +
            "FROM CONFIRM_DOC_DET P WHERE P.ORDNO=:ordNo AND P.ORTYP=:orTyp")
    List<CONFIRM_DOC_DET> getCONFIRM_DOC_DET(String ordNo, String orTyp);


    //Adnatco Starts here
    @Insert(onConflict = REPLACE)
    void insertSLOC_LIST_SPR(SLOC_LIST_SPR... sloc_list_sprs);

    @Query("DELETE FROM SLOC_LIST_SPR ")
    void deleteAllSLOC_LIST_SPR();

    @Query("SELECT BIN_ID AS CODE, LGOBE AS NAME,LGORT AS ID,BIN_ID FROM SLOC_LIST_SPR")
    List<DocType> getAllSLOC_LIST_SPR();

    @Insert(onConflict = REPLACE)
    void insertAD_PO_ITEMS_RECIEPT(AD_PO_ITEMS_RECIEPT ad_po_items_reciept);

    @Query("SELECT * FROM AD_PO_ITEMS_RECIEPT WHERE PO_NUMBER=:po_num")
    List<AD_PO_ITEMS_RECIEPT> getAD_PO_ITEMS_RECIEPTForUpload(String po_num);

    @Query("DELETE FROM AD_PO_ITEMS_RECIEPT WHERE SLNO = :sl_no AND PO_NUMBER =:po_number and PO_ITEM =:item ")
    void deleteAD_PO_ITEMS_RECIEPTByDet_ID(long sl_no,String po_number,String item);

    @Query("SELECT * FROM AD_PO_ITEMS_RECIEPT WHERE PO_NUMBER=:po_num and PO_ITEM =:item")
    List<AD_PO_ITEMS_RECIEPT> getAD_PO_ITEMS_RECIEPT(String po_num,String item);

    @Query("SELECT * FROM AD_PO_ITEMS_RECIEPT WHERE PO_NUMBER=:po_num ")
    List<AD_PO_ITEMS_RECIEPT> getAD_PO_ITEMS_RECIEPT(String po_num);

    @Query("DELETE FROM AD_PO_ITEMS_RECIEPT WHERE PO_NUMBER=:poNo ")
    void deleteAD_PO_ITEMS_RECIEPTdoc(String poNo);

    @Query("SELECT P.PO_NUMBER,P.PO_ITEM, P.MATERIAL,P.SHORT_TEXT,P.QUANTITY,P.ITEM_ID,P.BIN_ID,R.RCQTY,P.MATL_DESC,P.PO_UNIT," +
            "DELIV_QTY,PO_PR_QNT,PDTRECPT,BAL_TO_RCV,ENTRY_QNT,IS_DELETED,DEFAULTLOC FROM AD_PO_ITEMS_SPR P "+
            "LEFT OUTER JOIN (SELECT PO_NUMBER,PO_ITEM,MATERIAL,SUM(QUANTITY) AS RCQTY FROM AD_PO_ITEMS_RECIEPT GROUP BY PO_NUMBER,PO_ITEM,MATERIAL) R" +
            " ON R.PO_NUMBER = P.PO_NUMBER AND R.PO_ITEM = P.PO_ITEM AND R.MATERIAL = P.MATERIAL "+
            "WHERE P.PO_NUMBER=:ordNo ")
    List<AD_PO_ITEMS_SPR> getAD_PO_ITEMS_SPR(String ordNo);


    @Query("SELECT P.PO_NUMBER,P.PO_ITEM, P.MATERIAL,P.SHORT_TEXT,P.QUANTITY,P.ITEM_ID,P.BIN_ID,R.RCQTY,P.MATL_DESC,P.PO_UNIT," +
            "DELIV_QTY,PO_PR_QNT,PDTRECPT,BAL_TO_RCV,ENTRY_QNT,IS_DELETED,DEFAULTLOC,PO_UNIT FROM AD_PO_ITEMS_SPR P "+
            "LEFT OUTER JOIN (SELECT PO_NUMBER,PO_ITEM,MATERIAL,SUM(QUANTITY) AS RCQTY FROM AD_PO_ITEMS_RECIEPT GROUP BY PO_NUMBER,PO_ITEM,MATERIAL) R" +
            " ON R.PO_NUMBER = P.PO_NUMBER AND R.PO_ITEM = P.PO_ITEM AND R.MATERIAL = P.MATERIAL "+
            "WHERE P.PO_NUMBER=:ordNo AND P.PO_ITEM =:poItem ")
    AD_PO_ITEMS_SPR getAD_PO_ITEMS_SPR_ONE(String ordNo,String poItem);

    @Query("SELECT P.PO_NUMBER,P.PO_ITEM, P.MATERIAL,P.SHORT_TEXT,P.QUANTITY,P.ITEM_ID,P.BIN_ID,R.RCQTY,P.MATL_DESC," +
            "DELIV_QTY,PO_PR_QNT,PDTRECPT,BAL_TO_RCV,ENTRY_QNT,IS_DELETED,DEFAULTLOC,PO_UNIT FROM AD_PO_ITEMS_SPR P "+
            "LEFT OUTER JOIN (SELECT PO_NUMBER,PO_ITEM,MATERIAL,SUM(QUANTITY) AS RCQTY FROM AD_PO_ITEMS_RECIEPT GROUP BY PO_NUMBER,PO_ITEM,MATERIAL) R" +
            " ON R.PO_NUMBER = P.PO_NUMBER AND R.PO_ITEM = P.PO_ITEM AND R.MATERIAL = P.MATERIAL "+
            "WHERE P.PO_NUMBER=:ordNo AND P.MATERIAL =:materialNo ")
    AD_PO_ITEMS_SPR getAD_PO_ITEMS_SPR_ONE_MATERIAL(String ordNo,String materialNo);

    @Query("SELECT * FROM AD_PO_HDR WHERE PO_NUMBER = :poNum")
    AD_PO_HDR getAD_PO_HDRByDocNo(String poNum);

    @Query("DELETE FROM WO_HDR_SPR WHERE WO_NUMBER=:ordNo")
    void deleteWO_HDR_SPR(String ordNo);

    @Insert(onConflict = REPLACE)
    void insertWO_HDR_SPR(WO_HDR_SPR... po_hdr);

    @Query("DELETE FROM RE_WO_HDR_SPR WHERE WO_NUMBER=:ordNo")
    void deleteRE_WO_HDR_SPR(String ordNo);

    @Insert(onConflict = REPLACE)
    void insertRE_WO_HDR_SPR(RE_WO_HDR_SPR... po_hdr);

    @Query("DELETE FROM WO_ITEM_SPR WHERE ORDERID =:ordNo  ")
    void deleteWO_ITEM_SPR(String ordNo);

    @Insert()
    void insertWO_ITEM_SPR(WO_ITEM_SPR... wo_item_sprs);

    @Query("DELETE FROM RE_WO_ITEM_SPR WHERE ORDERID =:ordNo  ")
    void deleteRE_WO_ITEM_SPR(String ordNo);

    @Insert()
    void insertRE_WO_ITEM_SPR(RE_WO_ITEM_SPR... wo_item_sprs);

    @Query("SELECT * FROM WO_HDR_SPR WHERE WO_NUMBER = :poNum")
    WO_HDR_SPR getWO_HDR_SPRByDocNo(String poNum);

    @Query("SELECT * FROM RE_WO_HDR_SPR WHERE WO_NUMBER = :poNum")
    RE_WO_HDR_SPR getRE_WO_HDR_SPRByDocNo(String poNum);

    @Query("SELECT P.ITEM_ID,P.ORDERID , P.MATERIAL,P.ITEM_NUMBER,P.REQUIREMENT_QUANTITY,P.REQUIREMENT_QUANTITY_UNIT,P.REQUIREMENT_QUANTITY_UNIT_ISO,R.RCQTY," +
            "P.MOVE_TYPE,COMMITED_QUAN,P.BIN_ID,P.MATL_DESC,P.DEFAULTLOC FROM WO_ITEM_SPR P " +
            "LEFT OUTER JOIN (SELECT WO_NO,MATERIAL,SUM(ENTRY_QNT) AS RCQTY,MOVE_TYPE FROM WO_ITEMS_RECIEPT GROUP BY WO_NO,MATERIAL,MOVE_TYPE) R" +
            " ON R.WO_NO = P.ORDERID AND R.MATERIAL = P.MATERIAL AND R.MOVE_TYPE = P.MOVE_TYPE WHERE P.ORDERID=:ordNo AND P.MOVE_TYPE = :moveType ")
    List<WO_ITEM_SPR> getWO_ITEM_SPR(String ordNo,String moveType);

    @Query("SELECT P.ITEM_ID,P.ORDERID , P.MATERIAL,P.ITEM_NUMBER,P.REQUIREMENT_QUANTITY,P.REQUIREMENT_QUANTITY_UNIT,P.REQUIREMENT_QUANTITY_UNIT_ISO,R.RCQTY," +
            "MOVE_TYPE,COMMITED_QUAN,P.BIN_ID,P.MATL_DESC,P.DEFAULTLOC FROM RE_WO_ITEM_SPR P " +
            "LEFT OUTER JOIN (SELECT WO_NO,MATERIAL,SUM(ENTRY_QNT) AS RCQTY FROM RE_WO_ITEMS_RECIEPT GROUP BY WO_NO,MATERIAL) R" +
            " ON R.WO_NO = P.ORDERID AND R.MATERIAL = P.MATERIAL WHERE P.ORDERID=:ordNo ")
    List<RE_WO_ITEM_SPR> getRE_WO_ITEM_SPR(String ordNo);

    @Insert(onConflict = REPLACE)
    void insertWO_ITEMS_RECIEPT(WO_ITEMS_RECIEPT wo_items_reciept);

    @Insert(onConflict = REPLACE)
    void insertRE_WO_ITEMS_RECIEPT(RE_WO_ITEMS_RECIEPT wo_items_reciept);

    @Query("SELECT P.ITEM_ID,P.ORDERID , P.MATERIAL,P.ITEM_NUMBER,P.REQUIREMENT_QUANTITY,P.REQUIREMENT_QUANTITY_UNIT,P.REQUIREMENT_QUANTITY_UNIT_ISO,R.RCQTY," +
            "MOVE_TYPE,COMMITED_QUAN,P.BIN_ID,P.MATL_DESC,P.DEFAULTLOC FROM WO_ITEM_SPR P "+
            "LEFT OUTER JOIN (SELECT WO_NO,MATERIAL,SUM(ENTRY_QNT) AS RCQTY FROM WO_ITEMS_RECIEPT GROUP BY WO_NO,MATERIAL) R" +
            " ON R.WO_NO = P.ORDERID AND R.MATERIAL = P.MATERIAL WHERE P.ORDERID=:ordNo AND P.MATERIAL =:material AND MOVE_TYPE = :moveType")
    WO_ITEM_SPR getWO_ITEM_SPR_ONE(String ordNo,String material,String moveType);

    @Query("SELECT P.ITEM_ID,P.ORDERID , P.MATERIAL,P.ITEM_NUMBER,P.REQUIREMENT_QUANTITY,P.REQUIREMENT_QUANTITY_UNIT,P.REQUIREMENT_QUANTITY_UNIT_ISO,R.RCQTY," +
            "MOVE_TYPE,COMMITED_QUAN,P.BIN_ID,P.MATL_DESC,P.DEFAULTLOC FROM RE_WO_ITEM_SPR P "+
            "LEFT OUTER JOIN (SELECT WO_NO,MATERIAL,SUM(ENTRY_QNT) AS RCQTY FROM RE_WO_ITEMS_RECIEPT GROUP BY WO_NO,MATERIAL) R" +
            " ON R.WO_NO = P.ORDERID AND R.MATERIAL = P.MATERIAL WHERE P.ORDERID=:ordNo AND P.MATERIAL =:material ")
    RE_WO_ITEM_SPR getRE_WO_ITEM_SPR_ONE(String ordNo,String material);

    @Query("SELECT * FROM WO_ITEMS_RECIEPT WHERE WO_NO=:po_num and MATERIAL =:item AND MOVE_TYPE = :moveType")
    List<WO_ITEMS_RECIEPT> getWO_ITEMS_RECIEPT(String po_num,String item,String moveType);

    @Query("SELECT * FROM RE_WO_ITEMS_RECIEPT WHERE WO_NO=:po_num and MATERIAL =:item")
    List<RE_WO_ITEMS_RECIEPT> getRE_WO_ITEMS_RECIEPT(String po_num,String item);

    @Query("DELETE FROM WO_ITEMS_RECIEPT WHERE SLNO = :sl_no ")
    void deleteWO_ITEMS_RECIEPTByDet_ID(long sl_no);

    @Query("DELETE FROM RE_WO_ITEMS_RECIEPT WHERE SLNO = :sl_no ")
    void deleteRE_WO_ITEMS_RECIEPTByDet_ID(long sl_no);

    @Query("SELECT DISTINCT PO_NUMBER PO_NO FROM AD_PO_HDR")
    List<CLS_PO_NUMBER> getAllPOForUpload();

    @Query("SELECT * FROM WO_ITEMS_RECIEPT WHERE WO_NO=:po_num AND MOVE_TYPE = :moveType")
    List<WO_ITEMS_RECIEPT> getWO_ITEMS_RECIEPTForUpload(String po_num,String moveType);

    @Query("SELECT * FROM RE_WO_ITEMS_RECIEPT WHERE WO_NO=:po_num")
    List<RE_WO_ITEMS_RECIEPT> getRE_WO_ITEMS_RECIEPTForUpload(String po_num);

    @Query("DELETE FROM WO_ITEMS_RECIEPT WHERE WO_NO=:poNo AND MOVE_TYPE  = :moveType")
    void deleteWO_ITEMS_RECIEPTdoc(String poNo,String moveType);

    @Query("DELETE FROM RE_WO_ITEMS_RECIEPT WHERE WO_NO=:poNo ")
    void deleteRE_WO_ITEMS_RECIEPTdoc(String poNo);

    @Query("SELECT DISTINCT WH.WO_NUMBER PO_NO FROM WO_HDR_SPR WH,WO_ITEMS_RECIEPT WI WHERE WI.WO_NO = WH.WO_NUMBER  AND WI.MOVE_TYPE = :moveType")
    List<CLS_PO_NUMBER> getAllWOForUpload(String moveType);

    @Query("SELECT DISTINCT WH.WO_NUMBER PO_NO FROM RE_WO_HDR_SPR WH,RE_WO_ITEMS_RECIEPT WI WHERE WI.WO_NO = WH.WO_NUMBER")
    List<CLS_PO_NUMBER> getAllRWOForUpload();

    @Insert(onConflict = REPLACE)
    void insertPLANT_LIST(PLANT_LIST... plant_lists);

    @Query("DELETE FROM PLANT_LIST ")
    void deleteAllPLANT_LIST();

    @Insert(onConflict = REPLACE)
    void insertRET_REASON_LIST(RET_REASON_LIST... ret_reason_lists);

    @Query("DELETE FROM RET_REASON_LIST ")
    void deleteAllRET_REASON_LIST();

    @Insert(onConflict = REPLACE)
    void insertMATERIAL_LIST(MATERIAL_LIST... ret_reason_lists);

    @Query("DELETE FROM MATERIAL_LIST ")
    void deleteAllMATERIAL_LIST();

    @Insert(onConflict = REPLACE)
    void insertSTK_TRN_PRJ_WHR_RECIEPT(STK_TRN_PRJ_WHR_RECIEPT221 stk_trn_prj_whr_reciept221);

    @Insert(onConflict = REPLACE)
    void insertSTK_TRN_PRJ_WHR_RECIEPT415Q(STK_TRN_PRJ_WHR_RECIEPT415Q stk_trn_prj_whr_reciept415Q);

    @Insert(onConflict = REPLACE)
    void insertSTK_TRN_PRJ_WHR_RECIEPT411Q(STK_TRN_PRJ_WHR_RECIEPT411Q stk_trn_prj_whr_reciept411q);

    @Insert(onConflict = REPLACE)
    void insertSTK_TRN_PRJ_WHR_RECIEPT222(STK_TRN_PRJ_WHR_RECIEPT222 stk_trn_prj_whr_reciept222);

    @Insert(onConflict = REPLACE)
    void insertSTK_TRN_PRJ_WHR_RECIEPT222Q(STK_TRN_PRJ_WHR_RECIEPT222Q stk_trn_prj_whr_reciept222q);

    @Insert(onConflict = REPLACE)
    void insertSTK_TRN_PRJ_WHR_RECIEPT221Q(STK_TRN_PRJ_WHR_RECIEPT221Q stk_trn_prj_whr_reciept221q);

    @Query("SELECT * FROM STK_TRN_PRJ_WHR_RECIEPT221 ")
    List<STK_TRN_PRJ_WHR_RECIEPT221> getSTK_TRN_PRJ_WHR_RECIEPT();

    @Query("SELECT * FROM STK_TRN_PRJ_WHR_RECIEPT415Q ")
    List<STK_TRN_PRJ_WHR_RECIEPT415Q> getSTK_TRN_PRJ_WHR_RECIEPT415Q();

    @Query("SELECT * FROM STK_TRN_PRJ_WHR_RECIEPT411Q ")
    List<STK_TRN_PRJ_WHR_RECIEPT411Q> getSTK_TRN_PRJ_WHR_RECIEPT411Q();

    @Query("SELECT * FROM STK_TRN_PRJ_WHR_RECIEPT222 ")
    List<STK_TRN_PRJ_WHR_RECIEPT222> getSTK_TRN_PRJ_WHR_RECIEPT222();

    @Query("SELECT * FROM STK_TRN_PRJ_WHR_RECIEPT222Q ")
    List<STK_TRN_PRJ_WHR_RECIEPT222Q> getSTK_TRN_PRJ_WHR_RECIEPT222Q();

    @Query("SELECT * FROM STK_TRN_PRJ_WHR_RECIEPT221Q ")
    List<STK_TRN_PRJ_WHR_RECIEPT221Q> getSTK_TRN_PRJ_WHR_RECIEPT221Q();

    @Query("DELETE FROM STK_TRN_PRJ_WHR_RECIEPT221 WHERE SLNO = :sl_no ")
    void deleteSTK_TRN_PRJ_WHR_RECIEPTByDet_ID(long sl_no);

    @Query("DELETE FROM STK_TRN_PRJ_WHR_RECIEPT415Q WHERE SLNO = :sl_no ")
    void deleteSTK_TRN_PRJ_WHR_RECIEPT415QByDet_ID(long sl_no);

    @Query("DELETE FROM STK_TRN_PRJ_WHR_RECIEPT411Q WHERE SLNO = :sl_no ")
    void deleteSTK_TRN_PRJ_WHR_RECIEPT411QByDet_ID(long sl_no);

    @Query("DELETE FROM STK_TRN_PRJ_WHR_RECIEPT222 WHERE SLNO = :sl_no ")
    void deleteSTK_TRN_PRJ_WHR_RECIEPT222ByDet_ID(long sl_no);

    @Query("DELETE FROM STK_TRN_PRJ_WHR_RECIEPT222Q WHERE SLNO = :sl_no ")
    void deleteSTK_TRN_PRJ_WHR_RECIEPT222QByDet_ID(long sl_no);

    @Query("DELETE FROM STK_TRN_PRJ_WHR_RECIEPT221Q WHERE SLNO = :sl_no ")
    void deleteSTK_TRN_PRJ_WHR_RECIEPTQByDet_ID(long sl_no);

    @Query("SELECT * FROM MATERIAL_LIST WHERE MATERIAL=:material")
    MATERIAL_LIST getMATERIAL_LIST_ITEM(String material);

    @Query("SELECT DISTINCT MATERIAL AS PO_NO FROM STK_TRN_PRJ_WHR_RECIEPT221")
    List<CLS_PO_NUMBER> getAllSTK_TRNForUpload();

    @Query("SELECT * FROM STK_TRN_PRJ_WHR_RECIEPT221 WHERE MATERIAL =:material")
    List<STK_TRN_PRJ_WHR_RECIEPT221> getSTK_TRN_PRJ_WHR_RECIEPT_ITEM(String material);

    @Query("DELETE FROM STK_TRN_PRJ_WHR_RECIEPT221 WHERE MATERIAL=:material ")
    void deleteSTK_TRN_PRJ_WHR_RECIEPT(String material);

    @Insert(onConflict = REPLACE)
    void insertSTK_TRN_PRJ_WHR__REV_RECIEPT(STK_TRN_PRJ_WHR__REV_RECIEPT stk_trn_prj_whr_reciept);

    @Query("SELECT * FROM STK_TRN_PRJ_WHR__REV_RECIEPT ")
    List<STK_TRN_PRJ_WHR__REV_RECIEPT> getSTK_TRN_PRJ_WHR__REV_RECIEPT();

    @Query("DELETE FROM STK_TRN_PRJ_WHR__REV_RECIEPT WHERE SLNO = :sl_no ")
    void deleteSTK_TRN_PRJ_WHR__REV_RECIEPTByDet_ID(long sl_no);

    @Insert(onConflict = REPLACE)
    void insertSTK_TRN_PRJ_PRJ_RECIEPT(STK_TRN_PRJ_PRJ_RECIEPT stk_trn_prj_whr_reciept);

    @Query("SELECT * FROM STK_TRN_PRJ_PRJ_RECIEPT ")
    List<STK_TRN_PRJ_PRJ_RECIEPT> getSTK_TRN_PRJ_PRJ_RECIEPT();

    @Query("DELETE FROM STK_TRN_PRJ_PRJ_RECIEPT WHERE SLNO = :sl_no ")
    void deleteSTK_TRN_PRJ_PRJ_RECIEPTByDet_ID(long sl_no);

    @Query("SELECT DISTINCT MATERIAL AS PO_NO FROM STK_TRN_PRJ_WHR_RECIEPT221Q")
    List<CLS_PO_NUMBER> getAllSTK_TRN221QForUpload();

    @Query("SELECT DISTINCT MATERIAL AS PO_NO FROM STK_TRN_PRJ_WHR_RECIEPT222")
    List<CLS_PO_NUMBER> getAllSTK_TRN222ForUpload();

    @Query("SELECT DISTINCT MATERIAL AS PO_NO FROM STK_TRN_PRJ_WHR_RECIEPT222Q")
    List<CLS_PO_NUMBER> getAllSTK_TRN222QForUpload();

    @Query("SELECT DISTINCT MATERIAL AS PO_NO FROM STK_TRN_PRJ_WHR_RECIEPT411Q")
    List<CLS_PO_NUMBER> getAllSTK_TRN411QForUpload();

    @Query("SELECT DISTINCT MATERIAL AS PO_NO FROM STK_TRN_PRJ_WHR_RECIEPT415Q")
    List<CLS_PO_NUMBER> getAllSTK_TRN415QForUpload();

    @Query("SELECT * FROM STK_TRN_PRJ_WHR_RECIEPT221Q WHERE MATERIAL =:material")
    List<STK_TRN_PRJ_WHR_RECIEPT221Q> getSTK_TRN_PRJ_WHR_RECIEPT_ITEM221Q(String material);

    @Query("SELECT * FROM STK_TRN_PRJ_WHR_RECIEPT222 WHERE MATERIAL =:material")
    List<STK_TRN_PRJ_WHR_RECIEPT222> getSTK_TRN_PRJ_WHR_RECIEPT_ITEM222(String material);

    @Query("SELECT * FROM STK_TRN_PRJ_WHR_RECIEPT222Q WHERE MATERIAL =:material")
    List<STK_TRN_PRJ_WHR_RECIEPT222Q> getSTK_TRN_PRJ_WHR_RECIEPT_ITEM222Q(String material);

    @Query("SELECT * FROM STK_TRN_PRJ_WHR_RECIEPT411Q WHERE MATERIAL =:material")
    List<STK_TRN_PRJ_WHR_RECIEPT411Q> getSTK_TRN_PRJ_WHR_RECIEPT_ITEM411Q(String material);

    @Query("SELECT * FROM STK_TRN_PRJ_WHR_RECIEPT415Q WHERE MATERIAL =:material")
    List<STK_TRN_PRJ_WHR_RECIEPT415Q> getSTK_TRN_PRJ_WHR_RECIEPT_ITEM415Q(String material);

    @Query("DELETE FROM STK_TRN_PRJ_WHR_RECIEPT221Q WHERE MATERIAL=:material ")
    void deleteSTK_TRN_PRJ_WHR_RECIEPT221Q(String material);

    @Query("DELETE FROM STK_TRN_PRJ_WHR_RECIEPT222 WHERE MATERIAL=:material ")
    void deleteSTK_TRN_PRJ_WHR_RECIEPT222(String material);

    @Query("DELETE FROM STK_TRN_PRJ_WHR_RECIEPT222Q WHERE MATERIAL=:material ")
    void deleteSTK_TRN_PRJ_WHR_RECIEPT222Q(String material);

    @Query("DELETE FROM STK_TRN_PRJ_WHR_RECIEPT411Q WHERE MATERIAL=:material ")
    void deleteSTK_TRN_PRJ_WHR_RECIEPT411Q(String material);

    @Query("DELETE FROM STK_TRN_PRJ_WHR_RECIEPT415Q WHERE MATERIAL=:material ")
    void deleteSTK_TRN_PRJ_WHR_RECIEPT415Q(String material);

    @Query("DELETE FROM STK_COUNT_HDR WHERE DOC_NO=:ordNo")
    void deleteSTK_COUNT_HDR(String ordNo);

    @Insert(onConflict = REPLACE)
    void insertSTK_COUNT_HDR(STK_COUNT_HDR... stk_count_hdrs);

    @Query("DELETE FROM STK_COUNT_DET WHERE HEAD_ID =:id  ")
    void deleteSTK_COUNT_DET(long id);

    @Insert()
    void insertSTK_COUNT_DET(STK_COUNT_DET... stk_count_dets);

    @Query("SELECT P.MATERIAL,P.MATL_DESC,P.BATCH,P.WBS_ELEMENT,P.SLOC_NAME,P.ITEM,P.HEAD_ID,R.RCQTY,P.BIN_ID,P.DOC_NO  FROM STK_COUNT_DET P " +
            "LEFT OUTER JOIN (SELECT ITEM,SUM(ENTRY_QNT) AS RCQTY,MATERIAL AS RCQTY FROM STK_COUNT_ITEMS_RECIEPT GROUP BY ITEM,MATERIAL ) R" +
            " ON R.ITEM = P.ITEM WHERE P.HEAD_ID =:id")
    List<STK_COUNT_DET> getSTK_COUNT_DET(long id);

    @Query("SELECT * FROM STK_COUNT_DET WHERE MATERIAL=:material")
    STK_COUNT_DET getSTK_COUNT_DET_ITEM(String material);

    @Insert()
    void insertSTK_COUNT_ITEMS_RECIEPT(STK_COUNT_ITEMS_RECIEPT stk_count_items_reciept);

    @Query("SELECT * FROM STK_COUNT_ITEMS_RECIEPT WHERE HEAD_ID=:id")
    List<STK_COUNT_ITEMS_RECIEPT> getSTK_COUNT_ITEMS_RECIEPT(long id);

    @Query("SELECT * FROM STK_COUNT_ITEMS_RECIEPT WHERE DOC_NO=:docNo")
    List<STK_COUNT_ITEMS_RECIEPT> getSTK_COUNT_ITEMS_RECIEPT(String docNo);

    @Query("DELETE FROM STK_COUNT_ITEMS_RECIEPT WHERE SLNO = :sl_no ")
    void deleteSTK_COUNT_ITEMS_RECIEPTByDet_ID(long sl_no);

    @Query("SELECT * FROM STK_COUNT_DET WHERE MATERIAL = :material  AND  BATCH =:batch AND IFNULL(WBS_ELEMENT,'*') = :wbs")
    STK_COUNT_DET getMaterialItemCode(String material,String batch,String wbs);

    @Query("SELECT DISTINCT DOC_NO PO_NO FROM STK_COUNT_ITEMS_RECIEPT")
    List<CLS_PO_NUMBER> getAllSTKCOUNTForUpload();

    @Query("DELETE FROM STK_COUNT_DET WHERE DOC_NO =:docNo  ")
    void deleteSTK_COUNT_DET(String docNo);

    @Query("DELETE FROM STK_COUNT_ITEMS_RECIEPT WHERE DOC_NO = :docNo ")
    void deleteSTK_COUNT_ITEMS_RECIEPTByDoc(String docNo);

    @Query("SELECT * FROM STK_COUNT_HDR WHERE DOC_NO = :docNo")
    STK_COUNT_HDR getSTK_COUNT_HDR(String docNo);

    @Query("SELECT SUM(ENTRY_QNT) AS RCQTY FROM WO_ITEMS_RECIEPT   WHERE WO_NO =:wo_no AND MATERIAL = :material")
    float getWoMaterialTotQty(String wo_no,String material);

}

