package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "RET_REASON_LIST")
public class RET_REASON_LIST implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    @NotNull
    public int ID;

    @ColumnInfo(name = "GRUND")
    @NotNull
    public String GRUND;

    @ColumnInfo(name = "GRTXT")
    public String GRTXT;

    @ColumnInfo(name = "SAP_USER")
    public String SAP_USER;

    @ColumnInfo(name = "DOWNL_DT")
    public String DOWNL_DT;

    @ColumnInfo(name = "USER_ID")
    public String USER_ID;
}
