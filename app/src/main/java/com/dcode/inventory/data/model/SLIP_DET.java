package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "SLIP_DET")
public class SLIP_DET {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "DET_ID")
    public long DET_ID;

    @ColumnInfo(name = "HDR_ID")
    public long HDR_ID;

    @ColumnInfo(name = "ITCODE")
    public String ITCODE;

    @ColumnInfo(name = "ITUNIT")
    public String ITUNIT;

    @ColumnInfo(name = "FACTOR")
    public int FACTOR;

    @ColumnInfo(name = "SLNO")
    public long SLNO;

    @ColumnInfo(name = "LOCODE")
    public String LOCODE;

    @ColumnInfo(name = "SEQ_NO")
    public long SEQ_NO;

    @ColumnInfo(name = "QTY")
    public float QTY;

    @ColumnInfo(name = "BATCHNO")
    public String BATCHNO;

    @ColumnInfo(name = "MANFDATE")
    public String MANFDATE;

    @ColumnInfo(name = "EXPDATE")
    public String EXPDATE;

    @ColumnInfo(name = "REFNO")
    public String REFNO;
}
