package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "STK_TRN_PRJ_WHR_RECIEPT222Q")
public class STK_TRN_PRJ_WHR_RECIEPT222Q implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NotNull
    @ColumnInfo(name = "SLNO")
    public long SLNO;

    @ColumnInfo(name = "GUI_ID")
    public String GUI_ID;

    @ColumnInfo(name = "VAL_WBS_ELEM")
    public String VAL_WBS_ELEM;

    @ColumnInfo(name = "STGE_LOC")
    public String STGE_LOC;

    @ColumnInfo(name = "WBS_ELEM")
    public String WBS_ELEM;

    @ColumnInfo(name = "MOVE_STLOC")
    public String MOVE_STLOC;

    @ColumnInfo(name = "MATERIAL")
    public String MATERIAL;

    @ColumnInfo(name = "ENTRY_QNT")
    public float ENTRY_QNT;

    @ColumnInfo(name = "ENTRY_UOM_ISO")
    public String ENTRY_UOM_ISO;

    @ColumnInfo(name = "VAL_TYPE")
    public String VAL_TYPE;

    @ColumnInfo(name = "FROM_BIN_ID")
    public float FROM_BIN_ID;

    @ColumnInfo(name = "TO_BIN_ID")
    public float TO_BIN_ID;


    @ColumnInfo(name = "MAT_DESC")
    public String MAT_DESC;
}
