package com.dcode.inventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity(tableName = "VEHICLE_LIST")
public class VEHICLE_LIST {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "CODE")
    public String CODE;

    @NotNull
    @ColumnInfo(name = "NAME")
    public String NAME;


}
