package com.dcode.inventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity(tableName = "HELPER_LIST")
public class HELPER_LIST {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "CODE")
    public String CODE;

    @NotNull
    @ColumnInfo(name = "NAME")
    public String NAME;


}
