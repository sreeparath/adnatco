package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "GR_DET")
public class GR_DET {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "DET_ID")
    public long DET_ID;

    @ColumnInfo(name = "HDR_ID")
    public long HDR_ID;

    @ColumnInfo(name = "ITCODE")
    public String ITCODE;

    @ColumnInfo(name = "ITUNIT")
    public String ITUNIT;

    @ColumnInfo(name = "FACTOR")
    public float FACTOR;

    @ColumnInfo(name = "SLNO")
    public long SLNO;

    @ColumnInfo(name = "SEQ_NO")
    public long SEQ_NO;

    @ColumnInfo(name = "REC_TYPE")
    public String REC_TYPE;

    @ColumnInfo(name = "RCQTY")
    public float RCQTY;

    @ColumnInfo(name = "BATCHNO")
    public String BATCHNO;

    @ColumnInfo(name = "MANFDATE")
    public String MANFDATE;

    @ColumnInfo(name = "EXPDATE")
    public String EXPDATE;

    public String REC_DESC;

    public int REC_FACTOR;
}
