package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "STK_COUNT_DET")
public class STK_COUNT_DET implements Serializable {

    @PrimaryKey()
    @ColumnInfo(name = "ITEM")
    public long ITEM;

    @NotNull
    @ColumnInfo(name = "HEAD_ID")
    public long HEAD_ID;

    @NotNull
    @ColumnInfo(name = "DOC_NO")
    public String DOC_NO;

    @NotNull
    @ColumnInfo(name = "MATERIAL")
    public String MATERIAL;

    @ColumnInfo(name = "MATL_DESC")
    public String MATL_DESC;

    @ColumnInfo(name = "BATCH")
    public String BATCH;

    @ColumnInfo(name = "WBS_ELEMENT")
    public String WBS_ELEMENT;

    @ColumnInfo(name = "BIN_ID")
    public int BIN_ID;

    @ColumnInfo(name = "SLOC_NAME")
    public String SLOC_NAME;

    @ColumnInfo(name = "DEFAULTLOC")
    public String DEFAULTLOC;

    @ColumnInfo(name = "RCQTY")
    public float RCQTY;

}
