package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "AD_GR_HDR")
public class AD_GR_HDR {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "HDR_ID")
    public long HDR_ID;

    @ColumnInfo(name = "PO_NO")
    public String PO_NO;

    @ColumnInfo(name = "DOC_DATE")
    public String DOC_DATE;

    @ColumnInfo(name = "VENDOR")
    public String VENDOR;

    @ColumnInfo(name = "LINE_CNT")
    public int LINE_CNT;

    @ColumnInfo(name = "STATUS")
    public String STATUS;

    @ColumnInfo(name = "REMARK")
    public String REMARK;

    @ColumnInfo(name = "TYPE")
    public String TYPE;
}
