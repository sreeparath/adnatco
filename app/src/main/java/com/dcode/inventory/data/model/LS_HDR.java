package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "LS_HDR")
public class LS_HDR implements Serializable {

    @NotNull
    @PrimaryKey
    @ColumnInfo(name = "LSNO")
    public String LSNO;

    @ColumnInfo(name = "ORTYP")
    public String ORTYP;

    @ColumnInfo(name = "YRCD")
    public String YRCD;

    @ColumnInfo(name = "LOADSLIPDATE")
    public String LOADSLIPDATE;

    @ColumnInfo(name = "PRICING")
    public String PRICING;

    @ColumnInfo(name = "CUSTCODE")
    public String CUSTCODE;

    @ColumnInfo(name = "CUSTNAME")
    public String CUSTNAME;

    @ColumnInfo(name = "LOADTYPE")
    public String LOADTYPE;

    @ColumnInfo(name = "LOCODE")
    public String LOCODE;

    @ColumnInfo(name = "VEHICLE")
    public String VEHICLE;

    @ColumnInfo(name = "DRIVER")
    public String DRIVER;

    @ColumnInfo(name = "HELPER")
    public String HELPER;

    @ColumnInfo(name = "CAN_LOAD")
    public String CAN_LOAD;

    @ColumnInfo(name = "LOAD_MSG")
    public String LOAD_MSG;

}
