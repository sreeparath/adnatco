package com.dcode.inventory.ui.view;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.AppVariables;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.AD_PO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.AD_PO_ITEMS_SPR;
import com.dcode.inventory.data.model.BATCH;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.GR_DET;
import com.dcode.inventory.ui.adapter.ItemBatchesAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class DocumentItemScanReceiptFragment
        extends BaseFragment
        implements View.OnClickListener {
    private final ColorDrawable background = new ColorDrawable(Color.YELLOW);
    private DocType recType;
    private float rcd_qty;
    private long HDR_ID;
    private long DET_ID;
    private View root;
    private AD_PO_ITEMS_SPR po_det;
    private TextView tvCode;
    private TextView tvName;
    private TextView tvOriginalQty;
    private TextView tvPendingQty;
    private TextView tvReceiveQty;
    private TextView tvPackaging;
    private TextView tvUnits;
    private TextView tvRate;
    private TextInputEditText edReceiptQty;
    private TextInputEditText edMaterialNo;
    private TextInputEditText edShortText;
    private TextInputEditText edPOunit;
    private TextInputEditText edScanCode;
    private TextInputEditText edDefault;
    private RecyclerView recyclerView;
    private ArrayAdapter<DocType> objectsAdapter;
    private ItemBatchesAdapter batchesAdapter;
    private Drawable icon;
    private List<BATCH> batchList;
    private ArrayAdapter<BATCH> batchTypeAdapter;
    private AutoCompleteTextView tvBatch;
    private BATCH batchType;
    private String docNo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                HDR_ID = bundle.getLong(AppConstants.PARENT_ID, -1);
                docNo = (String) bundle.getSerializable(AppConstants.SELECTED_ID);
            }
        }

        root = inflater.inflate(R.layout.fragment_item_scan_receipt, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DET_ID = -1;
        //fillSLOC_LIST();
        //loadData();
        UpdateBatchView();
        GetBatch();

        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView,
                                          @NonNull RecyclerView.ViewHolder viewHolder,
                                          @NonNull RecyclerView.ViewHolder viewHolder1) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                        if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
                            int position = viewHolder.getAdapterPosition();
                            AD_PO_ITEMS_RECIEPT gr_det_batch = batchesAdapter.getItemByPosition(position);
                            if (gr_det_batch != null) {
                                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                alert.setTitle("Delete this batch?");
                                alert.setPositiveButton("OK", (dialog, whichButton) -> {
                                    App.getDatabaseClient().getAppDatabase().genericDao().deleteAD_PO_ITEMS_RECIEPTByDet_ID(gr_det_batch.SLNO,gr_det_batch.PO_NUMBER,gr_det_batch.PO_ITEM);
                                    recyclerView.removeViewAt(position);
                                    batchesAdapter.notifyItemRemoved(position);
                                    batchesAdapter.deleteBatch(position);
                                    batchesAdapter.notifyItemRangeChanged(position, batchesAdapter.getItemCount());
                                });
                                alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
                                    //Put actions for CANCEL button here, or leave in blank
                                    recyclerView.setAdapter(batchesAdapter);
                                });
                                alert.show();
                            }
                            UpdateBatchView();
                        }
                    }

                    @Override
                    public void onChildDraw(@NonNull Canvas c,
                                            @NonNull RecyclerView recyclerView,
                                            @NonNull RecyclerView.ViewHolder viewHolder,
                                            float dX, float dY, int actionState, boolean isCurrentlyActive) {
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        View itemView = viewHolder.itemView;
                        int backgroundCornerOffset = 20;

                        int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconBottom = iconTop + icon.getIntrinsicHeight();

                        if (dX > 0) {
                            int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                            int iconRight = itemView.getLeft() + iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getLeft(), itemView.getTop(),
                                    itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
                        } else if (dX < 0) {
                            int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                            int iconRight = itemView.getRight() - iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                                    itemView.getTop(), itemView.getRight(), itemView.getBottom());
                        } else {
                            background.setBounds(0, 0, 0, 0);
                        }
                        background.draw(c);
                        icon.draw(c);
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onClick(View view) {
        GR_DET gr_det = (GR_DET) view.getTag();
        DET_ID = gr_det.DET_ID;
    }

    private void setupView() {
        tvCode = root.findViewById(R.id.tvCode);
        tvName = root.findViewById(R.id.tvName);
        tvOriginalQty = root.findViewById(R.id.tvOriginalQty);
        //tvPendingQty = root.findViewById(R.id.tvPendingQty);
        //tvReceiveQty = root.findViewById(R.id.tvReceiveQty);
        tvPackaging = root.findViewById(R.id.tvPackaging);
        tvUnits = root.findViewById(R.id.tvUnits);
        tvRate = root.findViewById(R.id.tvRate);

        edReceiptQty = root.findViewById(R.id.edReceiptQty);
        edMaterialNo = root.findViewById(R.id.edmaterailCode);
        edMaterialNo.setEnabled(false);
        edShortText = root.findViewById(R.id.edShortName);
        edShortText.setEnabled(false);
        edPOunit = root.findViewById(R.id.edPoUnit);
        edPOunit.setEnabled(false);
        //tvRecType = root.findViewById(R.id.tvRecType);
        edDefault = root.findViewById(R.id.edDefault);
        edDefault.setEnabled(false);
        tvBatch = root.findViewById(R.id.tvBatch);
        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        MaterialButton btnAdd = root.findViewById(R.id.btnAdd);
        btnNext.setOnClickListener(v -> onNextClick());
        btnAdd.setOnClickListener(v -> onAddClick());
//        edMFDDate.setOnClickListener(v -> onDateEntryClick(1));
//        edEXPDate.setOnClickListener(v -> onDateEntryClick(2));

        recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        batchesAdapter = new ItemBatchesAdapter(new ArrayList<>());
        recyclerView.setAdapter(batchesAdapter);

        edScanCode = root.findViewById(R.id.edScanCode);
        edScanCode.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onScanCodeKeyEvent();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });
        edScanCode.requestFocus();
    }

    private boolean onScanCodeKeyEvent() {
        String ScanCode = edScanCode.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid bar code");
            return false;
        } else {
            ParseBarCode(ScanCode);
        }
        return true;
    }

    private void ParseBarCode(String scanCode) {
        if (scanCode.length() <= 0 ) {
            return;
        }

        resetUI();

        String[] barCodeData = scanCode.split(App.BarCodeSeparator);
        String materialCode = "";

        if (barCodeData.length>1) {
            materialCode = barCodeData[1].toString();
        }else{
            materialCode = barCodeData[0];
        }

        po_det = App.getDatabaseClient().getAppDatabase().genericDao().getAD_PO_ITEMS_SPR_ONE_MATERIAL(docNo,materialCode);

        if(po_det==null){
            showToastLong("Invalid Item! "+scanCode);
            edScanCode.setText("");
            edScanCode.requestFocus();
            return;
        }

        edMaterialNo.setText(po_det.MATERIAL);
        edShortText.setText(po_det.SHORT_TEXT);
        edPOunit.setText(po_det.PO_UNIT);
        edDefault.setText(po_det.DEFAULTLOC);
        edScanCode.setText("");
        edScanCode.requestFocus();

    }

    private void resetUI(){
        edMaterialNo.setText("");
        edShortText.setText("");
        edPOunit.setText("");
        edReceiptQty.setText("");
        tvBatch.setText("");
        edDefault.setText("");
        //tvRecType.setText("");
    }

    private void fillSLOC_LIST() {
//        List<DocType> recTypeList = App.getDatabaseClient().getAppDatabase().genericDao().getAllSLOC_LIST_SPR();
//        objectsAdapter = new ArrayAdapter<>(requireContext(),
//                R.layout.dropdown_menu_popup_item, recTypeList);
//        tvRecType.setAdapter(objectsAdapter);
//        tvRecType.setSelection(0);
//        tvRecType.setThreshold(100);
//        tvRecType.setOnItemClickListener((parent, view, position, id) -> {
//            if (position >= 0) {
//                recType = objectsAdapter.getItem(position);
//            } else {
//                recType = null;
//            }
//        });
    }

    private void loadData() {
        if (po_det == null || po_det.ITEM_ID <= 0) {
            return;
        }

        tvCode.setText(po_det.MATERIAL);
        tvName.setText(po_det.SHORT_TEXT);
        tvOriginalQty.setText(String.valueOf(po_det.QUANTITY));
        //tvPendingQty.setText(String.valueOf(po_det.TOTQTY));
        tvReceiveQty.setText(String.valueOf(po_det.RCQTY));
        //tvPackaging.setText(po_det.PACKING);
        //tvUnits.setText(po_det.PRICE_UNIT);
//        tvRate.setText(String.valueOf(po_det.NET_PRICE));
        edReceiptQty.setText("");
        //edBatchNo.setText("");
        //edMFDDate.setText("");
        //edEXPDate.setText("");
        edDefault.setText(po_det.DEFAULTLOC);
        UpdateBatchView();
    }



    private void onNextClick() {
        getActivity().onBackPressed();
    }

    private void onAddClick() {
        if (IsDataValid()) {
            SaveData();
        }
    }

    private boolean IsDataValid() {
        String errMessage;

//        if (batchType == null) {
//            errMessage ="Invalid Batch";
//            showToast(errMessage);
//            tvBatch.setError(errMessage);
//            return false;
//        }

        String sloc = edDefault.getText().toString();
        if (sloc.length()==0) {
            errMessage ="Invalid SLOC";
            showToast(errMessage);
            edDefault.setError(errMessage);
            return false;
        }

        rcd_qty = Utils.convertToFloat(edReceiptQty.getText().toString().trim(), -1);
        if (rcd_qty < 0 ) {
            errMessage = "Invalid Receipt quantity";
            showToast(errMessage);
            edReceiptQty.setError(errMessage);
            return false;
        }

        Log.d("rcd_qty##here ",String.valueOf(rcd_qty));
        Log.d("po_det##here ",String.valueOf(po_det.RCQTY));
        Log.d("po_det##here ",String.valueOf(po_det.QUANTITY));
        if ((rcd_qty+po_det.RCQTY)>po_det.QUANTITY) {
            errMessage = "Qty not matching!";
            showToast(errMessage);
            edReceiptQty.setError(errMessage);
            return false;
        }
//        if ((rcd_qty < 0 || rcd_qty > (po_det.QUANTITY+po_det.TOL_QTY)) && po_det.IS_QTY_VALIDATE==1) {
//            errMessage = "Invalid Receipt quantity";
//            showToast(errMessage);
//            edReceiptQty.setError(errMessage);
//            return false;
//        }

//        if (po_det.BATCHFLG == 1) {
//            String batchNo = edBatchNo.getText().toString().trim();
//            if (batchNo.length() == 0) {
//                errMessage = "Invalid batch no";
//                showToast(errMessage);
//                edBatchNo.setError(errMessage);
//                return false;
//            }

//            String mfd = edMFDDate.getText().toString();
//            if (mfd.length() == 0) {
//                errMessage = "Invalid manufacture date";
//                showToast(errMessage);
//                edMFDDate.setError(errMessage);
//                return false;
//            }

//            String exp = edEXPDate.getText().toString();
//            if (exp.length() == 0) {
//                errMessage = "Invalid expiry date";
//                showToast(errMessage);
//                edEXPDate.setError(errMessage);
//                return false;
          //  }

//            Date mfDate = Utils.convertStringToDate(mfd, Utils.PRINT_DATE_FORMAT);
//            Date exDate = Utils.convertStringToDate(exp, Utils.PRINT_DATE_FORMAT);
//            Date today = new Date();
//            if (mfDate.after(today)) {
//                errMessage = "Invalid manufacture date, date in future";
//                showToast(errMessage);
//                edMFDDate.setError(errMessage);
//                return false;
//            }
//
//            if (exDate.before(today)) {
//                errMessage = "Invalid expiry date, already expired";
//                showToast(errMessage);
//                edEXPDate.setError(errMessage);
//                return false;
//            }
      //  }
        return true;
    }

    private void SaveData() {
        AD_PO_ITEMS_RECIEPT gr_det;
        gr_det = new AD_PO_ITEMS_RECIEPT();
        long seqNo = Utils.GetMaxValue("AD_PO_ITEMS_RECIEPT", "SLNO", true, "PO_ITEM", po_det.PO_ITEM);
        gr_det.SLNO = seqNo;
        gr_det.PO_ITEM = po_det.PO_ITEM;
        gr_det.PO_NUMBER = po_det.PO_NUMBER;
        gr_det.MATERIAL = po_det.MATERIAL;
        gr_det.SHORT_TEXT = po_det.SHORT_TEXT;
        gr_det.MATL_DESC = po_det.SHORT_TEXT;
        gr_det.LOC = edDefault.getText().toString().substring(0,4);
        gr_det.STGE_LOC = edDefault.getText().toString().substring(0,4);
        gr_det.PLANT = App.currentUser.PLANT;
        gr_det.WBS_ELEMENT = "";
        gr_det.QUALITY = "";
        gr_det.MOVE_TYPE = "101";
        gr_det.MVT_IND = "B";
        gr_det.MOVE_REAS = "";
        gr_det.BIN_ID = po_det.BIN_ID;
        if(batchType!=null){
            gr_det.BATCH = batchType.Name;
        }
        if(batchType!=null){
            gr_det.VAL_TYPE = batchType.Name;
        }
        gr_det.RCQTY = rcd_qty;
        gr_det.QUANTITY = rcd_qty;

        App.getDatabaseClient().getAppDatabase().genericDao().insertAD_PO_ITEMS_RECIEPT(gr_det);
        po_det = App.getDatabaseClient().getAppDatabase().genericDao().getAD_PO_ITEMS_SPR_ONE(gr_det.PO_NUMBER,gr_det.PO_ITEM);
        UpdateBatchView();
        resetUI();
        edScanCode.setText("");
        edScanCode.requestFocus();
    }

    private void UpdateBatchView() {
        List<AD_PO_ITEMS_RECIEPT> grDetlist = App.getDatabaseClient().getAppDatabase().genericDao().getAD_PO_ITEMS_RECIEPT(docNo);
        //List<GR_DET> detList = App.getDatabaseClient().getAppDatabase().genericDao().getGR_DET(HDR_ID, po_det.SLNO);
        batchesAdapter.addItems(grDetlist);
    }

    private void GetBatch() {
        batchList = new ArrayList<BATCH>();
        BATCH batch;

        batch = new BATCH();
        batch.Code = "01";
        batch.Name = "OLD";
        batchList.add(batch);

        batch = new BATCH();
        batch.Code = "02";
        batch.Name = "NEW";
        batchList.add(batch);


        batchTypeAdapter = new ArrayAdapter<>(this.getContext(), R.layout.dropdown_menu_popup_item, batchList);
        tvBatch.setAdapter(batchTypeAdapter);
        tvBatch.setSelection(0);
        tvBatch.setThreshold(100);
        tvBatch.setOnItemClickListener((parent, view, position, id) -> {
            if (position >= 0) {
                batchType = batchTypeAdapter.getItem(position);
            } else {
                batchType = null;
            }
        });
    }
}
