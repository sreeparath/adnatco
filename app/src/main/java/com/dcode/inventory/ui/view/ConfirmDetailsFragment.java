package com.dcode.inventory.ui.view;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.CONFIRM_DOC_DET;
import com.dcode.inventory.data.model.CONFIRM_DOC_HDR;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.SALE_RET_DET;
import com.dcode.inventory.data.model.SALE_RET_DOC_DET;
import com.dcode.inventory.data.model.SALE_RET_DOC_HDR;
import com.dcode.inventory.data.model.SALE_RET_HDR;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.model.GenericSubmissionResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.dcode.inventory.ui.adapter.ConfirmDetAdapter;
import com.dcode.inventory.ui.adapter.SaleRetDetAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ConfirmDetailsFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    private CONFIRM_DOC_HDR co_hdr;
    private String DocNo;
    private DocType ordTyp;
    private TextInputEditText edSupInvNo;
    private TextInputEditText edSupInvDate;
    private TextInputEditText edBillNo;
    private TextInputEditText edContainerNo;
    private TextInputEditText edRemarks;
    private TextInputEditText edReceiptNo;
    private TextInputEditText edReturnNo;
    private MaterialButton btnPrintLabels;
    private View root;
    private ConfirmDetAdapter detAdapter;
    private String filterString;
    private FloatingActionButton proceed;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        //HDR_ID = App.sr_hdr_id;
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                DocNo = bundle.getString(AppConstants.SELECTED_ID, DocNo);
                ordTyp = (DocType) bundle.getSerializable(AppConstants.SELECTED_CODE);
                co_hdr = (CONFIRM_DOC_HDR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_confirm_details, container, false);
        setupView();
//        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DownloadPO_DET(co_hdr.ORDNO,co_hdr.ORTYP);
    }

//    @Override
//    public void onPrepareOptionsMenu(Menu menu) {
//        MenuInflater inflater = requireActivity().getMenuInflater();
//        inflater.inflate(R.menu.top_app_bar, menu);
//
//        final MenuItem searchItem = menu.findItem(R.id.search);
//        final SearchView searchView = (SearchView) searchItem.getActionView();
//        searchView.setOnQueryTextListener(this);
//    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        detAdapter.getFilter().filter(newText);
        filterString = newText;
        return false;
    }

    private void setupView() {
        ///MaterialButton btnPostReceipt = root.findViewById(R.id.btnPostReceipt);
        //btnPostReceipt.setVisibility(View.INVISIBLE);
        //btnPostReceipt.setOnClickListener(v -> onPostReceiptClick());

//        btnPrintLabels = root.findViewById(R.id.btnPrintLabels);
//        btnPrintLabels.setVisibility(View.INVISIBLE);
//        btnPrintLabels.setOnClickListener(v -> onPrintLabels());
//        if (gr_hdr != null && gr_hdr.RECEIPT_NO > 0) {
//            btnPrintLabels.setEnabled(true);
//        } else {
//            btnPrintLabels.setEnabled(false);
//        }

        /*edSupInvNo = root.findViewById(R.id.edSupInvNo);
        edSupInvDate = root.findViewById(R.id.edSupInvDate);
        edSupInvDate.setOnClickListener(v -> {
            edSupInvDate.setError(null);
            Calendar currentDate = Calendar.getInstance();
            int curYear = currentDate.get(Calendar.YEAR);
            int curMonth = currentDate.get(Calendar.MONTH);
            int curDay = currentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), (datePicker, cYear, cMonth, cDay) -> {
                String dateString = String.format(Locale.ENGLISH, "%d-%d-%d", cDay, (cMonth + 1), cYear);
                String selectedDate = Utils.getFormattedDate(dateString, Utils.ISSUE_DATE_FORMAT, Utils.PRINT_DATE_FORMAT);

                edSupInvDate.setText(selectedDate);
            }, curYear, curMonth, curDay);
            mDatePicker.setTitle("Select date");
//            mDatePicker.getDatePicker().setMaxDate(new Date().getTime());
            mDatePicker.show();
        });
        edBillNo = root.findViewById(R.id.edBillNo);
        edContainerNo = root.findViewById(R.id.edContainerNo);
        edRemarks = root.findViewById(R.id.edRemarks);
        edReceiptNo = root.findViewById(R.id.edReceiptNo);
        edReturnNo = root.findViewById(R.id.edReturnNo);*/

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        detAdapter = new ConfirmDetAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(detAdapter);



    }

    private void DownloadPO_DET(final String docNo, final String docType) {
        JsonObject requestObject = ServiceUtils.SaleReturn.getConfirmDocDet(docNo, docType);

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        CONFIRM_DOC_DET[] poDet = new Gson().fromJson(xmlDoc, CONFIRM_DOC_DET[].class);
                        detAdapter.addItems(Arrays.asList(poDet));
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showToast(msg);
            }
        });
    }

}
