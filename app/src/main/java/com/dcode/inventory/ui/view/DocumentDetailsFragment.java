package com.dcode.inventory.ui.view;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.AD_PO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.AD_PO_ITEMS_SPR;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.GR_DET;
import com.dcode.inventory.data.model.GR_HDR;
import com.dcode.inventory.data.model.PO_DET;
import com.dcode.inventory.data.model.PO_HDR;
import com.dcode.inventory.network.model.GenericSubmissionResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.dcode.inventory.ui.adapter.DocumentDetAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DocumentDetailsFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    private GR_HDR gr_hdr;
    private long HDR_ID;
    private String DocNo;
    private DocType ordTyp;
    private View root;
    private DocumentDetAdapter detAdapter;
    private String filterString;
    private FloatingActionButton proceed;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        HDR_ID = App.gr_hdr_id;
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                DocNo = bundle.getString(AppConstants.SELECTED_ID, DocNo);
                ordTyp = (DocType) bundle.getSerializable(AppConstants.SELECTED_CODE);
            }
        }

        root = inflater.inflate(R.layout.fragment_doc_details, container, false);

//        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

//    @Override
//    public void onPrepareOptionsMenu(Menu menu) {
//        MenuInflater inflater = requireActivity().getMenuInflater();
//        inflater.inflate(R.menu.top_app_bar, menu);
//
//        final MenuItem searchItem = menu.findItem(R.id.search);
//        final SearchView searchView = (SearchView) searchItem.getActionView();
//        searchView.setOnQueryTextListener(this);
//    }

    @Override
    public void onClick(View view) {
        AD_PO_ITEMS_SPR detailData = (AD_PO_ITEMS_SPR) view.getTag();
        if (detailData == null || detailData.ITEM_ID <= 0) {
            return;
        }

//        if (detailData.TOTQTY <= 0) {
//            showToast("Nothing to receive");
//            return;
//        }

//        if (App.gr_hdr_id <= 0) {
//            PO_HDR po_hdr = App.getDatabaseClient().getAppDatabase().genericDao().getPO_HDR(DocNo, ordTyp.CODE);
//            gr_hdr = new GR_HDR();
//            gr_hdr.GUID = Utils.GetGUID();
//            gr_hdr.DEVICE_DATE = Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT);
//            gr_hdr.ORDNO = po_hdr.ORDNO;
//            gr_hdr.ORTYP = po_hdr.ORTYP;
//            gr_hdr.CLSUP = po_hdr.CLSUP;
//            gr_hdr.YRCD = po_hdr.YRCD == null ? "" : po_hdr.YRCD;
//            gr_hdr.POTYPE = po_hdr.POTYPE;
//            gr_hdr.POTYPENAME = po_hdr.POTYPENAME;
//
//            HDR_ID = App.getDatabaseClient().getAppDatabase().genericDao().insertGR_HDR(gr_hdr);
//            App.gr_hdr_id = HDR_ID;
//        }
//        HDR_ID = App.gr_hdr_id;


        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.MASTER_ID, DocNo);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, detailData);
        bundle.putSerializable(AppConstants.SELECTED_CODE, ordTyp);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_item_receipt, bundle);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        detAdapter.getFilter().filter(newText);
        filterString = newText;
        return false;
    }

    private void setupView() {
        MaterialButton btnPostReceipt = root.findViewById(R.id.btnPostReceipt);
        btnPostReceipt.setOnClickListener(v -> onPostReceiptClick());

        proceed = (FloatingActionButton) root.findViewById(R.id.proceed);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt(AppConstants.MODULE_ID, ModuleID);
                bundle.putSerializable(AppConstants.SELECTED_ID, DocNo);
                ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_doc_item_scan_receipt, bundle);
            }
        });

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        detAdapter = new DocumentDetAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(detAdapter);

        List<AD_PO_ITEMS_SPR> detList = App.getDatabaseClient().getAppDatabase().genericDao().getAD_PO_ITEMS_SPR(DocNo);
        detAdapter.addItems(detList);

    }

    private void onPostReceiptClick() {
        getActivity().onBackPressed();
    }


/*    private void PromptPrint(final String receiptNo) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(requireActivity());
        alert.setTitle("Print Labels?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Put actions for OK button here
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.SELECTED_ID, receiptNo);
                ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_print_labels, bundle);
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Put actions for CANCEL button here, or leave in blank
                Log.d(App.TAG, "for debugging");
            }
        });
        alert.show();
    }*/
}
