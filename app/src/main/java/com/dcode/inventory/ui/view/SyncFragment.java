package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.network.service.ServiceUtils;

public class SyncFragment extends BaseFragment {
    private View root;
    private Switch swMasters,swGrn,swWO,swRWO,swSTK221,swSTK221Q,swSTK222,swSTK222Q,swSTK411Q,swSTK415Q,swSTKcount;
//    private Switch swVisits;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
        }

        root = inflater.inflate(R.layout.fragment_sync, container, false);

        setupView();
        return root;
    }

    private void setupView() {
        Button btnSync = root.findViewById(R.id.btNext);
        btnSync.setOnClickListener(v -> OnSynchroniseClick());

        swMasters = root.findViewById(R.id.swMasters);
        swGrn = root.findViewById(R.id.swGrn);
        swWO = root.findViewById(R.id.swWO);
        swRWO = root.findViewById(R.id.swRWO);
        swSTK221 = root.findViewById(R.id.swSTK221);
        swSTK221Q = root.findViewById(R.id.swSTK221Q);
        swSTK222 = root.findViewById(R.id.swSTK222);
        swSTK222Q = root.findViewById(R.id.swSTK222Q);
        swSTK411Q = root.findViewById(R.id.swSTK411Q);
        swSTK415Q = root.findViewById(R.id.swSTK415Q);
        swSTKcount = root.findViewById(R.id.swSTKcount);
        LinearLayout llMasters = root.findViewById(R.id.llMasters);
        llMasters.setOnClickListener(v -> OnLocationsClick());
        LinearLayout llGrn = root.findViewById(R.id.llGrn);
        llGrn.setOnClickListener(v -> OnGrnClick());
        LinearLayout llWO = root.findViewById(R.id.llWO);
        llWO.setOnClickListener(v -> OnWOClick());
        LinearLayout llRWO = root.findViewById(R.id.llRWO);
        llRWO.setOnClickListener(v -> OnRWOClick());
        LinearLayout llSTK221 = root.findViewById(R.id.llSTK221);
        llSTK221.setOnClickListener(v -> OnSTKClick());
        LinearLayout llSTK221Q = root.findViewById(R.id.llSTK221Q);
        llSTK221Q.setOnClickListener(v -> OnSTK221QClick());
        LinearLayout llSTK222 = root.findViewById(R.id.llSTK222);
        llSTK222.setOnClickListener(v -> OnSTK222Click());
        LinearLayout llSTK222Q = root.findViewById(R.id.llSTK222Q);
        llSTK222Q.setOnClickListener(v -> OnSTK222QClick());
        LinearLayout llSTK411Q = root.findViewById(R.id.llSTK411Q);
        llSTK411Q.setOnClickListener(v -> OnSTKC411QClick());
        LinearLayout llSTK415Q = root.findViewById(R.id.llSTK415Q);
        llSTK415Q.setOnClickListener(v -> OnSTK415QClick());

        LinearLayout llSTKcount = root.findViewById(R.id.llSTKcount);
        llSTKcount.setOnClickListener(v -> OnSTKcountClick());


        if (ModuleID == AppConstants.UPLOADS) {
            llMasters.setVisibility(View.GONE);
            swMasters.setChecked(false);
            llGrn.setVisibility(View.VISIBLE);
            swGrn.setChecked(false);
            llWO.setVisibility(View.VISIBLE);
            swWO.setChecked(false);
            llRWO.setVisibility(View.VISIBLE);
            swRWO.setChecked(false);
            llSTK221.setVisibility(View.VISIBLE);
            swSTK221.setChecked(false);
            llSTK221Q.setVisibility(View.VISIBLE);
            swSTK221Q.setChecked(false);
            llSTK222.setVisibility(View.VISIBLE);
            swSTK222.setChecked(false);
            llSTK222Q.setVisibility(View.VISIBLE);
            swSTK222Q.setChecked(false);
            llSTK411Q.setVisibility(View.VISIBLE);
            swSTK411Q.setChecked(false);
            llSTK415Q.setVisibility(View.VISIBLE);
            swSTK415Q.setChecked(false);
            llSTKcount.setVisibility(View.VISIBLE);
            swSTKcount.setChecked(false);
        } else if (ModuleID == AppConstants.DOWNLOADS) {
            llMasters.setVisibility(View.VISIBLE);
            swMasters.setChecked(false);
            llGrn.setVisibility(View.GONE);
            swGrn.setChecked(false);
            llWO.setVisibility(View.GONE);
            swWO.setChecked(false);
            llRWO.setVisibility(View.GONE);
            swRWO.setChecked(false);
            llSTK221.setVisibility(View.GONE);
            swSTK221.setChecked(false);
            llSTK221Q.setVisibility(View.GONE);
            swSTK221Q.setChecked(false);
            llSTK222.setVisibility(View.GONE);
            swSTK222.setChecked(false);
            llSTK222Q.setVisibility(View.GONE);
            swSTK222Q.setChecked(false);
            llSTK411Q.setVisibility(View.GONE);
            swSTK411Q.setChecked(false);
            llSTK415Q.setVisibility(View.GONE);
            swSTK415Q.setChecked(false);
            llSTKcount.setVisibility(View.GONE);
            swSTKcount.setChecked(false);
        }

        TextView tvMessage = root.findViewById(R.id.tvMessage);
        tvMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // nothing to do.
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do.
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().contains("failed")) {
                    shortVibration();
                }
            }
        });
    }

    private void OnSynchroniseClick() {
        ServiceUtils.SyncActivity syncActivity = new ServiceUtils.SyncActivity(getContext(), false);

        TextView textView = root.findViewById(R.id.tvMessage);
        textView.setText(R.string.empty_string);
        if (swMasters.isChecked()) {
            syncActivity.DownloadSLocList(AppConstants.DatabaseEntities.SLOC_LIST_SPR);
            syncActivity.DownloadPlantList(AppConstants.DatabaseEntities.PLANT_LIST);
            syncActivity.DownloadReturnList(AppConstants.DatabaseEntities.RET_REASON_LIST);
            syncActivity.DownloadMaterialList(AppConstants.DatabaseEntities.MATERIAL_LIST);
        }

        if (ModuleID == AppConstants.UPLOADS) {
            if (swGrn.isChecked()) {
                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.GOODS_RECEIPT_HDR.toString());
                syncActivity.Uploads(AppConstants.DatabaseEntities.GOODS_RECEIPT_HDR,0);
            }
            if (swWO.isChecked()) {
                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.WORK_ORDER.toString());
                syncActivity.Uploads(AppConstants.DatabaseEntities.WORK_ORDER,0);
            }
            if (swRWO.isChecked()) {
                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.REVERSE_WORD_ORDER.toString());
                syncActivity.Uploads(AppConstants.DatabaseEntities.REVERSE_WORD_ORDER,0);
            }
            if (swSTK221.isChecked()) {
                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.STOCK_TRANSFER_221.toString());
                syncActivity.Uploads(AppConstants.DatabaseEntities.STOCK_TRANSFER_221,0);
            }
            if (swSTK221Q.isChecked()) {
                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.STOCK_TRANSFER_221Q.toString());
                syncActivity.Uploads(AppConstants.DatabaseEntities.STOCK_TRANSFER_221Q,0);
            }
            if (swSTK222.isChecked()) {
                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.STOCK_TRANSFER_222.toString());
                syncActivity.Uploads(AppConstants.DatabaseEntities.STOCK_TRANSFER_222,0);
            }
            if (swSTK222Q.isChecked()) {
                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.STOCK_TRANSFER_222Q.toString());
                syncActivity.Uploads(AppConstants.DatabaseEntities.STOCK_TRANSFER_222Q,0);
            }
            if (swSTK411Q.isChecked()) {
                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.STOCK_TRANSFER_411Q.toString());
                syncActivity.Uploads(AppConstants.DatabaseEntities.STOCK_TRANSFER_411Q,0);
            }
            if (swSTK415Q.isChecked()) {
                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.STOCK_TRANSFER_415Q.toString());
                syncActivity.Uploads(AppConstants.DatabaseEntities.STOCK_TRANSFER_415Q,0);
            }
            if (swSTKcount.isChecked()) {
                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.STOCK_COUNT.toString());
                syncActivity.Uploads(AppConstants.DatabaseEntities.STOCK_COUNT,0);
            }
        }

//        if (swVisits.isChecked()) {
//            syncActivity.Uploads(AppConstants.DatabaseEntities.VISITS_HDR, -1);
//        }
    }

    private void OnLocationsClick() {
        swMasters.setChecked(!swMasters.isChecked());
    }

    private void OnGrnClick() {
        swGrn.setChecked(!swGrn.isChecked());
    }
    private void OnWOClick() {
        swWO.setChecked(!swWO.isChecked());
    }
    private void OnRWOClick() {
        swRWO.setChecked(!swRWO.isChecked());
    }
    private void OnSTKClick() {
        swSTK221.setChecked(!swSTK221.isChecked());
    }
    private void OnSTK221QClick() {
        swSTK221Q.setChecked(!swSTK221Q.isChecked());
    }
    private void OnSTK222Click() {
        swSTK222.setChecked(!swSTK222.isChecked());
    }
    private void OnSTK222QClick() {
        swSTK222Q.setChecked(!swSTK222Q.isChecked());
    }
    private void OnSTKC411QClick() {
        swSTK411Q.setChecked(!swSTK411Q.isChecked());
    }
    private void OnSTK415QClick() {
        swSTK415Q.setChecked(!swSTK415Q.isChecked());
    }
    private void OnSTKcountClick() {
        swSTKcount.setChecked(!swSTKcount.isChecked());
    }
//    private void onVisitsClick() {
//        swVisits.setChecked(!swVisits.isChecked());
//    }
}
