package com.dcode.inventory.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.data.model.CONFIRM_DOC_DET;
import com.dcode.inventory.data.model.SALE_RET_DOC_DET;

import java.util.ArrayList;
import java.util.List;

public class ConfirmDetAdapter
        extends RecyclerView.Adapter<ConfirmDetAdapter.RecyclerViewHolder>
        implements Filterable {

    //    public static PO_DET editedRow;
    private List<CONFIRM_DOC_DET> poDetList;
    private List<CONFIRM_DOC_DET> poDetListFull;
    private View.OnClickListener shortClickListener;

    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<CONFIRM_DOC_DET> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(poDetListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (CONFIRM_DOC_DET item : poDetListFull) {
                    if ((item.ITDESC != null && item.ITDESC.toLowerCase().contains(filterPattern)) ||
                            (item.ITCODE != null && item.ITCODE.toLowerCase().contains(filterPattern))) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            poDetList.clear();
            poDetList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public ConfirmDetAdapter(List<CONFIRM_DOC_DET> detList,
                             View.OnClickListener shortClickListener) {
        this.poDetList = detList;
        this.shortClickListener = shortClickListener;
        setHasStableIds(true);
        poDetListFull = new ArrayList<>(detList);
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public ConfirmDetAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_confirm_items, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ConfirmDetAdapter.RecyclerViewHolder holder, int position) {
        final CONFIRM_DOC_DET po_det = poDetList.get(position);

        holder.tvCode.setText(po_det.ITCODE);
        holder.tvName.setText(po_det.ITDESC);
        holder.tvOriginalQty.setText(String.valueOf(po_det.ITQTY));
//        holder.tvPendingQty.setText(String.valueOf(po_det.TOTQTY));
       // holder.tvReceiveQty.setText(String.valueOf(po_det.RCQTY));
        holder.tvPackaging.setText(po_det.PACKING);
        holder.tvUnits.setText(po_det.ITUNIT);
        //holder.tvRate.setText(String.valueOf(po_det.ITRTE));

        holder.itemView.setTag(po_det);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return poDetList.size();
    }

    public void addItems(List<CONFIRM_DOC_DET> detList) {
        this.poDetList = detList;
        this.poDetListFull = new ArrayList<>(detList);

        notifyDataSetChanged();
    }

    public List<CONFIRM_DOC_DET> getFullList() {
        return poDetListFull;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCode;
        private TextView tvName;
        private TextView tvOriginalQty;
        private TextView tvPendingQty;
        private TextView tvReceiveQty;
        private TextView tvPackaging;
        private TextView tvUnits;
        private TextView tvRate;

        RecyclerViewHolder(View view) {
            super(view);
            tvCode = view.findViewById(R.id.tvCode);
            tvName = view.findViewById(R.id.tvName);
            tvOriginalQty = view.findViewById(R.id.tvOriginalQty);
            //tvPendingQty = view.findViewById(R.id.tvPendingQty);
            //tvReceiveQty = view.findViewById(R.id.tvReceiveQty);
            tvPackaging = view.findViewById(R.id.tvPackaging);
            tvUnits = view.findViewById(R.id.tvUnits);
            //tvRate = view.findViewById(R.id.tvRate);
        }
    }
}
