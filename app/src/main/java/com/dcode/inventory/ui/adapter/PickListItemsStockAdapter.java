package com.dcode.inventory.ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS_STOCK;

import java.util.ArrayList;
import java.util.List;

public class PickListItemsStockAdapter
        extends RecyclerView.Adapter<PickListItemsStockAdapter.RecyclerViewHolder>
        implements Filterable {
    private List<PICK_LIST_ITEMS_STOCK> plItemsList;
    private List<PICK_LIST_ITEMS_STOCK> plItemsListFull;
    private View.OnClickListener shortClickListener;
    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<PICK_LIST_ITEMS_STOCK> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(plItemsListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (PICK_LIST_ITEMS_STOCK item : plItemsListFull) {
                    if ((item.ITCODE != null && item.ITCODE.toLowerCase().contains(filterPattern)) ||
                            (item.BATCHNO != null && item.BATCHNO.toLowerCase().contains(filterPattern)) ||
                            (item.ITDESC != null && item.ITDESC.toLowerCase().contains(filterPattern))) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            plItemsList.clear();
            if (results != null && results.values != null) {
                plItemsList.addAll((List) results.values);
            }
            notifyDataSetChanged();
        }
    };

    public PickListItemsStockAdapter(List<PICK_LIST_ITEMS_STOCK> dataList, View.OnClickListener shortClickListener) {
        this.plItemsList = dataList;
        this.shortClickListener = shortClickListener;
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public PickListItemsStockAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_pl_stock, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PickListItemsStockAdapter.RecyclerViewHolder holder, int position) {
        final PICK_LIST_ITEMS_STOCK ls_hdr = plItemsList.get(position);
        holder.tvName.setText(ls_hdr.ITDESC);
        holder.tvCode.setText(ls_hdr.ITCODE);
        holder.tvUnits.setText(String.valueOf(ls_hdr.ITUNIT));
        holder.tvBatchNo.setText(ls_hdr.BATCHNO);
        holder.tvStock.setText(String.valueOf(ls_hdr.STOCK));
        holder.tvMFDDate.setText(ls_hdr.MANFDATE);
        holder.tvEXPDate.setText(ls_hdr.EXPDATE);
        holder.itemView.setTag(ls_hdr);
        holder.itemView.setOnClickListener(shortClickListener);



    }

    @Override
    public int getItemCount() {
        return plItemsList.size();
    }

    public void addItems(List<PICK_LIST_ITEMS_STOCK> dataList) {
        this.plItemsList = dataList;
        this.plItemsListFull = new ArrayList<>(dataList);

        notifyDataSetChanged();
    }

    private OnItemClickListener mListener;
    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    static class RecyclerViewHolder
            extends RecyclerView.ViewHolder  {
        private TextView tvName;
        private TextView tvCode;
        private TextView tvBatchNo;
        private TextView tvUnits;
        private TextView tvStock;
        private TextView tvMFDDate;
        private TextView tvEXPDate;


        RecyclerViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            tvCode = view.findViewById(R.id.tvCode);
            tvUnits = view.findViewById(R.id.tvUnits);
            tvBatchNo = view.findViewById(R.id.tvBatchNo);
            tvStock = view.findViewById(R.id.tvStock);
            tvMFDDate = view.findViewById(R.id.tvMFDDate);
            tvEXPDate = view.findViewById(R.id.tvEXPDate);
        }
    }
}
