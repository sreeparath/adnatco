package com.dcode.inventory.ui.view;

import android.app.AlertDialog;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.BATCH;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.MATERIAL_LIST;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT221;
import com.dcode.inventory.data.model.WO_HDR_SPR;
import com.dcode.inventory.ui.adapter.StkTransPrjWhrAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StockTransferProjectToWareFragment221
        extends BaseFragment
        implements View.OnClickListener {
    private final ColorDrawable background = new ColorDrawable(Color.YELLOW);
    private DocType recType;
    private float rcd_qty;
    private long HDR_ID;
    private long DET_ID;
    private View root;
    private MATERIAL_LIST po_det;
    private TextView tvCode;
    private TextView tvName;
    private TextView tvOriginalQty;
    private TextView tvPendingQty;
    private TextView tvReceiveQty;
    private TextView tvPackaging;
    private TextView tvUnits;
    private TextView tvRate;
    private TextInputEditText edReceiptQty;
    private TextInputEditText edMaterialNo;
    private TextInputEditText edmaterailDesc;
    private TextInputEditText edWBS;
    private TextInputEditText edDefaultLoc;
    private TextInputEditText edScanCode;
    //private AutoCompleteTextView tvRecType;
    private RecyclerView recyclerView;
    private ArrayAdapter<DocType> objectsAdapter;
    private StkTransPrjWhrAdapter batchesAdapter;
    private Drawable icon;
    private List<BATCH> batchList;
    private ArrayAdapter<BATCH> batchTypeAdapter;
    private AutoCompleteTextView tvBatch;
    private BATCH batchType;
    private WO_HDR_SPR wo_hdr;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                HDR_ID = bundle.getLong(AppConstants.PARENT_ID, -1);
                wo_hdr = (WO_HDR_SPR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_stk_trans_prj_ware, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DET_ID = -1;
        //fillSLOC_LIST();
        //loadData();
        UpdateBatchView();
        //GetBatch();

        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView,
                                          @NonNull RecyclerView.ViewHolder viewHolder,
                                          @NonNull RecyclerView.ViewHolder viewHolder1) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                        if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
                            int position = viewHolder.getAdapterPosition();
                            STK_TRN_PRJ_WHR_RECIEPT221 gr_det_batch = batchesAdapter.getItemByPosition(position);
                            if (gr_det_batch != null) {
                                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                alert.setTitle("Delete this batch?");
                                alert.setPositiveButton("OK", (dialog, whichButton) -> {
                                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSTK_TRN_PRJ_WHR_RECIEPTByDet_ID(gr_det_batch.SLNO);
                                    recyclerView.removeViewAt(position);
                                    batchesAdapter.notifyItemRemoved(position);
                                    batchesAdapter.deleteBatch(position);
                                    batchesAdapter.notifyItemRangeChanged(position, batchesAdapter.getItemCount());
                                });
                                alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
                                    //Put actions for CANCEL button here, or leave in blank
                                    recyclerView.setAdapter(batchesAdapter);
                                });
                                alert.show();
                            }
                            UpdateBatchView();
                        }
                    }

                    @Override
                    public void onChildDraw(@NonNull Canvas c,
                                            @NonNull RecyclerView recyclerView,
                                            @NonNull RecyclerView.ViewHolder viewHolder,
                                            float dX, float dY, int actionState, boolean isCurrentlyActive) {
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        View itemView = viewHolder.itemView;
                        int backgroundCornerOffset = 20;

                        int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconBottom = iconTop + icon.getIntrinsicHeight();

                        if (dX > 0) {
                            int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                            int iconRight = itemView.getLeft() + iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getLeft(), itemView.getTop(),
                                    itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
                        } else if (dX < 0) {
                            int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                            int iconRight = itemView.getRight() - iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                                    itemView.getTop(), itemView.getRight(), itemView.getBottom());
                        } else {
                            background.setBounds(0, 0, 0, 0);
                        }
                        background.draw(c);
                        icon.draw(c);
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onClick(View view) {
//        GR_DET gr_det = (GR_DET) view.getTag();
//        DET_ID = gr_det.DET_ID;
    }

    private void setupView() {
        tvCode = root.findViewById(R.id.tvCode);
        tvName = root.findViewById(R.id.tvName);
        tvOriginalQty = root.findViewById(R.id.tvOriginalQty);
        //tvPendingQty = root.findViewById(R.id.tvPendingQty);
        //tvReceiveQty = root.findViewById(R.id.tvReceiveQty);
        tvPackaging = root.findViewById(R.id.tvPackaging);
        tvUnits = root.findViewById(R.id.tvUnits);
        tvRate = root.findViewById(R.id.tvRate);


        edReceiptQty = root.findViewById(R.id.edReceiptQty);
        edMaterialNo = root.findViewById(R.id.edmaterailCode);
        edMaterialNo.setEnabled(false);
        edmaterailDesc = root.findViewById(R.id.edmaterailDesc);
        edmaterailDesc.setEnabled(false);
        //edShortText = root.findViewById(R.id.edShortName);
        //edShortText.setEnabled(false);
        edWBS = root.findViewById(R.id.edWBS);
        //tvRecType = root.findViewById(R.id.tvRecType);
        edDefaultLoc = root.findViewById(R.id.edDefaultLoc);
        edDefaultLoc.setEnabled(false);
        //tvBatch = root.findViewById(R.id.tvBatch);
        //edSloc = root.findViewById(R.id.edSLOC);

//        LinearLayout llMasters = root.findViewById(R.id.ilSLOCconfirm);
//        llMasters.setVisibility(View.INVISIBLE);

        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        MaterialButton btnAdd = root.findViewById(R.id.btnAdd);
        btnNext.setOnClickListener(v -> onNextClick());
        btnAdd.setOnClickListener(v -> onAddClick());

//        MaterialButton btnClearDoc = root.findViewById(R.id.btnCopy);
//        btnClearDoc.setOnClickListener(v -> {
//            edSloc.setText(edDefaultLoc.getText().toString());
//                }
//        );

        recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        batchesAdapter = new StkTransPrjWhrAdapter(new ArrayList<>());
        recyclerView.setAdapter(batchesAdapter);

        edScanCode = root.findViewById(R.id.edScanCode);
        edScanCode.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onScanCodeKeyEvent();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });
        edScanCode.requestFocus();
    }

    private boolean onScanCodeKeyEvent() {
        String ScanCode = edScanCode.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid bar code");
            return false;
        } else {
            ParseBarCode(ScanCode);
        }
        return true;
    }

    private void ParseBarCode(String scanCode) {
        if (scanCode.length() <= 0 ) {
            return;
        }

        resetUI();

        String[] barCodeData = scanCode.split(App.BarCodeSeparator);
        String materialCode = "";


        if (barCodeData.length>1) {
            materialCode = barCodeData[1].toString();
        }else{
            materialCode = barCodeData[0];
        }

        po_det = App.getDatabaseClient().getAppDatabase().genericDao().getMATERIAL_LIST_ITEM(materialCode);

        if(po_det==null){
            showToastLong("Invalid Item! "+scanCode);
            edScanCode.setText("");
            edScanCode.requestFocus();
            return;
        }
        edMaterialNo.setText(po_det.MATERIAL);
        edmaterailDesc.setText(po_det.MATL_DESC);
        edDefaultLoc.setText(po_det.DEFAULTLOC);
        edScanCode.setText("");
        edScanCode.requestFocus();

    }

    private void resetUI(){
        edMaterialNo.setText("");
        edmaterailDesc.setText("");
        edWBS.setText("");
        edReceiptQty.setText("");
       // tvBatch.setText("");
        edDefaultLoc.setText("");
        //edSloc.setText("");
        //tvRecType.setText("");
    }

    private void fillSLOC_LIST() {
        List<DocType> recTypeList = App.getDatabaseClient().getAppDatabase().genericDao().getAllSLOC_LIST_SPR();
        objectsAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, recTypeList);
//        tvRecType.setAdapter(objectsAdapter);
//        tvRecType.setSelection(0);
//        tvRecType.setThreshold(100);
//        tvRecType.setOnItemClickListener((parent, view, position, id) -> {
//            if (position >= 0) {
//                recType = objectsAdapter.getItem(position);
//            } else {
//                recType = null;
//            }
//        });
    }




    private void onNextClick() {
        getActivity().onBackPressed();
    }

    private void onAddClick() {
        if (IsDataValid()) {
            SaveData();
        }
    }

    private boolean IsDataValid() {
        String errMessage;

//        if (batchType == null) {
//            errMessage ="Invalid Val Type";
//            showToast(errMessage);
//            tvBatch.setError(errMessage);
//            return false;
//        }

        String sloc = edDefaultLoc.getText().toString();
        if (sloc.length()==0) {
            errMessage ="Invalid SLOC";
            showToast(errMessage);
            edDefaultLoc.setError(errMessage);
            return false;
        }

        String wbs = edWBS.getText().toString();
        if (wbs.length()==0) {
            errMessage ="Invalid WBS";
            showToast(errMessage);
            edWBS.setError(errMessage);
            return false;
        }

//        if (recType.BIN_ID!=po_det.BIN_ID){
//            errMessage ="Invalid BIN ID";
//            showToast(errMessage);
//            tvRecType.setError(errMessage);
//            return false;
//        }


        rcd_qty = Utils.convertToFloat(edReceiptQty.getText().toString().trim(), -1);
        if (rcd_qty < 0 ) {
            errMessage = "Invalid Receipt quantity";
            showToast(errMessage);
            edReceiptQty.setError(errMessage);
            return false;
        }
//        if ((rcd_qty < 0 || rcd_qty > (po_det.QUANTITY+po_det.TOL_QTY)) && po_det.IS_QTY_VALIDATE==1) {
//            errMessage = "Invalid Receipt quantity";
//            showToast(errMessage);
//            edReceiptQty.setError(errMessage);
//            return false;
//        }

//        if (po_det.BATCHFLG == 1) {
//            String batchNo = edBatchNo.getText().toString().trim();
//            if (batchNo.length() == 0) {
//                errMessage = "Invalid batch no";
//                showToast(errMessage);
//                edBatchNo.setError(errMessage);
//                return false;
//            }

//            String mfd = edMFDDate.getText().toString();
//            if (mfd.length() == 0) {
//                errMessage = "Invalid manufacture date";
//                showToast(errMessage);
//                edMFDDate.setError(errMessage);
//                return false;
//            }

//            String exp = edEXPDate.getText().toString();
//            if (exp.length() == 0) {
//                errMessage = "Invalid expiry date";
//                showToast(errMessage);
//                edEXPDate.setError(errMessage);
//                return false;
          //  }

//            Date mfDate = Utils.convertStringToDate(mfd, Utils.PRINT_DATE_FORMAT);
//            Date exDate = Utils.convertStringToDate(exp, Utils.PRINT_DATE_FORMAT);
//            Date today = new Date();
//            if (mfDate.after(today)) {
//                errMessage = "Invalid manufacture date, date in future";
//                showToast(errMessage);
//                edMFDDate.setError(errMessage);
//                return false;
//            }
//
//            if (exDate.before(today)) {
//                errMessage = "Invalid expiry date, already expired";
//                showToast(errMessage);
//                edEXPDate.setError(errMessage);
//                return false;
//            }
      //  }
        return true;
    }

    private void SaveData() {
        STK_TRN_PRJ_WHR_RECIEPT221 gr_det;
        gr_det = new STK_TRN_PRJ_WHR_RECIEPT221();
        //long seqNo = Utils.GetMaxValue("WO_ITEMS_RECIEPT", "SLNO", true, "MATERIAL", po_det.MATERIAL);
        //gr_det.SLNO = ++seqNo;
        gr_det.GUI_ID = Utils.GetGUID();
        gr_det.VAL_WBS_ELEM = edWBS.getText().toString();
        //gr_det.STGE_LOC = edSloc.getText().toString();
        gr_det.STGE_LOC = edDefaultLoc.getText().toString().substring(0,4);
        gr_det.WBS_ELEM = edWBS.getText().toString();
        //gr_det.MOVE_STLOC = edSloc.getText().toString();
        gr_det.MOVE_STLOC = edDefaultLoc.getText().toString().substring(0,4);
        gr_det.MATERIAL = po_det.MATERIAL;
        gr_det.MAT_DESC = po_det.MATL_DESC;
        gr_det.ENTRY_QNT = rcd_qty;
        gr_det.ENTRY_UOM_ISO = "";
        if(batchType!=null){
            gr_det.VAL_TYPE = batchType.Name;
        }
        gr_det.FROM_BIN_ID = po_det.MATBINID;
        gr_det.TO_BIN_ID = 0;
        App.getDatabaseClient().getAppDatabase().genericDao().insertSTK_TRN_PRJ_WHR_RECIEPT(gr_det);
        //po_det = App.getDatabaseClient().getAppDatabase().genericDao().getSTK_TRN_PRJ_WHR_RECIEPT();
        UpdateBatchView();
        resetUI();
        edScanCode.setText("");
        edScanCode.requestFocus();
    }

    private void UpdateBatchView() {
        List<STK_TRN_PRJ_WHR_RECIEPT221> grDetlist = App.getDatabaseClient().getAppDatabase().genericDao().getSTK_TRN_PRJ_WHR_RECIEPT();
        //List<GR_DET> detList = App.getDatabaseClient().getAppDatabase().genericDao().getGR_DET(HDR_ID, po_det.SLNO);
        batchesAdapter.addItems(grDetlist);
    }

    private void GetBatch() {
        batchList = new ArrayList<BATCH>();
        BATCH batch;

        batch = new BATCH();
        batch.Code = "01";
        batch.Name = "OLD";
        batchList.add(batch);

        batch = new BATCH();
        batch.Code = "02";
        batch.Name = "NEW";
        batchList.add(batch);


        batchTypeAdapter = new ArrayAdapter<>(this.getContext(), R.layout.dropdown_menu_popup_item, batchList);
        tvBatch.setAdapter(batchTypeAdapter);
        tvBatch.setSelection(0);
        tvBatch.setThreshold(100);
        tvBatch.setOnItemClickListener((parent, view, position, id) -> {
            if (position >= 0) {
                batchType = batchTypeAdapter.getItem(position);
            } else {
                batchType = null;
            }
        });
    }
}
