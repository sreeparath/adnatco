package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;

import com.dcode.inventory.R;

public class DownloadFragment
        extends BaseFragment {

    View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        root = inflater.inflate(R.layout.fragment_download, container, false);



        TextView tv_download_po = root.findViewById(R.id.tv_download_po);
        tv_download_po.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_download_po));


        TextView tv_bg_download_wo = root.findViewById(R.id.tv_bg_download_wo);
        tv_bg_download_wo.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_download_wo));

//        TextView tv_download_rwo = root.findViewById(R.id.tv_download_rewo);
//        tv_download_rwo.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_download_rwo));


        TextView tv_download_master = root.findViewById(R.id.tv_download_master);
        tv_download_master.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_download));

        TextView tv_bg_download_sc = root.findViewById(R.id.tv_bg_download_sc);
        tv_bg_download_sc.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_download_sc));

        return root;
    }

    private void OnModuleButtonClick(int action_id) {
        Navigation.findNavController(root).navigate(action_id);
    }

}
