package com.dcode.inventory.ui.view;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.data.model.CONFIRM_DOC_HDR;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.SALE_RET_DOC_HDR;
import com.dcode.inventory.network.model.GenericSubmissionResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ConfirmHeaderFragment extends BaseFragment {
    private long HDR_ID;
    private String DocNo;
    private DocType ordTyp;
    private CONFIRM_DOC_HDR poHdr;

    private TextView tvPOTypeName;
    private TextView tvDocType;
    private TextView tvDocNo;
    private TextView tvCLSup;
    private TextView tvCLName;
    private View root;
    private Switch swComplete;
    private TextInputEditText edRemarks;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Bundle bundle = null;
        if (this.getArguments() != null) {
            bundle = this.getArguments();
        }

        if (bundle != null) {
            ModuleID = bundle.getInt(AppConstants.MODULE_ID, 0);
            DocNo = bundle.getString(AppConstants.SELECTED_ID, DocNo);
            ordTyp = (DocType) bundle.getSerializable(AppConstants.SELECTED_CODE);
            HDR_ID = bundle.getLong(AppConstants.PARENT_ID, -1);
            poHdr = (CONFIRM_DOC_HDR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
        }

        root = inflater.inflate(R.layout.fragment_confirm_hdr, container, false);


        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

    private void setupView() {

        tvPOTypeName = root.findViewById(R.id.tvPOTypeName);
        tvDocType = root.findViewById(R.id.edType);
        tvDocNo = root.findViewById(R.id.edDocNo);
        tvCLSup = root.findViewById(R.id.edSupp);
        //tvCLName = root.findViewById(R.id.edCode);
        edRemarks = root.findViewById(R.id.edRemarks);
        swComplete = root.findViewById(R.id.swMasters);

        MaterialButton btDocNo = root.findViewById(R.id.btnNext);
        btDocNo.setOnClickListener(v -> OnNextClick());

        //if (ordTyp != null && DocNo.length() != 0) {
        //    poHdr = App.getDatabaseClient().getAppDatabase().genericDao().getCONFIRM_DOC_HDR(DocNo, ordTyp.CODE);
        //}

        if (poHdr != null) {
            //tvCLName.setText(poHdr.CLNAME);
            tvCLSup.setText(poHdr.CLSUP);
            tvDocNo.setText(poHdr.ORDNO);
            tvDocType.setText(poHdr.ORTYP);
            tvPOTypeName.setText(poHdr.POTYPENAME);
        }

        MaterialButton btnSubmit = root.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(v -> {

            if (poHdr != null) {
                String remarks = edRemarks.getText().toString();
                if(!swComplete.isChecked()&& remarks.length()<1){
                    showToast("Remarks not be empty!");
                    return;
                }

                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Are you sure want to upload to server ?");
                alert.setPositiveButton("OK", (dialog, whichButton) -> {

                    UploadPending(remarks);
                });
                alert.setNegativeButton("Cancel", (dialog, whichButton) -> {

                });
                alert.show();
            }else{
                showAlert("Message","No data to post!");
                return;
            }
        });
    }

    private void UploadPending(String remarks) {
        String isComplete = "";
        if(swComplete.isChecked()){
            isComplete="Y";
        }else{
            isComplete="N";
        }

        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LoadSlips.GetConfirmData(poHdr.ORDNO,isComplete,remarks);

        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLines(requestObject, new Callback<GenericSubmissionResponse>() {
            @Override
            public void success(GenericSubmissionResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String ReceiptNo = String.valueOf(genericSubmissionResponse.getRetId());
                        //edReceiptNo.setTag(ReceiptNo);
                        //edReceiptNo.setText(ReceiptNo);
                        //ServiceUtils.SyncActivity.truncateLocalTables(AppConstants.DatabaseEntities.PUR_RET_ITEMS_RECEIPT,recieptNo, genericSubmissionResponse.getRetId());
                        //String[] msg = genericSubmissionResponse.getErrMessage().split("~");
                        String msg = genericSubmissionResponse.getErrMessage();
                        //resetUI();
                        showToastLong(msg);
//                        if (msg.length > 1) {
//                            ReceiptNo = msg[2];
//                        }
                        getActivity().onBackPressed();
                        Bundle bundle = new Bundle();
                        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
                        bundle.putSerializable(AppConstants.SELECTED_ID, msg);
                        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_confirm, bundle);
                    } else {
                        showAlert("Error",genericSubmissionResponse.getErrMessage());
                        //showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showToast(genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }

    private void OnNextClick() {
        Bundle bundle = new Bundle();
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_CODE, ordTyp);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, poHdr);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_confirm_details, bundle);
    }
}
