package com.dcode.inventory.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.data.model.LS_ITEMS;

import java.util.ArrayList;
import java.util.List;

public class LoadSlipItemsAdapter
        extends RecyclerView.Adapter<LoadSlipItemsAdapter.RecyclerViewHolder>
        implements Filterable {

    private List<LS_ITEMS> detailsList;
    private List<LS_ITEMS> detailsListFull;
    private View.OnClickListener shortClickListener;
    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<LS_ITEMS> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(detailsListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (LS_ITEMS item : detailsListFull) {
                    if ((item.ITDESC != null && item.ITDESC.toLowerCase().contains(filterPattern)) ||
                            (item.ITCODE != null && item.ITCODE.toLowerCase().contains(filterPattern))) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            detailsList.clear();
            detailsList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public LoadSlipItemsAdapter(List<LS_ITEMS> dataList, View.OnClickListener shortClickListener) {
        this.detailsList = dataList;
        this.detailsListFull = new ArrayList<>(dataList);
        this.shortClickListener = shortClickListener;
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public LoadSlipItemsAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_items, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        final LS_ITEMS po_det = detailsList.get(position);

        holder.tvCode.setText(po_det.ITCODE);
        holder.tvName.setText(po_det.ITDESC);
        holder.tvOriginalQty.setText(String.valueOf(po_det.ITQTY));
        holder.tvPendingQty.setText(String.valueOf(po_det.TOTQTY));
        holder.tvReceiveQty.setText(String.valueOf(po_det.RCQTY));
        holder.tvPackaging.setText(po_det.PACKING);
        holder.tvUnits.setText(po_det.ITUNIT);

        holder.itemView.setTag(po_det);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return detailsList.size();
    }

    public void addItems(List<LS_ITEMS> dataList) {
        this.detailsList = dataList;
        this.detailsListFull = new ArrayList<>(dataList);
        notifyDataSetChanged();
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCode;
        private TextView tvName;
        private TextView tvOriginalQty;
        private TextView tvPendingQty;
        private TextView tvReceiveQty;
        private TextView tvPackaging;
        private TextView tvUnits;

        RecyclerViewHolder(View view) {
            super(view);
            tvCode = view.findViewById(R.id.tvCode);
            tvName = view.findViewById(R.id.tvName);
            tvOriginalQty = view.findViewById(R.id.tvOriginalQty);
            tvPackaging = view.findViewById(R.id.tvPackaging);
            tvUnits = view.findViewById(R.id.tvUnits);
        }
    }
}
