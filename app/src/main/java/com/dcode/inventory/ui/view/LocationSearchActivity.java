package com.dcode.inventory.ui.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.data.model.LOCATIONS;
import com.dcode.inventory.ui.adapter.LocationsAdapter;

import java.util.ArrayList;
import java.util.Objects;

public class LocationSearchActivity
        extends BaseActivity
        implements View.OnClickListener, SearchView.OnQueryTextListener {
    private int Master_ID;
    private int Parent_ID;
    private String filterString = "";
    private LocationsAdapter objectsAdapter;
    private int[] fkey_intArray;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_search;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        setupRecycleView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_app_bar, menu);

        final MenuItem searchItem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        objectsAdapter.getFilter().filter(newText);
        filterString = newText;
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setResult(RESULT_CANCELED);
        finishAndRemoveTask();
    }

    @Override
    public void onClick(View view) {
        Bundle bundle = new Bundle();

        LOCATIONS objectSearch = (LOCATIONS) view.getTag();
        bundle.putString(AppConstants.SELECTED_CODE, objectSearch.LOCODE);
        bundle.putString(AppConstants.SELECTED_NAME, objectSearch.LONAME);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, objectSearch);

        Intent intent = this.getIntent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finishAndRemoveTask();
    }

    private void setupRecycleView() {
        objectsAdapter = new LocationsAdapter(LocationSearchActivity.this, new ArrayList<>(), this);
        objectsAdapter.addItems(App.getDatabaseClient().getAppDatabase().genericDao().getAllLocations(App.currentCompany.Code));

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(objectsAdapter);
    }
}
