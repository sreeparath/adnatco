package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.PO_HDR;
import com.dcode.inventory.data.model.SALE_RET_DOC_HDR;
import com.google.android.material.button.MaterialButton;

public class SaleRetHeaderFragment extends BaseFragment {
    private long HDR_ID;
    private String DocNo;
    private DocType ordTyp;
    private SALE_RET_DOC_HDR poHdr;

    private TextView tvPOTypeName;
    private TextView tvDocType;
    private TextView tvDocNo;
    private TextView tvCLSup;
    private TextView tvCLName;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Bundle bundle = null;
        if (this.getArguments() != null) {
            bundle = this.getArguments();
        }

        if (bundle != null) {
            ModuleID = bundle.getInt(AppConstants.MODULE_ID, 0);
            DocNo = bundle.getString(AppConstants.SELECTED_ID, DocNo);
            ordTyp = (DocType) bundle.getSerializable(AppConstants.SELECTED_CODE);
            HDR_ID = bundle.getLong(AppConstants.PARENT_ID, -1);
        }

        View view = inflater.inflate(R.layout.fragment_sale_ret_hdr, container, false);
        tvPOTypeName = view.findViewById(R.id.tvPOTypeName);
        tvDocType = view.findViewById(R.id.edType);
        tvDocNo = view.findViewById(R.id.edDocNo);
        tvCLSup = view.findViewById(R.id.edSupp);
        tvCLName = view.findViewById(R.id.edCode);

        MaterialButton btDocNo = view.findViewById(R.id.btnNext);
        btDocNo.setOnClickListener(v -> OnNextClick());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

    private void setupView() {
        if (ordTyp != null && DocNo.length() != 0) {
            poHdr = App.getDatabaseClient().getAppDatabase().genericDao().getSALE_RET_DOC_HDR(DocNo, ordTyp.CODE);
        }

        if (poHdr != null) {
            tvCLName.setText(poHdr.CLNAME);
            tvCLSup.setText(poHdr.CLSUP);
            tvDocNo.setText(poHdr.ORDNO);
            tvDocType.setText(poHdr.ORTYP);
            tvPOTypeName.setText(poHdr.POTYPENAME);
        }
    }

    private void OnNextClick() {
        Bundle bundle = new Bundle();
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_CODE, ordTyp);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_sale_ret_details, bundle);
    }
}
