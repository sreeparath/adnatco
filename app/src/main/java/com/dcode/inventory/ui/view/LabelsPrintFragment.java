package com.dcode.inventory.ui.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.BARCODE_PRINTERS;
import com.dcode.inventory.data.model.GENERIC_OBJECT;
import com.dcode.inventory.data.model.GR_ITEM_PRINT_TYP;
import com.dcode.inventory.data.model.ITEMS_PRINT;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.model.GenericSubmissionResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.dcode.inventory.ui.adapter.ItemsLabelsPrintAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LabelsPrintFragment
        extends BaseFragment
        implements View.OnClickListener {

    private String ReceiptNo;
    private View root;
    private AutoCompleteTextView tvPrinter;
    private AutoCompleteTextView tvLabel;
    private List<ITEMS_PRINT> itemsPrintList;
    private List<GR_ITEM_PRINT_TYP> grItemPrintTyp;
    private ItemsLabelsPrintAdapter itemsLabelsPrintAdapter;
    private ArrayAdapter<BARCODE_PRINTERS> printersAdapter;
    private ArrayAdapter<GENERIC_OBJECT> labelsAdapter;
    private BARCODE_PRINTERS barcode_printers;
    private GENERIC_OBJECT label_formats;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        ReceiptNo = "";
        if (this.getArguments() != null) {
            Bundle bundle = this.getArguments();
            ReceiptNo = bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getString(AppConstants.SELECTED_ID, "") : "";
        }

        root = inflater.inflate(R.layout.fragment_labels_print, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (ReceiptNo.length() <= 0) {
            final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            alert.setTitle("Please Enter Receipt No");
            final EditText input = new EditText(getContext());
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            input.setRawInputType(Configuration.KEYBOARD_12KEY);
            alert.setView(input);
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //Put actions for OK button here
                    String inputValue = input.getText().toString().trim();
                    if (inputValue.length() <= 0) {
                        showToast("Invalid Receipt No");
                        requireActivity().onBackPressed();
                    } else {
                        ReceiptNo = inputValue;
                        DownloadPrintList();
                    }
                }
            });
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //Put actions for CANCEL button here, or leave in blank
                }
            });
            alert.show();
        } else {
            DownloadPrintList();
        }

        List<BARCODE_PRINTERS> printersList = App.getDatabaseClient().getAppDatabase().genericDao().getAllBarcodePrinters();
        printersAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, printersList);
        tvPrinter = root.findViewById(R.id.tvPrinter);
        tvPrinter.setAdapter(printersAdapter);
        tvPrinter.setSelection(0);
        tvPrinter.setThreshold(100);
        tvPrinter.setOnItemClickListener((parent, dropdownView, position, id) -> {
            if (position >= 0) {
                barcode_printers = printersAdapter.getItem(position);
            } else {
                barcode_printers = null;
            }
        });
        if(printersAdapter.getCount()>0) {
            tvPrinter.setText(printersAdapter.getItem(0).toString());
            barcode_printers = printersAdapter.getItem(0);
        }

        List<GENERIC_OBJECT> formatsList = App.getDatabaseClient().getAppDatabase().genericDao().getAllLabelFormats();
        labelsAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, formatsList);
        tvLabel = root.findViewById(R.id.tvLabel);
        tvLabel.setAdapter(labelsAdapter);
        tvLabel.setSelection(0);
        tvLabel.setThreshold(100);
        tvLabel.setOnItemClickListener((parent, dropdownView, position, id) -> {
            if (position >= 0) {
                label_formats = labelsAdapter.getItem(position);
            } else {
                label_formats = null;
            }
        });
        if(labelsAdapter.getCount()>0){
            tvLabel.setText(labelsAdapter.getItem(0).toString());
            label_formats = labelsAdapter.getItem(0);
        }


        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        itemsLabelsPrintAdapter = new ItemsLabelsPrintAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(itemsLabelsPrintAdapter);
        itemsLabelsPrintAdapter.addItems(itemsPrintList);

        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> onPrintClick());
    }

    @Override
    public void onClick(View view) {
        ITEMS_PRINT items_print = (ITEMS_PRINT) view.getTag();
        if (items_print != null) {
            final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            alert.setTitle("Please Enter Labels count");
            final EditText input = new EditText(getContext());
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            input.setRawInputType(Configuration.KEYBOARD_12KEY);
            alert.setView(input);
            alert.setPositiveButton("Next", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //Put actions for OK button here
                    String inputValue = input.getText().toString().trim();
                    if (inputValue.length() <= 0) {
                        showToast("Invalid Labels count");
                    } else {
                        int count = Utils.convertToInt(inputValue, items_print.QTY);
                        //if (count > items_print.ITQTY) {
                        //    showToast("Cannot exceed receipt");
                        //    count = items_print.ITQTY;
                       //}
                        items_print.QTY = count;
                        itemsLabelsPrintAdapter.notifyDataSetChanged();
                    }
                }
            });
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //Put actions for CANCEL button here, or leave in blank
                }
            });
            alert.show();
        }
    }

    private void DownloadPrintList() {
        if (ReceiptNo.length() <= 0) {
            return;
        }

        JsonObject requestObject = ServiceUtils.GoodsReceipt.GetGR_ItemsPrintList(ReceiptNo,"G");

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        ITEMS_PRINT[] items_prints = new Gson().fromJson(xmlDoc, ITEMS_PRINT[].class);
                        itemsPrintList = Arrays.asList(items_prints);
                        itemsLabelsPrintAdapter.addItems(itemsPrintList);
                        itemsLabelsPrintAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

    private void onPrintClick() {
        if (IsValid()) {
            showProgress(false);
            Gson gson = new Gson();

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(ServiceUtils.createJsonObject("@USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(ServiceUtils.createJsonObject("@RECEIPTNO", "22", ReceiptNo));
            jsonArray.add(ServiceUtils.createJsonObject("@LINES", "30", "pdt.GR_ITEM_PRINT_TYP", gson.toJson(grItemPrintTyp)));
            jsonArray.add(ServiceUtils.createJsonObject("@PRINT_TYPE", "22", "G"));
            jsonArray.add(ServiceUtils.createJsonObject("@COMP_CODE", "22", App.currentCompany.Code));

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "pdt.GR_ITEM_PRINT_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", jsonArray);

            JsonObject requestObject = new JsonObject();
            requestObject.add("labelFormat", gson.toJsonTree(label_formats));
            requestObject.add("printer", gson.toJsonTree(barcode_printers));
            requestObject.add("sqlpram", gson.toJsonTree(jsonObject));


            App.getNetworkClient().getAPIService().PrintBarcodeLabel(requestObject, new Callback<GenericSubmissionResponse>() {
                @Override
                public void success(GenericSubmissionResponse genericSubmissionResponse, Response response) {
                    String msg = "Done";
                    if (!genericSubmissionResponse.getErrCode().equals("S")) {
                        msg = genericSubmissionResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showAlert("Error", msg);
                    } else {
                        showToast(msg);
                    }
                    dismissProgress();
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                    dismissProgress();
                }
            });
        }
    }

    private boolean IsValid() {
        if (barcode_printers == null || barcode_printers.PrinterID <= 0) {
            showToast("Please select a printer");
            tvPrinter.setError("Select a printer");
            return false;
        }

        if (label_formats == null || label_formats.ID <= 0) {
            showToast("Please select label format");
            tvLabel.setError("Select label format");
            return false;
        }

        itemsPrintList = itemsLabelsPrintAdapter.getItems();
        grItemPrintTyp = new ArrayList<>();
        if(itemsPrintList!=null) {
            for (ITEMS_PRINT items : itemsPrintList) {
                if (items.QTY > 0) {
                    GR_ITEM_PRINT_TYP gr_item = new GR_ITEM_PRINT_TYP();
                    gr_item.SLNO = items.SLNO;
                    gr_item.ITCODE = items.ITCODE;
                    gr_item.QTY = items.QTY;
                    grItemPrintTyp.add(gr_item);
                }
            }
        }

        if (grItemPrintTyp.size() <= 0) {
            showAlert("Information", "Nothing to print");
            return false;
        }
        return true;
    }
}
