package com.dcode.inventory.ui.view;

import android.app.DatePickerDialog;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.GR_DET;
import com.dcode.inventory.data.model.LS_HDR;
import com.dcode.inventory.data.model.LS_ITEMS;
import com.dcode.inventory.data.model.PICK_HDR;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.SLIP_DET;
import com.dcode.inventory.data.model.SLIP_HDR;
import com.dcode.inventory.data.model.USERS;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.model.GenericSubmissionResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.dcode.inventory.ui.adapter.LoadSlipItemsAdapter;
import com.dcode.inventory.ui.adapter.PickListItemsAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PickListItemsFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    PICK_HDR pl_hdr;
    SLIP_HDR slip_hdr;
    List<PICK_LIST_ITEMS> pl_itemsList;
    PICK_LIST_ITEMS pl_list_items;
    private TextInputEditText edScanCode;
    private TextInputEditText edRefNo;
    private TextInputEditText edRefDate;
    private TextInputEditText edVehicle;
    private TextInputEditText edDriver;
    private TextInputEditText edHelper;
    private TextInputEditText edRemarks;
    private TextInputEditText edReceiptNo;
    private View root;
    private PickListItemsAdapter pickItemsAdapter;
    private FloatingActionButton proceed;
    private TextInputEditText edPostRefNo;
    private TextInputEditText edPostRemarks;
    private MaterialButton btnPostReceipt;
    private TextInputEditText edSearch;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                pl_hdr = (PICK_HDR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_pick_list_items_temp, container, false);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        pickItemsAdapter = new PickListItemsAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(pickItemsAdapter);

        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                pickItemsAdapter.getFilter().filter(s);

            }
        });


        DownloadPL_Items();
        //loadData();
    }

    private void setupView() {
//        edScanCode = root.findViewById(R.id.edScanCode);
//        edScanCode.setOnKeyListener((v, keyCode, event) -> {
//            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
//                    keyCode == KeyEvent.KEYCODE_ENTER) {
//                return onScanCodeKeyEvent();
//            }
//            return false;
//        });
        edSearch = root.findViewById(R.id.search);
        edSearch.requestFocus();
        proceed = (FloatingActionButton) root.findViewById(R.id.proceed);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putInt(AppConstants.MODULE_ID, ModuleID);
                bundle.putSerializable(AppConstants.SELECTED_OBJECT, pl_hdr);

                ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_load_pick_list_receipt, bundle);
            }
        });
        edPostRefNo = root.findViewById(R.id.edRefNo);
        edPostRemarks = root.findViewById(R.id.edRemarks);

        btnPostReceipt = root.findViewById(R.id.btnPostReceipt);
        btnPostReceipt.setOnClickListener(v -> onPostReceiptClick());

}

    private void onPostReceiptClick() {
        String errMessage;
        List<PICK_LIST_ITEMS_RECEIPT> plReceipts = App.getDatabaseClient().getAppDatabase().genericDao().GetPL_ITEMS_RECEIPT(pl_hdr.ORD_NO);
        List<PICK_LIST_ITEMS> plItems = App.getDatabaseClient().getAppDatabase().genericDao().GetPICKLIST_ITEMS_PENDING(pl_hdr.ORD_NO);

        if (plReceipts.size() < 1) {
            errMessage = "Nothing to post.";
            showAlert("Information", errMessage);
            return;
        }

        ArrayList itemList = new ArrayList();

        if(plItems.size() > 0){
            boolean pending = false;
            boolean noRemark = false;
            for (PICK_LIST_ITEMS rec :plItems ) {
                if((rec.QTY - rec.REC_QTY) > 0){
                    Log.d("itemRec.REMARKS## 1  ",rec.ITCODE );
                    for (PICK_LIST_ITEMS_RECEIPT itemRec : plReceipts) {
                        if(rec.ITCODE.equals(itemRec.ITCODE)) {
                            Log.d("itemRec.REMARKS## ",itemRec.ITCODE+" "+itemRec.REMARKS);
                            if (!(itemRec.REMARKS != null && itemRec.REMARKS.length() > 0)) {
                                pending = true;
                            }
                        }

                    }
                }
                Log.d("itemList##    ",String.valueOf(itemList));
                Log.d("pending##    ",String.valueOf(pending)+" "+ rec.ITCODE);
                Log.d("pending 1 ##    ",String.valueOf(itemList.contains(rec.ITCODE)));
                if(rec.REC_QTY==0 && !pending) {
                    PICK_LIST_ITEMS_RECEIPT plexist = App.getDatabaseClient().getAppDatabase().genericDao().getPickListItemReceipt(rec.ORD_NO,rec.ITCODE);
                    if(!(plexist!=null))
                        pending = true;

                }


            }

            if(pending){
                errMessage = "Partial picking not Allowed or Add Remarks!";
                showAlert("Information", errMessage);
                return;
            }
        }
        

        String postRecNo = edPostRefNo.getText().toString().trim();
        if (postRecNo.length() == 0) {
            errMessage = "Invalid Reference no";
            edPostRefNo.setError(errMessage);
            showToast(errMessage);
            return;
        }

        String postRemarks = edPostRemarks.getText().toString().trim();

        UploadPending();
    }

    /*public void onCopyDatabase() {
        USERS objUsr = App.getDatabaseClient().getAppDatabase().genericDao().getAllUsers();
        if (objUsr == null || objUsr.USER_NAME.length() == 0) {
            showToast("Current DB appears empty. Will not copy!!");
            return;
        }

        boolean copyresult = false;
        try {
            App.getDatabaseClient().getAppDatabase().close();

            String src = getDatabasePath("UPPDATA").getAbsolutePath();
            String filename = String.format("%s_%s_%s.db", objUsr.USER_NAME,
                    objUsr.USER_NAME, objUsr.USER_NAME);

            String tgt = Uri.withAppendedPath(Uri.parse(edExportDbPath.getText().toString()), filename).getPath();

            try {
                copyresult = copyAppDbToDownloadFolder(src, tgt);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (copyresult) {
                showToast("Done.");
            }

        } catch (Exception e) {
            showToast(e.getMessage());
            e.printStackTrace();
        }
    }*/

    public boolean copyAppDbToDownloadFolder(String src, String tgt) throws IOException {

        File backupDB = new File(tgt);
        backupDB.setReadable(true, false);
        if (!backupDB.exists()) {
            backupDB.createNewFile();
        }

        FileInputStream fis = new FileInputStream(src);
        FileOutputStream fos = new FileOutputStream(backupDB);
        fos.getChannel().transferFrom(fis.getChannel(), 0, fis.getChannel().size());
        fis.close();
        fos.close();
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        pickItemsAdapter.getFilter().filter(newText);
        String filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
//        LS_ITEMS ls_items = (LS_ITEMS) view.getTag();
//
//        if (ls_items == null) {
//            return;
//        }

        //NavigateToItemDetails(ls_items, "");
    }

    /*private void NavigateToItemDetails(LS_ITEMS ls_items, String scanCode) {
        if (App.ls_hdr_id <= 0) {
            slip_hdr = new SLIP_HDR();
            slip_hdr.GUID = Utils.GetGUID();
            slip_hdr.IS_UPLOADED = 0;
            slip_hdr.ORDNO = ls_hdr.LSNO;
            slip_hdr.ORTYP = ls_hdr.ORTYP;
            slip_hdr.YRCD = ls_hdr.YRCD;
            slip_hdr.LOCODE = App.currentLocation.LOCODE;
            slip_hdr.PRICING = ls_hdr.PRICING;

            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_HDR(ls_hdr);;
            LS_ITEMS[] array = new LS_ITEMS[ls_itemsList.size()];
            ls_itemsList.toArray(array);
            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_ITEMS(array);

            long HDR_ID = App.getDatabaseClient().getAppDatabase().genericDao().insertSlip_HDR(slip_hdr);
            App.ls_hdr_id = HDR_ID;
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, ls_items);
        bundle.putString(AppConstants.SELECTED_CODE, scanCode);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_load_slip_item_detail, bundle);
    }*/

    private boolean onScanCodeKeyEvent() {
        String ScanCode = edScanCode.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid bar code");
            return false;
        } else {
            ParseBarCode(ScanCode);
        }
        return true;
    }

    private void onClickSave() {
        SaveData();
    }

    private void loadData() {
        if (pl_hdr == null) {
            showToast("Some error");
            return;
        }

        pl_itemsList = App.getDatabaseClient().getAppDatabase().genericDao().GetPICKLIST_ITEMS(pl_hdr.ORD_NO);
        if (pl_itemsList != null && pl_itemsList.size()>0 ) {
            pickItemsAdapter.addItems(pl_itemsList);
            btnPostReceipt.setEnabled(true);
            proceed.setEnabled(true);
        }
        else {
            btnPostReceipt.setEnabled(false);
            proceed.setEnabled(false);
            showToast(" Alert : No data found.");
        }
    }

    private void DownloadPL_Items() {
        showProgress(false);

        JsonObject jsonObject;
        JsonArray array = new JsonArray();
        jsonObject = ServiceUtils.createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
        array.add(jsonObject);
        jsonObject = ServiceUtils.createJsonObject("LOCODE", "22", App.currentLocation.LOCODE);
        array.add(jsonObject);
        jsonObject = ServiceUtils.createJsonObject("ORDNO", "22", pl_hdr.ORD_NO);
        array.add(jsonObject);

        JsonObject requestObject = new JsonObject();
        requestObject.addProperty("ProcName", "PDT.PICK_SLIP_ITEMS_SPR");
        requestObject.addProperty("DBName", App.currentCompany.DbName);
        requestObject.add("dbparams", array);

        System.out.println("requestObject##"+requestObject.toString());

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        PICK_LIST_ITEMS[] pl_items = new Gson().fromJson(xmlDoc, PICK_LIST_ITEMS[].class);
                        App.getDatabaseClient().getAppDatabase().genericDao().insertPL_ITEMS(pl_items);
                        //pl_itemsList = Arrays.asList(pl_items);
                        //pickItemsAdapter.addItems(pl_itemsList);
                        loadData();
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showToast(msg);
            }
        });
    }

    private void SaveData() {
        List<SLIP_DET> slipDetList = App.getDatabaseClient().getAppDatabase().genericDao().getSLIP_DETByHDR_ID(App.ls_hdr_id);
        if (slipDetList.size() < 1) {
            showToast("Nothing to post.");
            App.getDatabaseClient().getAppDatabase().genericDao().deleteGR_HDR(App.ls_hdr_id);
            return;
        }

        String refNo = edRefNo.getText().toString().trim();
        if (refNo.length() == 0) {
            showToast("Invalid Reference no");
            return;
        }

        String refDate = edRefDate.getText().toString().trim();
        if (refDate.length() == 0) {
            showToast("Invalid Reference date");
            return;
        }

        String vehicle = edVehicle.getText().toString().trim();
        String driver = edDriver.getText().toString().trim();
        String helper = edHelper.getText().toString().trim();
        String remarks = edRemarks.getText().toString().trim();

        if (slip_hdr != null) {
            slip_hdr.DEVICE_DATE = Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT);
            slip_hdr.REFNO = refNo;
            slip_hdr.REFDT = refDate;
            slip_hdr.NEW_VEH_NO = vehicle;
            slip_hdr.NEW_DRIVER = driver;
            slip_hdr.NEW_HELPER = helper;
            slip_hdr.REMARKS = remarks;
            App.getDatabaseClient().getAppDatabase().genericDao().insertSlip_HDR(slip_hdr);
        }

        UploadPending();
    }

    private void UploadPending() {
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.PicklList.GetPicklList(pl_hdr.ORD_NO,edPostRefNo.getText().toString().trim(),
                edPostRemarks.getText().toString().trim());
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLines(requestObject, new Callback<GenericSubmissionResponse>() {
            @Override
            public void success(GenericSubmissionResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String ReceiptNo = String.valueOf(genericSubmissionResponse.getRetId());
                        //edRefNo.setTag(ReceiptNo);
                        ServiceUtils.SyncActivity.truncateLocalTables(AppConstants.DatabaseEntities.PICK_LIST_ITEMS,pl_hdr.ORD_NO, genericSubmissionResponse.getRetId());
                        ServiceUtils.SyncActivity.truncateLocalTables(AppConstants.DatabaseEntities.PICK_LIST_ITEMS_RECEIPT,null, genericSubmissionResponse.getRetId());
                        String[] msg = genericSubmissionResponse.getErrMessage().split("~");
                        edPostRefNo.setText("");
                        edPostRemarks.setText("");
                        showToast(msg[0]);
                        getActivity().onBackPressed();
                    } else {
                        showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showToast(genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }

    private void ParseBarCode(String scanCode) {
        if (scanCode.length() <= 0 ) {
            return;
        }
        String itcode = "";

        if (!scanCode.startsWith(App.BarCodeSeparator)) {
            // find item by itcode
            itcode = scanCode;
        } else {
            /*  1. ITCODE, 2. BatchNo, 3. MFD Date, 4. EXP Date, 5. Qty, 6. RefDoc */
            String[] barCodeData = scanCode.split(App.BarCodeSeparator);
            if (barCodeData.length > 2) {
                itcode = barCodeData[1];
            }
        }

//        LS_ITEMS ls_items = App.getDatabaseClient().getAppDatabase().genericDao().getLS_Items(ls_hdr.LSNO, ls_hdr.ORTYP, itcode);
//        if (ls_items != null && ls_items.ORDNO.length() > 0) {
//           // NavigateToItemDetails(ls_items, scanCode);
//        } else {
//            edScanCode.setText("");
//            showToast("Item not available in document");
//        }
    }
}
