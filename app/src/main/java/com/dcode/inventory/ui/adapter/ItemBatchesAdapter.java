package com.dcode.inventory.ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.data.model.AD_PO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.GR_DET;
import com.dcode.inventory.data.model.STORE_TRANSFER_ITEMS_RECEIPT;

import java.util.ArrayList;
import java.util.List;

public class ItemBatchesAdapter
        extends RecyclerView.Adapter<ItemBatchesAdapter.RecyclerViewHolder> {

    private List<AD_PO_ITEMS_RECIEPT> grDetList;
    private View.OnClickListener shortClickListener;
//    private List<GR_DET> grDetListFull;

    public ItemBatchesAdapter(List<AD_PO_ITEMS_RECIEPT> detList) {
        this.grDetList = detList;
        setHasStableIds(true);
//        grDetListFull = new ArrayList<>(detList);
    }

    public ItemBatchesAdapter(List<AD_PO_ITEMS_RECIEPT> dataList, View.OnClickListener shortClickListener) {
        this.grDetList = dataList;
        ///this.receiptsListFull = new ArrayList<>(dataList);
        this.shortClickListener = shortClickListener;
    }

    @NonNull
    @Override
    public ItemBatchesAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_grn_receipt, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemBatchesAdapter.RecyclerViewHolder holder, int position) {
        final AD_PO_ITEMS_RECIEPT gr_det = grDetList.get(position);

        holder.tvReceiptQty.setText(String.valueOf(gr_det.RCQTY));
        holder.tvBatch.setText(gr_det.BATCH);
        holder.tvCode.setText(String.valueOf(gr_det.MATERIAL));
        holder.tvDesc.setText(String.valueOf(gr_det.SHORT_TEXT));

        holder.itemView.setTag(gr_det);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return grDetList.size();
    }

    public void addItems(List<AD_PO_ITEMS_RECIEPT> detList) {
        this.grDetList = detList;
//        this.grDetListFull = new ArrayList<>(detList);

        notifyDataSetChanged();
    }

    public void addItems(AD_PO_ITEMS_RECIEPT gr_det) {
        this.grDetList.add(gr_det);
//        this.grDetListFull = new ArrayList<>(grDetList);

        notifyDataSetChanged();
    }

//    public List<GR_DET> getFullList() {
//        return grDetListFull;
//    }

    public AD_PO_ITEMS_RECIEPT getItemByPosition(int position) {
        return this.grDetList.get(position);
    }

    public void deleteBatch(int position) {
        try {
//            grDetListFull.remove(position);
            grDetList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, grDetList.size());
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvReceiptQty;
        private TextView tvCode;
        private TextView tvBatch;
        private TextView tvDesc;

        RecyclerViewHolder(View view) {
            super(view);
            tvReceiptQty = view.findViewById(R.id.tvReceiptQty);
            tvCode = view.findViewById(R.id.tvCode);
            tvBatch = view.findViewById(R.id.tvBatch);
            tvDesc = view.findViewById(R.id.tvDesc);
        }
    }
}
