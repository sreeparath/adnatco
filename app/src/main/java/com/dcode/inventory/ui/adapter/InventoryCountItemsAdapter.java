package com.dcode.inventory.ui.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.data.model.LOAD_SLIP_ITEMS_NEW;
import com.dcode.inventory.data.model.STOCK_HEADER;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

public class InventoryCountItemsAdapter
        extends RecyclerView.Adapter<InventoryCountItemsAdapter.RecyclerViewHolder>
        implements Filterable {
    private List<STOCK_HEADER> plItemsList;
    private List<STOCK_HEADER> plItemsListFull;
    private View.OnClickListener shortClickListener;
    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<STOCK_HEADER> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(plItemsListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (STOCK_HEADER item : plItemsListFull) {
                    if ((item.ORDNO != null && item.ORDNO.toLowerCase().contains(filterPattern)) ||
                            (item.ORTYP != null && item.ORTYP.toLowerCase().contains(filterPattern)) ) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            plItemsList.clear();
            if (results != null && results.values != null) {
                plItemsList.addAll((List) results.values);
            }
            notifyDataSetChanged();
        }
    };

    public InventoryCountItemsAdapter(List<STOCK_HEADER> dataList, View.OnClickListener shortClickListener) {
        this.plItemsList = dataList;
        this.shortClickListener = shortClickListener;
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public InventoryCountItemsAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_stock_head_items, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull InventoryCountItemsAdapter.RecyclerViewHolder holder, int position) {
        final STOCK_HEADER ls_hdr = plItemsList.get(position);
        holder.tvOrdType.setText(ls_hdr.ORTYP);
        holder.tvOrdNo.setText(ls_hdr.ORDNO);
        holder.tvOrdDt.setText(ls_hdr.ORDTE);
        holder.tvRefNo.setText(ls_hdr.REFNO);
        holder.tvItbrand.setText(ls_hdr.ITBRND);

        holder.itemView.setTag(ls_hdr);
        holder.itemView.setOnClickListener(shortClickListener);



    }

    @Override
    public int getItemCount() {
        return plItemsList.size();
    }

    public void addItems(List<STOCK_HEADER> dataList) {
        this.plItemsList = new ArrayList<>(dataList);
        this.plItemsListFull = new ArrayList<>(dataList);

        notifyDataSetChanged();
    }

    private OnItemClickListener mListener;
    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    static class RecyclerViewHolder
            extends RecyclerView.ViewHolder  {
        private TextView tvOrdType;
        private TextView tvOrdNo;
        private TextView tvOrdDt;
        private TextView tvRefNo;
        private TextView tvItbrand;


        RecyclerViewHolder(View view) {
            super(view);
            tvOrdType = view.findViewById(R.id.tvOrdType);
            tvOrdNo = view.findViewById(R.id.tvOrdNo);
            tvOrdDt = view.findViewById(R.id.tvOrdDt);
            tvRefNo = view.findViewById(R.id.tvRefNo);
            tvItbrand = view.findViewById(R.id.tvItbrand);
        }
    }
}
