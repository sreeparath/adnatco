package com.dcode.inventory.ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.data.model.GR_DET;
import com.dcode.inventory.data.model.SALE_RET_DET;

import java.util.List;

public class SaleRetItemRecAdapter
        extends RecyclerView.Adapter<SaleRetItemRecAdapter.RecyclerViewHolder> {

    private List<SALE_RET_DET> grDetList;
    private View.OnClickListener shortClickListener;
//    private List<GR_DET> grDetListFull;

    public SaleRetItemRecAdapter(List<SALE_RET_DET> detList) {
        this.grDetList = detList;
        setHasStableIds(true);
//        grDetListFull = new ArrayList<>(detList);
    }

    public SaleRetItemRecAdapter(List<SALE_RET_DET> dataList, View.OnClickListener shortClickListener) {
        this.grDetList = dataList;
        ///this.receiptsListFull = new ArrayList<>(dataList);
        this.shortClickListener = shortClickListener;
    }

    @NonNull
    @Override
    public SaleRetItemRecAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_batch, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SaleRetItemRecAdapter.RecyclerViewHolder holder, int position) {
        final SALE_RET_DET gr_det = grDetList.get(position);

        holder.tvReceiptQty.setText(String.valueOf(gr_det.RCQTY));
        holder.tvReceiptType.setText(gr_det.REC_DESC);
        holder.tvBatchNo.setText(String.valueOf(gr_det.BATCHNO));
        holder.tvMFDDate.setText(String.valueOf(gr_det.MANFDATE));
        holder.tvEXPDate.setText(String.valueOf(gr_det.EXPDATE));
        holder.itemView.setTag(gr_det);
        holder.itemView.setOnClickListener(shortClickListener);

//        holder.itemView.setTag(gr_det);
    }

    @Override
    public int getItemCount() {
        return grDetList.size();
    }

    public void addItems(List<SALE_RET_DET> detList) {
        this.grDetList = detList;
//        this.grDetListFull = new ArrayList<>(detList);

        notifyDataSetChanged();
    }

    public void addItems(SALE_RET_DET gr_det) {
        this.grDetList.add(gr_det);
//        this.grDetListFull = new ArrayList<>(grDetList);

        notifyDataSetChanged();
    }

//    public List<GR_DET> getFullList() {
//        return grDetListFull;
//    }

    public SALE_RET_DET getItemByPosition(int position) {
        return this.grDetList.get(position);
    }

    public void deleteBatch(int position) {
        try {
//            grDetListFull.remove(position);
            grDetList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, grDetList.size());
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvReceiptQty;
        private TextView tvReceiptType;
        private TextView tvBatchNo;
        private TextView tvMFDDate;
        private TextView tvEXPDate;

        RecyclerViewHolder(View view) {
            super(view);
            tvReceiptQty = view.findViewById(R.id.tvReceiptQty);
            tvReceiptType = view.findViewById(R.id.tvReceiptType);
            tvBatchNo = view.findViewById(R.id.tvBatchNo);
            tvMFDDate = view.findViewById(R.id.tvMFDDate);
            tvEXPDate = view.findViewById(R.id.tvEXPDate);
        }
    }
}
