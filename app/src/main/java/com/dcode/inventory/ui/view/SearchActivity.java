package com.dcode.inventory.ui.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.data.model.LOAD_SLIP_ITEMS_NEW;
import com.dcode.inventory.data.model.SEARCH_TYPE;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.dcode.inventory.ui.adapter.SearchTypeAdapter;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SearchActivity
        extends BaseActivity
        implements View.OnClickListener, SearchView.OnQueryTextListener {
    private int Master_ID;
    private int Parent_ID;
    private String[] TitlesArray;
    private String filterString = "";
    private SearchTypeAdapter objectsAdapter;
    private int[] fkey_intArray;


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_search;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        try {
            Intent intent = this.getIntent();
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Master_ID = !bundle.containsKey(AppConstants.MASTER_ID) ? -1 : bundle.getInt(AppConstants.MASTER_ID, -1);
                Parent_ID = !bundle.containsKey(AppConstants.PARENT_ID) ? -1 : bundle.getInt(AppConstants.PARENT_ID, -1);
                //TitlesArray = !bundle.containsKey(AppConstants.TITLE) ? null : bundle.getStringArray(AppConstants.TITLE);

                if (bundle.containsKey(AppConstants.FILTER_KEY_INT_ARRAY)) {
                    fkey_intArray = bundle.getIntArray(AppConstants.FILTER_KEY_INT_ARRAY);
                }
            }

            if (Master_ID != 0) {
                setupView();
            } else {
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage(), false);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        objectsAdapter.getFilter().filter(newText);
        filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
        try {
            Bundle bundle = new Bundle();

            SEARCH_TYPE objectSearch = (SEARCH_TYPE) view.getTag();
            bundle.putLong(AppConstants.SELECTED_ID, objectSearch.PK_ID);
            bundle.putString(AppConstants.SELECTED_CODE, objectSearch.CODE);
            bundle.putString(AppConstants.SELECTED_NAME, objectSearch.NAME);
            bundle.putLong(AppConstants.PARENT_ID, objectSearch.PARENT_ID);
            bundle.putSerializable(AppConstants.SELECTED_OBJECT, objectSearch);

            Intent intent = this.getIntent();
            intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
            finishAndRemoveTask();
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage(), false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setResult(RESULT_CANCELED);
        finishAndRemoveTask();
    }

    private void setupView() {
        try {
            String ScreenTitle = getString(R.string.search);
            String CodeTitle = getString(R.string.code);
            String NameTitle = getString(R.string.name);
            if (TitlesArray != null) {
                ScreenTitle = TitlesArray.length >= 1 && TitlesArray[0] != null ? TitlesArray[0] : getString(R.string.search);
                CodeTitle = TitlesArray.length >= 2 && TitlesArray[1] != null ? TitlesArray[1] : getString(R.string.code);
                NameTitle = TitlesArray.length >= 3 && TitlesArray[2] != null ? TitlesArray[2] : getString(R.string.name);
            }

            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle(ScreenTitle);

            SearchView searchView = findViewById(R.id.search_bar);
            searchView.setOnQueryTextListener(this);

            TextView tvUserName = findViewById(R.id.tvUserName);
            if (App.currentUser != null) {
                tvUserName.setText(App.currentUser.USER_ID);
            }

            RecyclerView recyclerView = findViewById(R.id.recyclerView);
            ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);

            objectsAdapter = new SearchTypeAdapter(SearchActivity.this, new ArrayList<>(), this, CodeTitle, NameTitle);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            switch (Master_ID) {
                case AppConstants.SLOC:
                    objectsAdapter.addItems(App.getDatabaseClient().getAppDatabase().genericDao().getDriverList());
                    break;
                case AppConstants.HELPER:
                    objectsAdapter.addItems(App.getDatabaseClient().getAppDatabase().genericDao().getHelperList());
                    break;
                case AppConstants.VEH:
                    objectsAdapter.addItems(App.getDatabaseClient().getAppDatabase().genericDao().getVehicleList());
                    break;
                default:
                    break;
            }

            recyclerView.setAdapter(objectsAdapter);
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage(), false);
        }
    }


}

