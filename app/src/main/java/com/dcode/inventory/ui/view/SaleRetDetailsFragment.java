package com.dcode.inventory.ui.view;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.GR_DET;
import com.dcode.inventory.data.model.GR_HDR;
import com.dcode.inventory.data.model.PO_DET;
import com.dcode.inventory.data.model.PO_HDR;
import com.dcode.inventory.data.model.SALE_RET_DET;
import com.dcode.inventory.data.model.SALE_RET_DOC_DET;
import com.dcode.inventory.data.model.SALE_RET_DOC_HDR;
import com.dcode.inventory.data.model.SALE_RET_HDR;
import com.dcode.inventory.network.model.GenericSubmissionResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.dcode.inventory.ui.adapter.DocumentDetAdapter;
import com.dcode.inventory.ui.adapter.SaleRetDetAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SaleRetDetailsFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    private SALE_RET_HDR gr_hdr;
    private long HDR_ID;
    private String DocNo;
    private DocType ordTyp;
    private TextInputEditText edSupInvNo;
    private TextInputEditText edSupInvDate;
    private TextInputEditText edBillNo;
    private TextInputEditText edContainerNo;
    private TextInputEditText edRemarks;
    private TextInputEditText edReceiptNo;
    private TextInputEditText edReturnNo;
    private TextInputEditText  edSearch;
    private MaterialButton btnPrintLabels;
    private View root;
    private SaleRetDetAdapter detAdapter;
    private String filterString;
    private FloatingActionButton proceed;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        HDR_ID = App.sr_hdr_id;
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                DocNo = bundle.getString(AppConstants.SELECTED_ID, DocNo);
                ordTyp = (DocType) bundle.getSerializable(AppConstants.SELECTED_CODE);
            }
        }

        root = inflater.inflate(R.layout.fragment_sale_ret_details, container, false);

//        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

//    @Override
//    public void onPrepareOptionsMenu(Menu menu) {
//        MenuInflater inflater = requireActivity().getMenuInflater();
//        inflater.inflate(R.menu.top_app_bar, menu);
//
//        final MenuItem searchItem = menu.findItem(R.id.search);
//        final SearchView searchView = (SearchView) searchItem.getActionView();
//        searchView.setOnQueryTextListener(this);
//    }

    @Override
    public void onClick(View view) {
        SALE_RET_DOC_DET detailData = (SALE_RET_DOC_DET) view.getTag();
        if (detailData == null || detailData.DET_ID <= 0) {
            return;
        }

        if (detailData.TOTQTY <= 0) {
            showToast("Nothing to receive");
            return;
        }

        if (App.sr_hdr_id <= 0) {
            SALE_RET_DOC_HDR po_hdr = App.getDatabaseClient().getAppDatabase().genericDao().getSALE_RET_DOC_HDR(DocNo, ordTyp.CODE);
            gr_hdr = new SALE_RET_HDR();
            gr_hdr.GUID = Utils.GetGUID();
            gr_hdr.DEVICE_DATE = Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT);
            gr_hdr.ORDNO = po_hdr.ORDNO;
            gr_hdr.ORTYP = po_hdr.ORTYP;
            gr_hdr.CLSUP = po_hdr.CLSUP;
            gr_hdr.YRCD = po_hdr.YRCD == null ? "" : po_hdr.YRCD;
            gr_hdr.POTYPE = po_hdr.POTYPE;
            gr_hdr.POTYPENAME = po_hdr.POTYPENAME;

            HDR_ID = App.getDatabaseClient().getAppDatabase().genericDao().insertSALE_RET_HDR(gr_hdr);
            App.sr_hdr_id = HDR_ID;
        }
        HDR_ID = App.sr_hdr_id;


        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.MASTER_ID, DocNo);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, detailData);
        bundle.putSerializable(AppConstants.SELECTED_CODE, ordTyp);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_sale_ret_exist_item_receipt, bundle);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        detAdapter.getFilter().filter(newText);
        filterString = newText;
        return false;
    }

    private void setupView() {
        MaterialButton btnPostReceipt = root.findViewById(R.id.btnPostReceipt);
        btnPostReceipt.setOnClickListener(v -> onPostReceiptClick());

        btnPrintLabels = root.findViewById(R.id.btnPrintLabels);
        btnPrintLabels.setVisibility(View.INVISIBLE);
        btnPrintLabels.setOnClickListener(v -> onPrintLabels());
        if (gr_hdr != null && gr_hdr.RECEIPT_NO > 0) {
            btnPrintLabels.setEnabled(true);
        } else {
            btnPrintLabels.setEnabled(false);
        }

        //edSupInvNo = root.findViewById(R.id.edSupInvNo);
        //edSupInvDate = root.findViewById(R.id.edSupInvDate);
//        edSupInvDate.setOnClickListener(v -> {
//            edSupInvDate.setError(null);
//            Calendar currentDate = Calendar.getInstance();
//            int curYear = currentDate.get(Calendar.YEAR);
//            int curMonth = currentDate.get(Calendar.MONTH);
//            int curDay = currentDate.get(Calendar.DAY_OF_MONTH);
//
//            DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), (datePicker, cYear, cMonth, cDay) -> {
//                String dateString = String.format(Locale.ENGLISH, "%d-%d-%d", cDay, (cMonth + 1), cYear);
//                String selectedDate = Utils.getFormattedDate(dateString, Utils.ISSUE_DATE_FORMAT, Utils.PRINT_DATE_FORMAT);
//
//                edSupInvDate.setText(selectedDate);
//            }, curYear, curMonth, curDay);
//            mDatePicker.setTitle("Select date");
////            mDatePicker.getDatePicker().setMaxDate(new Date().getTime());
//            mDatePicker.show();
//        });
//        edBillNo = root.findViewById(R.id.edBillNo);
//        edContainerNo = root.findViewById(R.id.edContainerNo);
        edRemarks = root.findViewById(R.id.edRemarks);
        edSearch = root.findViewById(R.id.search);
//        edReceiptNo = root.findViewById(R.id.edReceiptNo);
//        edReturnNo = root.findViewById(R.id.edReturnNo);

//        edSupInvNo.setVisibility(View.GONE);
//        edSupInvDate.setVisibility(View.GONE);
//        edBillNo.setVisibility(View.GONE);
//        edContainerNo.setVisibility(View.GONE);
//        edReceiptNo.setVisibility(View.GONE);
//        edReturnNo.setVisibility(View.GONE);


        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        detAdapter = new SaleRetDetAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(detAdapter);

        List<SALE_RET_DOC_DET> detList = App.getDatabaseClient().getAppDatabase().genericDao().getSALE_RET_DOC_DET(DocNo, ordTyp.CODE);
        detAdapter.addItems(detList);



        if (App.sr_hdr_id > 0) {
            HDR_ID = App.sr_hdr_id;
            gr_hdr = App.getDatabaseClient().getAppDatabase().genericDao().getSALE_RET_HDR(HDR_ID);
//            if (gr_hdr.S_INVOICE_NO != null) {
//                edSupInvNo.setText(gr_hdr.S_INVOICE_NO);
//            }
//            if (gr_hdr.S_INVOICE_DATE != null) {
//                edSupInvDate.setText(gr_hdr.S_INVOICE_DATE);
//            }
//            if (gr_hdr.BILL_NO != null) {
//                edBillNo.setText(gr_hdr.BILL_NO);
//            }
//            if (gr_hdr.CONTAINER_NO != null) {
//                edContainerNo.setText(gr_hdr.CONTAINER_NO);
//            }
            if (gr_hdr.REMARKS != null) {
                edRemarks.setText(gr_hdr.REMARKS);
            }
        }

        proceed = (FloatingActionButton) root.findViewById(R.id.proceed);
        proceed.setVisibility(View.INVISIBLE);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.MASTER_ID, DocNo);
                bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
                bundle.putSerializable(AppConstants.SELECTED_CODE, ordTyp);
                ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_sale_ret_unknown_item_receipt, bundle);
            }
        });

        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                detAdapter.getFilter().filter(s);

            }
        });
    }

    private void onPostReceiptClick() {
        String errMessage;
        Log.d("App.sr_hdr_id#",String.valueOf(App.sr_hdr_id));
        List<SALE_RET_DET> grDetlist = App.getDatabaseClient().getAppDatabase().genericDao().getSALE_RET_DETByHDR_ID(App.sr_hdr_id);
        Log.d("grDetlist#",String.valueOf(grDetlist.size()));
        if (grDetlist.size() < 1) {
            errMessage = "Nothing to post.";
//            showToast("Nothing to post.");
            showAlert("Information", errMessage);
            App.getDatabaseClient().getAppDatabase().genericDao().deleteSALE_RET_HDR(App.sr_hdr_id);
            return;
        }

//        String supInvNo = edSupInvNo.getText().toString().trim();
//        if (supInvNo.length() == 0) {
//            errMessage = "Invalid Supplier invoice no";
//            edSupInvNo.setError(errMessage);
//            showToast(errMessage);
//            return;
//        }

//        String supInvDate = edSupInvDate.getText().toString().trim();
//        if (supInvDate.length() == 0) {
//            errMessage = "Invalid Supplier invoice date";
//            edSupInvDate.setError(errMessage);
//            showToast(errMessage);
//            return;
//        }

        //String billNo = edBillNo.getText().toString().trim();
        //String containerNo = edContainerNo.getText().toString().trim();
        String remarks = edRemarks.getText().toString().trim();


        if (gr_hdr != null) {
            gr_hdr.DEVICE_DATE = Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT);
            //gr_hdr.S_INVOICE_NO = supInvNo;
           // gr_hdr.S_INVOICE_DATE = supInvDate;
           // gr_hdr.BILL_NO = billNo;
            //gr_hdr.CONTAINER_NO = containerNo;
            gr_hdr.REMARKS = remarks;
            App.getDatabaseClient().getAppDatabase().genericDao().insertSALE_RET_HDR(gr_hdr);
        }

        UploadPending();
    }

    private void onPrintLabels() {
        Bundle bundle = new Bundle();
        String ReceiptNo = edReceiptNo.getTag().toString();
        bundle.putString(AppConstants.SELECTED_ID, ReceiptNo);
        MainActivity mainActivity = (MainActivity) requireActivity();
//        mainActivity.ClearNavigationStack
        mainActivity.NavigateToFragment(R.id.nav_print_labels, bundle);
    }

    private void UploadPending() {
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.SaleReturn.GetSaleReturn(HDR_ID);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLines(requestObject, new Callback<GenericSubmissionResponse>() {
            @Override
            public void success(GenericSubmissionResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String ReceiptNo = String.valueOf(genericSubmissionResponse.getRetId());
                        //edReceiptNo.setTag(ReceiptNo);
                        //ServiceUtils.SyncActivity.updateLocalDB(AppConstants.DatabaseEntities.PUR_RET_ITEMS_RECEIPT, HDR_ID, genericSubmissionResponse.getRetId());
                        ServiceUtils.SyncActivity.truncateLocalTables(AppConstants.DatabaseEntities.SALE_RET_HDR,DocNo, genericSubmissionResponse.getRetId());
                        ServiceUtils.SyncActivity.truncateLocalTables(AppConstants.DatabaseEntities.SALE_RET_DET,"", HDR_ID);
                        ServiceUtils.SyncActivity.truncateLocalTables(AppConstants.DatabaseEntities.SALE_RET_DOC_HDR,DocNo, genericSubmissionResponse.getRetId());
                        ServiceUtils.SyncActivity.truncateLocalTables(AppConstants.DatabaseEntities.SALE_RET_DOC_DET,DocNo, genericSubmissionResponse.getRetId());
                        //String[] msg = genericSubmissionResponse.getErrMessage().split("~");
                        String[] msg = genericSubmissionResponse.getErrMessage().split("~");
                        if (msg.length > 1) {
                           // edReturnNo.setText(msg[1]);
                        }
                        //edReceiptNo.setText(ReceiptNo);
                        btnPrintLabels.setEnabled(true);
                        showToast(msg[0]);
                        getActivity().onBackPressed();

                        Bundle bundle = new Bundle();
                        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
                        bundle.putSerializable(AppConstants.SELECTED_ID, genericSubmissionResponse.getErrMessage());
                        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_sale_ret_no, bundle);

                    } else {
                        showAlert("Alert", genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showAlert("Error", genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                dismissProgress();
                showAlert("Error", error.getMessage());
            }
        });
    }

/*    private void PromptPrint(final String receiptNo) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(requireActivity());
        alert.setTitle("Print Labels?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Put actions for OK button here
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.SELECTED_ID, receiptNo);
                ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_print_labels, bundle);
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Put actions for CANCEL button here, or leave in blank
                Log.d(App.TAG, "for debugging");
            }
        });
        alert.show();
    }*/
}
