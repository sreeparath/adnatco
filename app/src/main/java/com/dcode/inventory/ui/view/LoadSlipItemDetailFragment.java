package com.dcode.inventory.ui.view;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.LS_ITEMS;
import com.dcode.inventory.data.model.SLIP_DET;
import com.dcode.inventory.ui.adapter.SlipItemBatchesAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class LoadSlipItemDetailFragment
        extends BaseFragment {
    private final ColorDrawable background = new ColorDrawable(Color.YELLOW);
    private LS_ITEMS ls_items;
    private View root;
    private Drawable icon;
    private TextView tvCode;
    private TextView tvName;
    private TextView tvOriginalQty;
    private TextView tvPendingQty;
    private TextView tvReceiveQty;
    private TextView tvPackaging;
    private TextView tvUnits;
    private TextInputEditText edScanCode;
    private TextInputEditText edReceiptQty;
    private TextInputEditText edBatchNo;
    private TextInputEditText edMFDDate;
    private TextInputEditText edEXPDate;
    private RecyclerView recyclerView;

    private String scanCode;
    private boolean IsBarCodeScanned;
    private float rcd_qty;
    private SlipItemBatchesAdapter batchesAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            scanCode = "";
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                ls_items = (LS_ITEMS) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
                scanCode = bundle.getString(AppConstants.SELECTED_CODE);
                IsBarCodeScanned = (scanCode != null && scanCode.length() > 0);
            }
        }

        root = inflater.inflate(R.layout.fragment_load_slip_item_detail, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (IsBarCodeScanned) {
            ParseBarCode(scanCode);
        } else {
            loadData();
        }

        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView,
                                          @NonNull RecyclerView.ViewHolder viewHolder,
                                          @NonNull RecyclerView.ViewHolder viewHolder1) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                        if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
                            int position = viewHolder.getAdapterPosition();
                            SLIP_DET slip_det_batch = batchesAdapter.getItemByPosition(position);
                            if (slip_det_batch != null) {
                                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                alert.setTitle("Delete this batch?");
                                alert.setPositiveButton("OK", (dialog, whichButton) -> {
                                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSLIP_DETByDet_ID(slip_det_batch.DET_ID);
                                    ls_items = App.getDatabaseClient().getAppDatabase().genericDao().getLS_Items(ls_items.ORDNO, ls_items.ORTYP, ls_items.ITCODE);
                                    recyclerView.removeViewAt(position);
                                    batchesAdapter.notifyItemRemoved(position);
                                    batchesAdapter.deleteBatch(position);
                                    batchesAdapter.notifyItemRangeChanged(position, batchesAdapter.getItemCount());
                                    loadData();
                                });
                                alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
                                    //Put actions for CANCEL button here, or leave in blank
//                                    batchesAdapter.notifyDataSetChanged();
                                    UpdateBatchView();
                                });
                                alert.show();
                            }
                        }
                    }

                    @Override
                    public void onChildDraw(@NonNull Canvas c,
                                            @NonNull RecyclerView recyclerView,
                                            @NonNull RecyclerView.ViewHolder viewHolder,
                                            float dX, float dY, int actionState, boolean isCurrentlyActive) {
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        View itemView = viewHolder.itemView;
                        int backgroundCornerOffset = 20;

                        int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconBottom = iconTop + icon.getIntrinsicHeight();

                        if (dX > 0) {
                            int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                            int iconRight = itemView.getLeft() + iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getLeft(), itemView.getTop(),
                                    itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
                        } else if (dX < 0) {
                            int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                            int iconRight = itemView.getRight() - iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                                    itemView.getTop(), itemView.getRight(), itemView.getBottom());
                        } else {
                            background.setBounds(0, 0, 0, 0);
                        }
                        background.draw(c);
                        icon.draw(c);
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void onNextClick() {
        requireActivity().onBackPressed();
    }

    private void onAddClick() {
        if (IsDataValid()) {
            SaveData();
            edScanCode.requestFocus();
        }
    }

    private void onDateEntryClick(int ID) {
        Calendar currentDate = Calendar.getInstance();
        int curYear = currentDate.get(Calendar.YEAR);
        int curMonth = currentDate.get(Calendar.MONTH);
        int curDay = currentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), (datePicker, cYear, cMonth, cDay) -> {
            String dateString = String.format(Locale.ENGLISH, "%d-%d-%d", cDay, (cMonth + 1), cYear);
            String selectedDate = Utils.getFormattedDate(dateString, Utils.ISSUE_DATE_FORMAT, Utils.PRINT_DATE_FORMAT);

            if (ID == 1) {
                edMFDDate.setText(selectedDate);
            } else if (ID == 2) {
                edEXPDate.setText(selectedDate);
            }
        }, curYear, curMonth, curDay);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }

    private void setupView() {
        tvCode = root.findViewById(R.id.tvCode);
        tvName = root.findViewById(R.id.tvName);
        tvOriginalQty = root.findViewById(R.id.tvOriginalQty);
        //tvPendingQty = root.findViewById(R.id.tvPendingQty);
        //tvReceiveQty = root.findViewById(R.id.tvReceiveQty);
        tvPackaging = root.findViewById(R.id.tvPackaging);
        tvUnits = root.findViewById(R.id.tvUnits);

        edScanCode = root.findViewById(R.id.edScanCode);
        edScanCode.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    keyCode == KeyEvent.KEYCODE_ENTER) {
                return onScanCodeKeyEvent();
            }
            return false;
        });
        edReceiptQty = root.findViewById(R.id.edReceiptQty);
        edBatchNo = root.findViewById(R.id.edBatchNo);
        edMFDDate = root.findViewById(R.id.edMFDDate);
        edEXPDate = root.findViewById(R.id.edEXPDate);
        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        MaterialButton btnAdd = root.findViewById(R.id.btnAdd);
        btnNext.setOnClickListener(v -> onNextClick());
        btnAdd.setOnClickListener(v -> onAddClick());
        edMFDDate.setOnClickListener(v -> onDateEntryClick(1));
        edEXPDate.setOnClickListener(v -> onDateEntryClick(2));

        recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        batchesAdapter = new SlipItemBatchesAdapter(new ArrayList<>());
        recyclerView.setAdapter(batchesAdapter);
    }

    private void loadData() {
        if (ls_items == null || ls_items.ORDNO.length() <= 0) {
            return;
        }

        tvCode.setText(ls_items.ITCODE);
        tvName.setText(ls_items.ITDESC);
        tvOriginalQty.setText(String.valueOf(ls_items.ITQTY));
        tvPendingQty.setText(String.valueOf(ls_items.TOTQTY));
        tvReceiveQty.setText(String.valueOf(ls_items.RCQTY));
        tvPackaging.setText(ls_items.PACKING);
        tvUnits.setText(ls_items.ITUNIT);
        edReceiptQty.setText("");
        edBatchNo.setText("");
        edMFDDate.setText("");
        edEXPDate.setText("");

        UpdateBatchView();
    }

    private boolean IsDataValid() {
        rcd_qty = Utils.convertToFloat(edReceiptQty.getText().toString().trim(), -1);
        if (rcd_qty < 0 || rcd_qty > ls_items.TOTQTY) {
            showToast("Invalid Receipt quantity");
            return false;
        }

        if (ls_items.BATCHFLG == 1) {
            String batchNo = edBatchNo.getText().toString().trim();
            if (batchNo.length() == 0) {
                showToast("Invalid batch no");
                return false;
            }

            String mfd = edMFDDate.getText().toString();
            if (mfd.length() == 0) {
                showToast("Invalid manufacture date");
                return false;
            }

            String exp = edEXPDate.getText().toString();
            if (exp.length() == 0) {
                showToast("Invalid expiry date");
                return false;
            }

            Date mfDate = Utils.convertStringToDate(mfd, Utils.PRINT_DATE_FORMAT);
            Date exDate = Utils.convertStringToDate(exp, Utils.PRINT_DATE_FORMAT);
            Date today = new Date();
            if (mfDate.after(today)) {
                showToast("Invalid manufacture date, future");
                return false;
            }

            if (exDate.before(mfDate)) {
                showToast("Invalid expiry date, before manufacture");
                return false;
            }
        }
        return true;
    }

    private void SaveData() {
        SLIP_DET slip_det = new SLIP_DET();
        long seqNo = Utils.GetMaxValue("SLIP_DET", "SEQ_NO", true, "HDR_ID", App.ls_hdr_id);
        slip_det.SEQ_NO = ++seqNo;
        slip_det.ITCODE = ls_items.ITCODE;
        slip_det.ITUNIT = ls_items.ITUNIT;
        slip_det.FACTOR = ls_items.FACTOR;
        slip_det.SLNO = ls_items.SLNO;
        slip_det.HDR_ID = App.ls_hdr_id;
        slip_det.QTY = rcd_qty;
        slip_det.BATCHNO = edBatchNo.getText().toString().trim();
        slip_det.MANFDATE = edMFDDate.getText().toString();
        slip_det.EXPDATE = edEXPDate.getText().toString();

        App.getDatabaseClient().getAppDatabase().genericDao().insertSlip_DET(slip_det);
        ls_items = App.getDatabaseClient().getAppDatabase().genericDao().getLS_Items(ls_items.ORDNO, ls_items.ORTYP, ls_items.ITCODE);
        loadData();
    }

    private void UpdateBatchView() {
        List<SLIP_DET> detList = App.getDatabaseClient().getAppDatabase().genericDao().getSLIP_DET(App.ls_hdr_id, ls_items.SLNO);
        batchesAdapter.addItems(detList);
    }

    private boolean onScanCodeKeyEvent() {
        String ScanCode = edScanCode.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid bar code");
            return false;
        } else {
            IsBarCodeScanned = true;
            ParseBarCode(ScanCode);
        }
        return true;
    }

    private void ParseBarCode(final String scanCode) {
        if (scanCode.length() <= 0) {
            return;
        }
        String itcode = "";

        if (!scanCode.startsWith(App.BarCodeSeparator)) {
            // find item by itcode
            itcode = scanCode;
        } else {
            /*  1. ITCODE, 2. BatchNo, 3. MFD Date, 4. EXP Date, 5. Qty, 6. RefDoc */
            String[] barCodeData = scanCode.split(App.BarCodeSeparator);
            if (barCodeData.length > 2) {
                itcode = barCodeData[1];
            }
        }

        LS_ITEMS ls_items_scanned = App.getDatabaseClient().getAppDatabase().genericDao().getLS_Items(ls_items.ORDNO, ls_items.ORTYP, itcode);
        if (ls_items_scanned != null && ls_items_scanned.ORDNO.length() > 0) {
            if (!ls_items_scanned.ITCODE.equals(ls_items.ITCODE)) {
                // alert for item change
                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Change Item?");
                alert.setPositiveButton("OK", (dialog, whichButton) -> {
                    ls_items = ls_items_scanned;
                    loadData();
                    LoadBarcodeData(scanCode);
                });
                alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
                    //Put actions for CANCEL button here, or leave in blank
                    UpdateBatchView();
                });
                alert.show();
            } else {
                ls_items = ls_items_scanned;
                loadData();
                LoadBarcodeData(scanCode);
            }
        } else {
            edScanCode.setText("");
            showToast("Item not available in document");
        }
        this.scanCode = "";
        IsBarCodeScanned = false;
    }

    private void LoadBarcodeData(String scanCode) {
        if (scanCode.startsWith(App.BarCodeSeparator)) {
            /* 0. Empty String 1. ITCODE, 2. BatchNo, 3. MFD Date, 4. EXP Date, 5. Qty, 6. RefDoc */
            String[] barCodeData = scanCode.split(App.BarCodeSeparator);
            if (barCodeData.length >= 3) {
                edBatchNo.setText(barCodeData[2]);
            }

            if (barCodeData.length >= 4) {
                String formattedDate = Utils.getFormattedDate(barCodeData[3], Utils.DATE_NUM_FORMAT, Utils.PRINT_DATE_FORMAT);
                edMFDDate.setText(formattedDate);
            }

            if (barCodeData.length >= 5) {
                String formattedDate = Utils.getFormattedDate(barCodeData[4], Utils.DATE_NUM_FORMAT, Utils.PRINT_DATE_FORMAT);
                edEXPDate.setText(formattedDate);
            }

            if (barCodeData.length >= 6) {
                edReceiptQty.setText(barCodeData[5]);
            }

//            if (barCodeData.length >= 7) {
//                edRefNo.setText(barCodeData[6]);
//            }
        }
    }
}
