package com.dcode.inventory.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.data.model.STK_COUNT_DET;
import com.dcode.inventory.data.model.WO_ITEM_SPR;

import java.util.ArrayList;
import java.util.List;

public class StockDetailsAdapter
        extends RecyclerView.Adapter<StockDetailsAdapter.RecyclerViewHolder>
        implements Filterable {

    //    public static PO_DET editedRow;
    private List<STK_COUNT_DET> poDetList;
    private List<STK_COUNT_DET> poDetListFull;
    private View.OnClickListener shortClickListener;

    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<STK_COUNT_DET> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(poDetListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (STK_COUNT_DET item : poDetListFull) {
                    if ((item.MATERIAL != null && item.MATERIAL.toLowerCase().contains(filterPattern))) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            poDetList.clear();
            poDetList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public StockDetailsAdapter(List<STK_COUNT_DET> detList,
                               View.OnClickListener shortClickListener) {
        this.poDetList = detList;
        this.shortClickListener = shortClickListener;
        setHasStableIds(true);
        poDetListFull = new ArrayList<>(detList);
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public StockDetailsAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_stk_count_items, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull StockDetailsAdapter.RecyclerViewHolder holder, int position) {
        final STK_COUNT_DET po_det = poDetList.get(position);

        holder.tvCode.setText(po_det.MATERIAL);
        holder.tvName.setText(po_det.MATL_DESC);
        holder.tvBatch.setText((po_det.BATCH));

        holder.tvWbs.setText((po_det.WBS_ELEMENT));
        holder.tvRQty.setText(String.valueOf(po_det.RCQTY));

        holder.itemView.setTag(po_det);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return poDetList.size();
    }

    public void addItems(List<STK_COUNT_DET> detList) {
        this.poDetList = detList;
        this.poDetListFull = new ArrayList<>(detList);

        notifyDataSetChanged();
    }

    public List<STK_COUNT_DET> getFullList() {
        return poDetListFull;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCode;
        private TextView tvName;
        private TextView tvRQty;
        private TextView tvBatch;
        private TextView tvWbs;

        RecyclerViewHolder(View view) {
            super(view);
            tvCode = view.findViewById(R.id.tvCode);
            tvName = view.findViewById(R.id.tvName);
            tvRQty = view.findViewById(R.id.tvRqty);
            tvBatch = view.findViewById(R.id.tvBatch);
            tvWbs = view.findViewById(R.id.tvWbs);
        }
    }
}
