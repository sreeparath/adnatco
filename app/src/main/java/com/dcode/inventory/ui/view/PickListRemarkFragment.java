package com.dcode.inventory.ui.view;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.ColumnInfo;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.GR_DET;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.PO_DET;
import com.dcode.inventory.ui.adapter.ItemBatchesAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class PickListRemarkFragment
        extends BaseFragment
        implements View.OnClickListener {
    private final ColorDrawable background = new ColorDrawable(Color.YELLOW);
    private DocType recType;
    private float rcd_qty;
    private long HDR_ID;
    private long DET_ID;
    private View root;
    private PICK_LIST_ITEMS pick_list_item;
    private TextView tvCode;
    private TextView tvName;
    private TextView tvOriginalQty;
    private TextView tvPendingQty;
    private TextView tvReceiveQty;
    private TextView tvPackaging;
    private TextView tvUnits;
    private TextView tvRate;
    private TextInputEditText edItemcode;
    private TextInputEditText edDesc;
    private TextInputEditText edQty;
    private TextInputEditText edReceiptQty;
    private TextInputEditText edRemarks;
    private RecyclerView recyclerView;
    private ArrayAdapter<DocType> objectsAdapter;
    private ItemBatchesAdapter batchesAdapter;
    private Drawable icon;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                //HDR_ID = bundle.getLong(AppConstants.PARENT_ID, -1);
                pick_list_item = (PICK_LIST_ITEMS) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_pick_list_remarks, container, false);
        //icon = getResources().getDrawable(R.drawable.ic_delete_black);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DET_ID = -1;
        loadData();

    }

    @Override
    public void onClick(View view) {

    }

    private void setupView() {
        edItemcode = root.findViewById(R.id.edItemcode);
        edDesc = root.findViewById(R.id.edDesc);
        edQty = root.findViewById(R.id.edQty);
        edReceiptQty = root.findViewById(R.id.edReceiptQty);
        edRemarks = root.findViewById(R.id.edRemarks);


        MaterialButton btnNext = root.findViewById(R.id.btnClose);
        MaterialButton btnAdd = root.findViewById(R.id.btnUpdate);
        btnNext.setOnClickListener(v -> onNextClick());
        btnAdd.setOnClickListener(v -> onAddClick());
    }


    private void loadData() {
        if (pick_list_item == null ) {
            return;
        }

        edItemcode.setText(pick_list_item.ITCODE);
        edDesc.setText(pick_list_item.ITDESC);
        edQty.setText(String.valueOf(pick_list_item.QTY));
        edReceiptQty.setText(String.valueOf(pick_list_item.REC_QTY));
        edRemarks.requestFocus();
        //UpdateBatchView();
    }


    private void onNextClick() {
        getActivity().onBackPressed();
    }

    private void onAddClick() {
        if (IsDataValid()) {
            SaveData();
            showToastLong("Remark updated for pending item!");
            getActivity().onBackPressed();
        }
    }

    private boolean IsDataValid() {
        String errMessage;
        String remark =  edRemarks.getText().toString();
        if (remark.length() == 0) {
            errMessage = "Invalid Remarks";
            showToast(errMessage);
            edRemarks.setError(errMessage);
            return false;
        }
        return true;
    }

    private void SaveData() {

        String errMessage;
        if(pick_list_item==null){
            errMessage = "Invalid Item";
            showToast(errMessage);
        }

        PICK_LIST_ITEMS_RECEIPT pl_item_rec =  App.getDatabaseClient().getAppDatabase().genericDao().getPickListItemReceipt(pick_list_item.ORD_NO, pick_list_item.ITCODE);

        if(pl_item_rec!=null){
            App.getDatabaseClient().getAppDatabase().genericDao().updatePL_ITEMS_RECEIPT_REMARK(pick_list_item.ORD_NO, pick_list_item.ITCODE,edRemarks.getText().toString());
        }else{
            pl_item_rec = new PICK_LIST_ITEMS_RECEIPT();
            long seqNo = Utils.GetMaxValue("PICK_LIST_ITEMS_RECEIPT", "SL_NO", false, "", "");
            pl_item_rec.SL_NO = seqNo;
            pl_item_rec.ORD_NO =pick_list_item.ORD_NO;
            pl_item_rec.ITCODE  =pick_list_item.ITCODE;
            pl_item_rec.IUNITS= pick_list_item.BASE_UNIT;
            pl_item_rec.BATCHNO="";
            pl_item_rec.MANFDATE="";
            pl_item_rec.EXPDATE="";
            pl_item_rec.QTY=0;
            pl_item_rec.FACTOR=0;
            pl_item_rec.REC_NO="";
            pl_item_rec.REMARKS   = edRemarks.getText().toString();
            App.getDatabaseClient().getAppDatabase().genericDao().insertPL_ITEMS_RECEIPT(pl_item_rec);
        }



       /* GR_DET gr_det;

        if (DET_ID > 0) {
            gr_det = App.getDatabaseClient().getAppDatabase().genericDao().getGR_DETByDet_ID(DET_ID);
            DET_ID = -1;
        } else {
            gr_det = new GR_DET();
            long seqNo = Utils.GetMaxValue("GR_DET", "SEQ_NO", true, "HDR_ID", HDR_ID);
            gr_det.SEQ_NO = ++seqNo;
            gr_det.ITCODE = po_det.ITCODE;
            gr_det.ITUNIT = po_det.ITUNIT;
            gr_det.FACTOR = po_det.FACTOR;
            gr_det.SLNO = po_det.SLNO;
        }

        gr_det.HDR_ID = HDR_ID;
        gr_det.REC_TYPE = recType.CODE;
        gr_det.REC_DESC = recType.NAME;
        gr_det.REC_FACTOR = recType.CODE.equals("R") ? -1 : 1;
        gr_det.RCQTY = rcd_qty;
        gr_det.BATCHNO = edBatchNo.getText().toString().trim();
        gr_det.MANFDATE = edMFDDate.getText().toString();
        gr_det.EXPDATE = edEXPDate.getText().toString();

        App.getDatabaseClient().getAppDatabase().genericDao().insertGR_DET(gr_det);
        po_det = App.getDatabaseClient().getAppDatabase().genericDao().getPO_DET(po_det.ORDNO, po_det.ORTYP, po_det.SLNO);
        loadData();*/
    }


}
