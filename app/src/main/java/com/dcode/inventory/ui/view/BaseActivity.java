package com.dcode.inventory.ui.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.ui.dialog.CustomProgressDialog;

public abstract class BaseActivity extends AppCompatActivity {

    protected int selectedID;
    protected String selectedCode;
    protected String selectedName;
    private CustomProgressDialog progressDialog;
    private AlertDialog alertDialog;

    protected abstract int getLayoutResourceId();

    protected abstract void onViewReady(Bundle savedInstanceState);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
        onViewReady(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (intent == null) return;

        if (resultCode != Activity.RESULT_OK) return;

        selectedID = intent.hasExtra(AppConstants.SELECTED_ID) ? intent.getIntExtra(AppConstants.SELECTED_ID, -1) : -1;
        selectedCode = intent.hasExtra(AppConstants.SELECTED_CODE) ? intent.getStringExtra(AppConstants.SELECTED_CODE) : "";
        selectedName = intent.hasExtra(AppConstants.SELECTED_NAME) ? intent.getStringExtra(AppConstants.SELECTED_NAME) : "";
    }

/*    protected void setupToolBar(String screenTitle) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.tv_toolbar)).setText(screenTitle);
        setSupportActionBar(toolbar);
    }*/

    protected void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    protected void showAlert(String title, String message, final boolean isFinish) {
        if (alertDialog != null && alertDialog.isShowing())
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        alertDialog = builder.setTitle(title).setMessage(message).setPositiveButton("OK", (dialog, which) -> {
            if (isFinish) {
                finish();
            }
        }).create();
        alertDialog.setCancelable(isFinish);
        alertDialog.show();
        shortVibration();
    }

    protected void showProgress(boolean isCancelable) {
        if (progressDialog != null && progressDialog.isShowing())
            return;

        progressDialog = new CustomProgressDialog(this, isCancelable);
        progressDialog.show();
    }

    protected void updateProgressMessage(String message) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.updateMessage(message);
        }
    }

    protected void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    protected void shortVibration() {
        Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (vibe != null && vibe.hasVibrator()) vibe.vibrate(10);
    }

    protected void OnLookupClick(int master_id, int parent_id, @Nullable int[] array) {
        Intent intent = new Intent(this, searchFragment.class);
        intent.putExtra(AppConstants.MASTER_ID, master_id);
        intent.putExtra(AppConstants.PARENT_ID, parent_id);

        if (array != null) {
            intent.putExtra(AppConstants.FILTER_KEY_INT_ARRAY, array);
        }
        startActivityForResult(intent, master_id);
    }
}
