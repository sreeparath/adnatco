package com.dcode.inventory.ui.adapter;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.data.model.PICK_HDR;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS;
import com.dcode.inventory.ui.view.MainActivity;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

public class PickListItemsAdapter
        extends RecyclerView.Adapter<PickListItemsAdapter.RecyclerViewHolder>
        implements Filterable {
    private List<PICK_LIST_ITEMS> plItemsList;
    private List<PICK_LIST_ITEMS> plItemsListFull;
    private View.OnClickListener shortClickListener;
    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<PICK_LIST_ITEMS> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(plItemsListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (PICK_LIST_ITEMS item : plItemsListFull) {
                    if ((item.ITCODE != null && item.ITCODE.toLowerCase().contains(filterPattern)) ||
                            (item.ITDESC != null && item.ITDESC.toLowerCase().contains(filterPattern))) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            plItemsList.clear();
            if (results != null && results.values != null) {
                plItemsList.addAll((List) results.values);
            }
            notifyDataSetChanged();
        }
    };

    public PickListItemsAdapter(List<PICK_LIST_ITEMS> dataList, View.OnClickListener shortClickListener) {
        this.plItemsList = dataList;
        this.shortClickListener = shortClickListener;
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public PickListItemsAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_pl_items, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PickListItemsAdapter.RecyclerViewHolder holder, int position) {
        final PICK_LIST_ITEMS ls_hdr = plItemsList.get(position);
        Log.d("ls_hdr.REC_QTY# ",String.valueOf(ls_hdr.REC_QTY));
        holder.tvName.setText(ls_hdr.ITDESC);
        holder.tvCode.setText(ls_hdr.ITCODE);
        holder.tvSlno.setText(String.valueOf(ls_hdr.QTY));
        holder.tvFact.setText(ls_hdr.BASE_UNIT);
        holder.tvQty.setText(String.valueOf(ls_hdr.REC_QTY));

        holder.itemView.setTag(ls_hdr);
        holder.itemView.setOnClickListener(shortClickListener);

        if(ls_hdr.REC_QTY==ls_hdr.QTY){
            holder.btnRemark.setEnabled(false);
            holder.itemView.setBackgroundResource(R.drawable.border_green);
        }else if(ls_hdr.REC_QTY==0){
            holder.btnRemark.setEnabled(true);
            holder.itemView.setBackgroundResource(R.drawable.border_red);
        }else{
            holder.btnRemark.setEnabled(true);
            holder.itemView.setBackgroundResource(R.drawable.border_blue);
        }

        holder.btnAdvice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.SELECTED_OBJECT, ls_hdr);
                Navigation.findNavController(view).navigate(R.id.nav_load_pick_list_advice,bundle);
            }
        });

        holder.btnRemark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.SELECTED_OBJECT, ls_hdr);
                Navigation.findNavController(view).navigate(R.id.nav_load_pick_list_remarks,bundle);
            }
        });

        holder.btnStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.SELECTED_OBJECT, ls_hdr);
                Navigation.findNavController(view).navigate(R.id.nav_load_pick_list_stocks,bundle);
            }
        });

    }

    @Override
    public int getItemCount() {
        return plItemsList.size();
    }

    public void addItems(List<PICK_LIST_ITEMS> dataList) {
        this.plItemsList = dataList;
        this.plItemsListFull = new ArrayList<>(dataList);

        notifyDataSetChanged();
    }

    private OnItemClickListener mListener;
    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    static class RecyclerViewHolder
            extends RecyclerView.ViewHolder  {
        private TextView tvName;
        private TextView tvCode;
        private TextView tvSlno;
        private TextView tvFact;
        private TextView tvQty;
        MaterialButton btnRemark ;
        MaterialButton btnAdvice ;
        MaterialButton btnStock ;


        RecyclerViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            tvCode = view.findViewById(R.id.tvCode);
            tvSlno = view.findViewById(R.id.tvSlNo);
            tvFact = view.findViewById(R.id.tvFact);
            tvQty = view.findViewById(R.id.tvQty);
            btnRemark = view.findViewById(R.id.btnRemark);
            btnAdvice = view.findViewById(R.id.btnAdvice);
            btnStock = view.findViewById(R.id.btnStock);
        }
    }
}
