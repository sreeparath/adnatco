package com.dcode.inventory.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.data.model.ITEMS_PRINT;

import java.util.List;

public class ItemsLabelsPrintAdapter
        extends RecyclerView.Adapter<ItemsLabelsPrintAdapter.RecyclerViewHolder> {

    private ITEMS_PRINT editedRow;
    private List<ITEMS_PRINT> itemsPrintList;
    private View.OnClickListener shortClickListener;

    public ItemsLabelsPrintAdapter(List<ITEMS_PRINT> itemsPrints,
                                   View.OnClickListener shortClickListener) {
        this.itemsPrintList = itemsPrints;
        this.shortClickListener = shortClickListener;
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public ItemsLabelsPrintAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_items_print, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemsLabelsPrintAdapter.RecyclerViewHolder holder, int position) {
        final ITEMS_PRINT items_print = itemsPrintList.get(position);

        holder.tvName.setText(items_print.ITDESC);
        holder.tvCode.setText(items_print.ITCODE);
        holder.tvUnits.setText(items_print.ITUNIT);
        holder.tvBatchNo.setText(items_print.BATCHNO);
        holder.tvReceiptQty.setText(String.valueOf(items_print.ITQTY));
        holder.tvPrintQty.setText(String.valueOf(items_print.QTY));

        holder.itemView.setTag(items_print);
        holder.itemView.setOnClickListener(shortClickListener);
        editedRow = items_print;
    }

    @Override
    public int getItemCount() {
        if (itemsPrintList == null) {
            return 0;
        }
        return itemsPrintList.size();
    }

    public List<ITEMS_PRINT> getItems() {
        return this.itemsPrintList;
    }

    public void addItems(List<ITEMS_PRINT> detList) {
        this.itemsPrintList = detList;
        notifyDataSetChanged();
    }

    public void addItems(ITEMS_PRINT items_print) {
        this.itemsPrintList.add(items_print);
        notifyDataSetChanged();
    }

    public ITEMS_PRINT getItemByPosition(int position) {
        return this.itemsPrintList.get(position);
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;
        private TextView tvCode;
        private TextView tvUnits;
        private TextView tvBatchNo;
        private TextView tvReceiptQty;
        private TextView tvPrintQty;

        RecyclerViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            tvCode = view.findViewById(R.id.tvCode);
            tvUnits = view.findViewById(R.id.tvUnits);
            tvBatchNo = view.findViewById(R.id.tvBatchNo);
            tvReceiptQty = view.findViewById(R.id.tvReceiptQty);
            tvPrintQty = view.findViewById(R.id.tvPrintQty);
        }
    }
}
