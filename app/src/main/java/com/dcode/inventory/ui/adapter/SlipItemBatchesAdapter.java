package com.dcode.inventory.ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.data.model.SLIP_DET;

import java.util.List;

public class SlipItemBatchesAdapter
        extends RecyclerView.Adapter<SlipItemBatchesAdapter.RecyclerViewHolder> {

    private List<SLIP_DET> slipDetList;

    public SlipItemBatchesAdapter(List<SLIP_DET> detList) {
        this.slipDetList = detList;
        setHasStableIds(true);
//        grDetListFull = new ArrayList<>(detList);
    }

    @NonNull
    @Override
    public SlipItemBatchesAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_ls_batch, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SlipItemBatchesAdapter.RecyclerViewHolder holder, int position) {
        final SLIP_DET slip_det = slipDetList.get(position);

        holder.tvReceiptQty.setText(String.valueOf(slip_det.QTY));
        holder.tvBatchNo.setText(String.valueOf(slip_det.BATCHNO));
        holder.tvMFDDate.setText(String.valueOf(slip_det.MANFDATE));
        holder.tvEXPDate.setText(String.valueOf(slip_det.EXPDATE));

        holder.itemView.setTag(slip_det);
    }

    @Override
    public int getItemCount() {
        return slipDetList.size();
    }

    public void addItems(List<SLIP_DET> detList) {
        this.slipDetList = detList;
//        this.grDetListFull = new ArrayList<>(detList);

        notifyDataSetChanged();
    }

    public void addItems(SLIP_DET slip_det) {
        this.slipDetList.add(slip_det);
//        this.grDetListFull = new ArrayList<>(grDetList);

        notifyDataSetChanged();
    }

//    public List<GR_DET> getFullList() {
//        return grDetListFull;
//    }

    public SLIP_DET getItemByPosition(int position) {
        return this.slipDetList.get(position);
    }

    public void deleteBatch(int position) {
        try {
//            grDetListFull.remove(position);
            slipDetList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, slipDetList.size());
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvReceiptQty;
        private TextView tvBatchNo;
        private TextView tvMFDDate;
        private TextView tvEXPDate;

        RecyclerViewHolder(View view) {
            super(view);
            tvReceiptQty = view.findViewById(R.id.tvReceiptQty);
            tvBatchNo = view.findViewById(R.id.tvBatchNo);
            tvMFDDate = view.findViewById(R.id.tvMFDDate);
            tvEXPDate = view.findViewById(R.id.tvEXPDate);
        }
    }
}
