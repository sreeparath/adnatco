package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.data.model.PICK_HDR;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS_STOCK;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.dcode.inventory.ui.adapter.PickListItemsAdviceAdapter;
import com.dcode.inventory.ui.adapter.PickListItemsStockAdapter;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PickListStockFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    private View root;
    private PickListItemsStockAdapter picklistStockAdapter;
    private PICK_LIST_ITEMS  pl_item;
    private List<PICK_LIST_ITEMS> pl_advice_list;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            pl_item = (PICK_LIST_ITEMS) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
        }

        root = inflater.inflate(R.layout.fragment_pick_list_stock, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        picklistStockAdapter = new PickListItemsStockAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(picklistStockAdapter);

        loadData();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        picklistStockAdapter.getFilter().filter(newText);
        String filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
//        PICK_HDR ps_hdr = (PICK_HDR) view.getTag();
//        if (ps_hdr == null || ps_hdr.ORD_NO.length() == 0) {
//            return;
//        }
//
//        Bundle bundle = new Bundle();
//        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
//        bundle.putSerializable(AppConstants.SELECTED_OBJECT, ps_hdr);
//
//        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_picklist_items, bundle);
    }

    private void loadData() {
        showProgress(false);

        JsonObject jsonObject;
        JsonArray array = new JsonArray();
        jsonObject = ServiceUtils.createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
        array.add(jsonObject);
        jsonObject = ServiceUtils.createJsonObject("LOCCODE", "22", App.currentLocation.LOCODE);
        array.add(jsonObject);
        jsonObject = ServiceUtils.createJsonObject("ITCODE", "22", pl_item.ITCODE);
        array.add(jsonObject);

        JsonObject requestObject = new JsonObject();
        requestObject.addProperty("ProcName", "PDT.ITEM_STOCK_SPR");
        requestObject.addProperty("DBName", App.currentCompany.DbName);
        requestObject.add("dbparams", array);

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        PICK_LIST_ITEMS_STOCK[] pl_hdr = new Gson().fromJson(xmlDoc, PICK_LIST_ITEMS_STOCK[].class);
                        if(pl_hdr.length>0){
                            picklistStockAdapter.addItems(Arrays.asList(pl_hdr));
                        }else{
                            showToast("No stock details found!");
                        }

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showToast(msg);
            }
        });
    }
}
