package com.dcode.inventory.ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.data.model.LOAD_SLIP_ITEMS_NEW;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS;

import java.util.ArrayList;
import java.util.List;

public class LoadSlipItemsAdviceAdapter
        extends RecyclerView.Adapter<LoadSlipItemsAdviceAdapter.RecyclerViewHolder>
        implements Filterable {
    private List<LOAD_SLIP_ITEMS_NEW> plItemsList;
    private List<LOAD_SLIP_ITEMS_NEW> plItemsListFull;
    private View.OnClickListener shortClickListener;
    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<LOAD_SLIP_ITEMS_NEW> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(plItemsListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (LOAD_SLIP_ITEMS_NEW item : plItemsListFull) {
                    if ((item.ITCODE != null && item.ITCODE.toLowerCase().contains(filterPattern)) ||
                            (item.FACTOR != null && item.FACTOR.toLowerCase().contains(filterPattern))) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            plItemsList.clear();
            if (results != null && results.values != null) {
                plItemsList.addAll((List) results.values);
            }
            notifyDataSetChanged();
        }
    };

    public LoadSlipItemsAdviceAdapter(List<LOAD_SLIP_ITEMS_NEW> dataList, View.OnClickListener shortClickListener) {
        this.plItemsList = dataList;
        this.shortClickListener = shortClickListener;
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public LoadSlipItemsAdviceAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_pl_advice, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LoadSlipItemsAdviceAdapter.RecyclerViewHolder holder, int position) {
        final LOAD_SLIP_ITEMS_NEW ls_hdr = plItemsList.get(position);
        //Log.d("ls_hdr.REC_QTY# ",String.valueOf(ls_hdr.REC_QTY));
        holder.tvName.setText(ls_hdr.ITDESC);
        holder.tvCode.setText(ls_hdr.ITCODE);
        holder.tvUnits.setText(String.valueOf(ls_hdr.ITUNIT));
        holder.tvFact.setText(ls_hdr.FACTOR);
        holder.tvQty.setText(String.valueOf(ls_hdr.QTY));

        holder.itemView.setTag(ls_hdr);
        holder.itemView.setOnClickListener(shortClickListener);



    }

    @Override
    public int getItemCount() {
        return plItemsList.size();
    }

    public void addItems(List<LOAD_SLIP_ITEMS_NEW> dataList) {
        this.plItemsList = dataList;
        this.plItemsListFull = new ArrayList<>(dataList);

        notifyDataSetChanged();
    }

    private OnItemClickListener mListener;
    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    static class RecyclerViewHolder
            extends RecyclerView.ViewHolder  {
        private TextView tvName;
        private TextView tvCode;
        private TextView tvUnits;
        private TextView tvFact;
        private TextView tvQty;


        RecyclerViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            tvCode = view.findViewById(R.id.tvCode);
            tvUnits = view.findViewById(R.id.tvUnits);
            tvFact = view.findViewById(R.id.tvFact);
            tvQty = view.findViewById(R.id.tvQty);
        }
    }
}
