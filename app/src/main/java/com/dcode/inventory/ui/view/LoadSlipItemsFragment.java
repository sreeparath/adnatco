package com.dcode.inventory.ui.view;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.LS_HDR;
import com.dcode.inventory.data.model.LS_ITEMS;
import com.dcode.inventory.data.model.SLIP_DET;
import com.dcode.inventory.data.model.SLIP_HDR;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.model.GenericSubmissionResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.dcode.inventory.ui.adapter.LoadSlipItemsAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoadSlipItemsFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    LS_HDR ls_hdr;
    SLIP_HDR slip_hdr;
    List<LS_ITEMS> ls_itemsList;
    private TextInputEditText edScanCode;
    private TextInputEditText edRefNo;
    private TextInputEditText edRefDate;
    private TextInputEditText edVehicle;
    private TextInputEditText edDriver;
    private TextInputEditText edHelper;
    private TextInputEditText edRemarks;
    private TextInputEditText edReceiptNo;
    private View root;
    private LoadSlipItemsAdapter slipItemsAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                ls_hdr = (LS_HDR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_load_slip_items, container, false);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    private void setupView() {
        edScanCode = root.findViewById(R.id.edScanCode);
        edScanCode.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    keyCode == KeyEvent.KEYCODE_ENTER) {
                return onScanCodeKeyEvent();
            }
            return false;
        });
        edRefNo = root.findViewById(R.id.edRefNo);
        edRefDate = root.findViewById(R.id.edRefDate);
        edRefDate.setOnClickListener(v -> {
            Calendar currentDate = Calendar.getInstance();
            int curYear = currentDate.get(Calendar.YEAR);
            int curMonth = currentDate.get(Calendar.MONTH);
            int curDay = currentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), (datePicker, cYear, cMonth, cDay) -> {
                String dateString = String.format(Locale.ENGLISH, "%d-%d-%d", cDay, (cMonth + 1), cYear);
                String selectedDate = Utils.getFormattedDate(dateString, Utils.ISSUE_DATE_FORMAT, Utils.PRINT_DATE_FORMAT);

                edRefDate.setText(selectedDate);
            }, curYear, curMonth, curDay);
            mDatePicker.setTitle("Select date");
            mDatePicker.show();
        });
        edVehicle = root.findViewById(R.id.edVehicle);
        edDriver = root.findViewById(R.id.edDriver);
        edHelper = root.findViewById(R.id.edHelper);
        edRemarks = root.findViewById(R.id.edRemarks);
        edReceiptNo = root.findViewById(R.id.edReceiptNo);
        MaterialButton btnSave = root.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(v -> onClickSave());
        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        slipItemsAdapter = new LoadSlipItemsAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(slipItemsAdapter);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        slipItemsAdapter.getFilter().filter(newText);
        String filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
        LS_ITEMS ls_items = (LS_ITEMS) view.getTag();

        if (ls_items == null) {
            return;
        }

        NavigateToItemDetails(ls_items, "");
    }

    private void NavigateToItemDetails(LS_ITEMS ls_items, String scanCode) {
        if (App.ls_hdr_id <= 0) {
            slip_hdr = new SLIP_HDR();
            slip_hdr.GUID = Utils.GetGUID();
            slip_hdr.IS_UPLOADED = 0;
            slip_hdr.ORDNO = ls_hdr.LSNO;
            slip_hdr.ORTYP = ls_hdr.ORTYP;
            slip_hdr.YRCD = ls_hdr.YRCD;
            slip_hdr.LOCODE = App.currentLocation.LOCODE;
            slip_hdr.PRICING = ls_hdr.PRICING;

            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_HDR(ls_hdr);;
            LS_ITEMS[] array = new LS_ITEMS[ls_itemsList.size()];
            ls_itemsList.toArray(array);
            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_ITEMS(array);

            long HDR_ID = App.getDatabaseClient().getAppDatabase().genericDao().insertSlip_HDR(slip_hdr);
            App.ls_hdr_id = HDR_ID;
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, ls_items);
        bundle.putString(AppConstants.SELECTED_CODE, scanCode);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_load_slip_item_detail, bundle);
    }

    private boolean onScanCodeKeyEvent() {
        String ScanCode = edScanCode.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid bar code");
            return false;
        } else {
            ParseBarCode(ScanCode);
        }
        return true;
    }

    private void onClickSave() {
        SaveData();
    }

    private void loadData() {
        if (ls_hdr == null) {
            showToast("Some error");
            return;
        }

        slip_hdr = App.getDatabaseClient().getAppDatabase().genericDao().GetSLIP_HDR(App.ls_hdr_id);
        if (slip_hdr != null && slip_hdr.HDR_ID > 0) {
            ls_itemsList = App.getDatabaseClient().getAppDatabase().genericDao().getLS_Items(ls_hdr.LSNO, ls_hdr.ORTYP);
            slipItemsAdapter.addItems(ls_itemsList);

            edRefNo.setText(slip_hdr.REFNO);
            edRefDate.setText(slip_hdr.REFDT);
            edVehicle.setText(slip_hdr.NEW_VEH_NO);
            edDriver.setText(slip_hdr.NEW_DRIVER);
            edHelper.setText(slip_hdr.NEW_HELPER);
            edRemarks.setText(slip_hdr.REMARKS);
        } else {
            DownloadLS_Items();
        }
    }

    private void DownloadLS_Items() {
        showProgress(false);

        JsonObject jsonObject;
        JsonArray array = new JsonArray();
        jsonObject = ServiceUtils.createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
        array.add(jsonObject);
        jsonObject = ServiceUtils.createJsonObject("LOCODE", "22", App.currentLocation.LOCODE);
        array.add(jsonObject);
        jsonObject = ServiceUtils.createJsonObject("ORDNO", "22", ls_hdr.LSNO);
        array.add(jsonObject);
        jsonObject = ServiceUtils.createJsonObject("ORTYP", "22", ls_hdr.ORTYP);
        array.add(jsonObject);

        JsonObject requestObject = new JsonObject();
        requestObject.addProperty("ProcName", "PDT.LOAD_SLIP_ITEMS_SPR");
        requestObject.addProperty("DBName", App.currentCompany.DbName);
        requestObject.add("dbparams", array);

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        LS_ITEMS[] ls_items = new Gson().fromJson(xmlDoc, LS_ITEMS[].class);
                        App.getDatabaseClient().getAppDatabase().genericDao().insertLS_ITEMS(ls_items);
                        App.getDatabaseClient().getAppDatabase().genericDao().insertLS_HDR(ls_hdr);
                        ls_itemsList = Arrays.asList(ls_items);
                        slipItemsAdapter.addItems(ls_itemsList);
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showToast(msg);
            }
        });
    }

    private void SaveData() {
        List<SLIP_DET> slipDetList = App.getDatabaseClient().getAppDatabase().genericDao().getSLIP_DETByHDR_ID(App.ls_hdr_id);
        if (slipDetList.size() < 1) {
            showToast("Nothing to post.");
            App.getDatabaseClient().getAppDatabase().genericDao().deleteGR_HDR(App.ls_hdr_id);
            return;
        }

        String refNo = edRefNo.getText().toString().trim();
        if (refNo.length() == 0) {
            showToast("Invalid Reference no");
            return;
        }

        String refDate = edRefDate.getText().toString().trim();
        if (refDate.length() == 0) {
            showToast("Invalid Reference date");
            return;
        }

        String vehicle = edVehicle.getText().toString().trim();
        String driver = edDriver.getText().toString().trim();
        String helper = edHelper.getText().toString().trim();
        String remarks = edRemarks.getText().toString().trim();

        if (slip_hdr != null) {
            slip_hdr.DEVICE_DATE = Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT);
            slip_hdr.REFNO = refNo;
            slip_hdr.REFDT = refDate;
            slip_hdr.NEW_VEH_NO = vehicle;
            slip_hdr.NEW_DRIVER = driver;
            slip_hdr.NEW_HELPER = helper;
            slip_hdr.REMARKS = remarks;
            App.getDatabaseClient().getAppDatabase().genericDao().insertSlip_HDR(slip_hdr);
        }

        UploadPending();
    }

    private void UploadPending() {
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LoadSlips.GetLoadSlip(App.ls_hdr_id);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLines(requestObject, new Callback<GenericSubmissionResponse>() {
            @Override
            public void success(GenericSubmissionResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String ReceiptNo = String.valueOf(genericSubmissionResponse.getRetId());
                        edReceiptNo.setTag(ReceiptNo);
                        ServiceUtils.SyncActivity.updateLocalDB(AppConstants.DatabaseEntities.LOAD_SLIPS_HDR, App.ls_hdr_id, genericSubmissionResponse.getRetId());
                        String[] msg = genericSubmissionResponse.getErrMessage().split("~");
                        showToast(msg[0]);
                    } else {
                        showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }

    private void ParseBarCode(String scanCode) {
        if (scanCode.length() <= 0 ) {
            return;
        }
        String itcode = "";

        if (!scanCode.startsWith(App.BarCodeSeparator)) {
            // find item by itcode
            itcode = scanCode;
        } else {
            /*  1. ITCODE, 2. BatchNo, 3. MFD Date, 4. EXP Date, 5. Qty, 6. RefDoc */
            String[] barCodeData = scanCode.split(App.BarCodeSeparator);
            if (barCodeData.length > 2) {
                itcode = barCodeData[1];
            }
        }

        LS_ITEMS ls_items = App.getDatabaseClient().getAppDatabase().genericDao().getLS_Items(ls_hdr.LSNO, ls_hdr.ORTYP, itcode);
        if (ls_items != null && ls_items.ORDNO.length() > 0) {
            NavigateToItemDetails(ls_items, scanCode);
        } else {
            edScanCode.setText("");
            showToast("Item not available in document");
        }
    }
}
