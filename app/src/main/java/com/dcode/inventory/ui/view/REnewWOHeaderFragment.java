package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.WO_HDR_SPR;
import com.google.android.material.button.MaterialButton;

public class REnewWOHeaderFragment extends BaseFragment {
    private long HDR_ID;
    private String DocNo;
    private DocType ordTyp;
    private WO_HDR_SPR poHdr;

    private TextView tvPOTypeName;
    private TextView tvPlant;
    private TextView tvWoNo;
    private TextView tvTranType;
    private TextView tvDocDt;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Bundle bundle = null;
        if (this.getArguments() != null) {
            bundle = this.getArguments();
        }

        if (bundle != null) {
            ModuleID = bundle.getInt(AppConstants.MODULE_ID, 0);
            DocNo = bundle.getString(AppConstants.SELECTED_ID, DocNo);
//            ordTyp =  bundle.getString(AppConstants.SELECTED_CODE);
            HDR_ID = bundle.getLong(AppConstants.PARENT_ID, -1);
            poHdr = (WO_HDR_SPR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
        }

        View view = inflater.inflate(R.layout.fragment_wo_header, container, false);
        tvPOTypeName = view.findViewById(R.id.tvPOTypeName);
        tvDocDt = view.findViewById(R.id.edDocDt);
        tvWoNo = view.findViewById(R.id.edWoNo);
        tvTranType = view.findViewById(R.id.edTranType);
        tvPlant = view.findViewById(R.id.edPlant);

        MaterialButton btDocNo = view.findViewById(R.id.btnNext);
        btDocNo.setOnClickListener(v -> OnNextClick());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

    private void setupView() {
//        if (ordTyp != null && DocNo.length() != 0) {
//            poHdr = App.getDatabaseClient().getAppDatabase().genericDao().getAD_PO_HDR(DocNo);
//        }

        if (poHdr != null) {
            tvDocDt.setText(poHdr.DOC_DATE);
            tvWoNo.setText(poHdr.WO_NUMBER);
            tvPlant.setText(String.valueOf(poHdr.PLANT));
            tvTranType.setText(poHdr.TRAN_TYPE);
            tvPOTypeName.setText(R.string.work_order);
        }
    }

    private void OnNextClick() {
        Bundle bundle = new Bundle();
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_CODE, ordTyp);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, poHdr);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_reverse_new_wo_details, bundle);
    }
}
