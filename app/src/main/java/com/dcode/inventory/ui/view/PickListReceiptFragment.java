package com.dcode.inventory.ui.view;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.ColumnInfo;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.GR_DET;
import com.dcode.inventory.data.model.LS_HDR;
import com.dcode.inventory.data.model.LS_ITEMS;
import com.dcode.inventory.data.model.PICK_HDR;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.SLIP_DET;
import com.dcode.inventory.data.model.SLIP_HDR;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.model.GenericSubmissionResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.dcode.inventory.ui.adapter.LoadSlipItemsAdapter;
import com.dcode.inventory.ui.adapter.PickListItemsRecAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PickListReceiptFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    PICK_HDR pl_hdr;
    PICK_LIST_ITEMS_RECEIPT picklist_item_rec;
    List<PICK_LIST_ITEMS_RECEIPT> picklist_item_recList;
    private TextInputEditText edScanCode;
    private TextInputEditText edItemCode;
    private TextInputEditText edBatchNo;
    private TextInputEditText edMafDate;
    private TextInputEditText edExpdate;
    private TextInputEditText edReceiptNo;
    private TextInputEditText edQty;
    private View root;
    private PickListItemsRecAdapter plItemsRecAdapter;
    private AutoCompleteTextView tvUnits;
    private ArrayAdapter<DocType> objectsAdapter;
    private DocType unitType;
    //private Button btnAdd;
    private Drawable icon;
    private RecyclerView recyclerView;
    private final ColorDrawable background = new ColorDrawable(Color.RED);
    private float rcd_qty;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                pl_hdr = (PICK_HDR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_load_pick_list_receipt, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();

        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView,
                                          @NonNull RecyclerView.ViewHolder viewHolder,
                                          @NonNull RecyclerView.ViewHolder viewHolder1) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                        if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
                            int position = viewHolder.getAdapterPosition();
                            PICK_LIST_ITEMS_RECEIPT pl_item_rec = plItemsRecAdapter.getItemByPosition(position);
                            if (pl_item_rec != null) {
                                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                alert.setTitle("Delete this batch?");
                                alert.setPositiveButton("OK", (dialog, whichButton) -> {
                                    App.getDatabaseClient().getAppDatabase().genericDao().deletePICK_LIST_ITEMS_RECEIPTBysl_no(pl_item_rec.SL_NO);
                                    recyclerView.removeViewAt(position);
                                    plItemsRecAdapter.notifyItemRemoved(position);
                                    plItemsRecAdapter.deleteBatch(position);
                                    plItemsRecAdapter.notifyItemRangeChanged(position, plItemsRecAdapter.getItemCount());
                                });
                                alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
                                    //Put actions for CANCEL button here, or leave in blank
                                    plItemsRecAdapter.notifyDataSetChanged();
                                });
                                alert.show();
                            }
                            UpdateBatchView();
                        }
                    }

                    @Override
                    public void onChildDraw(@NonNull Canvas c,
                                            @NonNull RecyclerView recyclerView,
                                            @NonNull RecyclerView.ViewHolder viewHolder,
                                            float dX, float dY, int actionState, boolean isCurrentlyActive) {
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        View itemView = viewHolder.itemView;
                        int backgroundCornerOffset = 20;

                        int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconBottom = iconTop + icon.getIntrinsicHeight();

                        if (dX > 0) {
                            int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                            int iconRight = itemView.getLeft() + iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getLeft(), itemView.getTop(),
                                    itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
                        } else if (dX < 0) {
                            int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                            int iconRight = itemView.getRight() - iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                                    itemView.getTop(), itemView.getRight(), itemView.getBottom());
                        } else {
                            background.setBounds(0, 0, 0, 0);
                        }
                        background.draw(c);
                        icon.draw(c);
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void setupView() {
        edScanCode = root.findViewById(R.id.edScanCode);
        edScanCode.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    keyCode == KeyEvent.KEYCODE_ENTER) {
                return onScanCodeKeyEvent();
            }
            return false;
        });
        edItemCode = root.findViewById(R.id.edItemcode);
        edItemCode.setEnabled(false);
        edBatchNo = root.findViewById(R.id.edBatchNo);
        edMafDate = root.findViewById(R.id.edMFDDate);
        edExpdate = root.findViewById(R.id.edEXPDate);
        edQty = root.findViewById(R.id.edReceiptQty);
        edReceiptNo = root.findViewById(R.id.edReceiptNo);
        tvUnits = root.findViewById(R.id.tvUnits);
        MaterialButton btnSave = root.findViewById(R.id.btnAdd);
        btnSave.setOnClickListener(v -> onAddClick());
        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> onNextClick());
        recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        plItemsRecAdapter = new PickListItemsRecAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(plItemsRecAdapter);


    }

    private void fillUnits(String code) {
        tvUnits.setText("");
        List<DocType> recTypeList = App.getDatabaseClient().getAppDatabase().genericDao().getAllUnits(code);
        Log.d("data",recTypeList.toString());

        edScanCode.requestFocus();
        if(!(recTypeList!=null&& recTypeList.size()>0)){
            showAlert("Invalid","Item code not found! "+code);
            edItemCode.setText("");
            edScanCode.setText("");
            edScanCode.requestFocus();
            return;
        }

        objectsAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, recTypeList);
        tvUnits.setAdapter(objectsAdapter);
        tvUnits.setSelection(0);
        tvUnits.setThreshold(100);
        tvUnits.setOnItemClickListener((parent, view, position, id) -> {
            if (position >= 0) {
                unitType = objectsAdapter.getItem(position);
            } else {
                unitType = null;
            }

        });


    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        plItemsRecAdapter.getFilter().filter(newText);
        String filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
//        LS_ITEMS ls_items = (LS_ITEMS) view.getTag();
//
//        if (ls_items == null) {
//            return;
//        }

        //NavigateToItemDetails(ls_items, "");
    }

    /*private void NavigateToItemDetails(LS_ITEMS ls_items, String scanCode) {
        if (App.ls_hdr_id <= 0) {
            slip_hdr = new SLIP_HDR();
            slip_hdr.GUID = Utils.GetGUID();
            slip_hdr.IS_UPLOADED = 0;
            slip_hdr.ORDNO = ls_hdr.LSNO;
            slip_hdr.ORTYP = ls_hdr.ORTYP;
            slip_hdr.YRCD = ls_hdr.YRCD;
            slip_hdr.LOCODE = App.currentLocation.LOCODE;
            slip_hdr.PRICING = ls_hdr.PRICING;

            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_HDR(ls_hdr);;
            LS_ITEMS[] array = new LS_ITEMS[ls_itemsList.size()];
            ls_itemsList.toArray(array);
            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_ITEMS(array);

            long HDR_ID = App.getDatabaseClient().getAppDatabase().genericDao().insertSlip_HDR(slip_hdr);
            App.ls_hdr_id = HDR_ID;
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, ls_items);
        bundle.putString(AppConstants.SELECTED_CODE, scanCode);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_load_slip_item_detail, bundle);
    }*/

    private boolean onScanCodeKeyEvent() {
        String ScanCode = edScanCode.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid bar code");
            return false;
        } else {
            ParseBarCode(ScanCode);
        }
        return true;
    }

    private void onNextClick() {
        getActivity().onBackPressed();
    }

    private void onAddClick() {
        if (IsDataValid()) {
            SaveData();
            edScanCode.requestFocus();
        }
    }

    private boolean IsDataValid() {
        String errMessage;

        String itcode = edItemCode.getText().toString().trim();
        if (itcode.length() == 0) {
            errMessage = "Invalid Item Code";
            showToast(errMessage);
            edItemCode.setError(errMessage);
            return false;
        }

        if (unitType == null) {
            errMessage ="Invalid Unit type";
            showToast(errMessage);
            tvUnits.setError(errMessage);
            return false;
        }

        String qty = edQty.getText().toString().trim();
        if (qty.length() == 0) {
            errMessage = "Invalid Receipt quantity";
            showToast(errMessage);
            edQty.setError(errMessage);
            return false;
        }


        //rcd_qty = Utils.convertToFloat(edQty.getText().toString().trim(), -1);
        int totalReceived = App.getDatabaseClient().getAppDatabase().genericDao().getTotalPickListItemReceipt(pl_hdr.ORD_NO,itcode);
        int totalReceivable = App.getDatabaseClient().getAppDatabase().genericDao().getTotalPickListItem(pl_hdr.ORD_NO,itcode);
        int rc_qty = Integer.parseInt(edQty.getText().toString());



        if (rc_qty <= 0 || ((rc_qty*Float.parseFloat(unitType.CODE))+totalReceived) > totalReceivable) {
            errMessage = "Invalid Receipt quantity";
            showToast(errMessage);
            edQty.setError(errMessage);
            return false;
        }


//        String batchNo = edBatchNo.getText().toString().trim();
//        if (batchNo.length() == 0) {
//            errMessage = "Invalid batch no";
//            showToast(errMessage);
//            edBatchNo.setError(errMessage);
//            return false;
//        }

//        String mfd = edMafDate.getText().toString();
//        if (mfd.length() == 0) {
//            errMessage = "Invalid manufacture date";
//            showToast(errMessage);
//            edMafDate.setError(errMessage);
//            return false;
//        }

//        String exp = edExpdate.getText().toString();
//        if (exp.length() == 0) {
//            errMessage = "Invalid expiry date";
//            showToast(errMessage);
//            edExpdate.setError(errMessage);
//            return false;
//        }
        return true;
    }

    private void SaveData() {
        PICK_LIST_ITEMS_RECEIPT pl_rec = new PICK_LIST_ITEMS_RECEIPT();
        long seqNo = Utils.GetMaxValue("PICK_LIST_ITEMS_RECEIPT", "SL_NO", false, "", "");
        pl_rec.SL_NO = seqNo;
        pl_rec.ORD_NO = pl_hdr.ORD_NO;
        pl_rec.ITCODE = edItemCode.getText().toString().trim();
        pl_rec.IUNITS = unitType.NAME;
        pl_rec.FACTOR = Float.parseFloat(unitType.CODE);
        pl_rec.REC_NO = edReceiptNo.getText().toString().trim();
        pl_rec.BATCHNO = edBatchNo.getText().toString().trim();
        pl_rec.MANFDATE = edMafDate.getText().toString();
        pl_rec.EXPDATE = edExpdate.getText().toString();
        pl_rec.QTY =  Integer.parseInt(edQty.getText().toString());

        App.getDatabaseClient().getAppDatabase().genericDao().insertPL_ITEMS_RECEIPT(pl_rec);
        //ls_items = App.getDatabaseClient().getAppDatabase().genericDao().getLS_Items(ls_items.ORDNO, ls_items.ORTYP, ls_items.ITCODE);
        loadData();
        edItemCode.setText("");
        edBatchNo.setText("");
        edMafDate.setText("");
        edExpdate.setText("");
        edReceiptNo.setText("");
        edQty.setText("");
        edScanCode.setText("");
    }

    private void loadData() {
        picklist_item_recList = App.getDatabaseClient().getAppDatabase().genericDao().GetPL_ITEMS_RECEIPT(pl_hdr.ORD_NO);
        if (picklist_item_recList != null ) {
            plItemsRecAdapter.addItems(picklist_item_recList);
        }
    }


    private void UploadPending() {
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LoadSlips.GetLoadSlip(App.ls_hdr_id);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLines(requestObject, new Callback<GenericSubmissionResponse>() {
            @Override
            public void success(GenericSubmissionResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String ReceiptNo = String.valueOf(genericSubmissionResponse.getRetId());
                        edReceiptNo.setTag(ReceiptNo);
                        ServiceUtils.SyncActivity.updateLocalDB(AppConstants.DatabaseEntities.LOAD_SLIPS_HDR, App.ls_hdr_id, genericSubmissionResponse.getRetId());
                        String[] msg = genericSubmissionResponse.getErrMessage().split("~");
                        showToast(msg[0]);
                    } else {
                        showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }

    private void ParseBarCode(String scanCode) {
        if (scanCode.length() <= 0 ) {
            return;
        }
        String itcode = "";
        edItemCode.setText("");
        edBatchNo.setText("");
        edMafDate.setText("");
        edExpdate.setText("");
        edReceiptNo.setText("");
        edQty.setText("");

        if (!scanCode.startsWith(App.BarCodeSeparator)) {
            // find item by itcode
            itcode = scanCode;
            edItemCode.setText(itcode);
            edItemCode.setTag(itcode);
        } else {
            /*  1. ITCODE, 2. BatchNo, 3. MFD Date, 4. EXP Date, 5. Qty, 6. RefDoc */
            String[] barCodeData = scanCode.split(App.BarCodeSeparator);

            if (barCodeData.length > 5) {
                itcode = barCodeData[1];
                edItemCode.setText(itcode);
                edItemCode.setTag(itcode);
                edBatchNo.setText(barCodeData[2]);
                edMafDate.setText(barCodeData[3]);
                edExpdate.setText(barCodeData[4]);
                edReceiptNo.setText(barCodeData[6]);
            }
        }
        fillUnits(itcode);
        edScanCode.setText("");
    }

    private void UpdateBatchView() {
        picklist_item_recList = App.getDatabaseClient().getAppDatabase().genericDao().GetPL_ITEMS_RECEIPT(pl_hdr.ORD_NO);
        if (picklist_item_recList != null ) {
            plItemsRecAdapter.addItems(picklist_item_recList);
        }
    }
}
