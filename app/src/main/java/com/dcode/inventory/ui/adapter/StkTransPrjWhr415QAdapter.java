package com.dcode.inventory.ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT411Q;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT415Q;

import java.util.List;

public class StkTransPrjWhr415QAdapter
        extends RecyclerView.Adapter<StkTransPrjWhr415QAdapter.RecyclerViewHolder> {

    private List<STK_TRN_PRJ_WHR_RECIEPT415Q> grDetList;
    private View.OnClickListener shortClickListener;
//    private List<GR_DET> grDetListFull;

    public StkTransPrjWhr415QAdapter(List<STK_TRN_PRJ_WHR_RECIEPT415Q> detList) {
        this.grDetList = detList;
        setHasStableIds(true);
//        grDetListFull = new ArrayList<>(detList);
    }

    public StkTransPrjWhr415QAdapter(List<STK_TRN_PRJ_WHR_RECIEPT415Q> dataList, View.OnClickListener shortClickListener) {
        this.grDetList = dataList;
        ///this.receiptsListFull = new ArrayList<>(dataList);
        this.shortClickListener = shortClickListener;
    }

    @NonNull
    @Override
    public StkTransPrjWhr415QAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_stk_transfer, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull StkTransPrjWhr415QAdapter.RecyclerViewHolder holder, int position) {
        final STK_TRN_PRJ_WHR_RECIEPT415Q gr_det = grDetList.get(position);

        holder.tvReceiptQty.setText(String.valueOf(gr_det.ENTRY_QNT));
        holder.tvValType.setText(gr_det.VAL_TYPE);
        holder.tvWbs.setText(String.valueOf(gr_det.WBS_ELEM));
        holder.tvCode.setText(String.valueOf(gr_det.MATERIAL));
        holder.tvDesc.setText(String.valueOf(gr_det.MAT_DESC));

        holder.itemView.setTag(gr_det);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return grDetList.size();
    }

    public void addItems(List<STK_TRN_PRJ_WHR_RECIEPT415Q> detList) {
        this.grDetList = detList;
//        this.grDetListFull = new ArrayList<>(detList);

        notifyDataSetChanged();
    }

    public void addItems(STK_TRN_PRJ_WHR_RECIEPT415Q gr_det) {
        this.grDetList.add(gr_det);
//        this.grDetListFull = new ArrayList<>(grDetList);

        notifyDataSetChanged();
    }

//    public List<GR_DET> getFullList() {
//        return grDetListFull;
//    }

    public STK_TRN_PRJ_WHR_RECIEPT415Q getItemByPosition(int position) {
        return this.grDetList.get(position);
    }

    public void deleteBatch(int position) {
        try {
//            grDetListFull.remove(position);
            grDetList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, grDetList.size());
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvReceiptQty;
        private TextView tvValType;
        private TextView tvWbs;
        private TextView tvCode;
        private TextView tvDesc;

        RecyclerViewHolder(View view) {
            super(view);
            tvReceiptQty = view.findViewById(R.id.tvReceiptQty);
            tvValType = view.findViewById(R.id.tvValType);
            tvWbs = view.findViewById(R.id.tvWbs);
            tvCode = view.findViewById(R.id.tvCode);
            tvDesc = view.findViewById(R.id.tvDesc);
        }
    }
}
