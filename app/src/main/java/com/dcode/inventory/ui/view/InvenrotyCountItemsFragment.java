package com.dcode.inventory.ui.view;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.LOAD_SLIP_ITEMS_NEW;
import com.dcode.inventory.data.model.LOAD_SLIP_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.LS_HDR;
import com.dcode.inventory.data.model.SLIP_DET;
import com.dcode.inventory.data.model.SLIP_HDR;
import com.dcode.inventory.data.model.STOCK_HEADER;
import com.dcode.inventory.data.model.STOCK_HEADER_ITEMS;
import com.dcode.inventory.data.model.STOCK_ITEMS_RECEIPT;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.model.GenericSubmissionResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.dcode.inventory.ui.adapter.InventoryCountItemsAdapter;
import com.dcode.inventory.ui.adapter.LoadSlipItemsNewAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class InvenrotyCountItemsFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    LS_HDR pl_hdr;
    SLIP_HDR slip_hdr;
    List<STOCK_HEADER> pl_itemsList;
    STOCK_HEADER pl_list_items;
    private TextInputEditText edRefNo;
    private TextInputEditText edRefDate;
    private TextInputEditText edVehicle;
    private TextInputEditText edDriver;
    private TextInputEditText edHelper;
    private TextInputEditText edRemarks;
    private TextInputEditText edSearch;
    private View root;
    private InventoryCountItemsAdapter inventoryCountItemsAdapter;
    private TextInputEditText edPostRefNo;
    private TextInputEditText edPostRemarks;
    //private MaterialButton btnPostReceipt;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                pl_hdr = (LS_HDR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_stock_header_items, container, false);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        inventoryCountItemsAdapter = new InventoryCountItemsAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(inventoryCountItemsAdapter);

        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                inventoryCountItemsAdapter.getFilter().filter(s);

            }
        });

        DownloadPL_Items();
        //loadData();
    }

    private void setupView() {
        edSearch = root.findViewById(R.id.search);
        edSearch.requestFocus();


        //edReceiptNo = root.findViewById(R.id.edReceiptNo);
        //edReceiptNo.setEnabled(false);

        //btnPostReceipt = root.findViewById(R.id.btnSave);
        //btnPostReceipt.setOnClickListener(v -> onPostReceiptClick());

}

    private void onPostReceiptClick() {
        String errMessage;
        List<LOAD_SLIP_ITEMS_RECEIPT> plReceipts = App.getDatabaseClient().getAppDatabase().genericDao().GetLOAD_SLIP_ITEMS_RECEIPT(pl_hdr.LSNO);
        List<LOAD_SLIP_ITEMS_NEW> plItems = App.getDatabaseClient().getAppDatabase().genericDao().GetLOAD_SLIP_ITEMS_INDIVIDUAL(pl_hdr.LSNO);

        if (plReceipts.size() < 1) {
            errMessage = "Nothing to post.";
            showAlert("Information", errMessage);
            return;
        }

        /*ArrayList itemList = new ArrayList();

        if(plItems.size() > 0){
            boolean pending = false;
            boolean noRemark = false;
            for (LOAD_SLIP_ITEMS_NEW rec :plItems ) {
                if((rec.QTY - rec.REC_QTY) > 0){
                    Log.d("itemRec.REMARKS## 1  ",rec.ITCODE );
                    for (LOAD_SLIP_ITEMS_RECEIPT itemRec : plReceipts) {
                        if(rec.ITCODE.equals(itemRec.ITCODE)) {
                            Log.d("itemRec.REMARKS## ",itemRec.ITCODE+" "+itemRec.REMARKS);
                            if (!(itemRec.REMARKS != null && itemRec.REMARKS.length() > 0)) {
                                pending = true;
                            }
                        }

                    }
                }
                Log.d("itemList##    ",String.valueOf(itemList));
                Log.d("pending##    ",String.valueOf(pending)+" "+ rec.ITCODE);
                Log.d("pending 1 ##    ",String.valueOf(itemList.contains(rec.ITCODE)));
                if(rec.REC_QTY==0 && !pending) {
                    LOAD_SLIP_ITEMS_RECEIPT plexist = App.getDatabaseClient().getAppDatabase().genericDao().getLoadItemReceipt(rec.ORD_NO,rec.ITCODE);
                    if(!(plexist!=null))
                        pending = true;
                }


            }

            if(pending){
                errMessage = "Partial picking not Allowed or Add Remarks!";
                showAlert("Information", errMessage);
                return;
            }
        }*/
        boolean pending = false;
        if(plItems.size() > 0) {
            for (LOAD_SLIP_ITEMS_NEW rec :plItems ) {
                if ((rec.QTY - rec.REC_QTY) > 0) {
                    if( !(rec.REMARKS!=null)) {
                        pending = true;
                    }
                }
            }
        }

        if(pending){
            errMessage = "Partial picking not Allowed or Add Remarks!";
            showAlert("Information", errMessage);
            return;
        }

        String refNo = edRefNo.getText().toString().trim();
        if (refNo.length() == 0) {
            showToast("Invalid Reference no");
            return;
        }

        String refDate = edRefDate.getText().toString().trim();
        if (refDate.length() == 0) {
            showToast("Invalid Reference date");
            return;
        }

        String vehicle = edVehicle.getText().toString().trim();
        String driver = edDriver.getText().toString().trim();
        String helper = edHelper.getText().toString().trim();
        String remarks = edRemarks.getText().toString().trim();

       // if (slip_hdr != null) {
        slip_hdr = new SLIP_HDR();
        slip_hdr.GUID = Utils.GetGUID();
        slip_hdr.IS_UPLOADED = 0;
        slip_hdr.ORDNO = pl_hdr.LSNO;
        slip_hdr.ORTYP = pl_hdr.ORTYP;
        slip_hdr.YRCD = pl_hdr.YRCD;
        slip_hdr.LOCODE = App.currentLocation.LOCODE;
        slip_hdr.PRICING = pl_hdr.PRICING;
        slip_hdr.DEVICE_DATE = Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT);
        slip_hdr.REFNO = refNo;
        slip_hdr.REFDT = refDate;
        slip_hdr.NEW_VEH_NO = vehicle;
        slip_hdr.NEW_DRIVER = driver;
        slip_hdr.NEW_HELPER = helper;
        slip_hdr.REMARKS = remarks;
            //App.getDatabaseClient().getAppDatabase().genericDao().insertSlip_HDR(slip_hdr);
        //}

       // UploadPending();
    }

    /*public void onCopyDatabase() {
        USERS objUsr = App.getDatabaseClient().getAppDatabase().genericDao().getAllUsers();
        if (objUsr == null || objUsr.USER_NAME.length() == 0) {
            showToast("Current DB appears empty. Will not copy!!");
            return;
        }

        boolean copyresult = false;
        try {
            App.getDatabaseClient().getAppDatabase().close();

            String src = getDatabasePath("UPPDATA").getAbsolutePath();
            String filename = String.format("%s_%s_%s.db", objUsr.USER_NAME,
                    objUsr.USER_NAME, objUsr.USER_NAME);

            String tgt = Uri.withAppendedPath(Uri.parse(edExportDbPath.getText().toString()), filename).getPath();

            try {
                copyresult = copyAppDbToDownloadFolder(src, tgt);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (copyresult) {
                showToast("Done.");
            }

        } catch (Exception e) {
            showToast(e.getMessage());
            e.printStackTrace();
        }
    }*/

    public boolean copyAppDbToDownloadFolder(String src, String tgt) throws IOException {

        File backupDB = new File(tgt);
        backupDB.setReadable(true, false);
        if (!backupDB.exists()) {
            backupDB.createNewFile();
        }

        FileInputStream fis = new FileInputStream(src);
        FileOutputStream fos = new FileOutputStream(backupDB);
        fos.getChannel().transferFrom(fis.getChannel(), 0, fis.getChannel().size());
        fis.close();
        fos.close();
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        inventoryCountItemsAdapter.getFilter().filter(newText);
        String filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
        STOCK_HEADER st_items = (STOCK_HEADER) view.getTag();

        if (st_items == null) {
            return;
        }

        //NavigateToItemDetails(ls_items, "");
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, st_items);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_inventory_count_receipt, bundle);
    }

    /*private void NavigateToItemDetails(LS_ITEMS ls_items, String scanCode) {
        if (App.ls_hdr_id <= 0) {
            slip_hdr = new SLIP_HDR();
            slip_hdr.GUID = Utils.GetGUID();
            slip_hdr.IS_UPLOADED = 0;
            slip_hdr.ORDNO = ls_hdr.LSNO;
            slip_hdr.ORTYP = ls_hdr.ORTYP;
            slip_hdr.YRCD = ls_hdr.YRCD;
            slip_hdr.LOCODE = App.currentLocation.LOCODE;
            slip_hdr.PRICING = ls_hdr.PRICING;

            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_HDR(ls_hdr);;
            LS_ITEMS[] array = new LS_ITEMS[ls_itemsList.size()];
            ls_itemsList.toArray(array);
            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_ITEMS(array);

            long HDR_ID = App.getDatabaseClient().getAppDatabase().genericDao().insertSlip_HDR(slip_hdr);
            App.ls_hdr_id = HDR_ID;
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, ls_items);
        bundle.putString(AppConstants.SELECTED_CODE, scanCode);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_load_slip_item_detail, bundle);
    }*/


    private void onClickSave() {
        SaveData();
    }

    private void loadData() {
//        if (pl_hdr == null) {
//            showToast("Some error");
//            return;
//        }

        //pl_itemsList = App.getDatabaseClient().getAppDatabase().genericDao().GetLOAD_SLIP_ITEMS_NEW(pl_hdr.LSNO);// summary list
        pl_itemsList = App.getDatabaseClient().getAppDatabase().genericDao().getStockHeadItems();
        if (pl_itemsList != null && pl_itemsList.size()>0 ) {
            inventoryCountItemsAdapter.addItems(pl_itemsList);
            //btnPostReceipt.setEnabled(true);
            //proceed.setEnabled(true);
        }
        else {
            //btnPostReceipt.setEnabled(false);
            //proceed.setEnabled(false);
            showToast(" Alert : No data found.");
        }
    }

    private void DownloadPL_Items() {
        showProgress(false);

        JsonObject jsonObject;
        JsonArray array = new JsonArray();
        jsonObject = ServiceUtils.createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
        array.add(jsonObject);
        jsonObject = ServiceUtils.createJsonObject("LOCODE", "22", App.currentLocation.LOCODE);
        array.add(jsonObject);


        JsonObject requestObject = new JsonObject();
        requestObject.addProperty("ProcName", "PDT.STOCK_COUNT_HD_SPR");
        requestObject.addProperty("DBName", App.currentCompany.DbName);
        requestObject.add("dbparams", array);

        System.out.println("requestObject##"+requestObject.toString());

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        STOCK_HEADER[] sh_items = new Gson().fromJson(xmlDoc, STOCK_HEADER[].class);
                        App.getDatabaseClient().getAppDatabase().genericDao().insertSTOCK_HEADER(sh_items);
                        //pl_itemsList = Arrays.asList(pl_items);
                        //loadSlipItemsNewAdapter.addItems(pl_itemsList);
                        loadData();
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showToast(msg);
            }
        });
    }

    private void SaveData() {
        List<STOCK_ITEMS_RECEIPT> slipDetList = App.getDatabaseClient().getAppDatabase().genericDao().GetSTOCK_ITEMS_RECEIPT(slip_hdr.ORDNO);
        if (slipDetList.size() < 1) {
            showToast("Nothing to post.");
            return;
        }

       // UploadPending();
    }



    private void resetUI(){
        edRefNo.setText("");
        edRefDate.setText("");
        edVehicle.setText("");
        edDriver.setText("");
        edHelper.setText("");
        edRemarks.setText("");
        //edReceiptNo.setText("");
    }

    private void ParseBarCode(String scanCode) {
        if (scanCode.length() <= 0 ) {
            return;
        }
        String itcode = "";

        if (!scanCode.startsWith(App.BarCodeSeparator)) {
            // find item by itcode
            itcode = scanCode;
        } else {
            /*  1. ITCODE, 2. BatchNo, 3. MFD Date, 4. EXP Date, 5. Qty, 6. RefDoc */
            String[] barCodeData = scanCode.split(App.BarCodeSeparator);
            if (barCodeData.length > 2) {
                itcode = barCodeData[1];
            }
        }

//        LS_ITEMS ls_items = App.getDatabaseClient().getAppDatabase().genericDao().getLS_Items(ls_hdr.LSNO, ls_hdr.ORTYP, itcode);
//        if (ls_items != null && ls_items.ORDNO.length() > 0) {
//           // NavigateToItemDetails(ls_items, scanCode);
//        } else {
//            edScanCode.setText("");
//            showToast("Item not available in document");
//        }
    }
}
