package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;

import com.dcode.inventory.R;

public class TransferFragment
        extends BaseFragment {

    View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        root = inflater.inflate(R.layout.fragment_transfers, container, false);



        TextView tv_bg_str_trn_221q = root.findViewById(R.id.tv_bg_str_trn_221q);
        tv_bg_str_trn_221q.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_stk_trans_prj_wr_221q));


        TextView tv_stk_trans_prj_prj_415q = root.findViewById(R.id.tv_stk_trans_prj_prj_415q);
        tv_stk_trans_prj_prj_415q.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_stk_trans_prj_wr_415q));


        TextView tv_stk_trans_prj_prj_222 = root.findViewById(R.id.tv_stk_trans_prj_prj_222);
        tv_stk_trans_prj_prj_222.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_stk_trans_prj_prj_222));

        TextView tv_bg_stk_trans_prj_wr = root.findViewById(R.id.tv_bg_status_enquiry);
        tv_bg_stk_trans_prj_wr.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_stk_trans_prj_wr));

        TextView tv_bg_stk_trans_prj_wr_rev_222q = root.findViewById(R.id.tv_bg_stk_trans_prj_wr_rev_222q);
        tv_bg_stk_trans_prj_wr_rev_222q.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_stk_trans_prj_wr_rev_222q));

        TextView tv_stk_trans_prj_prj_411q = root.findViewById(R.id.tv_stk_trans_prj_prj_411q);
        tv_stk_trans_prj_prj_411q.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_stk_trans_prj_wr_411q));





        return root;
    }

    private void OnModuleButtonClick(int action_id) {
        Navigation.findNavController(root).navigate(action_id);
    }

}
