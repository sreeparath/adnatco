package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.data.model.AD_PO_HDR;
import com.dcode.inventory.data.model.AD_PO_ITEMS_SPR;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.STK_COUNT_DET;
import com.dcode.inventory.data.model.STK_COUNT_HDR;
import com.dcode.inventory.data.model.WO_HDR_SPR;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class StockCountFragment extends BaseFragment {
    private TextInputEditText edDocNo;
//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    private DocType ordType;
    private String DocNo;
    private long HDR_ID;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
        }

        promptResource = R.string.doc_no;

        View root = inflater.inflate(R.layout.fragment_stock_count, container, false);
        ImageView imgDocNo = root.findViewById(R.id.imgDocNo);
        imgDocNo.setImageResource(R.drawable.ic_005);

        edDocNo = root.findViewById(R.id.edDocNo);
        edDocNo.setHint(promptResource);
/*        edDocNo.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    keyCode == KeyEvent.KEYCODE_ENTER) {
                return onDocNoKeyEvent();
            }
            return false;
        });*/

        MaterialButton btDocNo = root.findViewById(R.id.btDocNo);
        btDocNo.setOnClickListener(v -> OnNextClick());

/*        tvDocTypes = root.findViewById(R.id.acDocType);
        tvDocTypes.setOnItemClickListener((parent, view, position, id) -> {
            if (position > 0) {
                ordType = objectsAdapter.getItem(position);
            } else {
                ordType = null;
            }
        });*/

        return root;
    }

/*    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDocTypes();
    }*/

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/

    private void OnNextClick() {
        Validate();
    }

    private void Validate() {
        String errMessage;
        DocNo = edDocNo.getText().toString();
        if (DocNo.length() == 0) {
            errMessage = String.format("%s required", getString(promptResource));
            showToast(errMessage);
            edDocNo.setError(errMessage);
            return;
        }

//        if (ordType == null) {
//            showToast("Document type required");
//            return;
//        }


        STK_COUNT_HDR stk_count_hdr = App.getDatabaseClient().getAppDatabase().genericDao().getSTK_COUNT_HDR(DocNo);
        if (stk_count_hdr != null && stk_count_hdr.ID > 0) {
            OpenDocHeader(stk_count_hdr);
        }else{
            showToast("Error : No document found!");
            return;
        }
        //else {

        //DownloadPO_HDR(DocNo, "");
         //   App.gr_hdr_id = -1;
        //}
    }

/*    private void getDocTypes() {
        List<DocType> orTypList = App.getDatabaseClient().getAppDatabase().genericDao().getAllOrTyps();
        objectsAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, orTypList);
        tvDocTypes.setAdapter(objectsAdapter);
        tvDocTypes.setSelection(0);
        tvDocTypes.setThreshold(100);

        objectsAdapter.notifyDataSetChanged();
    }*/

    private void OpenDocHeader(STK_COUNT_HDR stk_count_hdr) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, stk_count_hdr.DOC_NO);
        //bundle.putSerializable(AppConstants.SELECTED_CODE, poHdr.TYPE);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, stk_count_hdr);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_stock_count_header, bundle);
    }

    private void DownloadPO_HDR(final String docNo, final String docType) {
        JsonObject requestObject = ServiceUtils.StockCount.getStockCountHdr(docNo, docType);


        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        STK_COUNT_HDR[] st_hdr = new Gson().fromJson(xmlDoc, STK_COUNT_HDR[].class);
                        if (st_hdr==null || st_hdr.length<=0) {
                            showToast(" Error: No data received.");
                            return;
                        }
                        App.getDatabaseClient().getAppDatabase().genericDao().deleteSTK_COUNT_HDR(docNo);
                        App.getDatabaseClient().getAppDatabase().genericDao().insertSTK_COUNT_HDR(st_hdr);
                        DownloadPO_DET(docNo, "",st_hdr[0]);
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showToast(msg);
            }
        });
    }

    private void DownloadPO_DET(final String docNo, final String docType,final STK_COUNT_HDR st_hdr) {
        JsonObject requestObject = ServiceUtils.StockCount.getStockCountDet(docNo, docType);

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        STK_COUNT_DET[] poDet = new Gson().fromJson(xmlDoc, STK_COUNT_DET[].class);
                        App.getDatabaseClient().getAppDatabase().genericDao().deleteSTK_COUNT_DET(poDet[0].HEAD_ID);
                        App.getDatabaseClient().getAppDatabase().genericDao().insertSTK_COUNT_DET(poDet);
                        OpenDocHeader(st_hdr);
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showToast(msg);
            }
        });
    }
}
