package com.dcode.inventory.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.data.model.LOCATIONS;

import java.util.ArrayList;
import java.util.List;

public class LocationsAdapter
        extends RecyclerView.Adapter<LocationsAdapter.RecyclerViewHolder>
        implements Filterable {

    private Context context;
    private View.OnClickListener shortClickListener;
    private List<LOCATIONS> dataList;
    private ArrayList<LOCATIONS> dataListFull;

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<LOCATIONS> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(dataListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (LOCATIONS item : dataListFull) {
                    if (item.LOCODE.toLowerCase().contains(filterPattern.toLowerCase()) ||
                            item.LONAME.toLowerCase().contains(filterPattern.toLowerCase())) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            dataList.clear();
            dataList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public LocationsAdapter(Context context, List<LOCATIONS> objects, View.OnClickListener shortClickListener) {
        this.context = context;
        this.shortClickListener = shortClickListener;
        this.dataList = objects;
        this.dataListFull = new ArrayList<>(objects);
        setHasStableIds(true);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_code_desc, parent, false));
    }

    @Override
    public void onBindViewHolder(final LocationsAdapter.RecyclerViewHolder holder, int position) {
        final LOCATIONS objectItem = this.dataList.get(position);

        holder.tvCode.setText(objectItem.LOCODE);
        holder.tvName.setText(objectItem.LONAME);

        holder.itemView.setTag(objectItem);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void addItems(List<LOCATIONS> list) {
        this.dataList = list;
        this.dataListFull = new ArrayList<>(list);

        notifyDataSetChanged();
    }

    public List<LOCATIONS> getItems() {
        return this.dataList;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView tvCode;
        TextView tvName;

        RecyclerViewHolder(View view) {
            super(view);
            tvCode = view.findViewById(R.id.tvCode);
            tvName = view.findViewById(R.id.tvName);
        }
    }
}
