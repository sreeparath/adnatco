package com.dcode.inventory.ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.data.model.PUR_RET_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.STOCK_ITEMS_RECEIPT;

import java.util.ArrayList;
import java.util.List;

public class PurRetItemsRecAdapter
        extends RecyclerView.Adapter<PurRetItemsRecAdapter.RecyclerViewHolder>
        implements Filterable {

    private List<PUR_RET_ITEMS_RECEIPT> receiptsList;
    private List<PUR_RET_ITEMS_RECEIPT> receiptsListFull;
    private View.OnClickListener shortClickListener;
    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<PUR_RET_ITEMS_RECEIPT> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(receiptsListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (PUR_RET_ITEMS_RECEIPT item : receiptsListFull) {
                    if ((item.ITCODE != null && item.ITCODE.toLowerCase().contains(filterPattern)) ||
                            (item.BATCHNO != null && item.BATCHNO.toLowerCase().contains(filterPattern))) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            receiptsList.clear();
            receiptsList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public PurRetItemsRecAdapter(List<PUR_RET_ITEMS_RECEIPT> dataList, View.OnClickListener shortClickListener) {
        this.receiptsList = dataList;
        this.receiptsListFull = new ArrayList<>(dataList);
        this.shortClickListener = shortClickListener;
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public PurRetItemsRecAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_pur_ret_items_rec, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        final PUR_RET_ITEMS_RECEIPT po_det = receiptsList.get(position);
        Log.d("po_det#",po_det.toString());
        holder.tvCode.setText(po_det.ITCODE);
        holder.tvName.setText("");
        holder.tvUnits.setText(String.valueOf(po_det.IUNITS));
        holder.tvBatchNo.setText(String.valueOf(po_det.BATCHNO));
        holder.tvManDt.setText(String.valueOf(po_det.MANFDATE));
        holder.tvQty.setText(String.valueOf(po_det.CNTQTY));
        holder.tvExpDate.setText(String.valueOf(po_det.EXPDATE));

        holder.itemView.setTag(po_det);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return receiptsList.size();
    }

    public PUR_RET_ITEMS_RECEIPT getItemByPosition(int position) {
        return this.receiptsList.get(position);
    }

    public void deleteBatch(int position) {
        try {
//            grDetListFull.remove(position);
            receiptsList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, receiptsList.size());
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
    }

    public void addItems(List<PUR_RET_ITEMS_RECEIPT> dataList) {
        this.receiptsList = dataList;
        this.receiptsListFull = new ArrayList<>(dataList);
        notifyDataSetChanged();
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCode;
        private TextView tvName;
        private TextView tvUnits;
        private TextView tvBatchNo;
        private TextView tvManDt;
        private TextView tvQty;
        private TextView tvExpDate;

        RecyclerViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            tvCode = view.findViewById(R.id.tvCode);
            tvUnits = view.findViewById(R.id.tvUnits);
            tvBatchNo = view.findViewById(R.id.tvBatchNo);
            tvManDt = view.findViewById(R.id.tvManDt);
            tvQty = view.findViewById(R.id.tvQty);
            tvExpDate = view.findViewById(R.id.tvExpDate);
        }
    }
}
