package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.data.model.LS_HDR;
import com.dcode.inventory.data.model.SLIP_HDR;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.dcode.inventory.ui.adapter.LoadSlipsAdapter;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoadSlipsFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    private View root;
    private LoadSlipsAdapter slipsAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
        }

        root = inflater.inflate(R.layout.fragment_load_slips, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        slipsAdapter = new LoadSlipsAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(slipsAdapter);

        loadData();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        slipsAdapter.getFilter().filter(newText);
        String filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
        LS_HDR ls_hdr = (LS_HDR) view.getTag();
        if (ls_hdr == null || ls_hdr.LSNO.length() == 0) {
            return;
        }

        SLIP_HDR slip_hdr = App.getDatabaseClient().getAppDatabase().genericDao().GetSLIP_HDR(ls_hdr.LSNO, ls_hdr.ORTYP);
        if (slip_hdr != null && slip_hdr.HDR_ID > 0) {
            App.ls_hdr_id = slip_hdr.HDR_ID;
        } else {
            App.ls_hdr_id = -1;
        }

        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, ls_hdr);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_load_slip_items, bundle);
    }

    private void loadData() {
        showProgress(false);

        JsonObject jsonObject;
        JsonArray array = new JsonArray();
        /*
        @USER_ID USER_ID		VARCHAR(50)
	,@LOCCODE	LOCODE	VARCHAR(50)
	,@ORDNO			VARCHAR(100)
	,@ORTYP			VARCHAR(1)  --L/O
         */
        jsonObject = ServiceUtils.createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
        array.add(jsonObject);
        jsonObject = ServiceUtils.createJsonObject("LOCCODE", "22", App.currentLocation.LOCODE);
        array.add(jsonObject);

        JsonObject requestObject = new JsonObject();
        requestObject.addProperty("ProcName", "PDT.LOAD_SLIP_HDR_SPR");
        requestObject.addProperty("DBName", App.currentCompany.DbName);
        requestObject.add("dbparams", array);

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        LS_HDR[] ls_hdr = new Gson().fromJson(xmlDoc, LS_HDR[].class);
                        slipsAdapter.addItems(Arrays.asList(ls_hdr));
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showToast(msg);
            }
        });
    }
}
