package com.dcode.inventory.ui.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppVariables;
import com.dcode.inventory.common.ManagePermissions;
import com.dcode.inventory.data.model.AppSettings;
import com.dcode.inventory.data.model.COMPANY;
import com.dcode.inventory.data.model.LOCATIONS;
import com.dcode.inventory.data.model.USERS;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.model.LoginParams;
import com.dcode.inventory.network.model.ValidUserResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginActivity
        extends BaseActivity {
    private ImageButton imgSettings;
    private Button btSignIn;
    private TextInputEditText edUserName;
    private TextInputEditText edPassword;
    private TextInputEditText edWorkSites;
    //    private ArrayAdapter<LOCATIONS> locationsAdapter;
    private ArrayAdapter<COMPANY> companyAdapter;
    private List<COMPANY> companyList;
    private AutoCompleteTextView acCompany;
    private boolean isSignClicked = false;
    private boolean isUserValid = false;
    private AppSettings appSettings;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        imgSettings = findViewById(R.id.imgSettings);
        imgSettings.setOnClickListener(v -> onSettingsClick());
        edUserName = findViewById(R.id.edUserName);
        edPassword = findViewById(R.id.edPassword);

        btSignIn = findViewById(R.id.btSignIn);
        btSignIn.setOnClickListener(v -> OnSignInClick());

        ManagePermissions.verifyWriteStoragePermissions(this);
        ManagePermissions.verifyWriteStoragePermissions(this);

        GetCompanies();

//        //TODO: delete before deploy
//        edUserName.setText(getString(R.string.uname));
//        edPassword.setText(getString(R.string.pwd));
    }

    @Override
    public void onBackPressed() {
        finishAndRemoveTask();
        super.onBackPressed();
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//        super.onActivityResult(requestCode, resultCode, intent);
//
//        if (resultCode != RESULT_OK) {
//            return;
//        }
//
//        if (requestCode == AppConstants.LOCATION) {
//            LOCATIONS selectedObject = (LOCATIONS) intent.getSerializableExtra(AppConstants.SELECTED_OBJECT);
//            App.currentLocation = selectedObject;
//            if (selectedObject != null && selectedObject.LOCODE.length() > 0) {
//                edWorkSites.setText(selectedObject.LONAME);
//                btSignIn.setText(R.string.next);
//            }
//        }
//    }

    private void OnSignInClick() {
        String errMessage = "";
        edUserName.setError(null);
        edPassword.setError(null);
        //acCompany.setError(null);
        //edWorkSites.setError(null);





        if (App.currentCompany != null && App.currentLocation != null && isSignClicked && isUserValid) {
            //showToast(App.currentCompany.Code + " ## "+App.currentLocation.LOCODE);
            GetAppSettings();
            return;
        }

        if (edUserName.getText().length() < 1) {
            errMessage = String.format("%s required..", getString(R.string.username));
            showToast(errMessage);
            edUserName.setError(errMessage);
            return;
        }

        if (edPassword.getText().length() < 1) {
            errMessage = String.format("%s required..", getString(R.string.password));
            showToast(errMessage);
            edPassword.setError(errMessage);
            return;
        }

/*        if (App.currentLocation == null) {
            showToast(String.format("%s required..", getString(R.string.work_site)));
            return;
        }*/

//        if (App.currentCompany == null) {
//            errMessage = String.format("%s required..", getString(R.string.company));
//            showToast(errMessage);
//            acCompany.setError(errMessage);
//            return;
//        }
        String loginName = edUserName.getText().toString();
        String passWd = edPassword.getText().toString().trim();

        showProgress(false);
//        ValidateUserOnlineNew(loginName, passWd, passWd.length());
        ValidateUserOnline(loginName, passWd, passWd.length());
    }

/*    private void ValidateUser() {
        String loginName = edUserName.getText().toString();
        String passWd = edPassword.getText().toString().trim();
//        String ePassWd = CryptoSecurity.encrypt(passWd);

        USERS users = App.getDatabaseClient().getAppDatabase().genericDao().getUser(loginName, passWd);
        if (users != null && users.USER_ID.length() > 0) {
            App.currentUser = users;
            GetAppSettings();
        } else {
            ValidateUserOnline(loginName, passWd, passWd.length());
        }
    }*/

    private void ValidateUserOnline(String loginName, String passWd, int passWdLen) {
        LoginParams loginParams = new LoginParams();
        loginParams.LoginName = loginName;
        loginParams.LoginPassword = passWd;
//        loginParams.LoginPassword = CryptoSecurity.encrypt(passWd);
        loginParams.PasswordLength = passWdLen;
        loginParams.SPName = "SAP.VALIDATEUSER";
        loginParams.PasswordEncrypted = 0;
        loginParams.EncryptToDB = 1;
        loginParams.DBName = App.currentCompany.DbName;
        loginParams.DeviceIP = App.DeviceIP;

        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String json = gson.toJson(loginParams);


        JsonObject jsonObject = gson.fromJson(json, JsonObject.class);
        ///JsonObject jsonObject = ServiceUtils.ValidateUserLogin(loginName,passWd);

        App.getNetworkClient().getAPIService().ValidateUser(jsonObject, new Callback<ValidUserResponse>() {
            @Override
            public void success(ValidUserResponse validUserResponse, Response response) {
                try {
                    String errMessage;

                    USERS users = new USERS();
                    users.ID = validUserResponse.getID();
                    users.USER_ID = validUserResponse.getUserID();
                    users.FULL_NAME = validUserResponse.getFullName();
                    users.USER_NAME = validUserResponse.getLoginName();
                    users.USER_PWD = validUserResponse.getLoginPassword();
                    users.TEAM_ID = validUserResponse.getTeamID();
                    users.PLANT = validUserResponse.getPlant();
                    users.PLANT_NAME = validUserResponse.getPlantName();

                    if (users.ID <= 0 || users.USER_ID.length() <= 0 || users.USER_ID.equals("-1")) {
                        errMessage = "Invalid credentials. " + users.FULL_NAME;
                        showToast(errMessage);
                        edUserName.setError(errMessage);
                        edPassword.setError(errMessage);
                        isUserValid = false;
                        return;
                    }
                    if (users.PLANT.length()<= 0) {
                        errMessage = "Invalid Plant for user. " + users.FULL_NAME;
                        showToast(errMessage);
                        isUserValid = false;
                        return;
                    }

                    App.getDatabaseClient().getAppDatabase().genericDao().deleteUser(users.ID);
                    App.getDatabaseClient().getAppDatabase().genericDao().insertUser(users);
                    App.appSettings().destroyInstance();
                    isUserValid = true;
                    GetAppSettings();
                    App.currentUser = users;

                } catch (Exception e) {
                    Log.d(App.TAG, e.getStackTrace().toString());
                    showToast(e.getMessage());
                } finally {
                    dismissProgress();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                Log.d(App.TAG, error.getMessage());
                showToast("User login: Network call failure");

            }
        });
    }

/*    private void ValidateUserOnlineNew(String loginName, String passWd, int passWdLen) {
        JsonObject requestObject = ServiceUtils.UserValidation.getUserLoginDetails(loginName, passWd);


        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        try {
                            USERS[] users = new Gson().fromJson(xmlDoc, USERS[].class);
                            if (users==null || users.length<=0) {
                                String errMessage = "Not a valid user :"+loginName;
                                showToast(errMessage);
                                //edUserName.setError(errMessage);
                                //edPassword.setError(errMessage);
                                return;

                            }else if(users[0].PLANT.length()<=0){
                                    String errMessagePlant = "Not a valid Plant ";
                                    showToast(errMessagePlant);
                                    //edUserName.setError(errMessage);
                                    //edPassword.setError(errMessage);
                                    return;
                            } else {
                                App.getDatabaseClient().getAppDatabase().genericDao().deleteUser(users[0].USER_ID);
                                App.getDatabaseClient().getAppDatabase().genericDao().insertUser(users);
                                App.appSettings().destroyInstance();
                                App.currentUser = users[0];
                                GetAppSettings();
                            }

                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                        }


                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showToast(msg);
            }
        });
    }*/

/*    private void GetLocations() {
        if (App.currentCompany == null || App.currentCompany.Code.length() <= 0) {
            showToast("Please sign-in to select location");
        }

        if (Utils.GetRowsCount("LOCATIONS", true, "COCODE", App.currentCompany.Code) == 0) {
            GetLocationsOnline();
        } else {
            Intent intent = new Intent(this, LocationSearchActivity.class);
            startActivityForResult(intent, AppConstants.LOCATION);
        }
    }

    private void GetLocationsOnline() {
        // download the locations
        try {
            showProgress(false);
            JsonObject requestObject = ServiceUtils.MastersDownload.getLocationsObject();
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    if (genericRetResponse.getErrCode().equals("S")) {
                        LOCATIONS[] locations = new Gson().fromJson(genericRetResponse.getXmlDoc(), LOCATIONS[].class);
                        for (LOCATIONS location : locations) {
                            location.COCODE = App.currentCompany.Code;
                        }
                        App.getDatabaseClient().getAppDatabase().genericDao().deleteAllLocationsByCompany(App.currentCompany.Code);
                        App.getDatabaseClient().getAppDatabase().genericDao().insertLocations(locations);
                        GetLocations();
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            });
        } catch (Exception e) {
            String msg = e.getMessage();
            Log.d(App.TAG, msg);
            showToast(msg);
        } finally {
            dismissProgress();
        }
    }*/

    private void GetAppSettings() {
//        JsonObject jsonObject = new JsonObject();
//        jsonObject.addProperty("ProcName", "APPMASTER.HHT_APP_SETTINGS_SPR");
//        jsonObject.add("dbparams", ServiceUtils.GenerateCommonReadParams(0));
//
//        showProgress(false);
//        App.getNetworkClient().getAPIService().getGenericRet(jsonObject, new Callback<GenericRetResponse>() {
//            @Override
//            public void success(GenericRetResponse genericRetResponse, Response response) {
//                if (genericRetResponse.getErrCode().equals("E")) {
//                    showToast(genericRetResponse.getErrMessage());
//                    dismissProgress();
//                    return;
//                }
//
//                try {
//                    Gson gson = new Gson();
//                    APP_SETTINGS[] appSettings = gson.fromJson(genericRetResponse.getXmlDoc(), APP_SETTINGS[].class);
//                    App.getDatabaseClient().getAppDatabase().genericDao().deleteAllAppSettings();
//                    App.getDatabaseClient().getAppDatabase().genericDao().insertAllAppSettings(appSettings);

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
//                } catch (JsonSyntaxException e) {
//                    Log.d(App.TAG, e.getMessage());
//                    showToast(e.getMessage());
//                } finally {
//                    dismissProgress();
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                dismissProgress();
//                Log.d(App.TAG, error.getMessage());
//                showToast("App Settings: Network call failure");
//            }
//        });
    }

    private void onSettingsClick() {
        Intent intent = new Intent(LoginActivity.this, SettingsActivity.class);
        startActivity(intent);
    }

    private void GetCompanies() {
        companyList = new ArrayList<COMPANY>();
        COMPANY company;

        company = new COMPANY();
        company.Code = "01";
        company.Name = "SAWHNEY FOODSTUFF TRADING";
        company.DbName = "MIMS_1ERP_20201008";
        companyList.add(company);

//        company = new COMPANY();
//        company.Code = "02";
//        company.Name = "SAFCO INTERNATIONAL TRADING";
//        company.DbName = "SAFCOINT";
//        companyList.add(company);
//
//        company = new COMPANY();
//        company.Code = "03";
//        company.Name = "SFG GENERAL TRADING";
//        company.DbName = "SFG";
//        companyList.add(company);

        App.currentCompany = company;

//        companyAdapter = new ArrayAdapter<>(LoginActivity.this, R.layout.dropdown_menu_popup_item, companyList);
//        acCompany.setAdapter(companyAdapter);
//        acCompany.setThreshold(100);
    }

    private void getPdtId(){

        File file = new File(AppVariables.getAppSettingFile());

        String strRlativePath = file.getAbsolutePath();



        Gson gson = new Gson();
        try
        {
            Log.d("appset##",file.getAbsolutePath());
            //BufferedReader br = new BufferedReader(new FileReader(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath(),"storage.json")));
            BufferedReader br = new BufferedReader(new FileReader(file));
            appSettings = gson.fromJson(br, AppSettings.class);
            Log.d("appSettings##",appSettings.pdtId);
            Log.d("appSettings##plant#",appSettings.plant);
            //If array use like below
            //pricechkconfig[] entries = gson.fromJson(br, pricechkconfig[].class);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setPositiveButton("OK", null)
                    .setTitle("Error")
                    .setMessage("Error While Reading Config File")
                    .setCancelable(false)
                    .setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    finish();
                                }
                            })
                    .show();
            return;
        }

        //ServiceGenerator.BASE_URL = pricechkconfig.ServiceURL;
        AppVariables.setPdtId(appSettings.pdtId);
        AppVariables.setPlant(appSettings.plant);
        if(AppVariables.getPdtId()!=null && AppVariables.getPdtId().length()>0){
           // edPdtId.setText("PDT ID : "+ AppVariables.getPdtId());
        }
    }

}