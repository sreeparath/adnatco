package com.dcode.inventory.ui.view;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.GR_DET;
import com.dcode.inventory.data.model.LOAD_SLIP_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.PO_DET;
import com.dcode.inventory.data.model.REASON_TYPE;
import com.dcode.inventory.data.model.SALE_RET_DET;
import com.dcode.inventory.data.model.SALE_RET_DOC_DET;
import com.dcode.inventory.ui.adapter.ItemBatchesAdapter;
import com.dcode.inventory.ui.adapter.SaleRetItemRecAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class SaleRetExistItemReceiptFragment
        extends BaseFragment
        implements View.OnClickListener {
    private final ColorDrawable background = new ColorDrawable(Color.RED);
    private REASON_TYPE recType;
    private float rcd_qty;
    private long HDR_ID;
    private long DET_ID;
    private View root;
    private SALE_RET_DOC_DET po_det;
    private TextView tvCode;
    private TextView tvName;
    private TextView tvOriginalQty;
    private TextView tvPendingQty;
    private TextView tvReceiveQty;
    private TextView tvPackaging;
    private TextView tvUnits;
    private TextView tvRate;
    private TextInputEditText edReceiptQty;
    private TextInputEditText edBatchNo;
    private TextInputEditText edMFDDate;
    private TextInputEditText edEXPDate;
    private TextInputEditText edERPBatch;
    private AutoCompleteTextView tvRecType;
    private RecyclerView recyclerView;
    private ArrayAdapter<REASON_TYPE> objectsAdapter;
    private SaleRetItemRecAdapter batchesAdapter;
    private Drawable icon;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                HDR_ID = bundle.getLong(AppConstants.PARENT_ID, -1);
                po_det = (SALE_RET_DOC_DET) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_sale_ret_exist_item_receipt, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DET_ID = -1;
        fillRecTypes();
        loadData();

        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView,
                                          @NonNull RecyclerView.ViewHolder viewHolder,
                                          @NonNull RecyclerView.ViewHolder viewHolder1) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                        if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
                            int position = viewHolder.getAdapterPosition();
                            SALE_RET_DET pl_item_rec = batchesAdapter.getItemByPosition(position);
                            if (pl_item_rec != null) {
                                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                alert.setTitle("Delete this batch?");
                                alert.setPositiveButton("OK", (dialog, whichButton) -> {
                                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSALE_RET_DETByDet_ID(pl_item_rec.DET_ID);
                                    recyclerView.removeViewAt(position);
                                    batchesAdapter.notifyItemRemoved(position);
                                    batchesAdapter.deleteBatch(position);
                                    batchesAdapter.notifyItemRangeChanged(position, batchesAdapter.getItemCount());
                                });
                                alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
                                    //Put actions for CANCEL button here, or leave in blank
                                    batchesAdapter.notifyDataSetChanged();
                                });
                                alert.show();
                            }
                            UpdateBatchView();
                        }
                    }

                    @Override
                    public void onChildDraw(@NonNull Canvas c,
                                            @NonNull RecyclerView recyclerView,
                                            @NonNull RecyclerView.ViewHolder viewHolder,
                                            float dX, float dY, int actionState, boolean isCurrentlyActive) {
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        View itemView = viewHolder.itemView;
                        int backgroundCornerOffset = 20;

                        int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconBottom = iconTop + icon.getIntrinsicHeight();

                        if (dX > 0) {
                            int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                            int iconRight = itemView.getLeft() + iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getLeft(), itemView.getTop(),
                                    itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
                        } else if (dX < 0) {
                            int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                            int iconRight = itemView.getRight() - iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                                    itemView.getTop(), itemView.getRight(), itemView.getBottom());
                        } else {
                            background.setBounds(0, 0, 0, 0);
                        }
                        background.draw(c);
                        icon.draw(c);
                    }
                };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onClick(View view) {
        GR_DET gr_det = (GR_DET) view.getTag();
        DET_ID = gr_det.DET_ID;
    }

    private void setupView() {
        tvCode = root.findViewById(R.id.tvCode);
        tvName = root.findViewById(R.id.tvName);
        tvOriginalQty = root.findViewById(R.id.tvOriginalQty);
        //tvPendingQty = root.findViewById(R.id.tvPendingQty);
        //tvReceiveQty = root.findViewById(R.id.tvReceiveQty);
        tvPackaging = root.findViewById(R.id.tvPackaging);
        tvUnits = root.findViewById(R.id.tvUnits);
        tvRate = root.findViewById(R.id.tvRate);

        edReceiptQty = root.findViewById(R.id.edReceiptQty);
        edBatchNo = root.findViewById(R.id.edBatchNo);
        edMFDDate = root.findViewById(R.id.edMFDDate);
        edEXPDate = root.findViewById(R.id.edEXPDate);
        edERPBatch = root.findViewById(R.id.edERPBatch);
        edBatchNo.setEnabled(false);
        edMFDDate.setEnabled(false);
        edEXPDate.setEnabled(false);
        edERPBatch.setEnabled(false);
        tvRecType = root.findViewById(R.id.tvRecType);
        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        MaterialButton btnAdd = root.findViewById(R.id.btnAdd);
        btnNext.setOnClickListener(v -> onNextClick());
        btnAdd.setOnClickListener(v -> onAddClick());
        edMFDDate.setOnClickListener(v -> onDateEntryClick(1));
        edEXPDate.setOnClickListener(v -> onDateEntryClick(2));

        recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        batchesAdapter = new SaleRetItemRecAdapter(new ArrayList<>(),this);
        recyclerView.setAdapter(batchesAdapter);
    }

    private void fillRecTypes() {
        List<REASON_TYPE> recTypeList = App.getDatabaseClient().getAppDatabase().genericDao().getAllReasonTypes();
        objectsAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, recTypeList);
        tvRecType.setAdapter(objectsAdapter);
        tvRecType.setSelection(0);
        tvRecType.setThreshold(100);
        tvRecType.setOnItemClickListener((parent, view, position, id) -> {
            if (position >= 0) {
                recType = objectsAdapter.getItem(position);
            } else {
                recType = null;
            }
        });
    }

    private void loadData() {
        if (po_det == null || po_det.DET_ID <= 0) {
            return;
        }

        tvCode.setText(po_det.ITCODE);
        tvName.setText(po_det.ITDESC);
        tvOriginalQty.setText(String.valueOf(po_det.ITQTY));
        tvPendingQty.setText(String.valueOf(po_det.TOTQTY));
        tvReceiveQty.setText(String.valueOf(po_det.RCQTY));
        tvPackaging.setText(po_det.PACKING);
        tvUnits.setText(po_det.ITUNIT);
        tvRate.setText(String.valueOf(po_det.ITRTE));
        edReceiptQty.setText("");
        Log.d("po_det.BATCHNO##",po_det.BATCHNO);
        edBatchNo.setText(po_det.BATCHNO);
        edMFDDate.setText(po_det.MANFDATE);
        edEXPDate.setText(po_det.EXPDATE);
        edERPBatch.setText(po_det.ERPBATCH);

        UpdateBatchView();
    }

    private void onDateEntryClick(int ID) {
        Calendar currentDate = Calendar.getInstance();
        int curYear = currentDate.get(Calendar.YEAR);
        int curMonth = currentDate.get(Calendar.MONTH);
        int curDay = currentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), (datePicker, cYear, cMonth, cDay) -> {
            String dateString = String.format(Locale.ENGLISH, "%d-%d-%d", cDay, (cMonth + 1), cYear);
            String selectedDate = Utils.getFormattedDate(dateString, Utils.ISSUE_DATE_FORMAT, Utils.PRINT_DATE_FORMAT);

            if (ID == 1) {
                edMFDDate.setText(selectedDate);
                edMFDDate.setError(null);
            } else if (ID == 2) {
                edEXPDate.setText(selectedDate);
                edEXPDate.setError(null);
            }
        }, curYear, curMonth, curDay);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }

    private void onNextClick() {
        getActivity().onBackPressed();
    }

    private void onAddClick() {
        if (IsDataValid()) {
            SaveData();
        }
    }

    private boolean IsDataValid() {
        String errMessage;
        if (recType == null) {
            errMessage ="Invalid Reason type";
            showToast(errMessage);
            tvRecType.setError(errMessage);
            return false;
        }

//        rcd_qty = Utils.convertToFloat(edReceiptQty.getText().toString().trim(), -1);
//        if ((rcd_qty < 0 || rcd_qty > (po_det.TOTQTY+po_det.TOL_QTY)) && po_det.IS_QTY_VALIDATE==1) {
//            errMessage = "Invalid Receipt quantity";
//            showToast(errMessage);
//            edReceiptQty.setError(errMessage);
//            return false;
//        }
        rcd_qty = Utils.convertToFloat(edReceiptQty.getText().toString().trim(), -1);
        if (rcd_qty < 0 || rcd_qty > po_det.TOTQTY ) {
            errMessage = "Invalid Receipt quantity";
            showToast(errMessage);
            edReceiptQty.setError(errMessage);
            return false;
        }

        if (po_det.BATCHFLG == 1) {
            String batchNo = edBatchNo.getText().toString().trim();
            if (batchNo.length() == 0) {
                errMessage = "Invalid batch no";
                showToast(errMessage);
                edBatchNo.setError(errMessage);
                return false;
            }

            String mfd = edMFDDate.getText().toString();
            if (mfd.length() == 0) {
                errMessage = "Invalid manufacture date";
                showToast(errMessage);
                edMFDDate.setError(errMessage);
                return false;
            }

            String exp = edEXPDate.getText().toString();
            if (exp.length() == 0) {
                errMessage = "Invalid expiry date";
                showToast(errMessage);
                edEXPDate.setError(errMessage);
                return false;
            }

//            Date mfDate = Utils.convertStringToDate(mfd, Utils.PRINT_DATE_FORMAT);
//            Date exDate = Utils.convertStringToDate(exp, Utils.PRINT_DATE_FORMAT);
//            Date today = new Date();
//            if (mfDate.after(today)) {
//                errMessage = "Invalid manufacture date, date in future";
//                showToast(errMessage);
//                edMFDDate.setError(errMessage);
//                return false;
//            }
//
//            if (exDate.before(today)) {
//                errMessage = "Invalid expiry date, already expired";
//                showToast(errMessage);
//                edEXPDate.setError(errMessage);
//                return false;
//            }
        }
        return true;
    }

    private void SaveData() {
        SALE_RET_DET gr_det;

        if (DET_ID > 0) {
            gr_det = App.getDatabaseClient().getAppDatabase().genericDao().getSALE_RET_DETByDet_ID(DET_ID);
            DET_ID = -1;
        } else {
            gr_det = new SALE_RET_DET();
            long seqNo = Utils.GetMaxValue("SALE_RET_DET", "SEQ_NO", true, "HDR_ID", HDR_ID);
            gr_det.SEQ_NO = ++seqNo;
            gr_det.ITCODE = po_det.ITCODE;
            gr_det.ITUNIT = po_det.ITUNIT;
            gr_det.FACTOR = po_det.FACTOR;
            gr_det.SLNO = po_det.SLNO;
        }

        gr_det.HDR_ID = HDR_ID;
        //gr_det.REC_TYPE = recType.CODE;
        gr_det.REC_DESC = recType.NAME;
        gr_det.REASON = recType.CODE;
        gr_det.REC_FACTOR = recType.CODE.equals("R") ? -1 : 1;
        gr_det.RCQTY = rcd_qty;
        gr_det.BATCHNO = edBatchNo.getText().toString().trim();
        gr_det.MANFDATE = edMFDDate.getText().toString();
        gr_det.EXPDATE = edEXPDate.getText().toString();
        gr_det.ERPBATCH = edERPBatch.getText().toString();

        App.getDatabaseClient().getAppDatabase().genericDao().insertSALE_RET_DET(gr_det);
        po_det = App.getDatabaseClient().getAppDatabase().genericDao().getSALE_RET_DOC_DET(po_det.ORDNO, po_det.ORTYP, po_det.SLNO);
        loadData();
//        UpdateBatchView();
//        edEXPDate.setText("");
//        edMFDDate.setText("");
//        edBatchNo.setText("");
//        edReceiptQty.setText("");
    }

    private void UpdateBatchView() {
        //Log.d("HDR_ID##",String.valueOf(HDR_ID));
        List<SALE_RET_DET> detList = App.getDatabaseClient().getAppDatabase().genericDao().getSALE_RET_DET(HDR_ID, po_det.SLNO);
        //Log.d("po_det.SLNO##",String.valueOf(detList.size()));
        batchesAdapter.addItems(detList);
    }
}
