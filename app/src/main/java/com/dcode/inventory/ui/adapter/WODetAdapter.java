package com.dcode.inventory.ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.data.model.AD_PO_ITEMS_SPR;
import com.dcode.inventory.data.model.WO_ITEM_SPR;

import java.util.ArrayList;
import java.util.List;

public class WODetAdapter
        extends RecyclerView.Adapter<WODetAdapter.RecyclerViewHolder>
        implements Filterable {

    //    public static PO_DET editedRow;
    private List<WO_ITEM_SPR> poDetList;
    private List<WO_ITEM_SPR> poDetListFull;
    private View.OnClickListener shortClickListener;

    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<WO_ITEM_SPR> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(poDetListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (WO_ITEM_SPR item : poDetListFull) {
                    if ((item.MATERIAL != null && item.MATERIAL.toLowerCase().contains(filterPattern))) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            poDetList.clear();
            poDetList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public WODetAdapter(List<WO_ITEM_SPR> detList,
                        View.OnClickListener shortClickListener) {
        this.poDetList = detList;
        this.shortClickListener = shortClickListener;
        setHasStableIds(true);
        poDetListFull = new ArrayList<>(detList);
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public WODetAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_wo_items, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull WODetAdapter.RecyclerViewHolder holder, int position) {
        final WO_ITEM_SPR po_det = poDetList.get(position);

        holder.tvCode.setText(po_det.MATERIAL);
        holder.tvName.setText(po_det.MATL_DESC);
        holder.tvUnit.setText(String.valueOf(po_det.REQUIREMENT_QUANTITY_UNIT));
        //holder.tvRerQty.setText(String.valueOf(po_det.REQUIREMENT_QUANTITY));
        if(po_det.REQUIREMENT_QUANTITY<0){
            holder.tvRerQty.setText("0");
        }else{
            holder.tvRerQty.setText(String.valueOf(po_det.REQUIREMENT_QUANTITY));
        }
        holder.tvReceiptQty.setText(String.valueOf(po_det.RCQTY));

        holder.itemView.setTag(po_det);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return poDetList.size();
    }

    public void addItems(List<WO_ITEM_SPR> detList) {
        this.poDetList = detList;
        this.poDetListFull = new ArrayList<>(detList);

        notifyDataSetChanged();
    }

    public List<WO_ITEM_SPR> getFullList() {
        return poDetListFull;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCode;
        private TextView tvName;
        private TextView tvUnit;
        private TextView tvRerQty;
        private TextView tvReceiptQty;

        RecyclerViewHolder(View view) {
            super(view);
            tvCode = view.findViewById(R.id.tvCode);
            tvName = view.findViewById(R.id.tvName);
            tvUnit = view.findViewById(R.id.tvUnit);
            tvRerQty = view.findViewById(R.id.tvRerQty);
            tvReceiptQty = view.findViewById(R.id.tvReceiptQty);
        }
    }
}
