package com.dcode.inventory.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.data.model.LS_HDR;

import java.util.ArrayList;
import java.util.List;

public class LoadSlipsAdapter
        extends RecyclerView.Adapter<LoadSlipsAdapter.RecyclerViewHolder>
        implements Filterable {
    private List<LS_HDR> lsHdrList;
    private List<LS_HDR> lsHdrListFull;
    private View.OnClickListener shortClickListener;
    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<LS_HDR> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(lsHdrListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (LS_HDR item : lsHdrListFull) {
                    if ((item.CUSTCODE != null && item.CUSTCODE.toLowerCase().contains(filterPattern)) ||
                            (item.CUSTNAME != null && item.CUSTNAME.toLowerCase().contains(filterPattern)) ||
                            (item.LSNO != null && item.LSNO.toLowerCase().contains(filterPattern)) ||
                            (item.VEHICLE != null && item.VEHICLE.toLowerCase().contains(filterPattern))) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            lsHdrList.clear();
            if (results != null && results.values != null) {
                lsHdrList.addAll((List) results.values);
            }
            notifyDataSetChanged();
        }
    };

    public LoadSlipsAdapter(List<LS_HDR> dataList, View.OnClickListener shortClickListener) {
        this.lsHdrList = dataList;
        this.shortClickListener = shortClickListener;
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public LoadSlipsAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_ls_hdr, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LoadSlipsAdapter.RecyclerViewHolder holder, int position) {
        final LS_HDR ls_hdr = lsHdrList.get(position);

        holder.tvName.setText(ls_hdr.CUSTNAME);
        holder.tvCode.setText(ls_hdr.CUSTCODE);
        holder.tvDate.setText(ls_hdr.LOADSLIPDATE);
        holder.tvDocNo.setText(ls_hdr.LSNO);
        holder.tvDocType.setText(ls_hdr.ORTYP);
        holder.tvYCRD.setText(ls_hdr.YRCD);
        holder.tvPricing.setText(ls_hdr.PRICING);
        holder.tvLoadType.setText(ls_hdr.LOADTYPE);
        holder.tvVehicle.setText(ls_hdr.DRIVER);

        holder.itemView.setTag(ls_hdr);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return lsHdrList.size();
    }

    public void addItems(List<LS_HDR> dataList) {
        this.lsHdrList = dataList;
        this.lsHdrListFull = new ArrayList<>(dataList);

        notifyDataSetChanged();
    }

    static class RecyclerViewHolder
            extends RecyclerView.ViewHolder {
        private TextView tvName;
        private TextView tvCode;
        private TextView tvDate;
        private TextView tvDocNo;
        private TextView tvDocType;
        private TextView tvYCRD;
        private TextView tvPricing;
        private TextView tvLoadType;
        private TextView tvVehicle;

        RecyclerViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            tvCode = view.findViewById(R.id.tvCode);
            tvDate = view.findViewById(R.id.tvSlipDate);
            tvDocNo = view.findViewById(R.id.tvDocNo);
            tvDocType = view.findViewById(R.id.tvDocType);
            tvYCRD = view.findViewById(R.id.tvYCRD);
            tvPricing = view.findViewById(R.id.tvPricing);
            tvLoadType = view.findViewById(R.id.tvLoadType);
            tvVehicle = view.findViewById(R.id.tvVehicle);
        }
    }
}
