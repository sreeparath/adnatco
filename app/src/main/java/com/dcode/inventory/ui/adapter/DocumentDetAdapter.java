package com.dcode.inventory.ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.data.model.AD_PO_ITEMS_SPR;
import com.dcode.inventory.data.model.PO_DET;

import java.util.ArrayList;
import java.util.List;

public class DocumentDetAdapter
        extends RecyclerView.Adapter<DocumentDetAdapter.RecyclerViewHolder>
        implements Filterable {

    //    public static PO_DET editedRow;
    private List<AD_PO_ITEMS_SPR> poDetList;
    private List<AD_PO_ITEMS_SPR> poDetListFull;
    private View.OnClickListener shortClickListener;

    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<AD_PO_ITEMS_SPR> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(poDetListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (AD_PO_ITEMS_SPR item : poDetListFull) {
                    if ((item.MATERIAL != null && item.MATERIAL.toLowerCase().contains(filterPattern)) ||
                            (item.SHORT_TEXT != null && item.SHORT_TEXT.toLowerCase().contains(filterPattern))) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            poDetList.clear();
            poDetList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public DocumentDetAdapter(List<AD_PO_ITEMS_SPR> detList,
                              View.OnClickListener shortClickListener) {
        this.poDetList = detList;
        this.shortClickListener = shortClickListener;
        setHasStableIds(true);
        poDetListFull = new ArrayList<>(detList);
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public DocumentDetAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_grn_items, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DocumentDetAdapter.RecyclerViewHolder holder, int position) {
        final AD_PO_ITEMS_SPR po_det = poDetList.get(position);

        holder.tvCode.setText(po_det.MATERIAL);
        holder.tvName.setText(po_det.SHORT_TEXT);
        holder.tvPoQty.setText(String.valueOf(po_det.QUANTITY));
        holder.tvRqty.setText(String.valueOf(po_det.RCQTY));
        holder.tvUnit.setText(po_det.PO_UNIT);

        holder.itemView.setTag(po_det);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return poDetList.size();
    }

    public void addItems(List<AD_PO_ITEMS_SPR> detList) {
        this.poDetList = detList;
        this.poDetListFull = new ArrayList<>(detList);

        notifyDataSetChanged();
    }

    public List<AD_PO_ITEMS_SPR> getFullList() {
        return poDetListFull;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCode;
        private TextView tvName;
        private TextView tvRqty;
        private TextView tvPoQty;
        private TextView tvUnit;

        RecyclerViewHolder(View view) {
            super(view);
            tvCode = view.findViewById(R.id.tvCode);
            tvName = view.findViewById(R.id.tvName);
            tvRqty = view.findViewById(R.id.tvRqty);
            tvUnit = view.findViewById(R.id.tvUnit);
            tvPoQty = view.findViewById(R.id.tvPoQty);
        }
    }
}
