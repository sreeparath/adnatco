package com.dcode.inventory.ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.data.model.LS_HDR;
import com.dcode.inventory.data.model.PICK_HDR;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.ConsoleHandler;

public class PickListAdapter
        extends RecyclerView.Adapter<PickListAdapter.RecyclerViewHolder>
        implements Filterable {
    private List<PICK_HDR> plHdrList;
    private List<PICK_HDR> plHdrListFull;
    private View.OnClickListener shortClickListener;
    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<PICK_HDR> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(plHdrListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (PICK_HDR item : plHdrListFull) {
                    if ((item.ORD_NO != null && item.ORD_NO.toLowerCase().contains(filterPattern)) ||
                            (item.REMARKS != null && item.REMARKS.toLowerCase().contains(filterPattern)) ||
                            (item.PICKBY != null && item.PICKBY.toLowerCase().contains(filterPattern))) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            plHdrList.clear();
            if (results != null && results.values != null) {
                plHdrList.addAll((List) results.values);
            }
            notifyDataSetChanged();
        }
    };

    public PickListAdapter(List<PICK_HDR> dataList, View.OnClickListener shortClickListener) {
        this.plHdrList = dataList;
        this.shortClickListener = shortClickListener;
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public PickListAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_pl_hdr, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PickListAdapter.RecyclerViewHolder holder, int position) {
        final PICK_HDR ls_hdr = plHdrList.get(position);
        Log.d("lshdr##",ls_hdr.ORD_NO);
        holder.tvDocNo.setText(ls_hdr.ORD_NO);
        holder.tvDocType.setText(ls_hdr.PICKBY);
        holder.tvYCRD.setText(ls_hdr.REMARKS);

        holder.itemView.setTag(ls_hdr);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return plHdrList.size();
    }

    public void addItems(List<PICK_HDR> dataList) {
        this.plHdrList = dataList;
        this.plHdrListFull = new ArrayList<>(dataList);

        notifyDataSetChanged();
    }

    static class RecyclerViewHolder
            extends RecyclerView.ViewHolder {
        private TextView tvDocNo;
        private TextView tvDocType;
        private TextView tvYCRD;

        RecyclerViewHolder(View view) {
            super(view);
            tvDocNo = view.findViewById(R.id.tvCode);
            tvDocType = view.findViewById(R.id.tvDocNo);
            tvYCRD = view.findViewById(R.id.tvYCRD);
        }
    }
}
