package com.dcode.inventory.ui.view;

import android.app.AlertDialog;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.BATCH;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.GR_DET;
import com.dcode.inventory.data.model.MATERIAL_LIST;
import com.dcode.inventory.data.model.STK_COUNT_DET;
import com.dcode.inventory.data.model.STK_COUNT_HDR;
import com.dcode.inventory.data.model.STK_COUNT_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.WO_HDR_SPR;
import com.dcode.inventory.data.model.WO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.WO_ITEM_SPR;
import com.dcode.inventory.ui.adapter.StockCountReceiptAdapter;
import com.dcode.inventory.ui.adapter.WOScanReceiptAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import org.apache.commons.codec.binary.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StockCountReceiptFragment
        extends BaseFragment
        implements View.OnClickListener {
    private final ColorDrawable background = new ColorDrawable(Color.YELLOW);
    private DocType recType;
    private float rcd_qty;
    private long HDR_ID;
    private long DET_ID;
    private View root;
    private WO_ITEM_SPR po_det;
    private STK_COUNT_DET material_list;;
    private TextInputEditText edMaterialNo;
    private TextInputEditText edPOunit;
    private TextInputEditText edScanCode;
    private TextInputEditText edReceiptQty;
    private TextInputEditText edSloc;
    private TextInputEditText edWBS ;
    private TextInputEditText edDefaultLoc;
    private RecyclerView recyclerView;
    private ArrayAdapter<DocType> objectsAdapter;
    private StockCountReceiptAdapter batchesAdapter;
    private Drawable icon;
    private List<BATCH> batchList;
    private ArrayAdapter<BATCH> batchTypeAdapter;
    private AutoCompleteTextView tvBatch;
    private BATCH batchType;
    private STK_COUNT_HDR st_hdr;
    private boolean wbsFlag = false;
    long item;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                HDR_ID = bundle.getLong(AppConstants.PARENT_ID, -1);
                st_hdr = (STK_COUNT_HDR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_stock_count_item_scan_receipt, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DET_ID = -1;
        //loadData();
        UpdateBatchView();
        GetBatch();

        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView,
                                          @NonNull RecyclerView.ViewHolder viewHolder,
                                          @NonNull RecyclerView.ViewHolder viewHolder1) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                        if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
                            int position = viewHolder.getAdapterPosition();
                            STK_COUNT_ITEMS_RECIEPT gr_det_batch = batchesAdapter.getItemByPosition(position);
                            if (gr_det_batch != null) {
                                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                alert.setTitle("Delete this batch?");
                                alert.setPositiveButton("OK", (dialog, whichButton) -> {
                                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSTK_COUNT_ITEMS_RECIEPTByDet_ID(gr_det_batch.SLNO);
                                    recyclerView.removeViewAt(position);
                                    batchesAdapter.notifyItemRemoved(position);
                                    batchesAdapter.deleteBatch(position);
                                    batchesAdapter.notifyItemRangeChanged(position, batchesAdapter.getItemCount());
                                });
                                alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
                                    recyclerView.setAdapter(batchesAdapter);
                                });
                                alert.show();
                            }
                            UpdateBatchView();
                        }
                    }

                    @Override
                    public void onChildDraw(@NonNull Canvas c,
                                            @NonNull RecyclerView recyclerView,
                                            @NonNull RecyclerView.ViewHolder viewHolder,
                                            float dX, float dY, int actionState, boolean isCurrentlyActive) {
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        View itemView = viewHolder.itemView;
                        int backgroundCornerOffset = 20;

                        int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconBottom = iconTop + icon.getIntrinsicHeight();

                        if (dX > 0) {
                            int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                            int iconRight = itemView.getLeft() + iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getLeft(), itemView.getTop(),
                                    itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
                        } else if (dX < 0) {
                            int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                            int iconRight = itemView.getRight() - iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                                    itemView.getTop(), itemView.getRight(), itemView.getBottom());
                        } else {
                            background.setBounds(0, 0, 0, 0);
                        }
                        background.draw(c);
                        icon.draw(c);
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onClick(View view) {
        GR_DET gr_det = (GR_DET) view.getTag();
        DET_ID = gr_det.DET_ID;
    }

    private void setupView() {

        edReceiptQty = root.findViewById(R.id.edReceiptQty);
        edWBS = root.findViewById(R.id.edWBS);
        edMaterialNo = root.findViewById(R.id.edmaterailCode);
        edMaterialNo.setEnabled(false);
        edPOunit = root.findViewById(R.id.edPoUnit);
        edPOunit.setEnabled(false);
        edSloc = root.findViewById(R.id.edSloc);
        edSloc.setEnabled(false);
        tvBatch = root.findViewById(R.id.tvBatch);
        edDefaultLoc= root.findViewById(R.id.edDefaultLoc);
        edDefaultLoc.setEnabled(false);
        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        MaterialButton btnAdd = root.findViewById(R.id.btnAdd);
        btnNext.setOnClickListener(v -> onNextClick());
        btnAdd.setOnClickListener(v -> onAddClick());
//        edMFDDate.setOnClickListener(v -> onDateEntryClick(1));
//        edEXPDate.setOnClickListener(v -> onDateEntryClick(2));

        recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        batchesAdapter = new StockCountReceiptAdapter(new ArrayList<>());
        recyclerView.setAdapter(batchesAdapter);

        edScanCode = root.findViewById(R.id.edScanCode);
        edScanCode.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onScanCodeKeyEvent();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });
        edScanCode.requestFocus();
    }

    private boolean onScanCodeKeyEvent() {
        String ScanCode = edScanCode.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid bar code");
            return false;
        } else {
            ParseBarCode(ScanCode);
        }
        return true;
    }

    private void ParseBarCode(String scanCode) {
        if (scanCode.length() <= 0 ) {
            return;
        }

        resetUI();

        String materialCode = "";
        String[] barCodeData = scanCode.split(App.BarCodeSeparator);


        if (barCodeData.length>1) {
            materialCode = barCodeData[1].toString();
        }else{
            materialCode = barCodeData[0];
        }

        if(scanCode.startsWith("P")){
            wbsFlag = true;
        }else{
            wbsFlag = false;
        }


        Log.d("length##",String.valueOf(barCodeData.length));




        material_list = App.getDatabaseClient().getAppDatabase().genericDao().getSTK_COUNT_DET_ITEM(materialCode);

        if(material_list==null){
            showToastLong("Invalid Item! "+scanCode);
            edScanCode.setText("");
            edScanCode.requestFocus();
            return;
        }

        edMaterialNo.setText(material_list.MATERIAL);
        edPOunit.setText(material_list.MATL_DESC);
        edSloc.setText(material_list.SLOC_NAME);
        edDefaultLoc.setText(material_list.DEFAULTLOC);

        edWBS.setText(material_list.WBS_ELEMENT);
        edScanCode.setText("");
        edScanCode.requestFocus();

    }

    private void resetUI(){
        edMaterialNo.setText("");
        edPOunit.setText("");
        edReceiptQty.setText("");
        tvBatch.setText("");
        edSloc.setText("");
        edWBS.setText("");
    }
    private void onNextClick() {
        getActivity().onBackPressed();
    }

    private void onAddClick() {
        if (IsDataValid()) {
            SaveData();
        }
    }

    private boolean IsDataValid() {
        String errMessage;

        if (batchType == null) {
            errMessage ="Invalid Batch";
            showToast(errMessage);
            tvBatch.setError(errMessage);
            return false;
        }

        String material = edMaterialNo.getText().toString();
        if(edMaterialNo.length()==0){
            errMessage ="Invalid MaterialNo";
            showToast(errMessage);
            edMaterialNo.setError(errMessage);
            return false;
        }


        String wbs = edWBS.getText().toString();
        if(wbsFlag && wbs.length()==0){
            errMessage ="Invalid WBS Element";
            showToast(errMessage);
            edWBS.setError(errMessage);
            return false;
        }

        rcd_qty = Utils.convertToFloat(edReceiptQty.getText().toString().trim(), -1);
        if (rcd_qty < 0 ) {
            errMessage = "Invalid Receipt quantity";
            showToast(errMessage);
            edReceiptQty.setError(errMessage);
            return false;
        }

        if(wbs.length()==0){
            wbs = "*";
        }
        material_list = App.getDatabaseClient().getAppDatabase().genericDao().getMaterialItemCode(material,batchType.Name,wbs);
        //Log.d("item###",String.valueOf(material_list.ITEM));
        if(material_list==null){
            errMessage = "Invalid Material";
            showAlert("Warnig",errMessage);

            return false;
        }

        return true;
    }

    private void SaveData() {
        STK_COUNT_ITEMS_RECIEPT gr_det;
        gr_det = new STK_COUNT_ITEMS_RECIEPT();
        long seqNo = Utils.GetMaxValue("STK_COUNT_ITEMS_RECIEPT", "SLNO", false, "", "");
        gr_det.SLNO = seqNo;
        gr_det.DOC_NO = st_hdr.DOC_NO;
        gr_det.MATERIAL = material_list.MATERIAL;
        gr_det.MAT_DESC = material_list.MATL_DESC;
        gr_det.GUI_ID = Utils.GetGUID();
        gr_det.ENTRY_QNT = rcd_qty;
        gr_det.ENTRY_UOM = "";
        gr_det.HEAD_ID = material_list.HEAD_ID;
        gr_det.WBS = edWBS.getText().toString();
        gr_det.BATCH = batchType.Name;
        gr_det.ITEM = material_list.ITEM;
        App.getDatabaseClient().getAppDatabase().genericDao().insertSTK_COUNT_ITEMS_RECIEPT(gr_det);
        UpdateBatchView();
        resetUI();
        edScanCode.setText("");
        edScanCode.requestFocus();
    }

    private void UpdateBatchView() {
        List<STK_COUNT_ITEMS_RECIEPT> grDetlist = App.getDatabaseClient().getAppDatabase().genericDao().getSTK_COUNT_ITEMS_RECIEPT(st_hdr.ID);
        batchesAdapter.addItems(grDetlist);
    }

    private void GetBatch() {
        batchList = new ArrayList<BATCH>();
        BATCH batch;

        batch = new BATCH();
        batch.Code = "01";
        batch.Name = "OLD";
        batchList.add(batch);

        batch = new BATCH();
        batch.Code = "02";
        batch.Name = "NEW";
        batchList.add(batch);


        batchTypeAdapter = new ArrayAdapter<>(this.getContext(), R.layout.dropdown_menu_popup_item, batchList);
        tvBatch.setAdapter(batchTypeAdapter);
        tvBatch.setSelection(0);
        tvBatch.setThreshold(100);
        tvBatch.setOnItemClickListener((parent, view, position, id) -> {
            if (position >= 0) {
                batchType = batchTypeAdapter.getItem(position);
            } else {
                batchType = null;
            }
        });
    }
}
