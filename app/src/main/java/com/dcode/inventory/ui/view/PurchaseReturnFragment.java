package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.LS_HDR;
import com.dcode.inventory.data.model.PUR_RET_HDR;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PurchaseReturnFragment extends BaseFragment {
    private TextInputEditText edDocNo;
    private TextInputEditText edRecNo;
//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    private DocType ordType;
    private String DocNo;
    private long HDR_ID;
    private String receiptNo;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Bundle bundle = null;
        if (this.getArguments() != null) {
            bundle = this.getArguments();
        }

        if (bundle != null) {
            ModuleID = bundle.getInt(AppConstants.MODULE_ID, 0);
            receiptNo = bundle.getString(AppConstants.SELECTED_ID, null);
        }

        promptResource = R.string.ref_no;

        View root = inflater.inflate(R.layout.fragment_purchase_ret_no, container, false);
        ImageView imgDocNo = root.findViewById(R.id.imgDocNo);
        imgDocNo.setImageResource(R.drawable.ic_005);

        edRecNo = root.findViewById(R.id.edReceiptNo);
        if(receiptNo!= null && receiptNo.length()>0){
            edRecNo.setVisibility(View.VISIBLE);
            edRecNo.setText(receiptNo);
        }else{
            edRecNo.setVisibility(View.INVISIBLE);
            edRecNo.setText("");
        }


        edDocNo = root.findViewById(R.id.edDocNo);
        edDocNo.setHint(promptResource);
/*        edDocNo.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    keyCode == KeyEvent.KEYCODE_ENTER) {
                return onDocNoKeyEvent();
            }
            return false;
        });*/

        MaterialButton btDocNo = root.findViewById(R.id.btDocNo);
        btDocNo.setOnClickListener(v -> OnNextClick());

/*        tvDocTypes = root.findViewById(R.id.acDocType);
        tvDocTypes.setOnItemClickListener((parent, view, position, id) -> {
            if (position > 0) {
                ordType = objectsAdapter.getItem(position);
            } else {
                ordType = null;
            }
        });*/

        return root;
    }

/*    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDocTypes();
    }*/

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/

    private void OnNextClick() {
        Validate();
    }

    private void Validate() {

        edRecNo.setVisibility(View.INVISIBLE);
        edRecNo.setText("");

        String errMessage;
        DocNo = edDocNo.getText().toString();
        if (DocNo.length() == 0) {
            errMessage = String.format("%s required", getString(promptResource));
            showToast(errMessage);
            edDocNo.setError(errMessage);
            return;
        }

        OpenDocHeader(DocNo);

//        if (ordType == null) {
//            showToast("Document type required");
//            return;
//        }
//        GR_HDR gr_hdr = App.getDatabaseClient().getAppDatabase().genericDao().getPendingGRByDocNo(DocNo);
//        if (gr_hdr != null && gr_hdr.HDR_ID > 0) {
//            HDR_ID = gr_hdr.HDR_ID;
//            ordType = new DocType();
//            ordType.CODE = gr_hdr.ORTYP;
//            ordType.NAME = gr_hdr.ORTYP;
//            App.gr_hdr_id = HDR_ID;
//            OpenDocHeader();
//        } else {
            //DownloadPO_HDR(DocNo, "");
            //pp.gr_hdr_id = -1;
        //}
    }

/*    private void getDocTypes() {
        List<DocType> orTypList = App.getDatabaseClient().getAppDatabase().genericDao().getAllOrTyps();
        objectsAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, orTypList);
        tvDocTypes.setAdapter(objectsAdapter);
        tvDocTypes.setSelection(0);
        tvDocTypes.setThreshold(100);

        objectsAdapter.notifyDataSetChanged();
    }*/

    private void OpenDocHeader(String receiptNo) {

        //Log.d("ls_hdr.ORDNO#",receiptNo.ORDNO);
        if (receiptNo==null) {
            showToast(" Error: No document found.");
            return;
        }

//        if (ls_hdr.CAN_LOAD!=null && ls_hdr.CAN_LOAD.length()>0 && ls_hdr.CAN_LOAD.equals("N")) {
//            showToast(ls_hdr.LOAD_MSG);
//            return;
//        }

        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, receiptNo);
        //bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_purchase_ret_rec, bundle);

    }

    private void DownloadPO_HDR(final String docNo, final String docType) {
        JsonObject requestObject = ServiceUtils.LoadSlips.getPurRetHdr(docNo, docType);

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (!(xmlDoc != null || xmlDoc.trim().length() != 0)) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        PUR_RET_HDR[] ls_hdr = new Gson().fromJson(xmlDoc, PUR_RET_HDR[].class);
                        if (ls_hdr==null || ls_hdr.length<=0) {
                            showToast(" Error: No data received.");
                            return;
                        }
                        //OpenDocHeader(ls_hdr[0]);
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showToast(msg);
            }
        });
    }


}
