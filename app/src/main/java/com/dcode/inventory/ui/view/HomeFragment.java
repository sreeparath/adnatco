package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;

import com.dcode.inventory.R;

public class HomeFragment
        extends BaseFragment {

    View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        root = inflater.inflate(R.layout.fragment_home, container, false);

        TextView tv_bg_goods_receipt = root.findViewById(R.id.tv_bg_goods_receipt);
        tv_bg_goods_receipt.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_goods_receipt));

        TextView tv_bg_wo = root.findViewById(R.id.tv_bg_wo);
        tv_bg_wo.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_wo_no));

        TextView tv_bg_transfer = root.findViewById(R.id.tv_bg_transfer);
        tv_bg_transfer.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_transfers));


        TextView tv_bg_stock_count = root.findViewById(R.id.tv_bg_stock_count);
        tv_bg_stock_count.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_stock_count));

//        TextView tv_bg_reverse_wo = root.findViewById(R.id.tv_bg_reverse_wo);
//        tv_bg_reverse_wo.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_reverse_wo));

        TextView tv_bg_download = root.findViewById(R.id.tv_bg_download);
        tv_bg_download.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_downloads_menu));

        TextView tv_bg_uploads = root.findViewById(R.id.tv_bg_uploads);
        tv_bg_uploads.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_upload));

        TextView tv_bg_settings = root.findViewById(R.id.tv_bg_settings);
        tv_bg_settings.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_settings));


        TextView tv_bg_logout = root.findViewById(R.id.tv_bg_logout);
        tv_bg_logout.setOnClickListener(v -> OnLogoutClick());

//        RelativeLayout cardView2 = root.findViewById(R.id.cardView2);
//        cardView2.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_goods_receipt));
//
//        RelativeLayout cardStockCountPanel = root.findViewById(R.id.cardStockCountPanel);
//        cardStockCountPanel.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_stock_count));
//
//        RelativeLayout cardTransferPanel = root.findViewById(R.id.cardTransferPanel);
//        cardTransferPanel.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_transfers));
//
//        RelativeLayout cardReverseWoPanel = root.findViewById(R.id.cardReverseWoPanel);
//        cardReverseWoPanel.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_reverse_wo));
//
//        RelativeLayout cardDownloadPanel = root.findViewById(R.id.cardDownloadPanel);
//        cardDownloadPanel.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_downloads_menu));
//
//        RelativeLayout cardUploadPanel = root.findViewById(R.id.cardUploadPanel);
//        cardUploadPanel.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_upload));
//
//        RelativeLayout cardSettingPanel = root.findViewById(R.id.cardSettingPanel);
//        cardSettingPanel.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_settings));
//
//        RelativeLayout cardLogoutPanel = root.findViewById(R.id.cardLogoutPanel);
//        cardLogoutPanel.setOnClickListener(v -> OnLogoutClick());


        return root;
    }

    private void OnModuleButtonClick(int action_id) {
        Navigation.findNavController(root).navigate(action_id);
    }

    private void OnLogoutClick() {
        ((MainActivity) requireActivity()).SignOut();
    }
}
