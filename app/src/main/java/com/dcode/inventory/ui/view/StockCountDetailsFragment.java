package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.STK_COUNT_DET;
import com.dcode.inventory.data.model.STK_COUNT_HDR;
import com.dcode.inventory.data.model.STK_COUNT_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.WO_HDR_SPR;
import com.dcode.inventory.data.model.WO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.WO_ITEM_SPR;
import com.dcode.inventory.network.model.GenericSubmissionResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.dcode.inventory.ui.adapter.StockDetailsAdapter;
import com.dcode.inventory.ui.adapter.WODetAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class StockCountDetailsFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    private STK_COUNT_HDR st_hdr;
    private long HDR_ID;
    private String DocNo;
    private DocType ordTyp;
    private View root;
    private StockDetailsAdapter detAdapter;
    private String filterString;
    private FloatingActionButton proceed;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        HDR_ID = App.gr_hdr_id;
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                DocNo = bundle.getString(AppConstants.SELECTED_ID, DocNo);
                ordTyp = (DocType) bundle.getSerializable(AppConstants.SELECTED_CODE);
                st_hdr = (STK_COUNT_HDR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_stock_count_details, container, false);

//        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

//    @Override
//    public void onPrepareOptionsMenu(Menu menu) {
//        MenuInflater inflater = requireActivity().getMenuInflater();
//        inflater.inflate(R.menu.top_app_bar, menu);
//
//        final MenuItem searchItem = menu.findItem(R.id.search);
//        final SearchView searchView = (SearchView) searchItem.getActionView();
//        searchView.setOnQueryTextListener(this);
//    }

    @Override
    public void onClick(View view) {
//        WO_ITEM_SPR detailData = (WO_ITEM_SPR) view.getTag();
//        if (detailData == null || detailData.ITEM_ID <= 0) {
//            return;
//        }

//        Bundle bundle = new Bundle();
//        bundle.putString(AppConstants.MASTER_ID, DocNo);
//        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
//        bundle.putSerializable(AppConstants.SELECTED_OBJECT, detailData);
//        bundle.putSerializable(AppConstants.SELECTED_CODE, ordTyp);
//        bundle.putSerializable(AppConstants.SELECTED_NAME, st_hdr);
//        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_wo_item_receipt, bundle);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        detAdapter.getFilter().filter(newText);
        filterString = newText;
        return false;
    }

    private void setupView() {
        MaterialButton btnPostReceipt = root.findViewById(R.id.btnPostReceipt);
        btnPostReceipt.setOnClickListener(v -> onPostReceiptClick());

        proceed = (FloatingActionButton) root.findViewById(R.id.proceed);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt(AppConstants.MODULE_ID, ModuleID);
                bundle.putSerializable(AppConstants.SELECTED_OBJECT, st_hdr);
                ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_stock_count_receipt, bundle);
            }
        });

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        detAdapter = new StockDetailsAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(detAdapter);

        List<STK_COUNT_DET> detList = App.getDatabaseClient().getAppDatabase().genericDao().getSTK_COUNT_DET(st_hdr.ID);
        detAdapter.addItems(detList);

//        if(detList.isEmpty()){
//            proceed.setEnabled(true);
//        }else{
//            proceed.setEnabled(false);
//        }

    }

    private void onPostReceiptClick() {
       getActivity().onBackPressed();
    }

}
