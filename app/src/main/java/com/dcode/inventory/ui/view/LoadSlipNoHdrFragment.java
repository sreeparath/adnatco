package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.LS_HDR;
import com.dcode.inventory.data.model.PICK_HDR;
import com.dcode.inventory.data.model.PO_HDR;
import com.google.android.material.button.MaterialButton;

public class LoadSlipNoHdrFragment extends BaseFragment {
    private long HDR_ID;
    private String DocNo;
    private DocType ordTyp;
    private LS_HDR lsHdr;

    private TextView tvPOTypeName;
    private TextView tvDocType;
    private TextView tvDocNo;
    private TextView tvCustname;
    private TextView tvDriver;
    private TextView tvHelper;
    private TextView tvVehicle;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Bundle bundle = null;
        if (this.getArguments() != null) {
            bundle = this.getArguments();
        }

        if (bundle != null) {
            ModuleID = bundle.getInt(AppConstants.MODULE_ID, 0);
            DocNo = bundle.getString(AppConstants.SELECTED_ID, DocNo);
            //ordTyp = (DocType) bundle.getSerializable(AppConstants.SELECTED_CODE);
            HDR_ID = bundle.getLong(AppConstants.PARENT_ID, -1);
            lsHdr = (LS_HDR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
        }

        View view = inflater.inflate(R.layout.fragment_load_slip_no_hdr_new, container, false);

        //tvPOTypeName = view.findViewById(R.id.tvPOTypeName);
        tvDocType = view.findViewById(R.id.edType);
        tvDocNo = view.findViewById(R.id.edDocNo);
        tvCustname = view.findViewById(R.id.edCust);
        tvDriver = view.findViewById(R.id.edDriver);
        tvHelper = view.findViewById(R.id.edHelper);
        tvVehicle = view.findViewById(R.id.edVehicle);

        MaterialButton btDocNo = view.findViewById(R.id.btnNext);
        btDocNo.setOnClickListener(v -> OnNextClick());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

    private void setupView() {
        if (lsHdr != null) {
            //tvPOTypeName.setText("LOAD SLIP");
            tvCustname.setText(lsHdr.CUSTNAME);
            tvDriver.setText(lsHdr.DRIVER);
            tvHelper.setText(lsHdr.HELPER);
            tvDocNo.setText(lsHdr.LSNO);
            tvDocType.setText(lsHdr.ORTYP);
            tvVehicle.setText(lsHdr.VEHICLE);
            //tvPOTypeName.setText(lsHdr.POTYPENAME);
        }
    }

    private void OnNextClick() {
        Bundle bundle = new Bundle();
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_CODE, ordTyp);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, lsHdr);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_load_slips_item_new, bundle);
    }
}
