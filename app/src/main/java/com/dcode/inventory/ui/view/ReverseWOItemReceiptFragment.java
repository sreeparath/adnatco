package com.dcode.inventory.ui.view;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.BATCH;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.GR_DET;
import com.dcode.inventory.data.model.RE_WO_HDR_SPR;
import com.dcode.inventory.data.model.RE_WO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.RE_WO_ITEM_SPR;
import com.dcode.inventory.data.model.WO_HDR_SPR;
import com.dcode.inventory.data.model.WO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.WO_ITEM_SPR;
import com.dcode.inventory.ui.adapter.ReWoItemReceiptAdapter;
import com.dcode.inventory.ui.adapter.WoItemReceiptAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class ReverseWOItemReceiptFragment
        extends BaseFragment
        implements View.OnClickListener {
    private final ColorDrawable background = new ColorDrawable(Color.YELLOW);
    private DocType recType;
    private float rcd_qty;
    private long HDR_ID;
    private long DET_ID;
    private View root;
    private RE_WO_ITEM_SPR po_det;
    private RE_WO_HDR_SPR wo_hdr;
    private TextView tvCode;
    private TextView tvName;
    private TextView tvUnit;
    private TextView tvRerQty;
    private TextView tvReceiptQty;
    private TextView tvDefaultStore;
    private TextView tvUnits;
    private TextView tvRate;
    private TextInputEditText edReceiptQty;
    private TextInputEditText edBatchNo;
    private TextInputEditText edMFDDate;
    private TextInputEditText edEXPDate;
    private AutoCompleteTextView tvRecType;
    private TextInputEditText edDefault;
    private RecyclerView recyclerView;
    private ArrayAdapter<DocType> objectsAdapter;
    private ReWoItemReceiptAdapter batchesAdapter;
    private Drawable icon;
    private List<BATCH> batchList;
    private ArrayAdapter<BATCH> batchTypeAdapter;
    private AutoCompleteTextView tvBatch;
    private BATCH batchType;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                HDR_ID = bundle.getLong(AppConstants.PARENT_ID, -1);
                po_det = (RE_WO_ITEM_SPR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
                wo_hdr = (RE_WO_HDR_SPR) bundle.getSerializable(AppConstants.SELECTED_NAME);
            }
        }

        root = inflater.inflate(R.layout.fragment_wo_item_receipt, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DET_ID = -1;
        //fillSLOC_LIST();
        loadData();
        GetBatch();

        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView,
                                          @NonNull RecyclerView.ViewHolder viewHolder,
                                          @NonNull RecyclerView.ViewHolder viewHolder1) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                        if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
                            int position = viewHolder.getAdapterPosition();
                            RE_WO_ITEMS_RECIEPT gr_det_batch = batchesAdapter.getItemByPosition(position);
                            if (gr_det_batch != null) {
                                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                alert.setTitle("Delete this batch?");
                                alert.setPositiveButton("OK", (dialog, whichButton) -> {
                                    App.getDatabaseClient().getAppDatabase().genericDao().deleteRE_WO_ITEMS_RECIEPTByDet_ID(gr_det_batch.SLNO);
                                    recyclerView.removeViewAt(position);
                                    batchesAdapter.notifyItemRemoved(position);
                                    batchesAdapter.deleteBatch(position);
                                    batchesAdapter.notifyItemRangeChanged(position, batchesAdapter.getItemCount());
                                    po_det = App.getDatabaseClient().getAppDatabase().genericDao().getRE_WO_ITEM_SPR_ONE(gr_det_batch.WO_NO,gr_det_batch.MATERIAL);
                                    Log.d("2po2_det##",String.valueOf(po_det.RCQTY));
                                    tvReceiptQty.setText(String.valueOf(po_det.RCQTY));
                                });
                                alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
                                    //Put actions for CANCEL button here, or leave in blank
                                    batchesAdapter.notifyDataSetChanged();
                                });
                                alert.show();
                            }
                            loadData();
                            //UpdateBatchView();
                        }
                    }

                    @Override
                    public void onChildDraw(@NonNull Canvas c,
                                            @NonNull RecyclerView recyclerView,
                                            @NonNull RecyclerView.ViewHolder viewHolder,
                                            float dX, float dY, int actionState, boolean isCurrentlyActive) {
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        View itemView = viewHolder.itemView;
                        int backgroundCornerOffset = 20;

                        int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconBottom = iconTop + icon.getIntrinsicHeight();

                        if (dX > 0) {
                            int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                            int iconRight = itemView.getLeft() + iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getLeft(), itemView.getTop(),
                                    itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
                        } else if (dX < 0) {
                            int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                            int iconRight = itemView.getRight() - iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                                    itemView.getTop(), itemView.getRight(), itemView.getBottom());
                        } else {
                            background.setBounds(0, 0, 0, 0);
                        }
                        background.draw(c);
                        icon.draw(c);
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onClick(View view) {
        GR_DET gr_det = (GR_DET) view.getTag();
        DET_ID = gr_det.DET_ID;
    }

    private void setupView() {

        tvCode = root.findViewById(R.id.tvCode);
        tvName = root.findViewById(R.id.tvName);
        tvUnit = root.findViewById(R.id.tvUnit);
        tvRerQty = root.findViewById(R.id.tvRerQty);
        tvReceiptQty = root.findViewById(R.id.tvReceiptQty);
        //tvDefaultStore =  root.findViewById(R.id.edDefaultStore);

        edReceiptQty = root.findViewById(R.id.edReceiptQty);
        //edBatchNo = root.findViewById(R.id.edBatchNo);
        //edMFDDate = root.findViewById(R.id.edMFDDate);
        //edEXPDate = root.findViewById(R.id.edEXPDate);
        //tvRecType = root.findViewById(R.id.tvRecType);
        edDefault = root.findViewById(R.id.edDefault);
        edDefault.setEnabled(false);
        tvBatch = root.findViewById(R.id.tvBatch);
        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        MaterialButton btnAdd = root.findViewById(R.id.btnAdd);
        btnNext.setOnClickListener(v -> onNextClick());
        btnAdd.setOnClickListener(v -> onAddClick());
//        edMFDDate.setOnClickListener(v -> onDateEntryClick(1));
//        edEXPDate.setOnClickListener(v -> onDateEntryClick(2));

        recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        batchesAdapter = new ReWoItemReceiptAdapter(new ArrayList<>());
        recyclerView.setAdapter(batchesAdapter);
    }

    private void fillSLOC_LIST() {
        List<DocType> recTypeList = App.getDatabaseClient().getAppDatabase().genericDao().getAllSLOC_LIST_SPR();
        objectsAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, recTypeList);
        tvRecType.setAdapter(objectsAdapter);
        tvRecType.setSelection(0);
        tvRecType.setThreshold(100);
        tvRecType.setOnItemClickListener((parent, view, position, id) -> {
            if (position >= 0) {
                recType = objectsAdapter.getItem(position);
            } else {
                recType = null;
            }
        });
    }

    private void loadData() {
        if (po_det == null || po_det.ITEM_ID <= 0) {
            return;
        }

        tvCode.setText(po_det.MATERIAL);
        tvName.setText(po_det.MATL_DESC);
        tvRerQty.setText(String.valueOf(po_det.REQUIREMENT_QUANTITY));
        tvReceiptQty.setText(String.valueOf(po_det.RCQTY));
        tvUnit.setText(po_det.REQUIREMENT_QUANTITY_UNIT);
        edReceiptQty.setText("");
        edDefault.setText(po_det.DEFAULTLOC);
        //tvDefaultStore.setText(po_det.SLOC_NAME);
        UpdateBatchView();
    }

    private void onDateEntryClick(int ID) {
        Calendar currentDate = Calendar.getInstance();
        int curYear = currentDate.get(Calendar.YEAR);
        int curMonth = currentDate.get(Calendar.MONTH);
        int curDay = currentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), (datePicker, cYear, cMonth, cDay) -> {
            String dateString = String.format(Locale.ENGLISH, "%d-%d-%d", cDay, (cMonth + 1), cYear);
            String selectedDate = Utils.getFormattedDate(dateString, Utils.ISSUE_DATE_FORMAT, Utils.PRINT_DATE_FORMAT);

            if (ID == 1) {
                edMFDDate.setText(selectedDate);
                edMFDDate.setError(null);
            } else if (ID == 2) {
                edEXPDate.setText(selectedDate);
                edEXPDate.setError(null);
            }
        }, curYear, curMonth, curDay);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }

    private void onNextClick() {
        getActivity().onBackPressed();
    }

    private void onAddClick() {
        if (IsDataValid()) {
            SaveData();
        }
    }

    private boolean IsDataValid() {
        String errMessage;

//        if (batchType == null) {
//            errMessage ="Invalid Batch";
//            showToast(errMessage);
//            tvBatch.setError(errMessage);
//            return false;
//        }

//        if (recType == null) {
//            errMessage ="Invalid SLOC";
//            showToast(errMessage);
//            tvRecType.setError(errMessage);
//            return false;
//        }

//        if (recType.BIN_ID!=po_det.BIN_ID){
//            errMessage ="Invalid BIN ID";
//            showToast(errMessage);
//            tvRecType.setError(errMessage);
//            return false;
//        }


        rcd_qty = Utils.convertToFloat(edReceiptQty.getText().toString().trim(), -1);
        if (rcd_qty < 0 ) {
            errMessage = "Invalid Receipt quantity";
            showToast(errMessage);
            edReceiptQty.setError(errMessage);
            return false;
        }

        Log.d("po_det##here ",String.valueOf(po_det.RCQTY));
        if ((rcd_qty+po_det.RCQTY)>po_det.REQUIREMENT_QUANTITY) {
            errMessage = "Qty not matching!";
            showToast(errMessage);
            edReceiptQty.setError(errMessage);
            return false;
        }
//        if ((rcd_qty < 0 || rcd_qty > (po_det.QUANTITY+po_det.TOL_QTY)) && po_det.IS_QTY_VALIDATE==1) {
//            errMessage = "Invalid Receipt quantity";
//            showToast(errMessage);
//            edReceiptQty.setError(errMessage);
//            return false;
//        }

//        if (po_det.BATCHFLG == 1) {
//            String batchNo = edBatchNo.getText().toString().trim();
//            if (batchNo.length() == 0) {
//                errMessage = "Invalid batch no";
//                showToast(errMessage);
//                edBatchNo.setError(errMessage);
//                return false;
//            }

//            String mfd = edMFDDate.getText().toString();
//            if (mfd.length() == 0) {
//                errMessage = "Invalid manufacture date";
//                showToast(errMessage);
//                edMFDDate.setError(errMessage);
//                return false;
//            }

//            String exp = edEXPDate.getText().toString();
//            if (exp.length() == 0) {
//                errMessage = "Invalid expiry date";
//                showToast(errMessage);
//                edEXPDate.setError(errMessage);
//                return false;
          //  }

//            Date mfDate = Utils.convertStringToDate(mfd, Utils.PRINT_DATE_FORMAT);
//            Date exDate = Utils.convertStringToDate(exp, Utils.PRINT_DATE_FORMAT);
//            Date today = new Date();
//            if (mfDate.after(today)) {
//                errMessage = "Invalid manufacture date, date in future";
//                showToast(errMessage);
//                edMFDDate.setError(errMessage);
//                return false;
//            }
//
//            if (exDate.before(today)) {
//                errMessage = "Invalid expiry date, already expired";
//                showToast(errMessage);
//                edEXPDate.setError(errMessage);
//                return false;
//            }
      //  }
        return true;
    }

    private void SaveData() {
        RE_WO_ITEMS_RECIEPT gr_det;
        gr_det = new RE_WO_ITEMS_RECIEPT();
        //long seqNo = Utils.GetMaxValue("WO_ITEMS_RECIEPT", "SLNO", false, "MATERIAL", po_det.MATERIAL);
        //gr_det.SLNO = seqNo;
        gr_det.WO_NO = po_det.ORDERID;
        gr_det.MATERIAL = po_det.MATERIAL;
        gr_det.MATL_DESC = po_det.MATL_DESC;
        gr_det.GUI_ID = Utils.GetGUID();
        gr_det.ENTRY_QNT = rcd_qty;
        gr_det.MATERIAL = po_det.MATERIAL;
        gr_det.ENTRY_UOM_ISO = po_det.REQUIREMENT_QUANTITY_UNIT;
        gr_det.PLANT = wo_hdr.PLANT;
        gr_det.STGE_LOC = edDefault.getText().toString().substring(0,4);
        if(batchType!=null){
            gr_det.VAL_TYPE = batchType.Name;
        }
        gr_det.MOVE_STLOC = edDefault.getText().toString().substring(0,4);
        gr_det.BIN_ID =po_det.BIN_ID;
        App.getDatabaseClient().getAppDatabase().genericDao().insertRE_WO_ITEMS_RECIEPT(gr_det);
        po_det = App.getDatabaseClient().getAppDatabase().genericDao().getRE_WO_ITEM_SPR_ONE(gr_det.WO_NO,gr_det.MATERIAL);
        loadData();
        UpdateBatchView();
        edReceiptQty.setText("");

    }

    private void UpdateBatchView() {
        List<RE_WO_ITEMS_RECIEPT> grDetlist = App.getDatabaseClient().getAppDatabase().genericDao().getRE_WO_ITEMS_RECIEPT(po_det.ORDERID,po_det.MATERIAL);
        //List<GR_DET> detList = App.getDatabaseClient().getAppDatabase().genericDao().getGR_DET(HDR_ID, po_det.SLNO);
        batchesAdapter.addItems(grDetlist);
    }

    private void GetBatch() {
        batchList = new ArrayList<BATCH>();
        BATCH batch;

        batch = new BATCH();
        batch.Code = "01";
        batch.Name = "OLD";
        batchList.add(batch);

        batch = new BATCH();
        batch.Code = "02";
        batch.Name = "NEW";
        batchList.add(batch);


        batchTypeAdapter = new ArrayAdapter<>(this.getContext(), R.layout.dropdown_menu_popup_item, batchList);
        tvBatch.setAdapter(batchTypeAdapter);
        tvBatch.setSelection(0);
        tvBatch.setThreshold(100);
        tvBatch.setOnItemClickListener((parent, view, position, id) -> {
            if (position >= 0) {
                batchType = batchTypeAdapter.getItem(position);
            } else {
                batchType = null;
            }
        });
    }
}
