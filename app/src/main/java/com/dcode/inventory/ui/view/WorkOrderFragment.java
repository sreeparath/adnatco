package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;

import com.dcode.inventory.R;

public class WorkOrderFragment
        extends BaseFragment {

    View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        root = inflater.inflate(R.layout.fragment_work_order, container, false);

        TextView tv_bg_issue = root.findViewById(R.id.tv_bg_issue);
        tv_bg_issue.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_issue));

        TextView tv_bg_reverse = root.findViewById(R.id.tv_bg_reverse);
        tv_bg_reverse.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_reverse_issue));

        return root;
    }

    private void OnModuleButtonClick(int action_id) {
        Navigation.findNavController(root).navigate(action_id);
    }

}
