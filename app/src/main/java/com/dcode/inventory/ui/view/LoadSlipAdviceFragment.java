package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.data.model.LOAD_SLIP_ITEMS_NEW;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS;
import com.dcode.inventory.ui.adapter.LoadSlipItemsAdviceAdapter;
import com.dcode.inventory.ui.adapter.PickListItemsAdviceAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LoadSlipAdviceFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    private View root;
    private LoadSlipItemsAdviceAdapter picklistAdviceAdapter;
    private LOAD_SLIP_ITEMS_NEW  pl_item;
    private List<LOAD_SLIP_ITEMS_NEW> pl_advice_list;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            pl_item = (LOAD_SLIP_ITEMS_NEW) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
        }

        root = inflater.inflate(R.layout.fragment_pick_list_advice, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        picklistAdviceAdapter = new LoadSlipItemsAdviceAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(picklistAdviceAdapter);

        loadData();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        picklistAdviceAdapter.getFilter().filter(newText);
        String filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
//        PICK_HDR ps_hdr = (PICK_HDR) view.getTag();
//        if (ps_hdr == null || ps_hdr.ORD_NO.length() == 0) {
//            return;
//        }
//
//        Bundle bundle = new Bundle();
//        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
//        bundle.putSerializable(AppConstants.SELECTED_OBJECT, ps_hdr);
//
//        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_picklist_items, bundle);
    }

    private void loadData() {
        if (pl_item == null) {
            showToast("Some error");
            return;
        }

        pl_advice_list = App.getDatabaseClient().getAppDatabase().genericDao().GetLOAD_SLIP_ITEMS_DETAILS(pl_item.ORD_NO,pl_item.ITCODE);
        if (pl_advice_list != null && pl_advice_list.size()>0 ) {
            picklistAdviceAdapter.addItems(pl_advice_list);
        }
        else {
            showToast(" Alert : No data found.");
        }
    }
}
