package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.data.model.AD_GR_HDR;
import com.dcode.inventory.data.model.AD_PO_HDR;
import com.dcode.inventory.data.model.AD_PO_ITEMS_SPR;
import com.dcode.inventory.data.model.CONFIRM_DOC_HDR;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.PO_HDR;
import com.google.android.material.button.MaterialButton;

import java.util.List;

public class DocumentHeaderFragment extends BaseFragment {
    private long HDR_ID;
    private String DocNo;
    private DocType ordTyp;
    private AD_PO_HDR poHdr;

    private TextView tvPOTypeName;
    private TextView tvPoLines;
    private TextView tvDocNo;
    private TextView tvVendor;
    private TextView tvDocDt;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Bundle bundle = null;
        if (this.getArguments() != null) {
            bundle = this.getArguments();
        }

        if (bundle != null) {
            ModuleID = bundle.getInt(AppConstants.MODULE_ID, 0);
            DocNo = bundle.getString(AppConstants.SELECTED_ID, DocNo);
//            ordTyp =  bundle.getString(AppConstants.SELECTED_CODE);
            HDR_ID = bundle.getLong(AppConstants.PARENT_ID, -1);
            poHdr = (AD_PO_HDR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
        }

        View view = inflater.inflate(R.layout.fragment_doc_header_new, container, false);
        tvPOTypeName = view.findViewById(R.id.tvPOTypeName);
        tvDocDt = view.findViewById(R.id.edDocDt);
        tvDocNo = view.findViewById(R.id.edDocNo);
        tvVendor = view.findViewById(R.id.edVendor);
        tvPoLines = view.findViewById(R.id.edpoline);

        MaterialButton btDocNo = view.findViewById(R.id.btnNext);
        btDocNo.setOnClickListener(v -> OnNextClick());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

    private void setupView() {
//        if (ordTyp != null && DocNo.length() != 0) {
//            poHdr = App.getDatabaseClient().getAppDatabase().genericDao().getAD_PO_HDR(DocNo);
//        }

        if (poHdr != null) {
            tvDocDt.setText(poHdr.CREAT_DATE);
            tvDocNo.setText(poHdr.PO_NUMBER);
            Log.d("LineCnt",String.valueOf(poHdr.LineCnt));
            tvPoLines.setText(String.valueOf(poHdr.LineCnt));
            tvVendor.setText(poHdr.VENDOR);
            tvPOTypeName.setText(R.string.goods_receipt);
        }
    }

    private void OnNextClick() {
            List<AD_PO_ITEMS_SPR> detList = App.getDatabaseClient().getAppDatabase().genericDao().getAD_PO_ITEMS_SPR(DocNo);

            if(detList.size()==0){
               showAlert("Error","No items Found!");
               return;
            }
            Bundle bundle = new Bundle();
            bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
            bundle.putString(AppConstants.SELECTED_ID, DocNo);
            bundle.putSerializable(AppConstants.SELECTED_CODE, ordTyp);
            ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_doc_details, bundle);
    }
}
