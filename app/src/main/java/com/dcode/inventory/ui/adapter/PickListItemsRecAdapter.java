package com.dcode.inventory.ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.data.model.GR_DET;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS_RECEIPT;

import java.util.ArrayList;
import java.util.List;

public class PickListItemsRecAdapter
        extends RecyclerView.Adapter<PickListItemsRecAdapter.RecyclerViewHolder>
        implements Filterable {

    private List<PICK_LIST_ITEMS_RECEIPT> receiptsList;
    private List<PICK_LIST_ITEMS_RECEIPT> receiptsListFull;
    private View.OnClickListener shortClickListener;
    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<PICK_LIST_ITEMS_RECEIPT> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(receiptsListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (PICK_LIST_ITEMS_RECEIPT item : receiptsListFull) {
                    if ((item.ITCODE != null && item.ITCODE.toLowerCase().contains(filterPattern)) ||
                            (item.BATCHNO != null && item.BATCHNO.toLowerCase().contains(filterPattern))) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            receiptsList.clear();
            receiptsList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public PickListItemsRecAdapter(List<PICK_LIST_ITEMS_RECEIPT> dataList, View.OnClickListener shortClickListener) {
        this.receiptsList = dataList;
        this.receiptsListFull = new ArrayList<>(dataList);
        this.shortClickListener = shortClickListener;
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public PickListItemsRecAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_pl_items_rec, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        final PICK_LIST_ITEMS_RECEIPT po_det = receiptsList.get(position);
        Log.d("po_det#",po_det.toString());
        holder.tvCode.setText(po_det.ITCODE);
        holder.tvName.setText("");
        holder.tvUnits.setText(String.valueOf(po_det.IUNITS));
        holder.tvBatchNo.setText(String.valueOf(po_det.BATCHNO));
        holder.tvFact.setText(String.valueOf(po_det.FACTOR));
        holder.tvQty.setText(String.valueOf(po_det.QTY));
        holder.tvTotalQty.setText(String.valueOf(po_det.FACTOR*po_det.QTY));

        holder.itemView.setTag(po_det);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return receiptsList.size();
    }

    public PICK_LIST_ITEMS_RECEIPT getItemByPosition(int position) {
        return this.receiptsList.get(position);
    }

    public void deleteBatch(int position) {
        try {
//            grDetListFull.remove(position);
            receiptsList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, receiptsList.size());
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
    }

    public void addItems(List<PICK_LIST_ITEMS_RECEIPT> dataList) {
        this.receiptsList = dataList;
        this.receiptsListFull = new ArrayList<>(dataList);
        notifyDataSetChanged();
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCode;
        private TextView tvName;
        private TextView tvUnits;
        private TextView tvBatchNo;
        private TextView tvFact;
        private TextView tvQty;
        private TextView tvTotalQty;

        RecyclerViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            tvCode = view.findViewById(R.id.tvCode);
            tvUnits = view.findViewById(R.id.tvUnits);
            tvBatchNo = view.findViewById(R.id.tvBatchNo);
            tvFact = view.findViewById(R.id.tvFact);
            tvQty = view.findViewById(R.id.tvQty);
            tvTotalQty = view.findViewById(R.id.tvTotalQty);
        }
    }
}
