package com.dcode.inventory.ui.view;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.STOCK_HEADER;
import com.dcode.inventory.data.model.STOCK_HEADER_ITEMS;
import com.dcode.inventory.data.model.STOCK_ITEMS_RECEIPT;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.model.GenericSubmissionResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.dcode.inventory.ui.adapter.StockItemsRecAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class InventoryCountReceiptFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    STOCK_HEADER st_hdr;
    STOCK_HEADER picklist_item_rec;
    List<STOCK_ITEMS_RECEIPT> stock_items_receipts;
    private TextInputEditText edScanCode;
    private TextInputEditText edItemCode;
    private TextInputEditText edBatchNo;
    private TextInputEditText edMafDate;
    private TextInputEditText edExpdate;
    private TextInputEditText edReceiptNo;
    //private TextInputEditText edUnit;
    private TextInputEditText edQty;
    private View root;
    private StockItemsRecAdapter stockItemsRecAdapter;
    private ArrayAdapter<DocType> objectsAdapter;
    //private Button btnAdd;
    private Drawable icon;
    private RecyclerView recyclerView;
    private final ColorDrawable background = new ColorDrawable(Color.RED);
    private float rcd_qty;
    private boolean validateItem = false;
    private AutoCompleteTextView edUnit;
    private DocType unitType;
    private TextInputEditText edERPBatch;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                st_hdr = (STOCK_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_stock_head_receipt, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();

        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView,
                                          @NonNull RecyclerView.ViewHolder viewHolder,
                                          @NonNull RecyclerView.ViewHolder viewHolder1) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                        if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
                            int position = viewHolder.getAdapterPosition();
                            STOCK_ITEMS_RECEIPT pl_item_rec = stockItemsRecAdapter.getItemByPosition(position);
                            if (pl_item_rec != null) {
                                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                alert.setTitle("Delete this batch?");
                                alert.setPositiveButton("OK", (dialog, whichButton) -> {
                                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSTOCK_ITEMS_RECEIPTBysl_no(pl_item_rec.SL_NO);
                                    recyclerView.removeViewAt(position);
                                    stockItemsRecAdapter.notifyItemRemoved(position);
                                    stockItemsRecAdapter.deleteBatch(position);
                                    stockItemsRecAdapter.notifyItemRangeChanged(position, stockItemsRecAdapter.getItemCount());
                                });
                                alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
                                    //Put actions for CANCEL button here, or leave in blank
                                    stockItemsRecAdapter.notifyDataSetChanged();
                                });
                                alert.show();
                            }
                            UpdateBatchView();
                        }
                    }

                    @Override
                    public void onChildDraw(@NonNull Canvas c,
                                            @NonNull RecyclerView recyclerView,
                                            @NonNull RecyclerView.ViewHolder viewHolder,
                                            float dX, float dY, int actionState, boolean isCurrentlyActive) {
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        View itemView = viewHolder.itemView;
                        int backgroundCornerOffset = 20;

                        int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconBottom = iconTop + icon.getIntrinsicHeight();

                        if (dX > 0) {
                            int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                            int iconRight = itemView.getLeft() + iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getLeft(), itemView.getTop(),
                                    itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
                        } else if (dX < 0) {
                            int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                            int iconRight = itemView.getRight() - iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                                    itemView.getTop(), itemView.getRight(), itemView.getBottom());
                        } else {
                            background.setBounds(0, 0, 0, 0);
                        }
                        background.draw(c);
                        icon.draw(c);
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        DownloadStockItems();
    }

    private void DownloadStockItems() {
        showProgress(false);

        JsonObject jsonObject;
        JsonArray array = new JsonArray();
        jsonObject = ServiceUtils.createJsonObject("YRCD", "22", st_hdr.YRCD);
        array.add(jsonObject);
        jsonObject = ServiceUtils.createJsonObject("ORTYP", "22", st_hdr.ORTYP);
        array.add(jsonObject);
        jsonObject = ServiceUtils.createJsonObject("ORDNO", "22", st_hdr.ORDNO);
        array.add(jsonObject);

        JsonObject requestObject = new JsonObject();
        requestObject.addProperty("ProcName", "PDT.STOCK_COUNT_ITEMS_SPR");
        requestObject.addProperty("DBName", App.currentCompany.DbName);
        requestObject.add("dbparams", array);

        System.out.println("requestObject##"+requestObject.toString());

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        STOCK_HEADER_ITEMS[] sh_items = new Gson().fromJson(xmlDoc, STOCK_HEADER_ITEMS[].class);
                        App.getDatabaseClient().getAppDatabase().genericDao().insertSTOCK_HEADER_ITEMS(sh_items);
                        if (sh_items==null || sh_items.length<=0) {
                            validateItem  = false;
                        }else{
                            validateItem  = true;
                        }
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showToast(msg);
            }
        });
    }

    private void setupView() {
        edItemCode = root.findViewById(R.id.edItemcode);
        edItemCode.setEnabled(false);
        edBatchNo = root.findViewById(R.id.edBatchNo);
        edMafDate = root.findViewById(R.id.edMFDDate);
        //edMafDate.setEnabled(false);
        edExpdate = root.findViewById(R.id.edEXPDate);
        //edExpdate.setEnabled(false);
        edMafDate.setOnClickListener(v -> onDateEntryClick(1));
        edExpdate.setOnClickListener(v -> onDateEntryClick(2));
        edQty = root.findViewById(R.id.edReceiptQty);
        edReceiptNo = root.findViewById(R.id.edReceiptNo);
        edUnit = root.findViewById(R.id.edUnit);
        edERPBatch = root.findViewById(R.id.edERPBATCH);

        fillUnits();
        MaterialButton btnSave = root.findViewById(R.id.btnAdd);
        btnSave.setOnClickListener(v -> onAddClick());
        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> {
            List<STOCK_ITEMS_RECEIPT> details = App.getDatabaseClient().getAppDatabase().genericDao().GetSTOCK_ITEMS_RECEIPT_upload(st_hdr.ORDNO);
            if(details!=null && details.size()>0) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Are you sure want to upload to server ?");
                alert.setPositiveButton("OK", (dialog, whichButton) -> {
                    UploadPending();
                });
                alert.setNegativeButton("Cancel", (dialog, whichButton) -> {

                });
                alert.show();
            }else{
                showAlert("Message","No data to post!");
                return;
            }
        });
        recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        stockItemsRecAdapter = new StockItemsRecAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(stockItemsRecAdapter);

        edScanCode = root.findViewById(R.id.edScanCode);
        edScanCode.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onScanCodeKeyEvent();
                }else{
                    return false;
                }
            }else{
                return false;
            }

        });

    }

    private void onDateEntryClick(int ID) {
        Calendar currentDate = Calendar.getInstance();
        int curYear = currentDate.get(Calendar.YEAR);
        int curMonth = currentDate.get(Calendar.MONTH);
        int curDay = currentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), (datePicker, cYear, cMonth, cDay) -> {
            String dateString = String.format(Locale.ENGLISH, "%d-%d-%d", cDay, (cMonth + 1), cYear);
            String selectedDate = Utils.getFormattedDate(dateString, Utils.ISSUE_DATE_FORMAT, Utils.PRINT_DATE_FORMAT);

            if (ID == 1) {
                edMafDate.setText(selectedDate);
                edMafDate.setError(null);
            } else if (ID == 2) {
                edExpdate.setText(selectedDate);
                edExpdate.setError(null);
            }
        }, curYear, curMonth, curDay);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }





    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        stockItemsRecAdapter.getFilter().filter(newText);
        String filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
//        LS_ITEMS ls_items = (LS_ITEMS) view.getTag();
//
//        if (ls_items == null) {
//            return;
//        }

        //NavigateToItemDetails(ls_items, "");
    }

    /*private void NavigateToItemDetails(LS_ITEMS ls_items, String scanCode) {
        if (App.ls_hdr_id <= 0) {
            slip_hdr = new SLIP_HDR();
            slip_hdr.GUID = Utils.GetGUID();
            slip_hdr.IS_UPLOADED = 0;
            slip_hdr.ORDNO = ls_hdr.LSNO;
            slip_hdr.ORTYP = ls_hdr.ORTYP;
            slip_hdr.YRCD = ls_hdr.YRCD;
            slip_hdr.LOCODE = App.currentLocation.LOCODE;
            slip_hdr.PRICING = ls_hdr.PRICING;

            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_HDR(ls_hdr);;
            LS_ITEMS[] array = new LS_ITEMS[ls_itemsList.size()];
            ls_itemsList.toArray(array);
            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_ITEMS(array);

            long HDR_ID = App.getDatabaseClient().getAppDatabase().genericDao().insertSlip_HDR(slip_hdr);
            App.ls_hdr_id = HDR_ID;
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, ls_items);
        bundle.putString(AppConstants.SELECTED_CODE, scanCode);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_load_slip_item_detail, bundle);
    }*/

    private boolean onScanCodeKeyEvent() {
        String ScanCode = edScanCode.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid bar code");
            return false;
        } else {
            ParseBarCode(ScanCode.toUpperCase().trim().toString());
        }
        return true;
    }

    private void onNextClick() {
        resetUI();
        getActivity().onBackPressed();
    }

    private void onAddClick() {
        if (IsDataValid()) {
            SaveData();
            edScanCode.requestFocus();
        }
    }

    private boolean IsDataValid() {
        String errMessage;

        String itcode = edItemCode.getText().toString().trim();
        if (itcode.length() == 0) {
            errMessage = "Invalid Item Code";
            showToast(errMessage);
            edItemCode.setError(errMessage);
            return false;
        }


        String qty = edQty.getText().toString().trim();
        if (qty.length() == 0) {
            errMessage = "Receipt quantity not be zero";
            showToast(errMessage);
            edQty.setError(errMessage);
            return false;
        }

        if (!(unitType.CODE!=null && unitType.CODE.length()>0)) {
            errMessage ="Invalid Unit type";
            showToast(errMessage);
            edUnit.setError(errMessage);
            return false;
        }

        String erpbatch = edERPBatch.getText().toString().trim();
        if (erpbatch.length() > 0) {
            String erpbatchCom =  erpbatch.substring(erpbatch.length() - 2);
            if(!App.currentCompany.Code.equalsIgnoreCase(erpbatchCom)){
                errMessage ="Company not matching with  erpbatch!";
                showToast(errMessage);
                edERPBatch.setError(errMessage);
                return false;
            }
        }

//        String mfd = edMafDate.getText().toString();
//        Date mfDate = Utils.convertStringToDate(mfd, Utils.PRINT_DATE_FORMAT);
//        if (mfDate==null) {
//            errMessage = "Invalid manufacture date";
//            showToast(errMessage);
//            edMafDate.setError(errMessage);
//            return false;
//        }

//        String exp = edExpdate.getText().toString();
//        Date exDate = Utils.convertStringToDate(exp, Utils.PRINT_DATE_FORMAT);
//        if (exDate==null) {
//            errMessage = "Invalid expiry date";
//            showToast(errMessage);
//            edExpdate.setError(errMessage);
//            return false;
//        }

       /* String mfd = edMafDate.getText().toString();
        if (mfd.length() == 0) {
            errMessage = "Invalid manufacture date";
            showToast(errMessage);
            edMafDate.setError(errMessage);
            return false;
        }

        String exp = edExpdate.getText().toString();
        if (exp.length() == 0) {
            errMessage = "Invalid expiry date";
            showToast(errMessage);
            edExpdate.setError(errMessage);
            return false;
        }

        Date mfDate = Utils.convertStringToDate(mfd, Utils.PRINT_DATE_FORMAT);
        Date exDate = Utils.convertStringToDate(exp, Utils.PRINT_DATE_FORMAT);
        Date today = new Date();
        if (mfDate.after(today)) {
            errMessage = "Invalid manufacture date, date in future";
            showToast(errMessage);
            edMafDate.setError(errMessage);
            return false;
        }

        if (exDate.before(today)) {
            errMessage = "Invalid expiry date, already expired";
            showToast(errMessage);
            edExpdate.setError(errMessage);
            return false;
        }*/

        return true;
    }

    private void SaveData() {
        STOCK_ITEMS_RECEIPT st_rec = new STOCK_ITEMS_RECEIPT();
        long seqNo = Utils.GetMaxValue("STOCK_ITEMS_RECEIPT", "SL_NO", false, "", "");
        st_rec.SL_NO = seqNo;
        st_rec.ORD_NO = st_hdr.ORDNO;
        st_rec.ITCODE = edItemCode.getText().toString().trim();
        st_rec.ITUNITS = edUnit.getText().toString().trim();
        st_rec.BATCHNO = edBatchNo.getText().toString().trim();
        String mfd = edMafDate.getText().toString();
        Date mfDate = Utils.convertStringToDate(mfd, Utils.PRINT_DATE_FORMAT);
        if(mfDate==null){
            st_rec.MANFDATE = null;
        }else{
            st_rec.MANFDATE = mfd;
        }
        String exp = edExpdate.getText().toString();
        Date exDate = Utils.convertStringToDate(exp, Utils.PRINT_DATE_FORMAT);
        if(exDate==null){
            st_rec.EXPDATE = null;
        }else{
            st_rec.EXPDATE = exp;
        }

        st_rec.CNTQTY = Utils.convertToFloat(edQty.getText().toString().trim(), -1);
        st_rec.SCANTIME = Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT);
        st_rec.ERPBATCH = edERPBatch.getText().toString();

        App.getDatabaseClient().getAppDatabase().genericDao().insertSTOCK_ITEMS_RECEIPT(st_rec);
        //ls_items = App.getDatabaseClient().getAppDatabase().genericDao().getLS_Items(ls_items.ORDNO, ls_items.ORTYP, ls_items.ITCODE);
        loadData();
        resetUI();
    }

    private void resetUI(){
        edItemCode.setText("");
        edBatchNo.setText("");
        edMafDate.setText("");
        edExpdate.setText("");
        edReceiptNo.setText("");
        edQty.setText("");
        edUnit.setText("");
        edScanCode.setText("");
        edERPBatch.setText("");
    }

    private void loadData() {
        stock_items_receipts = App.getDatabaseClient().getAppDatabase().genericDao().GetSTOCK_ITEMS_RECEIPT(st_hdr.ORDNO);
        if (stock_items_receipts != null ) {
            stockItemsRecAdapter.addItems(stock_items_receipts);
        }
    }


    private void UploadPending() {
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LoadSlips.GetStockCountData(st_hdr);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLines(requestObject, new Callback<GenericSubmissionResponse>() {
            @Override
            public void success(GenericSubmissionResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String ReceiptNo = String.valueOf(genericSubmissionResponse.getRetId());
                        //edReceiptNo.setTag(ReceiptNo);
                        //edReceiptNo.setText(ReceiptNo);
                        ServiceUtils.SyncActivity.truncateLocalTables(AppConstants.DatabaseEntities.STOCK_HEADER,st_hdr.ORDNO, genericSubmissionResponse.getRetId());
                        ServiceUtils.SyncActivity.truncateLocalTables(AppConstants.DatabaseEntities.STOCK_HEADER_ITEMS,st_hdr.ORDNO, genericSubmissionResponse.getRetId());
                        ServiceUtils.SyncActivity.truncateLocalTables(AppConstants.DatabaseEntities.STOCK_ITEMS_RECEIPT,st_hdr.ORDNO, genericSubmissionResponse.getRetId());
                        //String[] msg = genericSubmissionResponse.getErrMessage().split("~");
                        String msg = genericSubmissionResponse.getErrMessage();
                        //resetUI();
                        showAlert("Upload Success",msg);
//                        if (msg.length > 1) {
//                            ReceiptNo = msg[2];
//                        }
                        getActivity().onBackPressed();
                        Bundle bundle = new Bundle();
                        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
                        bundle.putSerializable(AppConstants.SELECTED_ID, msg);
                        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_inventory_count, bundle);
                    } else {
                        showAlert("Error",genericSubmissionResponse.getErrMessage());
                        //showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showToast(genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }

    private void ParseBarCode(String scanCode) {
        if (scanCode.length() <= 0 ) {
            return;
        }

        String itcode = "";
        edItemCode.setText("");
        edBatchNo.setText("");
        edMafDate.setText("");
        edExpdate.setText("");
        edReceiptNo.setText("");
        edQty.setText("");
        edUnit.setText("");
        edERPBatch.setText("");

        Log.d("validateItem##",String.valueOf(validateItem));

        if (scanCode.startsWith(App.BarCodeSeparator)) {
            /*  1. ITCODE, 2. BatchNo, 3. MFD Date, 4. EXP Date, 5. Qty, 6. RefDoc */
            String[] barCodeData = scanCode.split(App.BarCodeSeparator);

            if (barCodeData.length > 5) {
                itcode = barCodeData[1];
                if(!validateItemExist(itcode)){
                    edItemCode.setText("");
                    edScanCode.setText("");
                    edScanCode.requestFocus();
                    return;
                }
                edItemCode.setText(itcode);
                edItemCode.setTag(itcode);
                edBatchNo.setText(barCodeData[2]);
                edMafDate.setText(barCodeData[3]);
                if(barCodeData[3]!=null && barCodeData[3].length()>0){
                    edMafDate.setEnabled(false);
                }else{
                    edMafDate.setEnabled(true);
                }
                edExpdate.setText(barCodeData[4]);
                if(barCodeData[4]!=null && barCodeData[4].length()>0){
                    edExpdate.setEnabled(false);
                }else{
                    edExpdate.setEnabled(true);
                }
                edReceiptNo.setText(barCodeData[6]);
                Log.d("length##",String.valueOf(barCodeData.length));
                if(barCodeData.length >= 8){
                    edUnit.setText(barCodeData[7]);
                    unitType.CODE = barCodeData[7];
                    unitType.NAME = barCodeData[7];
                }
                if(barCodeData.length == 9){
                    edERPBatch.setText(barCodeData[8]);
                    if(barCodeData[8]!=null && barCodeData[8].length()>0){
                        edERPBatch.setEnabled(false);
                    }else{
                        edERPBatch.setEnabled(true);
                    }

                }
            }
        } else if(scanCode.startsWith("$")){
            edMafDate.setEnabled(true);
            edExpdate.setEnabled(true);
            edERPBatch.setEnabled(true);
            String[] barCodeData = scanCode.split(App.BarCodeSeparatorDollor);
            itcode = barCodeData[1];
            if(!validateItemExist(itcode)){
                edItemCode.setText("");
                edScanCode.setText("");
                edScanCode.requestFocus();
                return;
            }
            edItemCode.setText(String.valueOf(barCodeData[1]));
            edItemCode.setTag(String.valueOf(barCodeData[1]));
            Log.d("barCodeData.length##",String.valueOf(barCodeData.length));
            if(barCodeData.length > 2){
                edUnit.setText(barCodeData[2]);
                unitType.CODE = barCodeData[2];
                unitType.NAME = barCodeData[2];
            }
        }else {
            edMafDate.setEnabled(true);
            edExpdate.setEnabled(true);
            edERPBatch.setEnabled(true);
            // find item by itcode
            itcode = scanCode;
            if(!validateItemExist(itcode)){
                edItemCode.setText("");
                edScanCode.setText("");
                edScanCode.requestFocus();
                return;
            }

            edItemCode.setText(itcode);
            edItemCode.setTag(itcode);
        }
        edScanCode.setText("");
    }


    private boolean validateItemExist(String itemCode){
        List<STOCK_HEADER_ITEMS> itemDetails = App.getDatabaseClient().getAppDatabase().genericDao().getStockItemDetails(itemCode);
        if(itemDetails.size()==0 && validateItem){
            showAlert("Invalid","Item code not found! "+itemCode);
            return false;
        }
        return true;
    }
    private void UpdateBatchView() {
        stock_items_receipts = App.getDatabaseClient().getAppDatabase().genericDao().GetSTOCK_ITEMS_RECEIPT(st_hdr.ORDNO);
        if (stock_items_receipts != null ) {
            stockItemsRecAdapter.addItems(stock_items_receipts);
        }
    }

    private void fillUnits() {
        unitType = new DocType();
        edUnit.setText("");
        List<DocType> recTypeList = App.getDatabaseClient().getAppDatabase().genericDao().getAllUnits();
        Log.d("data",recTypeList.toString());
        objectsAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, recTypeList);
        edUnit.setAdapter(objectsAdapter);
        edUnit.setSelection(0);
        edUnit.setThreshold(100);
        edUnit.setOnItemClickListener((parent, view, position, id) -> {
            if (position >= 0) {
                unitType = objectsAdapter.getItem(position);
            } else {
                unitType = null;
            }
        });


    }
}
