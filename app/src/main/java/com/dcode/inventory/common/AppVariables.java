package com.dcode.inventory.common;

import android.content.Context;
import android.util.Log;

import com.dcode.inventory.App;
import com.dcode.inventory.R;

public class AppVariables {
    private static String MASTERS_PATH;
    private static String IMAGES_PATH;
    private static String OUT_FILES_PATH;
    public static String PdtId = "";
    public static String plant = "";

    public AppVariables(Context context) {
        try {
            Utils.GetExternalSDCardPath(context);

            String sMastersData = Utils.getValue(context, AppConstants.MASTERS_PATH, false);
            if (sMastersData.isEmpty()) {
                sMastersData = Utils.getExternalSdPath().concat(context.getString(R.string.def_masters_path));
//            } else {
//                sMastersData = Utils.getExternalSdPath().concat(sMastersData);
            }
            MASTERS_PATH = sMastersData;
            Log.d("MASTERS_PATH#", MASTERS_PATH);

        } catch (Exception e) {
            //Log.d(App.TAG, e.toString());
        }
    }

    public static String getPlant() {
        return plant;
    }

    public static void setPlant(String plant) {
        AppVariables.plant = plant;
    }

    public static String getPdtId() {
        return PdtId;
    }

    public static void setPdtId(String pdtId) {
        PdtId = pdtId;
    }

    public static String getMastersPath() {
        return MASTERS_PATH;
    }

    public static void setMastersPath(String value) {
        MASTERS_PATH = value.endsWith("/") ? value : value + "/";
    }

    public static String getImagesPath() {
        return IMAGES_PATH;
    }

    public static void setImagesPath(String value) {
        IMAGES_PATH = value.endsWith("/") ? value : value + "/";
    }

    public static String getOutFilesPath() {
        return OUT_FILES_PATH;
    }

    public static void setOutFilesPath(String value) {
        OUT_FILES_PATH = value.endsWith("/") ? value : value + "/";
    }

    public static String getMastersDb() {
        return getMastersPath().concat("ItemMast.db");
    }

    public static String getAppSettingFile() {
        return getMastersPath().concat("AppSettings.txt");
    }
}
