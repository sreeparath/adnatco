package com.dcode.inventory.network;

import com.dcode.inventory.App;
import com.dcode.inventory.network.adapter.ItemTypeAdapterFactory;
import com.dcode.inventory.network.service.GenericRetService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class NetworkClient {
    private static NetworkClient mInstance;
    private GenericRetService apiService;

    private NetworkClient() {

        String BASE_URL = App.appSettings().getBaseUrl();
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .create();

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(50, TimeUnit.SECONDS);
        client.setReadTimeout(60, TimeUnit.SECONDS);
        client.setWriteTimeout(60, TimeUnit.SECONDS);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .setClient(new OkClient(client))
                .setConverter(new GsonConverter(gson))
                .build();

        apiService = restAdapter.create(GenericRetService.class);
    }

    public static synchronized NetworkClient getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkClient();
        }

        return mInstance;
    }

    public GenericRetService getAPIService() {
        return apiService;
    }

    public void destroyInstance() {
        mInstance = null;
    }
}
