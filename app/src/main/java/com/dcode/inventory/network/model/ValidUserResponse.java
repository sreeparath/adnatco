package com.dcode.inventory.network.model;

import com.google.gson.annotations.SerializedName;

public class ValidUserResponse {

    @SerializedName("ID")
    int ID;

    @SerializedName("FullName")
    String FullName;

    @SerializedName("LoginName")
    String LoginName;

    @SerializedName("LoginPassword")
    private String LoginPassword;

    @SerializedName("SecurityToken")
    private String SecurityToken;

    @SerializedName("TokenExpiry")
    private String TokenExpiry;

    @SerializedName("UserID")
    private String UserID;

    @SerializedName("TeamID")
    private String TeamID;

    @SerializedName("LocCode")
    private String LocCode;

    @SerializedName("LocName")
    private String LocName;

    @SerializedName("Plant")
    String Plant;

    @SerializedName("PlantName")
    String PlantName;

//    @SerializedName("ValidFrom")
//    private String ValidFrom;
//
//    @SerializedName("ValidTo")
//    private String ValidTo;

    public  int getID() {
        return ID;
    }

    public String getUserID() {
        return UserID;
    }

    public String getLoginName() {
        return LoginName;
    }

    public String getLoginPassword() {
        return LoginPassword;
    }

    public String getFullName() {
        return FullName;
    }

    public String getTeamID() {
        return TeamID;
    }

    public String getLoCode() {
        return LocCode;
    }

    public String getLoName() {
        return LocName;
    }

    public String getPlant() {
        return Plant;
    }

    public String getPlantName() {
        return PlantName;
    }
}
