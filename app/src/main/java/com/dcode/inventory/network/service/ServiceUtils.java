package com.dcode.inventory.network.service;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.AppConstants.DatabaseEntities;
import com.dcode.inventory.common.AppVariables;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.AD_PO_HDR;
import com.dcode.inventory.data.model.AD_PO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.AD_PO_ITEMS_SPR;
import com.dcode.inventory.data.model.BARCODE_PRINTERS;
import com.dcode.inventory.data.model.CLS_PO_NUMBER;
import com.dcode.inventory.data.model.DRIVER_LIST;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.HELPER_LIST;
import com.dcode.inventory.data.model.LABEL_FORMATS;
import com.dcode.inventory.data.model.LOAD_SLIP_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.LOAD_SLIP_ITEMS_TYP;
import com.dcode.inventory.data.model.LOCATIONS;
import com.dcode.inventory.data.model.MATERIAL_LIST;
import com.dcode.inventory.data.model.ORTYP;
import com.dcode.inventory.data.model.PICK_LIST_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.PLANT_LIST;
import com.dcode.inventory.data.model.PO_DET;
import com.dcode.inventory.data.model.PO_HDR;
import com.dcode.inventory.data.model.PUR_RET_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.REASON_TYPE;
import com.dcode.inventory.data.model.REC_TYPE;
import com.dcode.inventory.data.model.RET_REASON_LIST;
import com.dcode.inventory.data.model.RE_WO_HDR_SPR;
import com.dcode.inventory.data.model.RE_WO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.RE_WO_ITEM_SPR;
import com.dcode.inventory.data.model.SALE_RET_HDR;
import com.dcode.inventory.data.model.SLIP_HDR;
import com.dcode.inventory.data.model.SLOC_LIST_SPR;
import com.dcode.inventory.data.model.SR_LINES_TYP;
import com.dcode.inventory.data.model.STK_COUNT_DET;
import com.dcode.inventory.data.model.STK_COUNT_HDR;
import com.dcode.inventory.data.model.STK_COUNT_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT221;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT221Q;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT222;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT222Q;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT411Q;
import com.dcode.inventory.data.model.STK_TRN_PRJ_WHR_RECIEPT415Q;
import com.dcode.inventory.data.model.STOCK_HEADER;
import com.dcode.inventory.data.model.STOCK_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.STORE_TRANSFER_ITEMS_RECEIPT;
import com.dcode.inventory.data.model.VEHICLE_LIST;
import com.dcode.inventory.data.model.WO_HDR_SPR;
import com.dcode.inventory.data.model.WO_ITEMS_RECIEPT;
import com.dcode.inventory.data.model.WO_ITEM_SPR;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.model.GenericSubmissionResponse;
import com.dcode.inventory.ui.dialog.CustomProgressDialog;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ServiceUtils {


    public static JsonObject createJsonObject(String paramName, String ParamType, String Value) {
        JsonObject jsonObject;
        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", paramName);
        jsonObject.addProperty("ParamType", ParamType);
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", Value);
        return jsonObject;
    }

    public static JsonObject createJsonObject(String paramName, String ParamType, String ParamTypName, String Value) {
        JsonObject jsonObject;
        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", paramName);
        jsonObject.addProperty("ParamType", ParamType);
        jsonObject.addProperty("TypName", ParamTypName);
        jsonObject.addProperty("Value", Value);
        return jsonObject;
    }

    public static JsonObject createJsonObject(String paramName, String ParamType, long Value) {
        JsonObject jsonObject;
        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", paramName);
        jsonObject.addProperty("ParamType", ParamType);
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", Value);
        return jsonObject;
    }

    public static JsonElement GenerateCommonReadParams(long PK_ID) {
        JsonArray array = new JsonArray();
        JsonObject jsonObject;

        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", "USER_ID");
        jsonObject.addProperty("ParamType", "22");
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", App.currentUser.USER_ID);
        array.add(jsonObject);

        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", "ID");
        jsonObject.addProperty("ParamType", "8");
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", PK_ID);
        array.add(jsonObject);
        return array;
    }

    public static class MastersDownload {

        static JsonObject getMastersRequestObject(final int masterID) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("MASTER_ID", "8", masterID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "APP.HHT_OBJECTS_MIN_SPR");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getLocationsObject() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "pdt.LOCLIST_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        static JsonObject getOrTypObject() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "pdt.ORTYP_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        static JsonObject geRecTypObject() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "pdt.REC_TYPE_LIST_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        static JsonObject geReasonTypObject() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "pdt.REASON_TYPE_LIST_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        static JsonObject getAuditInstructionsRequestObject() {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
//            jsonObject = createJsonObject("MASTER_ID", "8", masterID);
//            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "DBO.HHT_AUDIT_INSTRUCT_SPR");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        static JsonObject getMovementInstructionsRequestObject() {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
//            jsonObject = createJsonObject("MASTER_ID", "8", masterID);
//            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "DBO.HHT_MOVEMENT_INSTRUCT_SPR");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }
    }

    public static class GoodsReceipt {

        public static JsonObject getGoodsReceiptHdr(final String ordNo, final String ordType) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "8", App.currentUser.ID);
            array.add(jsonObject);
            jsonObject = createJsonObject("PO_NUMBER", "22", ordNo);
            array.add(jsonObject);
            jsonObject = createJsonObject("TYPE", "8", 1);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.PO_HEADER_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getGoodsReceiptDet(final String ordNo, final String ordType) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "8", App.currentUser.ID);
            array.add(jsonObject);
            jsonObject = createJsonObject("PO_NUMBER", "22", ordNo);
            array.add(jsonObject);
            jsonObject = createJsonObject("MASTER_ID", "8", 1025);
            array.add(jsonObject);


            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.PO_ITEM_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject GetGR(final String po_num,String delNo) {
            AD_PO_HDR data = App.getDatabaseClient().getAppDatabase().genericDao().getAD_PO_HDR(po_num);
            if (data == null || data.HDR_ID <= 0) {
                return null;
            }

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USER_ID", "8", App.currentUser.ID));
            jsonArray.add(createJsonObject("GUI_ID", "22", Utils.GetGUID()));
            //jsonArray.add(createJsonObject("DEV_TIME", "22", Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT)));
            jsonArray.add(createJsonObject("PO_NUMBER", "22", data.PO_NUMBER));
            jsonArray.add(createJsonObject("REF_DOC_NO", "22", delNo));
            jsonArray.add(createJsonObject("PLANT", "22", App.currentUser.PLANT));
            jsonArray.add(createJsonObject("MOVE_TYPE", "22", "101"));
            jsonArray.add(createJsonObject("GM_CODE", "22", "01"));
            jsonArray.add(createJsonObject("LINES", "30", "SAP.PORECEIPT_SUB_TYP", GenerateGR_Lines(data.PO_NUMBER)));

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.GRN_UL_TOMIMS_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GenerateGR_Lines(String pk_id) {
            Gson gson = new Gson();
            List<AD_PO_ITEMS_RECIEPT> details = App.getDatabaseClient().getAppDatabase().genericDao().getAD_PO_ITEMS_RECIEPTForUpload(pk_id);
            String jsonString = gson.toJson(details);
            return jsonString;
        }

        public static JsonObject GetGR_ItemsPrintList(String ReceiptNo,String type) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = createJsonObject("RECEIPTNO", "22", ReceiptNo);
            array.add(jsonObject);
            jsonObject = createJsonObject("PRINT_TYPE", "22", type);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.GR_ITEM_PRINT_LST_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }
    }

    public static class StockTransfer {

        public static JsonObject GetSTK_TRN221(final String po_num,String delNo) {
//            WO_HDR_SPR data = App.getDatabaseClient().getAppDatabase().genericDao().getWO_HDR_SPRByDocNo(po_num);
//            if (data == null || data.HDR_ID <= 0) {
//                return null;
//            }

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("HEAD", "30", "SAP.TRANSFERHEAD_TYP", GenerateSTK_TRN221_Head(po_num)));
            jsonArray.add(createJsonObject("LINES", "30", "SAP.TRANSFERSUB_TYP", GenerateSTK_TRN221_Lines(po_num)));

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.MAT_TRANSFER_UL_TOMIMS_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GenerateSTK_TRN221_Head(String pk_id) {
            Gson gson = new Gson();
            List headList = new ArrayList();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("GUI_ID", Utils.GetGUID());
            jsonObject.addProperty("WO_NO", pk_id);
            jsonObject.addProperty("MOVE_TYPE", "221");
            jsonObject.addProperty("GM_CODE", "03");
            jsonObject.addProperty("USER_ID", App.currentUser.ID);
            jsonObject.addProperty("START_DT",  Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT));
            jsonObject.addProperty("END_DT",  Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT));
            jsonObject.addProperty("STCK_TYPE",  "");
            headList.add(jsonObject);
            String jsonString = gson.toJson(headList);
            return jsonString;
        }

        private static String GenerateSTK_TRN221_Lines(String pk_id) {
            Gson gson = new Gson();
            List<STK_TRN_PRJ_WHR_RECIEPT221> details = App.getDatabaseClient().getAppDatabase().genericDao().getSTK_TRN_PRJ_WHR_RECIEPT_ITEM(pk_id);
            String jsonString = gson.toJson(details);
            return jsonString;
        }


        public static JsonObject GetSTK_TRN221Q(final String po_num,String delNo) {
            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("HEAD", "30", "SAP.TRANSFERHEAD_TYP", GenerateSTK_TRN221Q_Head(po_num)));
            jsonArray.add(createJsonObject("LINES", "30", "SAP.TRANSFERSUB_TYP", GenerateSTK_TRN221Q_Lines(po_num)));

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.MAT_TRANSFER_UL_TOMIMS_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GenerateSTK_TRN221Q_Head(String pk_id) {
            Gson gson = new Gson();
            List headList = new ArrayList();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("GUI_ID", Utils.GetGUID());
            jsonObject.addProperty("WO_NO", pk_id);
            jsonObject.addProperty("MOVE_TYPE", "221");
            jsonObject.addProperty("GM_CODE", "03");
            jsonObject.addProperty("USER_ID", App.currentUser.ID);
            jsonObject.addProperty("START_DT",  Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT));
            jsonObject.addProperty("END_DT",  Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT));
            jsonObject.addProperty("STCK_TYPE",  "Q");
            headList.add(jsonObject);
            String jsonString = gson.toJson(headList);
            return jsonString;
        }

        private static String GenerateSTK_TRN221Q_Lines(String pk_id) {
            Gson gson = new Gson();
            List<STK_TRN_PRJ_WHR_RECIEPT221Q> details = App.getDatabaseClient().getAppDatabase().genericDao().getSTK_TRN_PRJ_WHR_RECIEPT_ITEM221Q(pk_id);
            String jsonString = gson.toJson(details);
            return jsonString;
        }

        public static JsonObject GetSTK_TRN222(final String po_num,String delNo) {
            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("HEAD", "30", "SAP.TRANSFERHEAD_TYP", GenerateSTK_TRN222_Head(po_num)));
            jsonArray.add(createJsonObject("LINES", "30", "SAP.TRANSFERSUB_TYP", GenerateSTK_TRN222_Lines(po_num)));

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.MAT_TRANSFER_UL_TOMIMS_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GenerateSTK_TRN222_Head(String pk_id) {
            Gson gson = new Gson();
            List headList = new ArrayList();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("GUI_ID", Utils.GetGUID());
            jsonObject.addProperty("WO_NO", pk_id);
            jsonObject.addProperty("MOVE_TYPE", "222");
            jsonObject.addProperty("GM_CODE", "06");
            jsonObject.addProperty("USER_ID", App.currentUser.ID);
            jsonObject.addProperty("START_DT",  Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT));
            jsonObject.addProperty("END_DT",  Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT));
            jsonObject.addProperty("STCK_TYPE",  "");
            jsonObject.addProperty("MASTER_ID",  "1042");


            headList.add(jsonObject);
            String jsonString = gson.toJson(headList);
            return jsonString;
        }

        private static String GenerateSTK_TRN222_Lines(String pk_id) {
            Gson gson = new Gson();
            List<STK_TRN_PRJ_WHR_RECIEPT222> details = App.getDatabaseClient().getAppDatabase().genericDao().getSTK_TRN_PRJ_WHR_RECIEPT_ITEM222(pk_id);
            String jsonString = gson.toJson(details);
            return jsonString;
        }

        public static JsonObject GetSTK_TRN222Q(final String po_num,String delNo) {
            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("HEAD", "30", "SAP.TRANSFERHEAD_TYP", GenerateSTK_TRN222Q_Head(po_num)));
            jsonArray.add(createJsonObject("LINES", "30", "SAP.TRANSFERSUB_TYP", GenerateSTK_TRN222Q_Lines(po_num)));

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.MAT_TRANSFER_UL_TOMIMS_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GenerateSTK_TRN222Q_Head(String pk_id) {
            Gson gson = new Gson();
            List headList = new ArrayList();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("GUI_ID", Utils.GetGUID());
            jsonObject.addProperty("WO_NO", pk_id);
            jsonObject.addProperty("MOVE_TYPE", "222");
            jsonObject.addProperty("GM_CODE", "06");
            jsonObject.addProperty("USER_ID", App.currentUser.ID);
            jsonObject.addProperty("START_DT",  Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT));
            jsonObject.addProperty("END_DT",  Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT));
            jsonObject.addProperty("STCK_TYPE",  "Q");
            jsonObject.addProperty("MASTER_ID",  "1042");
            headList.add(jsonObject);
            String jsonString = gson.toJson(headList);
            return jsonString;
        }

        private static String GenerateSTK_TRN222Q_Lines(String pk_id) {
            Gson gson = new Gson();
            List<STK_TRN_PRJ_WHR_RECIEPT222Q> details = App.getDatabaseClient().getAppDatabase().genericDao().getSTK_TRN_PRJ_WHR_RECIEPT_ITEM222Q(pk_id);
            String jsonString = gson.toJson(details);
            return jsonString;
        }

        public static JsonObject GetSTK_TRN411Q(final String po_num,String delNo) {
            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("HEAD", "30", "SAP.TRANSFERHEAD_TYP", GenerateSTK_TRN411Q_Head(po_num)));
            jsonArray.add(createJsonObject("LINES", "30", "SAP.TRANSFERSUB_TYP", GenerateSTK_TRN411Q_Lines(po_num)));

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.MAT_TRANSFER_UL_TOMIMS_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GenerateSTK_TRN411Q_Head(String pk_id) {
            Gson gson = new Gson();
            List headList = new ArrayList();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("GUI_ID", Utils.GetGUID());
            jsonObject.addProperty("WO_NO", pk_id);
            jsonObject.addProperty("MOVE_TYPE", "411");
            jsonObject.addProperty("GM_CODE", "04");
            jsonObject.addProperty("USER_ID", App.currentUser.ID);
            jsonObject.addProperty("START_DT",  Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT));
            jsonObject.addProperty("END_DT",  Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT));
            jsonObject.addProperty("STCK_TYPE",  "Q");
            jsonObject.addProperty("MASTER_ID",  "1033");
            headList.add(jsonObject);
            String jsonString = gson.toJson(headList);
            return jsonString;
        }

        private static String GenerateSTK_TRN411Q_Lines(String pk_id) {
            Gson gson = new Gson();
            List<STK_TRN_PRJ_WHR_RECIEPT411Q> details = App.getDatabaseClient().getAppDatabase().genericDao().getSTK_TRN_PRJ_WHR_RECIEPT_ITEM411Q(pk_id);
            String jsonString = gson.toJson(details);
            return jsonString;
        }

        public static JsonObject GetSTK_TRN415Q(final String po_num,String delNo) {
            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("HEAD", "30", "SAP.TRANSFERHEAD_TYP", GenerateSTK_TRN415Q_Head(po_num)));
            jsonArray.add(createJsonObject("LINES", "30", "SAP.TRANSFERSUB_TYP", GenerateSTK_TRN415Q_Lines(po_num)));

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.MAT_TRANSFER_UL_TOMIMS_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GenerateSTK_TRN415Q_Head(String pk_id) {
            Gson gson = new Gson();
            List headList = new ArrayList();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("GUI_ID", Utils.GetGUID());
            jsonObject.addProperty("WO_NO", pk_id);
            jsonObject.addProperty("MOVE_TYPE", "415");
            jsonObject.addProperty("GM_CODE", "04");
            jsonObject.addProperty("USER_ID", App.currentUser.ID);
            jsonObject.addProperty("START_DT",  Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT));
            jsonObject.addProperty("END_DT",  Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT));
            jsonObject.addProperty("STCK_TYPE",  "Q");
            jsonObject.addProperty("MASTER_ID",  "1043");
            headList.add(jsonObject);
            String jsonString = gson.toJson(headList);
            return jsonString;
        }

        private static String GenerateSTK_TRN415Q_Lines(String pk_id) {
            Gson gson = new Gson();
            List<STK_TRN_PRJ_WHR_RECIEPT415Q> details = App.getDatabaseClient().getAppDatabase().genericDao().getSTK_TRN_PRJ_WHR_RECIEPT_ITEM415Q(pk_id);
            String jsonString = gson.toJson(details);
            return jsonString;
        }

    }

    public static class WorkOrder {

        public static JsonObject getWorkOrderHdr(final String ordNo, final String ordType) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "8", App.currentUser.ID);
            array.add(jsonObject);
            jsonObject = createJsonObject("WO_NUMBER", "22", ordNo);
            array.add(jsonObject);
            jsonObject = createJsonObject("TYPE", "8", 1);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.GET_WO_HEADER_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getWorkOrderDet(final String ordNo, final String ordType) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "8", App.currentUser.ID);
            array.add(jsonObject);
            jsonObject = createJsonObject("WO_NUMBER", "22", ordNo);
            array.add(jsonObject);
//            jsonObject = createJsonObject("MOVE_TYPE", "22", ordType);
//            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.GET_WO_ITEM_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject GetWO(final String po_num,String moveType) {
            WO_HDR_SPR data = App.getDatabaseClient().getAppDatabase().genericDao().getWO_HDR_SPRByDocNo(po_num);
            if (data == null || data.HDR_ID <= 0) {
                return null;
            }

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("HEAD", "30", "SAP.MATISSUEHEAD_TYP", GenerateWO_Head(data.WO_NUMBER,moveType)));
            jsonArray.add(createJsonObject("LINES", "30", "SAP.MATISSUESUB_TYP", GenerateWO_Lines(data.WO_NUMBER,moveType)));

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.MATISSUE_UL_TOMIMS_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GenerateWO_Head(String pk_id,String moveType) {
            Gson gson = new Gson();
            List headList = new ArrayList();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("GUI_ID", Utils.GetGUID());
            jsonObject.addProperty("WO_NO", pk_id);
            jsonObject.addProperty("MOVE_TYPE", moveType);
            jsonObject.addProperty("GM_CODE", "03");
            jsonObject.addProperty("USER_ID", App.currentUser.ID);
            jsonObject.addProperty("START_DT",  Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT));
            headList.add(jsonObject);
            String jsonString = gson.toJson(headList);
            return jsonString;
        }

        private static String GenerateWO_Lines(String pk_id,String moveType) {
            Gson gson = new Gson();
            List<WO_ITEMS_RECIEPT> details = App.getDatabaseClient().getAppDatabase().genericDao().getWO_ITEMS_RECIEPTForUpload(pk_id,moveType);
            String jsonString = gson.toJson(details);
            return jsonString;
        }

        public static JsonObject GetREWO(final String po_num,String delNo) {
            RE_WO_HDR_SPR data = App.getDatabaseClient().getAppDatabase().genericDao().getRE_WO_HDR_SPRByDocNo(po_num);
            if (data == null || data.HDR_ID <= 0) {
                return null;
            }

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("HEAD", "30", "SAP.MATISSUEHEAD_TYP", GenerateREWO_Head(data.WO_NUMBER)));
            jsonArray.add(createJsonObject("LINES", "30", "SAP.MATISSUESUB_TYP", GenerateREWO_Lines(data.WO_NUMBER)));

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.MATISSUE_UL_TOMIMS_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GenerateREWO_Head(String pk_id) {
            Gson gson = new Gson();
            List headList = new ArrayList();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("GUI_ID", Utils.GetGUID());
            jsonObject.addProperty("WO_NO", pk_id);
            jsonObject.addProperty("MOVE_TYPE", "262");
            jsonObject.addProperty("GM_CODE", "03");
            jsonObject.addProperty("USER_ID", App.currentUser.ID);
            jsonObject.addProperty("START_DT",  Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT));
            headList.add(jsonObject);
            String jsonString = gson.toJson(headList);
            return jsonString;
        }

        private static String GenerateREWO_Lines(String pk_id) {
            Gson gson = new Gson();
            List<RE_WO_ITEMS_RECIEPT> details = App.getDatabaseClient().getAppDatabase().genericDao().getRE_WO_ITEMS_RECIEPTForUpload(pk_id);
            String jsonString = gson.toJson(details);
            return jsonString;
        }
    }

    public static class LoadSlips {
        public static JsonObject GetLoadSlip(final long PK_ID) {
            SLIP_HDR data = App.getDatabaseClient().getAppDatabase().genericDao().GetSLIP_HDR(PK_ID);
            if (data == null || data.HDR_ID <= 0) {
                return null;
            }

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(createJsonObject("DEVICE_ID", "22", App.DeviceID));
            jsonArray.add(createJsonObject("DEV_GUID", "22", data.GUID));
            jsonArray.add(createJsonObject("DEV_TIME", "22", data.DEVICE_DATE));
            jsonArray.add(createJsonObject("ORDNO", "22", data.ORDNO == null ? "" : data.ORDNO));
            jsonArray.add(createJsonObject("ORTYP", "22", data.ORTYP == null ? "" : data.ORTYP));
            jsonArray.add(createJsonObject("YRCD", "22", data.YRCD == null ? "" : data.YRCD));
            jsonArray.add(createJsonObject("LOCODE", "22", data.LOCODE == null ? "" : data.LOCODE));
            jsonArray.add(createJsonObject("PRICING", "22", data.PRICING == null ? "" : data.PRICING));
            jsonArray.add(createJsonObject("NOTES", "22", data.REMARKS == null ? "" : data.REMARKS));
            jsonArray.add(createJsonObject("REFNO", "22", data.REFNO == null ? "" : data.REFNO));
            jsonArray.add(createJsonObject("REFDT", "22", data.REFDT == null ? "" : data.REFDT));
            jsonArray.add(createJsonObject("NEW_VEH_NO", "22", data.NEW_VEH_NO == null ? "" : data.NEW_VEH_NO));
            jsonArray.add(createJsonObject("NEW_DRIVER", "22", data.NEW_DRIVER == null ? "" : data.NEW_DRIVER));
            jsonArray.add(createJsonObject("NEW_HELPER", "22", data.NEW_HELPER == null ? "" : data.NEW_HELPER));

            jsonArray.add(createJsonObject("LOAD_LINES", "30", "PDT.LOAD_SLIP_ITEMS", GenerateSLIP_DET_Lines(PK_ID)));

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.LOAD_SLIP_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GenerateSLIP_DET_Lines(long pk_id) {
            Gson gson = new Gson();
            List<LOAD_SLIP_ITEMS_TYP> details = App.getDatabaseClient().getAppDatabase().genericDao().getSLIP_DETByHDR_IDForUpload(pk_id);
            String jsonString = gson.toJson(details);
            return jsonString;
        }

        public static JsonObject getLoadSlipHdr(final String ordNo, final String ordType) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = createJsonObject("ORDNO", "22", ordNo);
            array.add(jsonObject);
            jsonObject = createJsonObject("ORTYP", "22", ordType);
            array.add(jsonObject);

            jsonObject = createJsonObject("LOCCODE", "22", App.currentLocation.LOCODE);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.LOAD_SLIP_HDR_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getStoreTransferHdr(final String ordNo, final String ordType) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = createJsonObject("ORDNO", "22", ordNo);
            array.add(jsonObject);
            jsonObject = createJsonObject("ORTYP", "22", ordType);
            array.add(jsonObject);

            jsonObject = createJsonObject("LOCCODE", "22", App.currentLocation.LOCODE);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.STORE_TRANSFER_HDR_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getPurRetHdr(final String ordNo, final String ordType) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = createJsonObject("ORDNO", "22", ordNo);
            array.add(jsonObject);
            jsonObject = createJsonObject("LOCODE", "22", App.currentLocation.LOCODE);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.PUR_RET_HD_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject GetLoadSlipNew(final String PK_ID,final SLIP_HDR ls_hdr) {
//Latest load slip method
            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(createJsonObject("DEVICE_ID", "22", App.DeviceID));
            jsonArray.add(createJsonObject("DEV_TIME", "22", Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT)));
            jsonArray.add(createJsonObject("DEV_GUID", "22", Utils.GetGUID()));
            jsonArray.add(createJsonObject("ORDNO", "22", ls_hdr.ORDNO == null ? "" : ls_hdr.ORDNO));
            jsonArray.add(createJsonObject("ORTYP", "22", "22", ls_hdr.ORTYP == null ? "" : ls_hdr.ORTYP));
            jsonArray.add(createJsonObject("YRCD", "22", ls_hdr.YRCD == null ? "" : ls_hdr.YRCD));
            jsonArray.add(createJsonObject("LOCODE", "22", ls_hdr.LOCODE == null ? "" : ls_hdr.LOCODE));
            jsonArray.add(createJsonObject("PRICING", "22", ls_hdr.PRICING == null ? "" : ls_hdr.PRICING));
            jsonArray.add(createJsonObject("NOTES", "22", ls_hdr.REMARKS == null ? "" : ls_hdr.REMARKS));
            jsonArray.add(createJsonObject("REFNO", "22", ls_hdr.REFNO == null ? "" : ls_hdr.REFNO));
            jsonArray.add(createJsonObject("REFDT", "22", ls_hdr.REFDT == null ? "" : ls_hdr.REFDT));
            jsonArray.add(createJsonObject("NEW_VEH_NO", "22", ls_hdr.NEW_VEH_NO == null ? "" : ls_hdr.NEW_VEH_NO));
            jsonArray.add(createJsonObject("NEW_DRIVER", "22", ls_hdr.NEW_DRIVER == null ? "" : ls_hdr.NEW_DRIVER));
            jsonArray.add(createJsonObject("NEW_HELPER", "22", ls_hdr.NEW_HELPER == null ? "" : ls_hdr.NEW_HELPER));
            jsonArray.add(createJsonObject("GENERTE_INVOICE", "22", ls_hdr.GENERTE_INVOICE ));
            jsonArray.add(createJsonObject("LOAD_LINES", "30", "PDT.LOAD_SLIP_ITEMS", GenerateLOADSLIP_DET_Lines(ls_hdr.ORDNO)));


            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.LOAD_SLIP_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GenerateLOADSLIP_DET_Lines(String pk_id) {
            Gson gson = new Gson();
            List<LOAD_SLIP_ITEMS_RECEIPT> details = App.getDatabaseClient().getAppDatabase().genericDao().GetLOAD_SLIP_ITEMS_RECEIPT(pk_id);
            String jsonString = gson.toJson(details);
            Log.d("jsonString##",jsonString);
            return jsonString;
        }

        public static JsonObject GetStoreTransfer(final String PK_ID,final SLIP_HDR ls_hdr) {

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(createJsonObject("DEVICE_ID", "22", App.DeviceID));
            jsonArray.add(createJsonObject("DEV_TIME", "22", Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT)));
            jsonArray.add(createJsonObject("DEV_GUID", "22", Utils.GetGUID()));
            jsonArray.add(createJsonObject("ORDNO", "22", ls_hdr.ORDNO == null ? "" : ls_hdr.ORDNO));
            jsonArray.add(createJsonObject("ORTYP", "22", "22", ls_hdr.ORTYP == null ? "" : ls_hdr.ORTYP));
            jsonArray.add(createJsonObject("YRCD", "22", ls_hdr.YRCD == null ? "" : ls_hdr.YRCD));
            jsonArray.add(createJsonObject("LOCODE", "22", ls_hdr.LOCODE == null ? "" : ls_hdr.LOCODE));
            jsonArray.add(createJsonObject("PRICING", "22", ls_hdr.PRICING == null ? "" : ls_hdr.PRICING));
            jsonArray.add(createJsonObject("NOTES", "22", ls_hdr.REMARKS == null ? "" : ls_hdr.REMARKS));
            jsonArray.add(createJsonObject("REFNO", "22", ls_hdr.REFNO == null ? "" : ls_hdr.REFNO));
            jsonArray.add(createJsonObject("REFDT", "22", ls_hdr.REFDT == null ? "" : ls_hdr.REFDT));
            jsonArray.add(createJsonObject("NEW_VEH_NO", "22", ls_hdr.NEW_VEH_NO == null ? "" : ls_hdr.NEW_VEH_NO));
            jsonArray.add(createJsonObject("NEW_DRIVER", "22", ls_hdr.NEW_DRIVER == null ? "" : ls_hdr.NEW_DRIVER));
            jsonArray.add(createJsonObject("NEW_HELPER", "22", ls_hdr.NEW_HELPER == null ? "" : ls_hdr.NEW_HELPER));
            jsonArray.add(createJsonObject("GENERTE_INVOICE", "22", ls_hdr.GENERTE_INVOICE ));
            jsonArray.add(createJsonObject("LOAD_LINES", "30", "PDT.STORE_TRANSFER_ITEMS", GenerateSTORE_TRANSFER_DET_Lines(ls_hdr.ORDNO)));


            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.STORE_TRANSFER_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GenerateSTORE_TRANSFER_DET_Lines(String pk_id) {
            Gson gson = new Gson();
            List<STORE_TRANSFER_ITEMS_RECEIPT> details = App.getDatabaseClient().getAppDatabase().genericDao().GetSTORE_TRANSFER_ITEMS_RECEIPT(pk_id);
            String jsonString = gson.toJson(details);
            Log.d("jsonString##",jsonString);
            return jsonString;
        }

        public static JsonObject GetStockCountData(final STOCK_HEADER st_hdr) {

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(createJsonObject("DEVICE_ID", "22", App.DeviceID));
            jsonArray.add(createJsonObject("DEV_TIME", "22", Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT)));
            jsonArray.add(createJsonObject("DEV_GUID", "22", Utils.GetGUID()));
            jsonArray.add(createJsonObject("ORDNO", "22", st_hdr.ORDNO == null ? "" : st_hdr.ORDNO));
            jsonArray.add(createJsonObject("YRCD", "22", st_hdr.YRCD == null ? "" : st_hdr.YRCD));
            jsonArray.add(createJsonObject("LOCODE", "22", st_hdr.LOCODE == null ? "" : st_hdr.LOCODE));
            jsonArray.add(createJsonObject("LOAD_LINES", "30", "PDT.PHYSTKFDEVICE", GenerateSTOCK_ITEMS_RECEIPT_Lines(st_hdr.ORDNO)));


            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.STOCK_COUNT_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GenerateSTOCK_ITEMS_RECEIPT_Lines(String pk_id) {
            Gson gson = new Gson();
            List<STOCK_ITEMS_RECEIPT> details = App.getDatabaseClient().getAppDatabase().genericDao().GetSTOCK_ITEMS_RECEIPT_upload(pk_id);
            String jsonString = gson.toJson(details);
            Log.d("jsonString##",jsonString);
            return jsonString;
        }

        public static JsonObject GetPurchaseReturnData(final String recNo) {

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(createJsonObject("DEVICE_ID", "22", App.DeviceID));
            jsonArray.add(createJsonObject("DEV_TIME", "22", Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT)));
            jsonArray.add(createJsonObject("DEV_GUID", "22", Utils.GetGUID()));
            jsonArray.add(createJsonObject("ORDNO", "22", recNo));
            jsonArray.add(createJsonObject("YRCD", "22", ""));
            jsonArray.add(createJsonObject("LOCODE", "22", App.currentLocation.LOCODE));
            jsonArray.add(createJsonObject("LOAD_LINES", "30", "PDT.PHYSTKFDEVICE", GeneratePUR_RET_ITEMS_RECEIPT_Lines(recNo)));


            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.PUR_RET_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GeneratePUR_RET_ITEMS_RECEIPT_Lines(String pk_id) {
            Gson gson = new Gson();
            List<PUR_RET_ITEMS_RECEIPT> details = App.getDatabaseClient().getAppDatabase().genericDao().GetPUR_RET_ITEMS_RECEIPT_upload(pk_id);
            String jsonString = gson.toJson(details);
            //Log.d("jsonString##",jsonString);
            return jsonString;
        }

        public static JsonObject GetConfirmData(final String recNo,final String isComplete,final String remarks) {

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(createJsonObject("DEVICE_ID", "22", App.DeviceID));
            jsonArray.add(createJsonObject("DEV_TIME", "22", Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT)));
            jsonArray.add(createJsonObject("DEV_GUID", "22", Utils.GetGUID()));
            jsonArray.add(createJsonObject("ORDNO", "22", recNo));
            jsonArray.add(createJsonObject("YRCD", "22", ""));
            jsonArray.add(createJsonObject("LOCODE", "22", App.currentLocation.LOCODE));
            jsonArray.add(createJsonObject("IS_CONFIRM", "22",isComplete));
            jsonArray.add(createJsonObject("REMARKS", "22",remarks));

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.CONFIRM_DOC_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

    }

    public static class PicklList {
        public static JsonObject GetPicklList(final String PK_ID,final String refNo,final String remarks) {

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(createJsonObject("DEVICE_ID", "22", App.DeviceID));
            jsonArray.add(createJsonObject("DEV_TIME", "22", Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT)));
            jsonArray.add(createJsonObject("DEV_GUID", "22", Utils.GetGUID()));
            jsonArray.add(createJsonObject("ORDNO", "22", PK_ID == null ? "" : PK_ID));
            jsonArray.add(createJsonObject("LOCODE", "22", App.currentLocation.LOCODE));
            jsonArray.add(createJsonObject("NOTES", "22", remarks == null ? "" : remarks));
            jsonArray.add(createJsonObject("REFNO", "22", refNo == null ? "" : refNo));
            jsonArray.add(createJsonObject("PICK_LINES", "30", "PDT.PICK_ITEMS", GeneratePICKLIST_DET_Lines(PK_ID)));

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.PICK_LIST_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GeneratePICKLIST_DET_Lines(String pk_id) {
            Gson gson = new Gson();
            List<PICK_LIST_ITEMS_RECEIPT> details = App.getDatabaseClient().getAppDatabase().genericDao().GetPL_ITEMS_RECEIPT(pk_id);
            String jsonString = gson.toJson(details);
            Log.d("jsonString##",jsonString);
            return jsonString;
        }
    }

    public static class SyncActivity {
        int callCounter = 0;

        private CustomProgressDialog progressDialog;
        private boolean mIsSilentMode;
        private Context mContext;

        public SyncActivity(Context context, boolean isSilentMode) {
            this.mContext = context;
            this.mIsSilentMode = isSilentMode;
        }

        public static void updateLocalDB(DatabaseEntities entities, final long PK_ID, final long RETURN_ID) {
            switch (entities) {
                case GOODS_RECEIPT_HDR:
                    App.getDatabaseClient().getAppDatabase().genericDao().updateGR_HDR(PK_ID, RETURN_ID);
                    App.gr_hdr_id = -1; //TODO: check if upload initiated
                    break;
                case GOODS_RECEIPT_DET:
                    // nothing to do
                    break;
                case LOAD_SLIPS_HDR:
                    App.getDatabaseClient().getAppDatabase().genericDao().updateSLIP_HDR(PK_ID, RETURN_ID);
                    App.ls_hdr_id = -1; //TODO: check if upload initiated
                    break;
                case PUR_RET_ITEMS_RECEIPT:
                    App.getDatabaseClient().getAppDatabase().genericDao().updateSALE_RET_HDR(PK_ID, RETURN_ID);
                    App.sr_hdr_id = -1; //TODO: check if upload initiated
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + entities);
            }
        }

        public static void truncateLocalTables(DatabaseEntities entities, final String ORD_NO,final long RETURN_ID) {
            switch (entities) {
                case PICK_LIST_ITEMS:
                    App.getDatabaseClient().getAppDatabase().genericDao().deletePICK_LIST_ITEMS(ORD_NO);
                    break;
                case PICK_LIST_ITEMS_RECEIPT:
                    App.getDatabaseClient().getAppDatabase().genericDao().deletePICK_LIST_ITEMS_RECEIPT();
                    break;
                case LOAD_SLIP_ITEMS_NEW:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteLOAD_SLIP_ITEMS_NEW(ORD_NO);
                    break;
                case LOAD_SLIP_ITEMS_RECEIPT:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteLOAD_SLIP_ITEMS_RECEIPT(ORD_NO);
                    break;
                case STOCK_HEADER:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSTOCK_HEADER(ORD_NO);
                    break;
                case STOCK_HEADER_ITEMS:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSTOCK_HEADER_ITEMS(ORD_NO);
                    break;
                case STOCK_ITEMS_RECEIPT:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSTOCK_ITEMS_RECEIPT(ORD_NO);
                    break;
                case STORE_TRANSFER_ITEMS:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSTORE_TRANSFER_ITEMS(ORD_NO);
                    break;
                case STORE_TRANSFER_ITEMS_RECEIPT:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSTORE_TRANSFER_ITEMS_RECEIPT(ORD_NO);
                    break;
                case PUR_RET_ITEMS_RECEIPT:
                    App.getDatabaseClient().getAppDatabase().genericDao().deletePUR_RET_ITEMS_RECEIPT(ORD_NO);
                    break;
                case SALE_RET_HDR:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSALE_RET_HDR(ORD_NO);
                    break;
                case SALE_RET_DET:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSALE_RET_DET(RETURN_ID);
                    break;
                case SALE_RET_DOC_HDR:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSALE_RET_DOC_HDR(ORD_NO);
                    break;
                case SALE_RET_DOC_DET:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSALE_RET_DOC_DET(ORD_NO);
                    break;
                case GOODS_RECEIPT_HDR:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteAD_PO_ITEMS_RECIEPTdoc(ORD_NO);
                    break;
                case WORK_ORDER:
                    //App.getDatabaseClient().getAppDatabase().genericDao().deleteWO_HDR_SPR(ORD_NO);
                    //App.getDatabaseClient().getAppDatabase().genericDao().deleteWO_ITEM_SPR(ORD_NO);
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteWO_ITEMS_RECIEPTdoc(ORD_NO,"261");
                    break;
                case REVERSE_WORD_ORDER:
                    //App.getDatabaseClient().getAppDatabase().genericDao().deleteWO_HDR_SPR(ORD_NO);
                    //App.getDatabaseClient().getAppDatabase().genericDao().deleteWO_ITEM_SPR(ORD_NO);
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteWO_ITEMS_RECIEPTdoc(ORD_NO,"262");
                    break;
                case STOCK_TRANSFER_221:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSTK_TRN_PRJ_WHR_RECIEPT(ORD_NO);
                    break;
                case STOCK_TRANSFER_221Q:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSTK_TRN_PRJ_WHR_RECIEPT221Q(ORD_NO);
                    break;
                case STOCK_TRANSFER_222:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSTK_TRN_PRJ_WHR_RECIEPT222(ORD_NO);
                    break;
                case STOCK_TRANSFER_222Q:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSTK_TRN_PRJ_WHR_RECIEPT222Q(ORD_NO);
                    break;
                case STOCK_TRANSFER_411Q:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSTK_TRN_PRJ_WHR_RECIEPT411Q(ORD_NO);
                    break;
                case STOCK_TRANSFER_415Q:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSTK_TRN_PRJ_WHR_RECIEPT415Q(ORD_NO);
                    break;
                case STOCK_COUNT:
                    App.getDatabaseClient().getAppDatabase().genericDao().deleteSTK_COUNT_ITEMS_RECIEPTByDoc(ORD_NO);
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + entities);
            }
        }

        void showToast(String message) {
            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
        }

        void showProgress(boolean isCancelable) {
            if (progressDialog != null && progressDialog.isShowing())
                return;

            progressDialog = new CustomProgressDialog(mContext, isCancelable);
            progressDialog.show();
        }

        public void updateProgressMessage(String message) {
            if (mIsSilentMode) {
                showToast(message);
                return;
            } else {
                writeProgressToUIActivity(message);
            }

/*            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.updateMessage(message);
            }*/
        }

        void writeProgressToUIActivity(String message) {
            try {

                if (TextUtils.isEmpty(message)) return;

                TextView textView;
                Activity activity = (Activity) mContext;
                if (activity != null) {
                    textView = activity.findViewById(R.id.tvMessage);
                } else {
                    return;
                }

                if (textView == null) return;

                activity.runOnUiThread(() -> {
                    textView.append("\n");
                    textView.append(message);
//                    textView.setMovementMethod(new ScrollingMovementMethod()); // no effect seen
                });

//                if (callCounter == 0 && textView.getText().toString().contains("failed")) {
//                    showToast("Something failed");
//                }

            } catch (Exception e) {
                Log.d(App.TAG, e.toString());
            }
        }

        void dismissProgress() {
            if (callCounter > 0) return;
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.cancel();
            }
        }

        public void Downloads(final DatabaseEntities entities, final int PK_ID) {
            showProgress(mIsSilentMode);
            JsonObject requestObject = null;
            switch (entities) {
                case LOCATION:
                    requestObject = MastersDownload.getLocationsObject();
                    break;
                case ORTYP:
                    requestObject = MastersDownload.getOrTypObject();
                    break;
                case RECTYP:
                    requestObject = MastersDownload.geRecTypObject();
                    break;
                case REASON_TYPE:
                    requestObject = MastersDownload.geReasonTypObject();
                    break;
                case BARCODE_PRINTERS:
                case LABEL_FORMATS:
                    requestObject = new JsonObject();
                    requestObject.addProperty("DBName", App.currentCompany.DbName);
                    break;
                default:
                    requestObject = MastersDownload.getMastersRequestObject(PK_ID);
                    break;
            }
            callCounter++;

            DownloadFromWeb(entities, PK_ID, requestObject);
        }

        public void DownloadDocuments(final DatabaseEntities entities, final String doc_no, final String doc_type) {
            showProgress(mIsSilentMode);
            JsonObject requestObject = null;
            switch (entities) {
                case GOODS_RECEIPT_HDR:
                    requestObject = GoodsReceipt.getGoodsReceiptHdr(doc_no, doc_type);
                    break;
                case GOODS_RECEIPT_DET:
                    requestObject = GoodsReceipt.getGoodsReceiptDet(doc_no, doc_type);
                    break;
                default:
//                    requestObject = ...;
                    break;
            }

            if (requestObject == null) {
                return;
            }
            callCounter++;

            DownloadFromWeb(entities, -1, requestObject, doc_no, doc_type);
        }


        public void DownloadPODocuments( final long from_doc_no, final long  to_doc_no) {
            showProgress(mIsSilentMode);
            JsonObject requestObject = null;
            long totalDocs = to_doc_no-from_doc_no;
            if(totalDocs<0){
                dismissProgress();
                return;
            }

            if(totalDocs>=0){
                for(int i=0;i<=totalDocs;i++){
                    requestObject = ServiceUtils.GoodsReceipt.getGoodsReceiptHdr(String.valueOf(from_doc_no+i), "");
                    DownloadPO_HDRWeb(requestObject,String.valueOf(from_doc_no+i));
                    callCounter++;
                }
            }
        }

        private void DownloadPO_HDRWeb(JsonObject requestObject,final String docNo) {

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    dismissProgress();
                    if (genericRetResponse.getErrCode().equals("S")) {
                        String xmlDoc = genericRetResponse.getXmlDoc();
                        try {
                            AD_PO_HDR[] po_hdr = new Gson().fromJson(xmlDoc, AD_PO_HDR[].class);
                            if (po_hdr==null || po_hdr.length<=0) {
                                Log.d("DownloadPO_HDRWeb## if : ",requestObject.toString());
                                updateProgressMessage("Error : No data received : " + docNo);
                            }else{
                                Log.d("DownloadPO_HDRWeb## else : ",requestObject.toString());
                                updateProgressMessage("Downloading po document : " + po_hdr[0].PO_NUMBER);
                                App.getDatabaseClient().getAppDatabase().genericDao().deleteAD_PO_HDR(po_hdr[0].PO_NUMBER, po_hdr[0].TYPE);
                                App.getDatabaseClient().getAppDatabase().genericDao().insertAD_PO_HDR(po_hdr);
                                DownloadPO_DETWeb(docNo,"");
                            }

                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                        }
                        callCounter--;
                        if (callCounter == 0) {
                            dismissProgress();
                        }
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    dismissProgress();
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            });
        }

        private void DownloadPO_DETWeb(final String docNo, final String docType) {
            JsonObject requestObject = ServiceUtils.GoodsReceipt.getGoodsReceiptDet(docNo, docType);

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    dismissProgress();
                    if (genericRetResponse.getErrCode().equals("S")) {
                        String xmlDoc = genericRetResponse.getXmlDoc();
                        if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                            showToast(" Error: No data received.");
                            return;
                        }
                        try {
                            AD_PO_ITEMS_SPR[] poDet = new Gson().fromJson(xmlDoc, AD_PO_ITEMS_SPR[].class);
                            if (poDet==null || poDet.length<=0) {
                                updateProgressMessage("Error : No data received : " + docNo);
                            }else{
                                App.getDatabaseClient().getAppDatabase().genericDao().deleteAD_PO_ITEMS_SPR(docNo);
                                App.getDatabaseClient().getAppDatabase().genericDao().insertAD_PO_ITEMS_SPR(poDet);
                                updateProgressMessage(docNo+" Received successfully");
                            }

                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                        }
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    dismissProgress();
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            });
        }


        public void DownloadWODocuments( final long from_doc_no, final long  to_doc_no) {
            showProgress(mIsSilentMode);
            JsonObject requestObject = null;
            long totalDocs = to_doc_no-from_doc_no;
            if(totalDocs<0){
                dismissProgress();
                return;
            }

            if(totalDocs>=0){
                for(int i=0;i<=totalDocs;i++){
                    requestObject = ServiceUtils.WorkOrder.getWorkOrderHdr(String.valueOf(from_doc_no+i), "");
                    DownloadWO_HDRWeb(requestObject,String.valueOf(from_doc_no+i));
                    callCounter++;
                }
            }
        }

        private void DownloadWO_HDRWeb(JsonObject requestObject,final String docNo) {

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    dismissProgress();
                    if (genericRetResponse.getErrCode().equals("S")) {
                        String xmlDoc = genericRetResponse.getXmlDoc();
                        try {
                            WO_HDR_SPR[] po_hdr = new Gson().fromJson(xmlDoc, WO_HDR_SPR[].class);
                            if (po_hdr==null || po_hdr.length<=0) {
                                updateProgressMessage("Error : No data received : " + docNo);
                            }else{
                                updateProgressMessage("Downloading po document : " + po_hdr[0].WO_NUMBER);
                                App.getDatabaseClient().getAppDatabase().genericDao().deleteWO_HDR_SPR(po_hdr[0].WO_NUMBER);
                                App.getDatabaseClient().getAppDatabase().genericDao().insertWO_HDR_SPR(po_hdr);
                                DownloadWO_DETWeb(docNo,"261");
                            }

                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                            updateProgressMessage("Error : No data received : "  + e.toString());
                        }
                        callCounter--;
                        if (callCounter == 0) {
                            dismissProgress();
                        }
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Error : No data received : "  + msg);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    dismissProgress();
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    updateProgressMessage("Error : No data received : "  + msg);
                }
            });
        }

        private void DownloadWO_DETWeb(final String docNo, final String docType) {
            JsonObject requestObject = ServiceUtils.WorkOrder.getWorkOrderDet(docNo, docType);

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    dismissProgress();
                    if (genericRetResponse.getErrCode().equals("S")) {
                        String xmlDoc = genericRetResponse.getXmlDoc();
                        if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                            showToast(" Error: No data received.");
                            return;
                        }
                        try {
                            WO_ITEM_SPR[] poDet = new Gson().fromJson(xmlDoc, WO_ITEM_SPR[].class);
                            if (poDet==null || poDet.length<=0) {
                                updateProgressMessage("Error : No data received : " + docNo);
                            }else{
                                App.getDatabaseClient().getAppDatabase().genericDao().deleteWO_ITEM_SPR(docNo);
                                App.getDatabaseClient().getAppDatabase().genericDao().insertWO_ITEM_SPR(poDet);
                                updateProgressMessage(docNo+" Received successfully");
                            }

                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                            updateProgressMessage("Error : No data received : " + e.toString());
                        }
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Error : No data received : " + msg);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    dismissProgress();
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    updateProgressMessage("Error : No data received : " + msg);
                }
            });
        }

        public void DownloadRWODocuments( final long from_doc_no, final long  to_doc_no) {
            showProgress(mIsSilentMode);
            JsonObject requestObject = null;
            long totalDocs = to_doc_no-from_doc_no;
            if(totalDocs<0){
                dismissProgress();
                return;
            }

            if(totalDocs>=0){
                for(int i=0;i<=totalDocs;i++){
                    requestObject = ServiceUtils.WorkOrder.getWorkOrderHdr(String.valueOf(from_doc_no+i), "");
                    DownloadRWO_HDRWeb(requestObject,String.valueOf(from_doc_no+i));
                    callCounter++;
                }
            }
        }

        private void DownloadRWO_HDRWeb(JsonObject requestObject,final String docNo) {

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    dismissProgress();
                    if (genericRetResponse.getErrCode().equals("S")) {
                        String xmlDoc = genericRetResponse.getXmlDoc();
                        try {
                            RE_WO_HDR_SPR[] re_po_hdr = new Gson().fromJson(xmlDoc, RE_WO_HDR_SPR[].class);
                            if (re_po_hdr==null || re_po_hdr.length<=0) {
                                updateProgressMessage("Error : No data received : " + docNo);
                            }else{
                                updateProgressMessage("Downloading rpo document : " + re_po_hdr[0].WO_NUMBER);
                                App.getDatabaseClient().getAppDatabase().genericDao().deleteRE_WO_HDR_SPR(re_po_hdr[0].WO_NUMBER);
                                App.getDatabaseClient().getAppDatabase().genericDao().insertRE_WO_HDR_SPR(re_po_hdr);
                                DownloadRWO_DETWeb(docNo,"262");
                            }

                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                            updateProgressMessage("Error : No data received : "  + e.toString());
                        }
                        callCounter--;
                        if (callCounter == 0) {
                            dismissProgress();
                        }
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Error : No data received : "  + msg);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    dismissProgress();
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    updateProgressMessage("Error : No data received : "  + msg);
                }
            });
        }

        private void DownloadRWO_DETWeb(final String docNo, final String docType) {
            JsonObject requestObject = ServiceUtils.WorkOrder.getWorkOrderDet(docNo, docType);

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    dismissProgress();
                    if (genericRetResponse.getErrCode().equals("S")) {
                        String xmlDoc = genericRetResponse.getXmlDoc();
                        if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                            showToast(" Error: No data received.");
                            return;
                        }
                        try {
                            RE_WO_ITEM_SPR[] repoDet = new Gson().fromJson(xmlDoc, RE_WO_ITEM_SPR[].class);
                            if (repoDet==null || repoDet.length<=0) {
                                updateProgressMessage("Error : No data received : " + docNo);
                            }else{
                                App.getDatabaseClient().getAppDatabase().genericDao().deleteRE_WO_ITEM_SPR(docNo);
                                App.getDatabaseClient().getAppDatabase().genericDao().insertRE_WO_ITEM_SPR(repoDet);
                                updateProgressMessage(docNo+" Received successfully");
                            }

                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                            updateProgressMessage("Error : No data received : " + e.toString());
                        }
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Error : No data received : " + msg);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    dismissProgress();
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    updateProgressMessage("Error : No data received : " + msg);
                }
            });
        }

        public void DownloadSCDocuments( final long from_doc_no, final long  to_doc_no) {
            showProgress(mIsSilentMode);
            JsonObject requestObject = null;
            long totalDocs = to_doc_no-from_doc_no;
            if(totalDocs<0){
                dismissProgress();
                return;
            }

            if(totalDocs>=0){
                for(int i=0;i<=totalDocs;i++){
                    requestObject = ServiceUtils.StockCount.getStockCountHdr("0"+String.valueOf(from_doc_no+i), "");
                    DownloadSC_HDRWeb(requestObject,"0"+String.valueOf(from_doc_no+i));
                    callCounter++;
                }
            }
        }

        private void DownloadSC_HDRWeb(JsonObject requestObject,final String docNo) {

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    dismissProgress();
                    if (genericRetResponse.getErrCode().equals("S")) {
                        String xmlDoc = genericRetResponse.getXmlDoc();
                        try {
                            STK_COUNT_HDR[] st_hdr = new Gson().fromJson(xmlDoc, STK_COUNT_HDR[].class);
                            if (st_hdr==null || st_hdr.length<=0) {
                                updateProgressMessage("Error : No data received : " + docNo);
                            }else{
                                updateProgressMessage("Downloading stock count document : " + st_hdr[0].DOC_NO);
                                App.getDatabaseClient().getAppDatabase().genericDao().deleteSTK_COUNT_HDR(docNo);
                                App.getDatabaseClient().getAppDatabase().genericDao().insertSTK_COUNT_HDR(st_hdr);
                                DownloadSC_DET(docNo,"");
                            }

                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                            updateProgressMessage("Error : No data received : "  + e.toString());
                        }
                        callCounter--;
                        if (callCounter == 0) {
                            dismissProgress();
                        }
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Error : No data received : "  + msg);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    dismissProgress();
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    updateProgressMessage("Error : No data received : "  + msg);
                }
            });
        }

        private void DownloadSC_DET(final String docNo, final String docType) {
            JsonObject requestObject = ServiceUtils.StockCount.getStockCountDet(docNo, docType);

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    dismissProgress();
                    if (genericRetResponse.getErrCode().equals("S")) {
                        String xmlDoc = genericRetResponse.getXmlDoc();
                        if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                            showToast(" Error: No data received.");
                            return;
                        }
                        try {
                            STK_COUNT_DET[] poDet = new Gson().fromJson(xmlDoc, STK_COUNT_DET[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteSTK_COUNT_DET(poDet[0].HEAD_ID);
                            App.getDatabaseClient().getAppDatabase().genericDao().insertSTK_COUNT_DET(poDet);
                            updateProgressMessage(docNo+" Received successfully");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                        }
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    dismissProgress();
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            });
        }

        public void Uploads(final DatabaseEntities entities, final long PK_ID) {
            List<CLS_PO_NUMBER> hdr_ids;
            showProgress(mIsSilentMode);
            switch (entities) {
                case GOODS_RECEIPT_HDR:
                    hdr_ids = App.getDatabaseClient().getAppDatabase().genericDao().getAllPOForUpload();
                    if(hdr_ids==null || hdr_ids.isEmpty()){
                        updateProgressMessage(entities.name() + ":" + " No data to Uploaded.");
                        break;
                    }
                    for (CLS_PO_NUMBER hdr_id : hdr_ids) {
                        UploadTransData(entities, hdr_id.PO_NO);
                    }
                    break;
                case WORK_ORDER:
                    hdr_ids = App.getDatabaseClient().getAppDatabase().genericDao().getAllWOForUpload("261");
                    if(hdr_ids==null || hdr_ids.isEmpty()){
                        updateProgressMessage(entities.name() + ":" + " No data to Uploaded.");
                        break;
                    }
                    for (CLS_PO_NUMBER hdr_id : hdr_ids) {
                        //Log.d("hdr_ids##",hdr_id.PO_NO);
                        UploadTransData(entities, hdr_id.PO_NO);
                    }
                    break;
                case REVERSE_WORD_ORDER:
                    hdr_ids = App.getDatabaseClient().getAppDatabase().genericDao().getAllWOForUpload("262");
                    if(hdr_ids==null || hdr_ids.isEmpty()){
                        updateProgressMessage(entities.name() + ":" + " No data to Uploaded.");
                        break;
                    }
                    for (CLS_PO_NUMBER hdr_id : hdr_ids) {
                        UploadTransData(entities, hdr_id.PO_NO);
                    }
                    break;
                case STOCK_TRANSFER_221:
                    hdr_ids = App.getDatabaseClient().getAppDatabase().genericDao().getAllSTK_TRNForUpload();
                    if(hdr_ids==null || hdr_ids.isEmpty()){
                        updateProgressMessage(entities.name() + ":" + " No data to Uploaded.");
                        break;
                    }
                    //Log.d("####",hdr_ids.size());
                    for (CLS_PO_NUMBER hdr_id : hdr_ids) {
                        Log.d("####",hdr_id.PO_NO);
                        UploadTransData(entities, hdr_id.PO_NO);
                    }
                    break;
                case STOCK_TRANSFER_221Q:
                    hdr_ids = App.getDatabaseClient().getAppDatabase().genericDao().getAllSTK_TRN221QForUpload();
                    if(hdr_ids==null || hdr_ids.isEmpty()){
                        updateProgressMessage(entities.name() + ":" + " No data to Uploaded.");
                        break;
                    }
                    //Log.d("####",hdr_ids.size());
                    for (CLS_PO_NUMBER hdr_id : hdr_ids) {
                        Log.d("####",hdr_id.PO_NO);
                        UploadTransData(entities, hdr_id.PO_NO);
                    }
                    break;
                case STOCK_TRANSFER_222:
                    hdr_ids = App.getDatabaseClient().getAppDatabase().genericDao().getAllSTK_TRN222ForUpload();
                    if(hdr_ids==null ||hdr_ids.isEmpty()){
                        updateProgressMessage(entities.name() + ":" + " No data to Uploaded.");
                        break;
                    }
                    //Log.d("####",hdr_ids.size());
                    for (CLS_PO_NUMBER hdr_id : hdr_ids) {
                        Log.d("####",hdr_id.PO_NO);
                        UploadTransData(entities, hdr_id.PO_NO);
                    }
                    break;
                case STOCK_TRANSFER_222Q:
                    hdr_ids = App.getDatabaseClient().getAppDatabase().genericDao().getAllSTK_TRN222QForUpload();
                    if(hdr_ids==null || hdr_ids.isEmpty()){
                        updateProgressMessage(entities.name() + ":" + " No data to Uploaded.");
                        break;
                    }
                    //Log.d("####",hdr_ids.size());
                    for (CLS_PO_NUMBER hdr_id : hdr_ids) {
                        Log.d("####",hdr_id.PO_NO);
                        UploadTransData(entities, hdr_id.PO_NO);
                    }
                    break;
                case STOCK_TRANSFER_411Q:
                    hdr_ids = App.getDatabaseClient().getAppDatabase().genericDao().getAllSTK_TRN411QForUpload();
                    if(hdr_ids==null || hdr_ids.isEmpty()){
                        updateProgressMessage(entities.name() + ":" + " No data to Uploaded.");
                        break;
                    }
                    //Log.d("####",hdr_ids.size());
                    for (CLS_PO_NUMBER hdr_id : hdr_ids) {
                        Log.d("####",hdr_id.PO_NO);
                        UploadTransData(entities, hdr_id.PO_NO);
                    }
                    break;
                case STOCK_TRANSFER_415Q:
                    hdr_ids = App.getDatabaseClient().getAppDatabase().genericDao().getAllSTK_TRN415QForUpload();
                    if(hdr_ids==null || hdr_ids.isEmpty()){
                        updateProgressMessage(entities.name() + ":" + " No data to Uploaded.");
                        break;
                    }
                    //Log.d("####",hdr_ids.size());
                    for (CLS_PO_NUMBER hdr_id : hdr_ids) {
                        Log.d("####",hdr_id.PO_NO);
                        UploadTransData(entities, hdr_id.PO_NO);
                    }
                    break;
                case STOCK_COUNT:
                    hdr_ids = App.getDatabaseClient().getAppDatabase().genericDao().getAllSTKCOUNTForUpload();
                    if(hdr_ids==null || hdr_ids.isEmpty()){
                        updateProgressMessage(entities.name() + ":" + " No data to Uploaded.");
                        break;
                    }
                    //Log.d("####",hdr_ids.size());
                    for (CLS_PO_NUMBER hdr_id : hdr_ids) {
                        Log.d("####",hdr_id.PO_NO);
                        UploadTransData(entities, hdr_id.PO_NO);
                    }
                    break;
                default:
                    break;
            }
            dismissProgress();
        }

        private void UploadTransData(final DatabaseEntities entities, final String PO_NO) {
            JsonObject requestObject = null;
            switch (entities) {
                case GOODS_RECEIPT_HDR:
                    requestObject = ServiceUtils.GoodsReceipt.GetGR(PO_NO,"");
                    break;
                case WORK_ORDER:
                    requestObject = ServiceUtils.WorkOrder.GetWO(PO_NO,"261");
                    break;
                case REVERSE_WORD_ORDER:
                    requestObject = ServiceUtils.WorkOrder.GetWO(PO_NO,"262");
                    //Log.d("hdr_ids##requestObject",requestObject.toString());
                    break;
                case STOCK_TRANSFER_221:
                    requestObject = ServiceUtils.StockTransfer.GetSTK_TRN221(PO_NO,"");
                    break;
                case STOCK_TRANSFER_221Q:
                    requestObject = ServiceUtils.StockTransfer.GetSTK_TRN221Q(PO_NO,"");
                    break;
                case STOCK_TRANSFER_222:
                    requestObject = ServiceUtils.StockTransfer.GetSTK_TRN222(PO_NO,"");
                    break;
                case STOCK_TRANSFER_222Q:
                    requestObject = ServiceUtils.StockTransfer.GetSTK_TRN222Q(PO_NO,"");
                    break;
                case STOCK_TRANSFER_411Q:
                    requestObject = ServiceUtils.StockTransfer.GetSTK_TRN411Q(PO_NO,"");
                    break;
                case STOCK_TRANSFER_415Q:
                    requestObject = ServiceUtils.StockTransfer.GetSTK_TRN415Q(PO_NO,"");
                    break;
                case STOCK_COUNT:
                    requestObject = ServiceUtils.StockCount.GetSTK_COUNT_HDR(PO_NO);
                    break;
                default:
                    break;
            }

            if (requestObject == null) {
                return;
            }
            callCounter++;

            UploadToWeb(entities, PO_NO, requestObject);
        }

        public void insertLocalDB(final String xmlDoc, final DatabaseEntities entities, final long PK_ID, final String DocNo, final String DocType) {
            if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                updateProgressMessage(entities.name() + ": No data received.");
                return;
            }

            Thread thread = new Thread(() -> {
                Gson gson = new Gson();
                switch (entities) {
                    case LOCATION:
                        try {
                            LOCATIONS[] locations = gson.fromJson(xmlDoc, LOCATIONS[].class);
                            for (LOCATIONS location:locations) {
                                location.COCODE = App.currentCompany.Code;
                            }
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllLocationsByCompany(App.currentCompany.Code);
                            App.getDatabaseClient().getAppDatabase().genericDao().insertLocations(locations);
                            updateProgressMessage(entities.name() + ": " + locations.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
//                            updateProgressMessage("Download failed: " + entities.name());
                        }
                        break;
                    case ORTYP:
                        try {
                            ORTYP[] ortyps = gson.fromJson(xmlDoc, ORTYP[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllOrTyps();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertOrTyps(ortyps);
                            updateProgressMessage(entities.name() + ": " + ortyps.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
//                            updateProgressMessage("Download failed: " + entities.name());
                        }
                        break;
                    case GOODS_RECEIPT_HDR:
                        try {
                            PO_HDR[] po_hdr = gson.fromJson(xmlDoc, PO_HDR[].class);
                            //App.getDatabaseClient().getAppDatabase().genericDao().deletePO_HDR(DocNo, DocType);
                           // App.getDatabaseClient().getAppDatabase().genericDao().insertPO_HDR(po_hdr);
                            DownloadDocuments(AppConstants.DatabaseEntities.GOODS_RECEIPT_DET, DocNo, DocType);
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                        }
                        break;
                    case GOODS_RECEIPT_DET:
                        try {
                            PO_DET[] poDet = gson.fromJson(xmlDoc, PO_DET[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deletePO_DET(DocNo, DocType);
                            App.getDatabaseClient().getAppDatabase().genericDao().insertPO_DET(poDet);
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                        }
                        break;
                    case RECTYP:
                        try {
                            REC_TYPE[] rec_types = gson.fromJson(xmlDoc, REC_TYPE[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllRecTypes();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertRecTypes(rec_types);
                            updateProgressMessage(entities.name() + ": " + rec_types.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                        }
                        break;
                    case REASON_TYPE:
                        try {
                            REASON_TYPE[] reason_types = gson.fromJson(xmlDoc, REASON_TYPE[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllReasonTypes();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertReasonTypes(reason_types);
                            updateProgressMessage(entities.name() + ": " + reason_types.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                        }
                        break;
                    case BARCODE_PRINTERS:
                        try {
                            BARCODE_PRINTERS[] printers = gson.fromJson(xmlDoc, BARCODE_PRINTERS[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllPrinters();
                            int printerCount = 0;
                            for (BARCODE_PRINTERS printer:printers
                                 ) {
                                if(printer.LocCode!=null && printer.LocCode.length()>0 && printer.LocCode.equalsIgnoreCase(App.currentLocation.LOCODE)){
                                    App.getDatabaseClient().getAppDatabase().genericDao().insertPrinters(printer);
                                    printerCount++;
                                }
                            }
                            //App.getDatabaseClient().getAppDatabase().genericDao().insertPrinters(printers);
                            updateProgressMessage(entities.name() + ": " + printerCount + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                        }
                        break;
                    case LABEL_FORMATS:
                        try {
                            LABEL_FORMATS[] label_formats = gson.fromJson(xmlDoc, LABEL_FORMATS[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllLabels();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertLabels(label_formats);
                            updateProgressMessage(entities.name() + ": " + label_formats.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                        }
                        break;
                    case DRIVER_LIST:
                        try {
                            DRIVER_LIST[] driver_lists = gson.fromJson(xmlDoc, DRIVER_LIST[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllDriverList();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertDriverList(driver_lists);
                            updateProgressMessage(entities.name() + ": " + driver_lists.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                        }
                        break;
                    case HELPER_LIST:
                        try {
                            HELPER_LIST[] helper_lists = gson.fromJson(xmlDoc, HELPER_LIST[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllHelperList();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertHelperList(helper_lists);
                            updateProgressMessage(entities.name() + ": " + helper_lists.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                        }
                        break;
                    case VEHICLE_LIST:
                        try {
                            VEHICLE_LIST[] vehicle_lists = gson.fromJson(xmlDoc, VEHICLE_LIST[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllVehicleList();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertVehicleList(vehicle_lists);
                            updateProgressMessage(entities.name() + ": " + vehicle_lists.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                        }
                        break;
                    case DocType:
                        try {
                            DocType[] docTypes = gson.fromJson(xmlDoc, DocType[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllUnitList();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertUnitList(docTypes);
                            updateProgressMessage("Units : " + docTypes.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                        }
                        break;
                    default:
                        break;
                }
            });
            thread.start();
        }


        public void insertLocalAdnatcoDB(final String xmlDoc, final DatabaseEntities entities, final long PK_ID, final String DocNo, final String DocType) {
            if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                updateProgressMessage(entities.name() + ": No data received.");
                return;
            }

            Thread thread = new Thread(() -> {
                Gson gson = new Gson();
                switch (entities) {
                    case SLOC_LIST_SPR:
                        try {
                            SLOC_LIST_SPR[] locations = gson.fromJson(xmlDoc, SLOC_LIST_SPR[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllSLOC_LIST_SPR();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertSLOC_LIST_SPR(locations);
                            updateProgressMessage(entities.name() + ": " + locations.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
//                            updateProgressMessage("Download failed: " + entities.name());
                        }
                        break;
                    case PLANT_LIST:
                        try {
                            PLANT_LIST[] plants = gson.fromJson(xmlDoc, PLANT_LIST[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllPLANT_LIST();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertPLANT_LIST(plants);
                            updateProgressMessage(entities.name() + ": " + plants.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
//                            updateProgressMessage("Download failed: " + entities.name());
                        }
                        break;
                    case RET_REASON_LIST:
                        try {
                            RET_REASON_LIST[] reasons = gson.fromJson(xmlDoc, RET_REASON_LIST[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllRET_REASON_LIST();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertRET_REASON_LIST(reasons);
                            updateProgressMessage(entities.name() + ": " + reasons.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
//                            updateProgressMessage("Download failed: " + entities.name());
                        }
                        break;
                    case MATERIAL_LIST:
                        try {
                            MATERIAL_LIST[] materials = gson.fromJson(xmlDoc, MATERIAL_LIST[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllMATERIAL_LIST();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertMATERIAL_LIST(materials);
                            updateProgressMessage(entities.name() + ": " + materials.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
//                            updateProgressMessage("Download failed: " + entities.name());
                        }
                        break;
                    default:
                        break;
                }
            });
            thread.start();
        }


        private void DownloadFromWeb(DatabaseEntities entities, int PK_ID, JsonObject requestObject) {
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalDB(genericRetResponse.getXmlDoc(), entities, PK_ID, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");

                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }
            });
        }

        private void DownloadFromWeb(DatabaseEntities entities, int PK_ID, JsonObject requestObject, String DocNo, String
                DocType) {
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalDB(genericRetResponse.getXmlDoc(), entities, PK_ID, DocNo, DocType);
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }
            });
        }

        private void UploadToWeb(DatabaseEntities entities, String PK_ID, JsonObject requestObject) {
            App.getNetworkClient().getAPIService().submitWithLines(requestObject, new Callback<GenericSubmissionResponse>() {
                @Override
                public void success(GenericSubmissionResponse genericSubmissionResponse, Response response) {
                    updateProgressMessage(genericSubmissionResponse.getErrMessage());
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        truncateLocalTables(entities, PK_ID, genericSubmissionResponse.getRetId());
                        updateProgressMessage(entities.name() + ":" + " data Uploaded.");
                    } else {
                        Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d(App.TAG, error.getMessage());
                    updateProgressMessage(error.getMessage());
                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }
            });
        }

        public void DownloadPrintersFromWeb(DatabaseEntities entities) {
            App.getNetworkClient().getAPIService().GetPrinterList(new JsonObject(), new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalDB(genericRetResponse.getXmlDoc(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }
            });
        }


        public void DownloadLabelFormatsFromWeb(DatabaseEntities entities) {
            App.getNetworkClient().getAPIService().GetPrintFormats(new JsonObject(), new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalDB(genericRetResponse.getXmlDoc(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }
            });
        }


        public void DownloadDriverList(DatabaseEntities entities) {
            showProgress(false);

            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = ServiceUtils.createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = ServiceUtils.createJsonObject("LOCODE", "22", App.currentLocation.LOCODE);
            array.add(jsonObject);

            JsonObject requestObject = new JsonObject();
            requestObject.addProperty("ProcName", "PDT.DRIVER_LIST_SPR");
            requestObject.addProperty("DBName", App.currentCompany.DbName);
            requestObject.add("dbparams", array);

            System.out.println("requestObject##"+requestObject.toString());

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalDB(genericRetResponse.getXmlDoc(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }
            });
        }

        public void DownloadVehicleList(DatabaseEntities entities) {
            showProgress(false);

            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = ServiceUtils.createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = ServiceUtils.createJsonObject("LOCODE", "22", App.currentLocation.LOCODE);
            array.add(jsonObject);

            JsonObject requestObject = new JsonObject();
            requestObject.addProperty("ProcName", "PDT.VEH_LIST_SPR");
            requestObject.addProperty("DBName", App.currentCompany.DbName);
            requestObject.add("dbparams", array);

            System.out.println("requestObject##"+requestObject.toString());

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalDB(genericRetResponse.getXmlDoc(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }
            });
        }

        public void DownloadHelperList(DatabaseEntities entities) {
            showProgress(false);

            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = ServiceUtils.createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = ServiceUtils.createJsonObject("LOCODE", "22", App.currentLocation.LOCODE);
            array.add(jsonObject);

            JsonObject requestObject = new JsonObject();
            requestObject.addProperty("ProcName", "PDT.HELPER_LIST_SPR");
            requestObject.addProperty("DBName", App.currentCompany.DbName);
            requestObject.add("dbparams", array);

            System.out.println("requestObject##"+requestObject.toString());

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalDB(genericRetResponse.getXmlDoc(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }
            });
        }

        public void DownloadUnitList(DatabaseEntities entities) {
            showProgress(false);

            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = ServiceUtils.createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = ServiceUtils.createJsonObject("LOCODE", "22", App.currentLocation.LOCODE);
            array.add(jsonObject);

            JsonObject requestObject = new JsonObject();
            requestObject.addProperty("ProcName", "PDT.UNIT_LIST_SPR");
            requestObject.addProperty("DBName", App.currentCompany.DbName);
            requestObject.add("dbparams", array);

            System.out.println("requestObject##"+requestObject.toString());

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", "Units"));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalDB(genericRetResponse.getXmlDoc(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }
            });
        }

        public void DownloadSLocList(DatabaseEntities entities) {
            showProgress(false);

            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            //jsonObject = ServiceUtils.createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            //array.add(jsonObject);
//            jsonObject = ServiceUtils.createJsonObject("LOCODE", "22", App.currentLocation.LOCODE);
//            array.add(jsonObject);

            JsonObject requestObject = new JsonObject();
            requestObject.addProperty("ProcName", "PDT.BIN_DL_TO_PDT");
            requestObject.addProperty("DBName", App.currentCompany.DbName);
            requestObject.add("dbparams", array);

            System.out.println("requestObject##"+requestObject.toString());

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalAdnatcoDB(genericRetResponse.getXmlDoc(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    //callCounter--;
                    //if (callCounter == 0) {
                        dismissProgress();
                    //}
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }
            });
        }

        public void DownloadPlantList(DatabaseEntities entities) {
            showProgress(false);

            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            //jsonObject = ServiceUtils.createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            //array.add(jsonObject);
//            jsonObject = ServiceUtils.createJsonObject("LOCODE", "22", App.currentLocation.LOCODE);
//            array.add(jsonObject);

            JsonObject requestObject = new JsonObject();
            requestObject.addProperty("ProcName", "PDT.PLANTS_LIST_SPR");
            requestObject.addProperty("DBName", App.currentCompany.DbName);
            requestObject.add("dbparams", array);

            System.out.println("requestObject##"+requestObject.toString());

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalAdnatcoDB(genericRetResponse.getXmlDoc(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    //callCounter--;
                    //if (callCounter == 0) {
                    dismissProgress();
                    //}
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }
            });
        }

        public void DownloadReturnList(DatabaseEntities entities) {
            showProgress(false);

            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            //jsonObject = ServiceUtils.createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            //array.add(jsonObject);
//            jsonObject = ServiceUtils.createJsonObject("LOCODE", "22", App.currentLocation.LOCODE);
//            array.add(jsonObject);

            JsonObject requestObject = new JsonObject();
            requestObject.addProperty("ProcName", "PDT.RETREASON_DL_TO_DEVICE");
            requestObject.addProperty("DBName", App.currentCompany.DbName);
            requestObject.add("dbparams", array);

            System.out.println("requestObject##"+requestObject.toString());

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalAdnatcoDB(genericRetResponse.getXmlDoc(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    //callCounter--;
                    //if (callCounter == 0) {
                    dismissProgress();
                    //}
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }
            });
        }

        public void DownloadMaterialList(DatabaseEntities entities) {
            showProgress(false);

            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            //jsonObject = ServiceUtils.createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            //array.add(jsonObject);
//            jsonObject = ServiceUtils.createJsonObject("LOCODE", "22", App.currentLocation.LOCODE);
//            array.add(jsonObject);

            JsonObject requestObject = new JsonObject();
            requestObject.addProperty("ProcName", "PDT.MATMASTER_DL_TO_DEVICE");
            requestObject.addProperty("DBName", App.currentCompany.DbName);
            requestObject.add("dbparams", array);

            System.out.println("requestObject##"+requestObject.toString());

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalAdnatcoDB(genericRetResponse.getXmlDoc(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    //callCounter--;
                    //if (callCounter == 0) {
                    dismissProgress();
                    //}
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }
            });
        }

    }

    public static class SaleReturn {

        public static JsonObject getSaleReturnHdr(final String ordNo, final String ordType) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = createJsonObject("ORDNO", "22", ordNo);
            array.add(jsonObject);
            jsonObject = createJsonObject("ORTYP", "22", ordType);
            array.add(jsonObject);

            jsonObject = createJsonObject("LOCCODE", "22", App.currentLocation.LOCODE);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.SALE_RET_HDR_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getConfirmDocHdr(final String ordNo, final String ordType) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = createJsonObject("ORDNO", "22", ordNo);
            array.add(jsonObject);
            jsonObject = createJsonObject("ORTYP", "22", ordType);
            array.add(jsonObject);

            jsonObject = createJsonObject("LOCCODE", "22", App.currentLocation.LOCODE);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.CONFIRM_DOC_HDR_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getConfirmDocDet(final String ordNo, final String ordType) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = createJsonObject("ORDNO", "22", ordNo);
            array.add(jsonObject);
            jsonObject = createJsonObject("ORTYP", "22", ordType);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.CONFIRM_DOC_ITEMS_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getSaleReturnDet(final String ordNo, final String ordType) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = createJsonObject("ORDNO", "22", ordNo);
            array.add(jsonObject);
            jsonObject = createJsonObject("ORTYP", "22", ordType);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.SALE_RET_ITEMS_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject GetSaleReturn(final long PK_ID) {
            SALE_RET_HDR data = App.getDatabaseClient().getAppDatabase().genericDao().getSALE_RET_HDR(PK_ID);
            if (data == null || data.HDR_ID <= 0) {
                return null;
            }

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(createJsonObject("DEV_GUID", "22", data.GUID));
            jsonArray.add(createJsonObject("DEV_TIME", "22", data.DEVICE_DATE));
            jsonArray.add(createJsonObject("ORDNO", "22", data.ORDNO));
            jsonArray.add(createJsonObject("ORTYP", "22", data.ORTYP));
            jsonArray.add(createJsonObject("POTYPE", "22", data.POTYPE));
            jsonArray.add(createJsonObject("DEVICE_ID", "22", App.DeviceID));
            jsonArray.add(createJsonObject("YRCD", "22", data.YRCD));
            jsonArray.add(createJsonObject("LOCODE", "22", App.currentLocation.LOCODE));
            jsonArray.add(createJsonObject("NOTES", "22", data.REMARKS == null ? "" : data.REMARKS));
            jsonArray.add(createJsonObject("REFNO", "22", ""));
            jsonArray.add(createJsonObject("REFDT", "22", ""));
            jsonArray.add(createJsonObject("BLNO", "22", ""));
            jsonArray.add(createJsonObject("PURCNAME", "22", ""));
            jsonArray.add(createJsonObject("CATEMGR", "22", ""));
            jsonArray.add(createJsonObject("CONTAINERNO", "22", ""));
            jsonArray.add(createJsonObject("GR_LINES", "30", "pdt.SR_ITEMS", GenerateSaleReturn_Lines(PK_ID)));

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "PDT.SALE_RETURN_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GenerateSaleReturn_Lines(long pk_id) {
            Gson gson = new Gson();
            List<SR_LINES_TYP> details = App.getDatabaseClient().getAppDatabase().genericDao().getSRLinesForUpload(pk_id);
            String jsonString = gson.toJson(details);
            return jsonString;
        }

        public static JsonObject GetGR_ItemsPrintList(String ReceiptNo) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = createJsonObject("RECEIPTNO", "22", ReceiptNo);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.GR_ITEM_PRINT_LST_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

    }

    public static JsonObject ValidateUserLogin(String userName,String password) {
        JsonObject jsonObject;

        JsonArray array = new JsonArray();
        jsonObject = createJsonObject("USER_NAME", "22",userName);
        array.add(jsonObject);
        jsonObject = createJsonObject("USER_PASSWORD", "22", password);
        array.add(jsonObject);

        jsonObject = new JsonObject();
        jsonObject.addProperty("SPName", "SAP.VALIDATEUSER");
        jsonObject.addProperty("DBName", App.currentCompany.DbName);
        jsonObject.addProperty("USER_PASSWORD", userName);
        jsonObject.addProperty("USER_NAME", password);
        //jsonObject.add("dbparams", array);
        return jsonObject;
    }

    public static class StockCount {

        public static JsonObject getStockCountHdr(final String ordNo, final String ordType) {
            Log.d("ordNo##",ordNo);

            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("DOC_NO", "22", ordNo);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.ST_TAKE_HEADER_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getStockCountDet(final String ordNo, final String ordType) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("DOC_NO", "22", ordNo);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.ST_TAKE_ITEMS_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject GetSTK_COUNT_HDR(final String docNo) {

            STK_COUNT_HDR data = App.getDatabaseClient().getAppDatabase().genericDao().getSTK_COUNT_HDR(docNo);
            if (data == null || data.ID <= 0) {
                return null;
            }

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("GUI_ID", "22", Utils.GetGUID()));
            jsonArray.add(createJsonObject("PHYSINV_ID", "8", data.ID));
            jsonArray.add(createJsonObject("PHYSINVENTORY", "22", data.DOC_NO));
            jsonArray.add(createJsonObject("FISCALYEAR", "8", 1));
            jsonArray.add(createJsonObject("USER_ID", "8", App.currentUser.ID));
            jsonArray.add(createJsonObject("SCAN_DT", "22", Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT)));
            jsonArray.add(createJsonObject("SCAN_MODE", "8", 0));
            jsonArray.add(createJsonObject("LINES", "30", "SAP.PHYINVITEM_SN_TYP", GenerateStkCount_Lines(docNo)));


            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "pdt.ST_TAKE_UL_TOMIMS_SPS");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        private static String GenerateStkCount_Lines(String pk_id) {
            Gson gson = new Gson();
            List<STK_COUNT_ITEMS_RECIEPT> details = App.getDatabaseClient().getAppDatabase().genericDao().getSTK_COUNT_ITEMS_RECIEPT(pk_id);
            String jsonString = gson.toJson(details);
            return jsonString;
        }


    }

    public static class UserValidation {

        public static JsonObject getUserLoginDetails(final String userName, final String passWord) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_NAME", "22",userName);
            array.add(jsonObject);
            jsonObject = createJsonObject("USER_PASSWORD", "22", passWord);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SAP.VALIDATEUSER");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }
    }


}
